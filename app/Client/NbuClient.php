<?php

namespace App\Client;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Models\Currency;


/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 13.06.17
 * Time: 16:11
 */
class NbuClient
{
    private $currency;

    /**
     * NbuClient constructor.
     * @param $currency
     */
    public function __construct()
    {
        // As for now we have only one currency in project we retrieving first currency from database.
        // If other currencies will be added - refactor constructor.
        // Make it to accept $valCode parameter as other methods do.
        $this->currency = Currency::first() ?? new Currency();
    }

    public function updateCurrency($valCode = "USD")
    {
        try {
            $date = Carbon::now();
            $dateFormatted = $date->year;

            if ($date->month < 10) {
                $dateFormatted .= '0' . $date->month;
            } else {
                $dateFormatted .= '' . $date->month;
            }

            if ($date->day < 10) {
                $dateFormatted .= '0' . $date->day;
            } else {
                $dateFormatted .= '' . $date->day;
            }

            $type = 'json';
            $url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=' . $valCode . '&date=' . $dateFormatted . '&' . $type;

            $client = new Client();
            $response = $client->request('GET', $url, ['timeout' => 2, 'connect_timeout' => 2]);
            $data = json_decode($response->getBody())[0];

            $this->currency->cc = $data->cc;
            $this->currency->exchangedate = $data->exchangedate;
            $this->currency->txt = $data->txt;
            $this->currency->rate = round($data->rate,2);
            $this->currency->save();

        } catch (\Exception $e) {
//            Log::error($e->getMessage());
//            Log::error($e->getTraceAsString());
        }
    }

    public function getLastUpdatedCurrency($valCode = 'USD')
    {
        if (!$this->currency->exists
            || $this->currency->updated_at->format('d') < Carbon::now()->day
            || $this->currency->updated_at->format('m') < Carbon::now()->month
            || $this->currency->updated_at->format('Y') < Carbon::now()->year
        ) {
            $this->updateCurrency($valCode);
        }

        return $this->currency;
    }
}