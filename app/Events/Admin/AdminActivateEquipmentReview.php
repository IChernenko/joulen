<?php

namespace App\Events\Admin;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class AdminActivateEquipmentReview
{
    use SerializesModels;

    public $user;
    public $brand_title;
    public $brand_route;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param string $brand_title
     * @param string $brand_route
     */
    public function __construct(User $user, string $brand_title, string $brand_route)
    {
        $this->user = $user;
        $this->brand_title = $brand_title;
        $this->brand_route = $brand_route;
    }
}