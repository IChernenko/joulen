<?php

namespace App\Events\Admin;


use App\Models\Equipment\EquipmentReview;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class AdminRejectedEquipmentReview
{
    use SerializesModels;

    public $user;
    public $review;
    public $brand_route;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param EquipmentReview $review
     * @param string $brand_route
     */
    public function __construct(User $user, EquipmentReview $review, string $brand_route)
    {
        $this->user = $user;
        $this->review = $review;
        $this->brand_route = $brand_route;
    }
}