<?php

namespace App\Events\Admin;

use App\Models\Master;
use Illuminate\Queue\SerializesModels;

class MasterVerificationRequest
{
    use SerializesModels;

    public $master;

    /**
     * Create a new event instance.

     * @param Master $master
     */
    public function __construct(Master $master)
    {
        $this->master = $master;
    }
}