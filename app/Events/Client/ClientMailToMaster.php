<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 29.06.17
 * Time: 15:07
 */

namespace App\Events\Client;


use App\Models\Client;
use App\Models\ClientEmail;
use App\Models\Master;
use App\Models\Order;
use Illuminate\Queue\SerializesModels;

class ClientMailToMaster
{
    use SerializesModels;

    public $client;

    public $master;

    public $order;

    public $clientEmail;

    public function __construct(Client $client, Master $master, Order $order, ClientEmail $clientEmail)
    {
        $this->client = $client;
        $this->master = $master;
        $this->order = $order;
        $this->clientEmail = $clientEmail;
    }
}