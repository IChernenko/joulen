<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:58
 */

namespace App\Events\Client;


use App\Models\Preorder;
use Illuminate\Queue\SerializesModels;

class PreorderCreated
{
    use SerializesModels;

    public $preorder;

    /**
     * Create a new event instance.
     *
     * @param Preorder $preorder
     */
    public function __construct(Preorder $preorder)
    {
        $this->preorder = $preorder;
    }
}