<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 14:55
 */

namespace App\Events;


use App\Models\Order;
use App\Models\Review;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class ClientLeftReview
{
    use SerializesModels;

    public $user;
    public $order;
    public $review;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $order
     * @internal param User $order
     */
    public function __construct(User $user, Order $order, Review $review)
    {
        $this->user = $user;
        $this->order = $order;
        $this->review =$review;
    }
}