<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 20.06.17
 * Time: 14:27
 */

namespace App\Events;


use App\Models\User;
use Illuminate\Queue\SerializesModels;

class ClientProfileDeleted
{
    use SerializesModels;

    public $user;


    /**
     * Create a new event instance.
     *
     * @param User $user
     * @internal param User $order
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}