<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 17:48
 */

namespace App\Events;

use App\Models\Order;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class ClientProjectPublished
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;


    /**
     * @var Order
     */
    public $order;


    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $order
     * @internal param User $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }
}