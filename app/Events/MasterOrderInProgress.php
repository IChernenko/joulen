<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 12:18
 */

namespace App\Events;


use App\Models\Master;
use App\Models\Order;
use Illuminate\Queue\SerializesModels;

class MasterOrderInProgress
{
    use SerializesModels;

    public $master;
    public $order;

    /**
     * Create a new event instance.
     *
     * @param Master $master
     * @param Order $order
     */
    public function __construct(Master $master, Order $order)
    {
        $this->master = $master;
        $this->order = $order;
    }
}