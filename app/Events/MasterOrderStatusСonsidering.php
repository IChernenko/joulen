<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 16:50
 */

namespace App\Events;


use App\Models\Order;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class MasterOrderStatusСonsidering
{
    use SerializesModels;


    public $order;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $order
     * @internal param User $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}