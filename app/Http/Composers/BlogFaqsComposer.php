<?php

namespace App\Http\Composers;

use App\Services\WPService;
use Illuminate\Contracts\View\View;

class BlogFaqsComposer
{
    protected $WPService;

    public function __construct(WPService $WPService)
    {
        $this->WPService = $WPService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('blog_faqs', $this->WPService->getLastFaqs())
            ->with('blog_faq_url', $this->WPService->getFaqUrl());
    }

}