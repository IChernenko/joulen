<?php

namespace App\Http\Composers;

use App\Services\WPService;
use Illuminate\Contracts\View\View;

class BlogNewsComposer
{
    protected $WPService;

    public function __construct(WPService $WPService)
    {
        $this->WPService = $WPService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('blog_news', $this->WPService->getLastNews());
        $view->with('blog_news_url', $this->WPService->getNewsUrl());
    }

}