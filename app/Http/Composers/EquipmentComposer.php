<?php

namespace App\Http\Composers;


use App\Models\Equipment\Equipment;
use Illuminate\Contracts\View\View;

class EquipmentComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = Equipment::all();

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('equipment_brand', $this->getData());
    }

}