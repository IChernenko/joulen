<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\OrderCategory;

/**
 * Order category composer
 *
 * @package App\Http\Composers
 */
class OrderCategoryComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = OrderCategory::get()->keyBy('slug');

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('order_categories', $this->getData());
    }

}

