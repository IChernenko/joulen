<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\OrderStatus;

/**
 * Order status composer
 *
 * @package App\Http\Composers
 */
class OrderStatusComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = OrderStatus::all()->keyBy('slug');

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('order_statuses', $this->getData());
    }

}

