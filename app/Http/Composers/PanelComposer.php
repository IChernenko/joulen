<?php

namespace App\Http\Composers;

use GuzzleHttp\Handler\EasyHandle;
use Illuminate\Contracts\View\View;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;

/**
 * Order category composer
 *
 * @package App\Http\Composers
 */
class PanelComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $panel_type_id = EquipmentType::where('slug', EquipmentType::TYPE_SOLAR_PANEL)->first()->id;
        $this->data = Equipment::where('type_id', $panel_type_id)->get();

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('panels', $this->getData());
    }

}

