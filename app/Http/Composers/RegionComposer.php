<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\Region;

/**
 * Region composer
 *
 * @package App\Http\Composers
 */
class RegionComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * @var \Collator
     */
    private $collator;

    /**
     * Prepares the data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }
        $regions = Region::all()->keyBy('slug');
        $this->collator = new \Collator('uk_UK');
        $regions = $regions->sort(function($a, $b) {
            $arr = [$a->name, $b->name];
            $this->collator->asort($arr, \Collator::SORT_STRING);
            return array_pop($arr) == $a->name;
        });
        $this->data = $regions;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('regions', $this->getData());
    }

}

