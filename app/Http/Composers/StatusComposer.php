<?php

namespace App\Http\Composers;


use App\Models\User\Master\Status;
use Illuminate\Contracts\View\View;

class StatusComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = Status::all();

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('statuses', $this->getData());
    }
}