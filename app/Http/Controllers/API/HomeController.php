<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getHeaderHtml()
    {
       return view('layouts.header');
    }

    public function getFooterHtml()
    {
       return view('layouts._footer');
    }
}

