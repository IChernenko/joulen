<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as Controller;
use App\Models\ClientEmail;
use App\Models\ClientMasterEmail;
use Illuminate\Http\Request;

class ClientEmailController extends Controller
{
    /**
     * Display a list of orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_emails = ClientEmail::with('client.user')->orderBy('created_at', 'desc')->paginate(20);

        return view('admin.client_email.index', compact('client_emails'));
    }

    /**
     * Update the specified client email in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        TODO: add validation
        $email = ClientEmail::findOrFail($id);

        $email->comment = $request->comment;

        $email->save();

        return redirect()->route('admin.client_email.index');
    }

    /**
     * Display a list of orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCatalog()
    {
        $client_master_emails = ClientMasterEmail::with('user')->orderBy('created_at', 'desc')->paginate(20);

        return view('admin.client_catalog_list.showCatalog', compact('client_master_emails'));
    }

    public function updateCatalog(Request $request, $id)
    {
//        TODO: add validation
        $email = ClientMasterEmail::findOrFail($id);

        $email->comment = $request->comment;

        $email->save();

        return redirect()->route('admin.client_catalog_list');
    }

}
