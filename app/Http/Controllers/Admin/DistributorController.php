<?php

namespace App\Http\Controllers\Admin;

use App\EmployeeType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Distributor\StoreRequest;
use App\Http\Requests\Distributor\UpdateRequest;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Employee;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Models\Master;
use App\Models\User;
use App\Models\User\Distributor;
use App\Models\UserRole;
use App\Repositories\AttachmentRepository;
use App\Repositories\DistributorRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DistributorController extends Controller
{
    protected $distributorRepository;
    protected $attachmentRepository;

    public function __construct(DistributorRepository $distributorRepository, AttachmentRepository $attachmentRepository)
    {
        $this->distributorRepository = $distributorRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $distributors_query = Distributor::withCount(['masters', 'panels', 'inverters'])->with(['panels', 'inverters']);

        if (!empty($filter['brand']))
            $distributors_query = $distributors_query->whereHas('equipment', function ($equipment) use ($filter) {
                $equipment->where('id', $filter['brand']);
            });

        $distributors = $distributors_query
            ->orderBy($request->input('sortfield', 'company_name'), $request->input('sortorder', 'asc'))
            ->paginate(20);

        $equipment = Equipment::all();
        $request->flash();

        return view('admin.distributor.index', compact('distributors', 'equipment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masters = Master::withActiveStatuses()->get();
        $equipment = Equipment::all();

        list($inverters, $panels) = $equipment->partition(function ($item) {
            return $item->type_id === EquipmentType::INVERTER_ID;
        });

        return view('admin.distributor.create', compact('masters', 'inverters', 'panels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $user = User::create([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'role_id' => UserRole::DISTRIBUTOR_ID
            ]);

            $second_phone = $request->input('second_phone');

            $request->merge([
                'user_id' => $user->id,
                'phone' => phone_format($request->input('phone')),
                'second_phone' => is_null($second_phone) ? $second_phone : phone_format($second_phone)
            ]);

            $distributor = Distributor::create($request->all());

            $this->distributorRepository->updateOrCreateManager($request, $distributor);

            $distributor->equipment()->sync($request->input('equipment', []));
            $distributor->masters()->sync($request->input('masters', []));

            if ($request->hasFile('avatar')) {
                $image = $request->file('avatar');

                $attachment = $this->attachmentRepository->saveAttachment($distributor, 'distributor',
                    AttachmentType::where('name', AttachmentType::DISTRIBUTOR_PHOTO)->first()->id, $image, true);
                $this->attachmentRepository->resizePhoto($attachment, null, Attachment::MASTER_LOGO_HEIGHT);
            }
        });

        return redirect()->route('admin.distributors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $distributor = Distributor::findOrFail($id);

        if(!$distributor->active)
            return 'дистриб\'ютор неактивований';

        return view('distributor.show', compact('distributor'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributor = Distributor::with(['masters', 'inverters', 'panels', 'user.manager'])->findOrFail($id);
        $masters = Master::withActiveStatuses()->get();
        $equipment = Equipment::all();

        list($inverters, $panels) = $equipment->partition(function ($item) {
            return $item->type_id === EquipmentType::INVERTER_ID;
        });

        return view('admin.distributor.edit', compact('distributor', 'masters', 'inverters', 'panels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = User::findOrFail($request->input('user_id'));
        $user_update['email'] = $request->input('email');

        if (!is_null($request->input('password'))) {
            $user_update['password'] = bcrypt($request->input('password'));
        }
        $user->update(array_filter_null($user_update));

        $second_phone = $request->input('second_phone');

        $distributor = Distributor::findOrFail($id);
        $distributor->update(array_merge(array_filter_null([
            'company_name' => $request->input('company_name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'website' => $request->input('website'),
            'corporate_email' => $request->input('corporate_email'),
            'phone' => phone_format($request->input('phone')),
            'balance' => $request->input('balance'),
            'active' => $request->input('active')
        ]), ['second_phone' => is_null($second_phone) ? $second_phone : phone_format($second_phone)]));

        $this->distributorRepository->updateOrCreateManager($request, $distributor);

        $distributor->equipment()->sync($request->input('equipment', []));
        $distributor->masters()->sync($request->input('masters', []));

        if ($request->hasFile('avatar')) {
            if ($distributor->avatar)
                $distributor->avatar->delete();
            $image = $request->file('avatar');

            $attachment = $this->attachmentRepository->saveAttachment($distributor, 'distributor',
                AttachmentType::where('name', AttachmentType::DISTRIBUTOR_PHOTO)->first()->id, $image, true);
            $this->attachmentRepository->resizePhoto($attachment, null, Attachment::MASTER_LOGO_HEIGHT);
        }

        return redirect()->route('admin.distributors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
