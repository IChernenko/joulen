<?php

namespace App\Http\Controllers\Admin;

use App\Events\Admin\AdminActivateEquipmentReview;
use App\Events\Admin\AdminRejectedEquipmentReview;
use App\Http\Controllers\Controller;
use App\Http\Requests\EquipmentReviewRequest;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentReview;
use App\Models\Equipment\EquipmentType;
use App\Models\User\Master\Status;
use Illuminate\Http\Request;

class EquipmentReviewController extends Controller
{
    public function index(Request $request)
    {
        $reviews = EquipmentReview::userEquipmentReviews();

        if ($status_id = $request->input('status_review', false)) {
            $reviews = $reviews->where('status_id', $status_id);
        }

        if ($user_role = $request->input('whose_review', false)) {
            $reviews = $reviews->whereHas('user', function ($query) use ($user_role) {
                $query->whereHas('role', function ($q) use ($user_role) {
                    $q->where('name', $user_role);
                });
            });
        }

        if ($brand_id = $request->input('brand_review', false)) {
            $reviews = $reviews->where('equipment_id', $brand_id);
        }

        $equipment_slug_mapping = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING;
        $reviews = $reviews->orderBy('updated_at', 'desc')->paginate(20);
        $request->flash();

        return view('admin.equipment-review.index', compact('reviews', 'equipment_slug_mapping'));
    }

    public function edit(string $equipment_type, string $brand_slug, $id)
    {
        $slug = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING[$equipment_type];

        $brand = Equipment::where('slug', $brand_slug)->whereHas('type', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->first();

        $review = EquipmentReview::findOrFail($id);

        return view('admin.equipment-review.edit', compact('review', 'brand'));
    }

    public function update(EquipmentReviewRequest $request, $id)
    {
        $review = EquipmentReview::findOrFail($id);

        $review->fill($request->all())->save();

        return redirect()->route('admin.equipment.review.index');
    }

    public function statusUpdate($id, $status_id, Request $request)
    {
        $review = EquipmentReview::where('id', $id)->with('status')->first();
        $brand_type = array_search(EquipmentType::find($review->equipment->type_id)->slug, Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING);
        $review->status_id = $status_id;

        if ($status_id == Status::rejectedStatus()->first()->id
            && $review->status->id != Status::rejectedStatus()->first()->id) {
            $review->rejection_reason = $request->rejection_reason;
            $review->save();
            event(new AdminRejectedEquipmentReview($review->user, $review, route('brand.show',
                ['equipment_type' => $brand_type, 'brand_slug' => $review->equipment->slug])));
        }

        if ($status_id == Status::verifiedStatus()->first()->id
            && $review->status->id != Status::verifiedStatus()->first()->id) {
            $review->save();
            event(new AdminActivateEquipmentReview($review->user, $review->equipment->title, route('brand.show',
                ['equipment_type' => $brand_type, 'brand_slug' => $review->equipment->slug])));
        }

        return $review;
    }
}
