<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\Master\PortfolioRequest;
use App\Models\AttachmentType;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Models\MasterPortfolio;
use App\Models\OrderSubcategory;
use App\Models\User;
use App\Repositories\AttachmentRepository;
use App\Repositories\Criteria\CriteriaHelper;
use App\Repositories\Criteria\Attribute;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\Criteria\Order\AdminList;
use App\Repositories\MasterPortfolioRepository;
use App\Repositories\MasterRepository;
use function foo\func;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Master;
use App\Models\Attachment;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ImageOptimize;

/**
 * @property  MasterPortfolioRepository
 */
class MasterPortfolioController extends Controller
{
    protected $portfolioRepository;
    protected $masterRepository;
    protected $attachmentRepository;

    public function __construct(MasterPortfolioRepository $portfolioRepository, MasterRepository $masterRepository, AttachmentRepository $attachmentRepository)
    {
        $this->portfolioRepository = $portfolioRepository;
        $this->masterRepository = $masterRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * Display a list of orders.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  boolean $home
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->request->all();

        if(!empty($filter['subcategory_id'])){
            if($filter['subcategory_id'] == 'ground') {
                $this->portfolioRepository->pushCriteria(new Attribute('ground', true));
            }
            elseif ($filter['subcategory_id'] == 'roof') {
                $this->portfolioRepository->pushCriteria(new Attribute('ground', false));
            }
            else {
                $this->portfolioRepository->pushCriteria(new Attribute('subcategory_id', $filter['subcategory_id']));
            }
        }

        foreach ($filter as $attribute => $value) {
            if (CriteriaHelper::isAttribute($value, $attribute)) {
                $this->portfolioRepository->pushCriteria(new Attribute($attribute, $value));
            }
        }
        $portfolios = $this->portfolioRepository->orderBy('created_at', 'desc')->paginate(20);
        $portfolios_all = $this->portfolioRepository->all();
        $masters = $this->masterRepository->all();
        $portfolios_coordinates = $portfolios->map(function ($portfolio) {
            return collect($portfolio->toArray())
                ->only(['id', 'lat', 'lng'])
                ->all();
        });
        $users = User::all()->keyBy('id');
        $equipment = Equipment::all();
        $statuses = User\Master\Status::all();
        $invertor_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->orderBy('title')->get();
        $panel_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->orderBy('title')->get();
        $subcategories = OrderSubcategory::all()->pluck('name', 'id');

        $request->flash();

        return view('admin.master.portfolio.index', compact('portfolios', 'portfolios_all', 'masters',
            'portfolios_coordinates', 'users', 'equipment', 'invertor_type_equipment',
            'panel_type_equipment', 'subcategories', 'statuses'));
    }

    /**
     * Display the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $portfolio = $this->portfolioRepository->findOrFail($id);

        return view('admin.master.portfolio.show', compact('portfolio'));
    }


    /**
     * Update the specified order in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {
        $portfolio = MasterPortfolio::findOrFail($id);
        $portfolio->status_id = $request->input('status');
        $portfolio->save();
        $portfolio->load('status');

        return $portfolio;
    }

    public function destroy($id)
    {
        $portfolio = MasterPortfolio::findOrFail($id);
        $portfolio->delete();

        return redirect()->route('admin.portfolio.index');
    }

    public function get($id)
    {
        $portfolio = $this->portfolioRepository->findOrFail($id);

        return $portfolio;
    }

    public function update(PortfolioRequest $request)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $portfolio = $this->masterRepository->portfolioUpdate($request, $master);

        return $portfolio;
    }
}


