<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\OrderRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class OrderRequestController extends Controller
{
    
    /**
     * @var OrderRequestRepository
     */
    protected $orderRequestRepository;

    public function __construct(OrderRequestRepository $orderRequestRepository)
    {
        $this->orderRequestRepository = $orderRequestRepository;
    }
    /**
     * Show the form for editing the specified client.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order_request = $this->orderRequestRepository->with(['order'])->findOrFail($id);
        
        return view('admin.request.edit', compact('order_request'));
    }

    /**
     * Update the specified client in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order_request = $this->orderRequestRepository->find($id);

        $this->orderRequestRepository->update($request->all(), $order_request);

        return redirect()->route('admin.order.index');
    }
}
