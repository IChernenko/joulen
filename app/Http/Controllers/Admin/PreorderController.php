<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as Controller;
use App\Models\Preorder;
use App\Repositories\PreorderRepository;
use Illuminate\Http\Request;

class PreorderController extends Controller
{

    /**
     * @var PreorderRepository
     */
    protected $preorderRepository;

    public function __construct(PreorderRepository $preorderRepository)
    {
        $this->preorderRepository = $preorderRepository;
    }

    /**
     * Display a list of orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preorders = Preorder::paginate(20);

        return view('admin.preorder.index', compact('preorders'));
    }

    /**
     * Update the specified preorder in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        TODO: add validation
        $this->preorderRepository->update($request->all(), Preorder::findOrFail($id));

        return redirect()->route('admin.preorder.index');
    }
}
