<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 27.06.17
 * Time: 17:40
 */

namespace App\Http\Controllers\Auth;


use App\Events\Equipment\EquipmentReviewStore;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Models\MasterStatus;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileType;
use App\Models\UserRole;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as SocialiteUser;

class OAuthController extends Controller
{
    public function facebookLogin()
    {
        Session::put('facebook', 'login');

        return Socialite::driver('facebook')->redirect();
    }


    public function facebookAttach()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookDetach()
    {
        return Socialite::driver('facebook')->redirect();
    }

    //TODO refactor all method
    //TODO delete phone from database profile
    public function facebookLoginCallback()
    {
        try {
            $facebookUser = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect()->route('login');
        }

        $facebookProfileTypeId = UserProfileType::where('slug', 'facebook')->first()->id;
//        if user logged in link facebook accaunt
        if (Auth::check()) {
            $user = Auth::user();

            $userProfile = UserProfile::where('profile_id', $facebookUser->id)->first();

            if ( is_null($userProfile) ) {

                $this->addClientProfile($user, $facebookUser, $facebookProfileTypeId);

                return $this->redirectToProfileSettings($user);
            } else {
                //            unlink account can only user that linked this account
                if ( $userProfile->user_id == Auth::user()->id ) {
                    $userProfile->delete();
                    return $this->redirectToProfileSettings($user);
                }

                return back()->withErrors(['error' => trans('errors.facebook_linked_to_another_account')]);
            }
        }

        if ($this->checkFacebookAction('client_register', $facebookUser)) {
            return $this->clientRegister($facebookUser, $facebookProfileTypeId);
        }

        if ($this->checkFacebookAction('master_register', $facebookUser)) {
            return $this->masterRegister($facebookUser, $facebookProfileTypeId);
        }

        if ($this->checkFacebookAction('login', $facebookUser)) {
            return $this->userLogin($facebookUser, $facebookProfileTypeId);
        }
    }


    private function clientRegister(SocialiteUser $facebookUser, $facebookProfileTypeId)
    {
//            $user = User::where('email', $facebookUser->email )->first();
//            TODO add exception if profile type does not exist
//            if user exist in profile table, than login
//            if user exist in user table, than add to profile table and login
//            if user master, and try to register such a client, show corresponding message
//            check if user have same email in user table, add them to profile table
//            check if user have tel, if not suggest to add tel or email ( field that exist have to be non active )
//            create user that have only tel or only email or both
//            user may have the same email or tel
//            if user don't registered


        // if facebook user already exist in profile table
        $facebookProfile = UserProfile::where('profile_id', $facebookUser->id)->first();


        if ($facebookProfile) {
            if ($facebookProfile->user->isMaster()) {
                return view('errors.facebook.already_registered');
            }
            $facebookProfile->name = $facebookUser->name;
            $facebookProfile->email = $facebookUser->email;
            $facebookProfile->token = $facebookUser->token;
            $facebookProfile->type_id = $facebookProfileTypeId;
            $facebookProfile->profile_url = $facebookUser->profileUrl;

            $facebookProfile->save();

            Auth::login($facebookProfile->user);
            if (Auth::user()->isTempMaster()) {
                return redirect()->route('master.profile');
            }

            //          TODO: use default intended method
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }

            return redirect()->route(Auth::user()->role->getDefaultPageRoute());
        }

//           if user email match to facebook user
        $user = User::where('email', $facebookUser->email)->first();

        if ($user) {
//                if user have client role
            if ($user->isClient()) {
                $userProfileUser = $this->addClientProfile($user, $facebookUser, $facebookProfileTypeId);
                Auth::login($userProfileUser->user);
//                if (Auth::user()->isTempMaster()) {
//                    return redirect()->route('master.profile');
//                }

                // TODO: use default intended method
                if(Session::get('intended_path', false)) {
                    $intended_path = Session::get('intended_path', false);
                    Session::forget('intended_path');
                    return redirect($intended_path);
                }

                return redirect()->route(Auth::user()->role->getDefaultPageRoute());

            } else {
                return 'you are already registered like a master';
            }
        }


        return view('client.auth.register', [
            'email' => $facebookUser->email,
            'name' => $facebookUser->name,
            'type_id' => $facebookUser->email,
            'profile_id' => $facebookUser->id,
            'avatar_link' => $facebookUser->avatar_original,
            'profile_url' => $facebookUser->profileUrl,
        ]);

    }


    private function masterRegister(SocialiteUser $facebookUser, $facebookProfileTypeId)
    {
        $facebookProfile = UserProfile::where('profile_id', $facebookUser->id)->first();
        if ($facebookProfile != null) {
            if ($facebookProfile->user->isClient()) {
                return 'you are already registered like a client';
            }
            $facebookProfile->name = $facebookUser->name;
            $facebookProfile->email = $facebookUser->email;
            $facebookProfile->token = $facebookUser->token;
            $facebookProfile->type_id = $facebookProfileTypeId;
            $facebookProfile->profile_url = $facebookUser->profileUrl;

            $facebookProfile->save();

            Auth::login($facebookProfile->user);
            if (Auth::user()->isTempMaster()) {
                return redirect()->route('master.profile');
            }

            //          TODO: use default intended method
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }

            return redirect()->route(Auth::user()->role->getDefaultPageRoute());
        }

        //  if user email match to facebook user
        $user = User::where('email', $facebookUser->email)->first();
        if ($user) {
//                if user have client role
            if ($user->isMaster()) {
                $userProfileUser = $this->addClientProfile($user, $facebookUser, $facebookProfileTypeId);


                Auth::login($userProfileUser->user);
                if (Auth::user()->isTempMaster()) {
                    return redirect()->route('master.profile');
                }

                // TODO: use default intended method
                if(Session::get('intended_path', false)) {
                    $intended_path = Session::get('intended_path', false);
                    Session::forget('intended_path');
                    return redirect($intended_path);
                }

                return redirect()->route(Auth::user()->role->getDefaultPageRoute());

            } else {
                return 'you are already registered like a client';
            }
        }

        if (!$facebookUser->email) {
            return view('master.auth.register', [
                'email' => $facebookUser->email,
                'name' => $facebookUser->name,
                'type_id' => $facebookUser->email,
                'profile_id' => $facebookUser->id,
                'avatar_link' => $facebookUser->avatar_original,
                'profile_url' => $facebookUser->profileUrl,
            ]);
        }

        $roleId = UserRole::where('name', UserRole::MASTER)->first()->id;
        $statusId = MasterStatus::where('slug', 'inactive')->first()->id;
        $facebookProfileTypeId = UserProfileType::where('slug', 'facebook')->first()->id;

        $user = User::create(['name' => $facebookUser->name, 'email' => $facebookUser->email, 'role_id' => $roleId]);
        $master = Master::create(['user_id' => $user->id, 'status_id' => $statusId]);

        UserProfile::create([
            'profile_id' => $facebookUser->id,
            'user_id' => $user->id,
            'type_id' => $facebookProfileTypeId,
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'profile_url' => $facebookUser->profileUrl
        ]);

        Auth::login($user);
        return redirect()->route('master.profile');
    }

    private function userLogin(SocialiteUser $facebookUser,  $facebookProfileTypeId)
    {

        //if user email equal to facebook email, then link accounts and login
        $user = User::where('email', $facebookUser->email)->first();

        $facebookProfile = UserProfile::where('profile_id', $facebookUser->id)->first();

        if ($facebookProfile != null) {

            if($facebookProfile->user->deleted)
                return redirect()->route('login')->withErrors(['Ваш обліковий запис був видалений']);
            if($facebookProfile->user->blocked)
                return redirect()->route('login')->withErrors(['Ваш кабінет заблоковано, для розблокування зверніться до адміністрації сайту']);

            $facebookProfile->name = $facebookUser->name;
            $facebookProfile->email = $facebookUser->email;
            $facebookProfile->token = $facebookUser->token;
            $facebookProfile->profile_url = $facebookUser->profileUrl;
            $facebookProfile->type_id = UserProfileType::where('slug', 'facebook')->first()->id;
            $facebookProfile->save();

            Auth::login($facebookProfile->user);

            if (Auth::user()->isTempMaster()) {
                return redirect()->route('master.profile');
            }

            // TODO: use default intended method
            if (Session::has('equipment_review_id')) {
                event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
                Session::forget('equipment_review_id');
            }
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }

            return redirect()->route(Auth::user()->role->getDefaultPageRoute());
        }
        //if user email equal to facebook email, then link accounts and login
        if ($user) {
            if($user->deleted)
                return redirect()->route('login')->withErrors(['Ваш обліковий запис був видалений']);
            if($user->blocked)
                return redirect()->route('login')->withErrors(['Ваш кабінет заблоковано, для розблокування зверніться до адміністрації сайту']);

            $this->addClientProfile($user, $facebookUser, $facebookProfileTypeId);

            Auth::login($user);
            if (Session::has('equipment_review_id')) {
                event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
                Session::forget('equipment_review_id');
            }
            if ( $user->role->name == 'temp_master' ) {
                return redirect()->route('master.profile');
            }
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }
            return redirect()->route(Auth::user()->role->getDefaultPageRoute());
        }


        return redirect()->route('registration');

    }

    private function checkFacebookAction(string $action, SocialiteUser $user)
    {
        return Session::get('facebook') == $action && $user;
    }

    private function addClientProfile(User $user, $facebookUser, $facebookProfileTypeId)
    {

        //if user have client role
        $userProfileUser = UserProfile::create([
            'profile_id' => $facebookUser->id,
            'user_id' => $user->id,
            'type_id' => $facebookProfileTypeId,
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'profile_url' => $facebookUser->profileUrl ?? null,
            'token' => $facebookUser->token
        ]);

        return $userProfileUser;
    }

    private function redirectToProfileSettings($user)
    {
        if ($user->isClient()) {
            return redirect()->route('client.settings');
        } else if ($user->isMaster()) {
            return redirect()->route('master.settings.private');
        }
        return redirect('/');
    }
}