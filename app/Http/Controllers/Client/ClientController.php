<?php

namespace App\Http\Controllers\Client;

use App\Events\Admin\ClientCancelMailing;
use App\Events\Client\ClientMailToMaster;
use App\Events\ClientProfileDeleted;
use App\Events\Guest\GuestMailToMaster;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\Master;
use App\Models\Order;
use App\Repositories\ClientEmailRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderRequestRepository;
use App\Traits\SendSms;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\ClientMasterEmail;
use Illuminate\Support\Facades\Validator;


class ClientController extends Controller
{
    /**
     * @var OrderRequestRepository
     */
    private $orderRepository;

    /**
     * @var ClientEmailRepository
     */
    private $clientEmailRepository;

    /**
     * ClientController constructor.
     * @param OrderRepository $orderRepository
     * @param ClientEmailRepository $clientEmailRepository
     * @internal param $orderRequestRepository
     */
    public function __construct(OrderRepository $orderRepository, ClientEmailRepository $clientEmailRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->clientEmailRepository = $clientEmailRepository;
    }


    /**
     * Show the form for editing client's settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $user = Auth::user();
        return view('client.settings', compact('user'));
    }

    /**
     * Update client's settings.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        TODO: add validation
        $user = Auth::user();

        $user->name = $request->input('name');
        $tel = str_replace(array('+', ' ', '(' , ')', '-'), '', $request->input('phone'));
        if(strpos($tel, '38') === 0) {
            $tel = substr($tel, 2);
        }
        $user->phone = $tel;
        $user->client->mailing = $request->input('mailing', false);
        $user->client->save();
        $user->save();

        return back();
    }

    /**
     * Perform soft delete on authorized client.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $user = Auth::user();

        $user->deleted = true;
        $user->deleted_at = Carbon::now();
        $user->save();

        event(new ClientProfileDeleted($user));

        Auth::logout();
        return redirect('/');
    }

    public function sendEmail(Request $request)
    {
//        TODO: add validation
        try {
            $client = Auth::user()->client;

            $master = Master::find($request->get('master_id'));
            $order = Order::find($request->get('order_id'));

            $emailData = [
                'master_id' => $master->id,
                'client_id' => $client->id,
                'text' => $request->get('text'),
                'order_id' => $order->id
            ];

            $clientEmail = $this->clientEmailRepository->create($emailData);

            event(new ClientMailToMaster($client, $master, $order, $clientEmail));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return response($e->getMessage(), 500);
        }
        return response('Ваше повідомлення відправлено', 200);
    }

    public function sendMasterEmail(Request $request)
    {
        $this->validate($request, [
            'master_id' => 'exists:masters,id',
            'message' => 'required|string|max:500'
        ]);

        $master_id = $request->input('master_id');
        $master = Master::find($master_id);
        $user_message = $request->input('message');
        $user_phone = Auth::user()->phone;
        $user_name = Auth::user()->name;
        $user_email = Auth::user()->email;
        $sanitized_message = htmlentities($user_message, ENT_QUOTES, 'UTF-8', false);

        if ($master) {
            ClientMasterEmail::create([
                'master_id' => $master_id,
                'user_phone' => $user_phone,
                'user_email' => $user_email,
                'user_name' => $user_name,
                'message' => $sanitized_message
            ]);

            event(new GuestMailToMaster($sanitized_message, $user_phone, $user_email, $user_name, $master));

            return Response('Successfully', 200);
        }else {
            return Response('False', 404);
        }
    }

    public function cancelMailing($order, $token = null)
    {
        $order = Order::find($order);
        $client = Client::findOrFail($order->client->id);
        $client->mailing = false;
        $client->save();

        event(new ClientCancelMailing($order));
        if($client->user->remember_token != $token)
            abort(404);

        return view('client.canceled_mailing', compact('order', 'token'));
    }

    public function confirmMailing($order, $token = null)
    {
        $order = Order::findOrFail($order);
        $client = Client::findOrFail($order->client->id);
        $client->mailing = true;
        $client->save();

        if($client->user->remember_token != $token)
            abort(404);

        return view('client.confirm_mailing', compact('order', 'token'));
    }
}
