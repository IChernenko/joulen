<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller as Controller;
use App\Models\PreorderCategory;
use App\Repositories\PreorderRepository;
use Illuminate\Http\Request;
use App\Http\Requests\PreorderRequest;

class PreorderController extends Controller
{

    protected $preorderRepository;

    public function __construct(PreorderRepository $preorderRepository)
    {
        $this->preorderRepository = $preorderRepository;
    }

    /**
     * Show the form for creating a new order.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category)
    {
//        TODO: make helper method for this or hide it somewhere (may be all this code should be moved to validator)
        $categories = PreorderCategory::all('slug')->pluck('slug')->all();

        if (!in_array($category, $categories))
            abort(404);
        
        return view("preorder.create.{$category}");
    }

    /**
     * Store a newly created order in storage.
     *
     * @param  PreorderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PreorderRequest $request)
    {
        $this->preorderRepository->create($request->all());

        return route('client.order.index');
    }
}
