<?php

namespace App\Http\Controllers\Distributor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Distributor\UpdateRequest;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Master;
use App\Models\User\Distributor;
use App\Repositories\AttachmentRepository;
use App\Repositories\DistributorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DistributorController extends Controller
{
    protected $distributorRepository;
    protected $attachmentRepository;

    public function __construct(DistributorRepository $distributorRepository, AttachmentRepository $attachmentRepository)
    {
        $this->distributorRepository = $distributorRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $distributor = Distributor::where('slug', $slug)->
            with(['masters.reviews', 'inverters', 'panels'])->first();

        $masters = $distributor->masters()
            ->withCount('reviews', 'negative_reviews', 'orders_in_progress')->get();

        return view('distributor.show', compact('distributor', 'masters'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $distributor = Auth::user()->distributor;

        return view('distributor.cabinet_company', compact('distributor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $second_phone = $request->input('second_phone');

        $distributor = Auth::user()->distributor;

        $distributor->update(array_merge(array_filter_null([
            'company_name' => $request->input('company_name'),
            'description' => $request->input('description'),
            'website' => $request->input('website'),
            'corporate_email' => $request->input('corporate_email'),
            'phone' => phone_format($request->input('phone'))
        ]), ['second_phone' => is_null($second_phone) ? $second_phone : phone_format($second_phone)]));

        $distributor->refresh();

        $this->distributorRepository->updateOrCreateManager($request, $distributor);

        if ($request->hasFile('avatar')) {
            if ($distributor->avatar)
                $distributor->avatar->delete();
            $image = $request->file('avatar');

            $attachment = $this->attachmentRepository->saveAttachment($distributor, 'distributor',
                AttachmentType::where('name', AttachmentType::DISTRIBUTOR_PHOTO)->first()->id, $image, true);
            $this->attachmentRepository->resizePhoto($attachment, null, Attachment::MASTER_LOGO_HEIGHT);
        }

        return redirect()->back();
    }

    public function myMasters()
    {
        $masters = Master::withActiveStatuses()->get();
        $selected_master_ids = Auth::user()->distributor->masters->pluck('id');

        return view('distributor.dealers', compact('masters', 'selected_master_ids'));
    }

    public function updateMyMasters(Request $request)
    {
        Auth::user()->distributor->masters()->sync($request->input('masters', []));

        return redirect()->back();
    }

    public function settings()
    {
        return view('distributor.settings');
    }

    public function updateSettings(Request $request)
    {
        return redirect()->back();
    }
}
