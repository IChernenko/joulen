<?php

namespace App\Http\Controllers\Equipment;

use App\Http\Controllers\Controller;
use App\Http\Requests\EquipmentRequest;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentReview;
use App\Models\Equipment\EquipmentType;
use App\Models\MasterPortfolio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EquipmentController extends Controller
{
    /**
     * Display equipment list.
     *
     * @param  string $equipment_type
     * @param  EquipmentRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(string $equipment_type, EquipmentRequest $request)
    {
        $slug = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING[$equipment_type];

        $order = $request->input('order', 'masters_count');

        if ($request->input('order') == 'photos_count') {
            $order = "{$slug}_portfolios_count";
        }

        $equipment = Equipment::with(['logo', 'inverter_portfolios', 'solar_panel_portfolios'])
            ->whereHas('type', function ($query) use ($slug){
                $query->where('slug', $slug);
            })
            ->withCount(['masters', 'inverter_portfolios', 'solar_panel_portfolios', 'reviews', 'distributors']);

        if ($id = $request->input('id', false)) {
            $equipment = $equipment->where('id', $id);
        }

        if ($order == 'rating') {
            $equipment->leftJoin(DB::raw('
            (SELECT round((avg(price_rating) + avg(quality_rating) + avg(service_rating))/3, 2) AS rating, equipment_id
            FROM equipment_reviews
            WHERE status_id = 2
            GROUP BY equipment_id) as reviews'), 'reviews.equipment_id', '=', 'equipment.id');
        }

        if (!is_null($request->input('only_official_distributors'))) {
            $equipment->whereHas('distributors');
        }

        $equipment = $equipment->orderBy($order, 'desc')->paginate(20);

        $request->flash();

        return view("equipment.index.$equipment_type", compact('equipment', 'equipment_type', 'slug'));
    }

    /**
     * Display the specified equipment.
     *
     * @param string $equipment_type
     * @param string $brand_slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $equipment_type, string $brand_slug)
    {
        $slug = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING[$equipment_type];

        $brand = Equipment::where('slug', $brand_slug)
            ->whereHas('type', function($query) use ($slug) {
                $query->where('slug', $slug);})
            ->with('master_reviews', 'client_reviews', 'distributors.masters', 'masters')
            ->withCount('reviews', 'master_reviews', 'client_reviews', 'masters', 'distributors')->first();

        $official_dealers = collect();
        $brand->distributors->each(function ($distributor) use ($official_dealers) {
            $official_dealers->push($distributor->masters);
        });
        $official_dealers = $official_dealers->collapse()->unique('id')->keyBy('id')->whereIn('id', $brand->masters->pluck('id'));

        $dealers = $brand->masters->keyBy('id')->diffKeys($official_dealers);

        $left_comment = false;
        $auth_user_review = null;

        if (Auth::check() && $auth_user_review = EquipmentReview::where('user_id', Auth::user()->id)->where('equipment_id', $brand->id)->first()) {
            $left_comment = true;
        }

        return view('equipment.show.' . $equipment_type, compact('brand', 'equipment_type',
            'left_comment', 'auth_user_review', 'official_dealers', 'dealers'));
    }
}
