<?php

namespace App\Http\Controllers;

use App\Mail\Admin\ClientOrderConsulting;
use App\Models\Master;
use App\Models\Order;
use App\Models\OrderCategory;
use App\Repositories\Criteria\Master\IndexList;
use App\Repositories\MasterRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var MasterRepository
     */
    protected $masterRepository;

    public function __construct(MasterRepository $masterRepository)
    {
        $this->masterRepository = $masterRepository;
    }

    public function index()
    {
        $this->masterRepository->pushCriteria(new IndexList());

        $masters = $this->masterRepository->paginate(10);

        $all_masters = Master::validStatusMasters()->count();

        $masters_verified = Master::verifiedStatusMasters()->count();

        $orders_with_selected_master = Order::whereHas('status', function ($query) {
            $query->whereIn('slug', ['done', 'in_progress']);
        })->count();

        $orders = Order::whereHas('status', function ($query) {
            $query->where('slug', 'done');
        })->get();

        return view('index', compact('masters', 'all_masters', 'masters_verified', 'orders_with_selected_master', 'orders'));
    }

    public function privacyPage()
    {
        return view('privacy');
    }

    public function sendConsultingMail(Request $request)
    {
        $subject = $request->ses_type == 'home_ses'
            ? 'Нове замовлення послуг по консалтингу щодо домашніх СЕС'
            : 'Нове замовлення послуг по консалтингу щодо наземних СЕС';

        $text = $request->ses_type == 'home_ses'
            ? 'На сервісі Джоуль замовили послуги по консалтингу щодо домашніх СЕС:'
            : 'На сервісі Джоуль замовили послуги по консалтингу щодо наземних СЕС:';

        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();
        $message = (new ClientOrderConsulting($subject, $text, $request))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);

        return back();
    }
}

