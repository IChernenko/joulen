<?php

namespace App\Http\Controllers\Master;

use App\Client\LiqPay;
use App\Client\NbuClient;
use App\Events\MasterProfileDeleted;
use App\Models\Attachment;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Models\EquipmentExchangeFund;
use App\Models\EquipmentGuaranteeRepair;
use App\Models\MasterOffice;
use App\Models\MasterStatus;
use App\Models\Order;
use App\Models\OrderRequest;
use App\Models\OrderSubcategory;
use App\Models\Payment\Payment;
use App\Models\Payment\Status;
use App\Repositories\PaymentRepository;
use App\Repositories\PaymentStatusRepository;
use App\Repositories\Criteria\Attribute;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\Criteria\Order\CommissionList;
use App\Repositories\Criteria\Master\IndexList;
use App\Repositories\OrderRepository;
use App\Repositories\MasterRepository;
use App\Repositories\SubcategoryRepository;
use App\Repositories\TransactionRepository;
use App\Traits\ManagePortfolio;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\MasterPortfolio;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Region;
use App\Models\Master;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use File;


class MasterController extends Controller
{
    use ManagePortfolio;

    /**
     * @var MasterRepository
     */
    protected $masterRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var SubcategoryRepository
     */
    protected $subcategoryRepository;


    /**
     * @var LiqPay
     */
    protected $liqPay;


    /**
     * @var PaymentRepository
     */
    protected $payment;


    /**
     * @var PaymentStatusRepository
     */
    protected $paymentStatus;

    /**
     * @var TransactionRepository
     */
    protected $transaction;

    /**
     * @var NbuClient
     */
    protected $nbuClient;

    public function __construct(MasterRepository $masterRepository, OrderRepository $orderRepository,
                                SubcategoryRepository $subcategoryRepository,
                                PaymentRepository $paymentRepository,
                                PaymentStatusRepository $paymentStatusRepository,
                                TransactionRepository $transactionRepository,
                                LiqPay $liqPay)
    {
        $this->masterRepository = $masterRepository;
        $this->orderRepository = $orderRepository;
        $this->subcategoryRepository = $subcategoryRepository;
        $this->payment = $paymentRepository;
        $this->paymentStatus = $paymentStatusRepository;
        $this->transaction = $transactionRepository;
        $this->liqPay = $liqPay;
    }

    public function commission()
    {
        $subcategories = $this->subcategoryRepository->getSesWithFactors();
        $subcategoriesGroups = $subcategories->groupBy('commission_percent');

        $this->nbuClient = resolve(NbuClient::class);
        $currency = $this->nbuClient->getLastUpdatedCurrency();


        $master = Auth::user()->master;
        $orders = $this->orderRepository->pushCriteria(new CommissionList($master->id))->all();


        if (!$this->liqPay) {
            return view('master.commission_unavailable');
        }

        //map orders
        try {
            //todo:: decompose,  use eager loading for payment by order
            $orders->each(function (Order $order) {

                $order->isPaid = true;
                $payment = $this->payment->findBy('order_id', $order->id);

                $progressStatus = $this->paymentStatus->getInProgressStatus();

                //todo::hotfix
                //todo:: issue with eager loading relation 'request' for order
                $order->payment = $payment;

                if (is_null($payment) || !$payment->hasValidStatus()) {
                    $order->isPaid = false;
                } else if ($progressStatus->id == $payment->status_id) {
                    $this->checkPaymentByOrder($payment, $order);
                }
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        return view('master.commission', compact('subcategoriesGroups', 'currency', 'orders'));
    }

    /**
     * Display a list of masters.
     *
     * @param  string $filter
     * @return \Illuminate\Http\Response
     */
    public function index($filter = null)
    {
//        TODO: change sorting according to the old project
        $this->masterRepository->pushCriteria(new IndexList());

        if (!$this->masterRepository->validateUrlFilter($filter))
            abort(404);

        $filter = $this->masterRepository->decodeUrlFilter($filter);

        if (isset($filter['service_type']))
            $this->masterRepository->pushCriteria(new RelationAttribute('service_categories', 'slug', $filter['service_type']));

        if (isset($filter['region_slug'])) {
            $region_id = Region::where('slug', $filter['region_slug'])->first()->id;
            $this->masterRepository->pushCriteria(new RelationAttribute('offices', 'region_id', $region_id));
        }

        if (isset($filter['subcategory'])) {
            if ($filter['subcategory'] == 'commercial_ses') {
                $home = 0;
            }
            else if ($filter['subcategory'] == 'home_ses') {
                $home = 1;
            }
            $this->masterRepository->pushCriteria(new RelationAttribute('subcategories', 'home', $home));
        }

        if (isset($filter['verified_masters']))
            $this->masterRepository->pushCriteria(new RelationAttribute('status', 'slug', $filter['verified_masters']));

        $masters = $this->masterRepository->paginate(20);

        $regions =  Region::all()->keyBy('slug');
        if(isset($filter['region_slug'])){
            $region_name = 'в ' . $regions[$filter['region_slug']]->name;
            $region_name = strpos($region_name, 'а обл.') !== false ? str_replace('а обл.', 'ій області', $region_name) : $region_name;
        } else {
            $region_name = 'в Україні';
        }

//        if (array_key_exists('service_type', $filter) && $filter['service_type'] == 'ses') {
//            $page_title = 'Монтажники сонячних електростанцій';
//            $site_title = 'Каталог монтажників сонячних електростанцій ';
//        } elseif (array_key_exists('service_type', $filter) && $filter['service_type'] == 'energy_audit') {
//            $page_title = 'Енергоаудитори';
//            $site_title = 'Каталог енергоаудиторів ';
//        } elseif (array_key_exists('service_type', $filter) && $filter['service_type'] == 'waste_sorting'){
//            $page_title = 'Будівельники сміттєсортувальних ліній';
//            $site_title = 'Каталог спеціалістів у галузі енергетики та енергозбереження ';
//        }else{
//            $page_title = 'Всі спеціалісти';
//            $site_title = 'Каталог спеціалістів у галузі енергетики та енергозбереження ';
//        }

        $site_title = 'Каталог монтажників сонячних електростанцій '.$region_name;

        return view('master.index', compact('masters', 'filter', 'site_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('master.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        TODO: this page is different for authorized user and guest. Implement guest version.
//        TODO: withCount expression duplicate index_list condition in MasterRepository
        $master = Master::withCount([
            'reviews',
            'negative_reviews',
            'orders_in_progress',
        ])
            ->with([
                'orders_in_progress' => function ($order_query) {
                    $order_query->with(['region', 'subcategory.category', 'orderable']);
                },
                'regions',
                'portfolios',
                'reviews' => function ($review_query) {
                    $review_query->with(['client.user', 'order' => function ($order_query) {
                        $order_query->with(['region', 'subcategory.category', 'orderable']);
                    }]);
                },
            ])
            ->findOrFail($id);

        if($master->user->deleted)
            return 'спеціаліст видалений';

        if($master->user->blocked)
            return 'спеціаліст заблокований';

        $equipment_slug_mapping = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING;

        return view('master.show', compact('master', 'equipment_slug_mapping'));
    }

    /**
     * Show the form for editing private master's settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function privateSettings()
    {
        return view('master.settings.private', ['user' => Auth::user()]);
    }

    /**
     * Update private master's settings.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updatePrivate(Request $request)
    {
//        TODO: add validation
        $mailing = new Collection($request->input('mailing', []));

        foreach (Auth::user()->master->service_categories as $category) {

            Auth::user()->master->service_categories()->sync([$category->id => [
                'mailing' => $mailing->get($category->id, false)
            ]], false);

        }

        return back();
    }

    /**
     * Show the form for editing public master's settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function publicSettings()
    {
        $master = Auth::user()->master;

        return view('master.settings.public', [
            'user' => Auth::user(),
            'master' => $master,
            'portfolios' => $master->portfolios
        ]);
    }

    /**
     * Perform soft delete on authorized master.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $user = Auth::user();

        $user->deleted = true;
        $user->deleted_at = Carbon::now();
        $user->save();

        event(new MasterProfileDeleted($user));

        Auth::logout();
        return redirect()->route('home');
    }

    protected function checkPaymentByOrder(Payment $payment, Order $order)
    {
        $response = $this->liqPay->api('request', [
            'version' => '3',
            'action' => 'status',
            'order_id' => $order->id . '_' . Auth::user()->master->id
        ]);

        if ($response->result == "error") {
            $order->isPaid = false;
        } else if ($response->result == "ok") {
            $successStatus = $this->paymentStatus->getSuccessStatus();
            $this->payment->update([
                'status_id' => $successStatus->id
            ], $payment->id);
        }
    }

    public function profile() {
        $invertor_type_equipment = Equipment::where ('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->get();

        $panel_type_equipment = Equipment:: where ('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->get();

        $exchange_funds = EquipmentExchangeFund::all()->pluck('name', 'id');

        $guarantee_repairs = EquipmentGuaranteeRepair::all()->pluck('name', 'id');

        $equipment = Equipment::all()->pluck('title', 'id');

        $master = Auth::user()->master;

        return view('master.profile', compact(
            'invertor_type_equipment', 'panel_type_equipment', 'equipment', 'master', 'exchange_funds', 'guarantee_repairs', 'equipment_master'));
    }

    public function pageFAQ()
    {
        return view('master.faq');
    }
}
