<?php

namespace App\Http\Controllers\Master;

use App\Events\Master\OrderRequestMade;
use App\Repositories\OrderRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderRequest;
use App\Models\OrderRequestPrice;
use Illuminate\Support\Facades\Auth;

class OrderRequestController extends Controller
{

    /**
     * @var OrderRequestRepository
     */
    protected $orderRequestRepository;

    public function __construct(OrderRequestRepository $orderRequestRepository)
    {
        $this->orderRequestRepository = $orderRequestRepository;
    }

    /**
     * Store a newly created order_request in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        TODO: move this to repository and add validation and transaction 

        $order_request = OrderRequest::create([
            'master_id' => Auth::user()->master->id,
            'order_id' => $request->order_id,
            'final' => $request->input('final', 1),
        ]);

        foreach ($request->prices as $key => $price){
            OrderRequestPrice::create([
                'request_id' => $order_request->id,
                'type_id' => $key,
                'price' => $price,
                'comment' => $request->comments[$key],
            ]);
        }
//        TODO: fire event on transaction commit
        event(new OrderRequestMade($order_request));

        return back();
    }

    /**
     * Show the form for editing the specified order_request.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order_request = $this->orderRequestRepository->with(['order'])->findOrFail($id);

        if(!Auth::user()->can('update', $order_request))
            return redirect()->route('home');
        
        return view('master.request.edit', compact('order_request'));
    }

    /**
     * Update the specified order_request in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        TODO: add validation

        $order_request = $this->orderRequestRepository->find($id);

        if(!Auth::user()->can('update', $order_request))
            return redirect()->route('home');

        $this->orderRequestRepository->update($request->all(), $order_request);

        return redirect()->route('master.order.index');
    }
}
