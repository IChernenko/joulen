<?php

namespace App\Http\Controllers;

use App\Client\NbuClient;
use App\Repositories\Criteria\CriteriaHelper;
use App\Repositories\Criteria\Attribute;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\Criteria\Order\AdminList;
use App\Repositories\OrderRepository;
use App\Repositories\MasterRepository;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var MasterRepository
     */
    protected $masterRepository;


    public function __construct(OrderRepository $orderRepository, MasterRepository $masterRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->masterRepository = $masterRepository;
    }

    /**
     * Display the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->with(['subcategory.category', 'orderable', 'requests.master.user', 'status'])->findOrFail($id);

        if (!Auth::check() && $order->status->slug != 'done') {
            abort(404);
        }

        return view('guest.order.show', compact('order'));
    }
}
