<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 14.06.17
 * Time: 13:29
 */

namespace App\Http\Controllers;

use App\Client\LiqPay;
use App\Events\MasterCommissionPaid;
use App\Models\Master;
use App\Models\User;
use App\Repositories\Criteria\Order\RequestByMaster;
use App\Repositories\PaymentRepository as Payment;
use App\Repositories\PaymentStatusRepository as Status;
use App\Repositories\PaymentStatusRepository;
use App\Repositories\TransactionRepository as Transaction;
use App\Repositories\OrderRepository as Order;

use App\Models\Payment\Transaction as TransactionModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    /**
     * @var PaymentRepository
     */
    private $payment;

    /**
     * @var PaymentStatusRepository
     */
    private $status;

    /**
     * @var TransactionRepository
     */
    private $transaction;

    /**
     * @var Request
     */
    private $request;


    /**
     * @var Order
     */
    private $order;

    public function __construct(Request $request, Payment $payment, Status $status, Transaction $transaction, Order $order)
    {
        $this->request = $request;
        $this->payment = $payment;
        $this->transaction = $transaction;
        $this->status = $status;
        $this->order = $order;
    }


    public function initTransaction(int $orderId)
    {
        $this->middleware(['auth', 'role:master']);

        try {
            $liqPay = resolve(LiqPay::class);
            $master = Auth::user()->master;

            $this->order->pushCriteria(new RequestByMaster($master->id));
            $masterOrder = $this->order->findBy('id', $orderId);

            if (is_null($masterOrder)) {
                throw  new \Exception('Order with id ' . $orderId . ' by master with id' . $master->id . ' does not found');
            }

            $paymentParams = array(
                'version' => '3',
                'action' => 'pay',
                'amount' => $masterOrder->request->commission,
                'currency' => 'UAH',
                'description' => $masterOrder->title . ", ID: " . $orderId,
                'order_id' => $masterOrder->id . '_' . $master->id,
                'customer' => $master->id,
                'sandbox' => env('LIQPAY_SANDBOX', '0'),
                'result_url' => route('liqpay.callback')
            );
            $inProgressStatus = $this->status->getInProgressStatus();
            $paymentModel = $this->payment->findWhere([
                'order_id' => $orderId,
                'master_id' => $master->id
            ])->first();

            if (is_null($paymentModel)) {
                $this->payment->create([
                    'amount' => $paymentParams['amount'],
                    'order_id' => $masterOrder->id,
                    'master_id' => $master->id,
                    'status_id' => $inProgressStatus->id
                ]);
            }
            $liqPayLink = $liqPay->cnb_link($paymentParams);

            return redirect($liqPayLink);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        return redirect('/master/commission');
    }


    public function handleLiqPayCallback()
    {
        try {
            $data = json_decode(base64_decode($this->request->get('data')));

            $orderId = explode('_', $data->order_id)[0];
            $payment = $this->payment->findWhere([
                'master_id' => $data->customer,
                'order_id' => $orderId
            ])->first();

            $this->transaction->create([
                'action' => $data->action,
                'paytype' => $data->paytype,
                'acq_id' => $data->acq_id,
                'payment_system_payment_id' => $data->payment_id,
                'payment_system_order_id' => $data->liqpay_order_id,
                'description' => $data->description,
                'ip' => $data->ip,
                'order_id' => $orderId,
                'amount' => $data->amount,
                'sender_card_mask2' => $data->sender_card_mask2,
                'sender_card_bank' => $data->sender_card_bank,
                'sender_card_type' => $data->sender_card_type,
                'sender_card_country' => $data->sender_card_country,
                'currency' => $data->currency,
                'create_date' => Carbon::createFromTimestamp($data->create_date / 1000, date_default_timezone_get()),
                'transaction_id' => $data->transaction_id,
                'status' => $data->status,
                'payment_id' => $payment->id
            ]);

            if ( TransactionModel::isValidStatus($data->status)) {
                $payment->update(['status_id' => \App\Models\Payment\Status::where('name', 'success')->first()->id]);
                event(new MasterCommissionPaid(Master::find($data->customer)->user, \App\Models\Order::find($orderId)));
            }


        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        return redirect()->route('master.commission');
    }
}