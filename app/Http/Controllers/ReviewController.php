<?php

namespace App\Http\Controllers;

use App\Events\ClientLeftReview;
use App\Models\Master;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Review;
use Auth;
use Illuminate\Support\Facades\Validator;


class ReviewController extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('role:client,admin');
    }

    /**
     * Show the form for creating review.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        TODO: add validation

        return view('review.create', ['master_id' => $request->master_id, 'order_id' => $request->order_id]);
    }

    /**
     * Show the form for editing review.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
//        TODO: add validation
        $review = Review::findOrFail($id);
        if (!Auth::user()->can('update', $review))
            return redirect()->route('home');

        return view('review.edit', ['review' => $review, 'master_id' => $request->master_id, 'order_id' => $request->order_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array_rules = [
            'client_id' => 'exists:clients,id',
            'order_id' => 'exists:orders,id',
            'master_id' => 'exists:masters,id',
            'text' => 'required',
        ];
        $validator = Validator::make($request->all(), $array_rules);
        $errors = ($validator->messages()->all());
        $master_id = $request->master_id;
        $order_id = $request->order_id;

        if ($validator->fails()) { //return to view with data, because redirect back doesn't working
            return view('review.create', [
                'master_id' => $master_id,
                'order_id' => $order_id,
                'errors' => $errors,
            ]);
        }
        $sanitize_text = htmlentities($request->input('text'), ENT_QUOTES, 'UTF-8', false);
        $order = Order::find($order_id);

        $review = Review::create([
            'client_id' => $order->client->id,
            'order_id' => $request->order_id,
            'master_id' => $master_id,
            'text' => $sanitize_text,
            'positive' => $request->input('negative', true)
        ]);

        //todo: refactor
        event(new ClientLeftReview(Master::find($master_id)->user, $order, $review));

        return redirect()->route(Auth::user()->role->name . '.order.show', ['id' => $request->order_id]);
    }

    /**
     * Updates review in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $review = Review::findOrFail($id);
        $array_rules = [
            'master_id' => 'exists:masters,id',
            'order_id' => 'exists:orders,id',
            'text' => 'required',
        ];
        $validator = Validator::make($request->all(), $array_rules);
        $errors = ($validator->messages()->all());

        if ($validator->fails()) { //return to view with data, because redirect back doesn't working
            return view('review.edit', [
                'review' => $review,
                'master_id' => $request->master_id,
                'order_id' => $request->order_id,
                'errors' => $errors
            ]);
        }
        $sanitize_text = htmlentities($request->input('text'), ENT_QUOTES, 'UTF-8', false);

        $review = Review::findOrFail($id);
        if (!Auth::user()->can('update', $review))
            return redirect()->route('home');

        $review->positive = $request->input('negative', true);
        $review->text = $sanitize_text;
        $review->save();

        return redirect()->route(Auth::user()->role->name . '.order.show', ['id' => $review->order->id]);
    }
}