<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CommissionPaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (Auth::check() && Auth::user()->isMaster() && !Auth::user()->master->HasUnpaidCommission()) {
            return $next($request);
        }

        return redirect()->route('master.commission');
    }
}
