<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */


    protected $except = [
        'https://www.liqpay.com/', 'liqpay.com', 'http://joulen/commission/LiqPay', 'joulen/commission/LiqPay',
        '/commission/LiqPay','commission/LiqPay'
    ];
}
