<?php

namespace App\Http\Requests\Distributor;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string',
            'description' => 'string|required',
            'slug' => 'required|unique:distributors,slug',
            'website' => 'string|required',
            'corporate_email' => 'string|required',
            'phone' => 'required|string',
            'second_phone' => 'string|nullable',
            'balance' => 'required|integer',
            'active' => 'boolean',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6',
            'manager_name' => 'required|string',
            'manager_phone' => 'required|string',
            'manager_second_phone' => 'string|nullable',
            'manager_email' => 'required|string',
            'masters' => 'array',
            'masters.*' => 'required_with:masters|exists:masters,id',
            'equipment' => 'array',
            'equipment.*' => 'required_with:equipment|exists:equipment,id',
        ];
    }
}