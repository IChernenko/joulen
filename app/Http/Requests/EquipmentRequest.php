<?php

namespace App\Http\Requests;

use App\Models\Equipment\Equipment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EquipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $equipment_ids = Equipment::all()->pluck('id')->toArray();
        $equipment_ids[] = null;
        return [
            'order' => [Rule::in(['masters_count', 'reviews_count', 'photos_count', 'rating'])],
            'id' => [Rule::in($equipment_ids)],
        ];
    }
}
