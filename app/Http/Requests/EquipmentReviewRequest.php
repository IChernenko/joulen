<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EquipmentReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'equipment_id' => 'required|integer|exists:equipment,id',
            'positive' => 'required|boolean',
            'title' => 'required|string|max:45',
            'quality_rating' => 'required|max:5',
            'price_rating' => 'required|max:5',
            'service_rating' => 'required|max:5',
            'body' => 'required|string'
        ];
    }
}
