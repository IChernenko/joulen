<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EquipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isMaster() || Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->handleRules();
    }

    protected function handleRules()
    {
        return [
            'equipment_id' => 'required|integer',
            'warranty_increase' => 'required_if:equipment_type,=,1|nullable|boolean',
            'warranty_duration' => 'required|regex:/^\d*[\.]?\d?$/|max:3500',
            'warranty_case' => 'required|integer',
            'exchange_fund_loc' => 'required|integer',
            'built' => 'required|regex:/^(?=.+)(?:[1-9]\d*)?$/|max:3500',
        ];
    }

}