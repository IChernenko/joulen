<?php

namespace App\Http\Requests\Master;

use App\Models\OrderSubcategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PortfolioRequest extends FormRequest
{
    const IMG_RULE = 'image|mimes:jpeg,gif,png,jpg|max:15000';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isMaster() || Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->handleRules();
    }

    protected function handleRules()
    {
        $commercial = OrderSubcategory::where('slug', 'commercial')->first()->id;
        $network = OrderSubcategory::where('slug', 'network')->first()->id;
        $hybrid = OrderSubcategory::where('slug', 'hybrid')->first()->id;
        return [
            'portfolio_power' => 'required|regex:/^\d+[\.]?[,]?(\d+)?$/|max:3500',
            'portfolio_subcategory_id' => 'required|exists:order_subcategories,id',
            'portfolio_subcategory_type_id' => 'required_if:portfolio_subcategory_id,=,'.$commercial,
            'portfolio_month' => 'required|string|max:100',
            'portfolio_year' => 'required|digits:4',
            'portfolio_address' => 'required|string|max:190',
            'portfolio_invertor_id' => 'required|exists:equipment,id',
            'portfolio_panel_id' => 'required|exists:equipment,id',
            'portfolio_contractor' => 'required_if:portfolio_subcategory_id,=,'.$commercial.'|boolean',
            'portfolio_designing' => 'required_if:portfolio_subcategory_id,=,'.$commercial.'|boolean',
            'portfolio_installation' => 'required|boolean',
            'portfolio_delivery' => 'required|boolean',
            'portfolio_documentation' => 'required_if:portfolio_subcategory_id,=,'.$network.'|required_if:portfolio_subcategory_id,=,'.$hybrid.'|boolean',
            'invertor_photo' => self::IMG_RULE,
            'panel_photo' => self::IMG_RULE,
        ];
    }

}