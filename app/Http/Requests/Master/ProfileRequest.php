<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    const IMG_RULE = 'image|mimes:jpeg,gif,png,jpg|max:15000';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isMaster() || Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->handleRules();
    }

    protected function handleRules()
    {
        $data = $this->validationData();
        $rules = [];
        $stage = @$data['profile_stage'];

        switch ($stage) {
            case 1 : $rules = $this->stageOneRules(); break;
            case 2 : $rules = $this->stageTwoRules(); break;
            case 3 : $rules = $this->stageThreeRules(); break;
            case 4 : $rules = $this->stageFourRules(); break;
            case 5 : $rules = $this->stageFiveRules(); break;
            case 'all' : $rules = $this->allRules(); break;
            default : $rules = []; // TODO : figure out how to return error when profile_stage doesn't exist in request
        }

        return $rules;
    }

    protected function stageOneRules()
    {
        return [
            'company_name' => 'required|string|min:3|max:100',
            'master_avatar' => self::IMG_RULE,
            'about' => 'required|string|max:2500'
        ];
    }

    protected function stageTwoRules()
    {
        return [
            'work_phone' => 'required|regex:/^\+38[\(]\d{3}[\)]\s{1}\d{3}-\d{2}\-\d{2}$/',
            'work_email' => 'required|email',
            'site' => 'nullable|string|max:100',
            'second_work_phone' => 'nullable|regex:/^\+38[\(]\d{3}[\)]\s{1}\d{3}-\d{2}\-\d{2}$/',
            'main_office_address' => 'required_if:main_office_added,==,1|nullable|string|max:100',
            'second_office_address' => 'required_if:second_office_added,==,1|nullable|string|max:100',
            'third_office_address' => 'required_if:third_office_added,==,1|nullable|string|max:100',
            'main_office_room' => 'nullable|string|max:100',
            'second_office_room' => 'nullable|string|max:100',
            'third_office_room' => 'nullable|string|max:100',
            'main_office_photo' => 'nullable|' . self::IMG_RULE,
            'second_office_photo' => 'nullable|' . self::IMG_RULE,
            'third_office_photo' => 'nullable|' . self::IMG_RULE,
        ];
    }

    protected function stageThreeRules()
    {
        return [
            'manager_name' => 'required|string|max:100',
            'manager_phone' => 'required|min:11',
            'manager_email' => 'required|min:11|email',
            'owner_name' => 'required|string|max:100',
            'owner_phone' => 'required|min:11',
            'owner_email' => 'required|min:11|email',
            'edrpou' => 'required_if:legal,==,1|nullable|digits:8',
            'ipn' => 'required_if:physical,==,1|nullable|digits:10',
        ];
    }

    protected function stageFourRules()
    {
        return [
            'exchange_fund' => 'required',
            'exchange_fund_text' => 'required_if:exchange_fund,==,1|nullable|string|min:50|max:500',
            'car_photo' => self::IMG_RULE,
            'machinery' => 'required',
            'machinery_photo' => self::IMG_RULE,
            'favorite_work_location.1' => 'required|exists:regions,id',
            'year_started' => 'required',
            'work_location.*' => 'required|exists:regions,id'
        ];
    }

    protected function stageFiveRules()
    {
        return [];
    }

    protected function allRules()
    {
        return array_merge($this->stageOneRules(), $this->stageTwoRules(), $this->stageThreeRules(),
            $this->stageFourRules(), $this->stageFiveRules());
    }
}