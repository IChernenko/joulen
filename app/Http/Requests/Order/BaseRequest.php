<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\OrderSubcategory;
use Illuminate\Support\Facades\Auth;

class BaseRequest extends FormRequest
{
    /*
     * Contains result rules
     *
     */
    protected $resultRules;

    const TEXT_AREA_RULE = [
            'string',
            'max:10000',
            'nullable',
//            "regex:/^[ 0-9\/\p{L}\s\.!;:,\(\)\*\?\@\#\$\%\&\=-]{0,10000}$/u"
        ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isClient();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->handleRules();
    }

    /**
     * Returns array with rules that depends on category
     *
     * @return array $this->resultRules
     */
    protected function handleRules()
    {
        $data = $this->validationData();
        $price_types = $data['price_types'] ?? [];
        $toggles_rules = [
            'c1' => array_key_exists('1', $price_types) ? 'required' : 'nullable',
            'c2' => array_key_exists('2', $price_types) ? 'required' : 'nullable',
            'c3' => array_key_exists('3', $price_types) ? 'required' : 'nullable',
            'exploitation' => array_key_exists('exploitation', $data) && $data['exploitation'] ? 'required' : 'nullable',
            'c1_above_zero' => array_key_exists('1', $price_types) ? '|above_zero' : ''
        ];
        $this->resultRules = $this->common_rules($toggles_rules);
        $methodNameByCategory = $data['category'] . '_rules';
        $subcategories = OrderSubcategory::all()->pluck('slug', 'id')->toArray();
        $methodNameBySubCategory = $subcategories[$data['subcategory']] . '_rules';

        if (method_exists($this, $methodNameByCategory)) {
            $this->resultRules = array_merge($this->resultRules, $this->{$methodNameByCategory}($toggles_rules));
        }

        if (method_exists($this, $methodNameBySubCategory)) {
            $this->resultRules = array_merge($this->resultRules, $this->{$methodNameBySubCategory}($toggles_rules));
        }
        return $this->resultRules;
    }

    /**
     * Returns rules for ses category
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function ses_rules(array $toggle_rules)
    {
        return [
            'ses_price_category' => 'exists:ses_price_category,id',
            'lat' => ['nullable', 'regex:/^[0-9]{2}.[0-9]{3,4}$/'],
            'lng' => ['nullable', 'regex:/^[0-9]{2}.[0-9]{3,4}$/'],
            'zoom' => 'nullable|numeric|min:0|max:20',
            'area' => 'numeric|min:1|max:999999|' . $toggle_rules['c2'],
            'green_tariff' => 'boolean',
            'photocell_power' => 'numeric|max:999999|' . $toggle_rules['c1']  . $toggle_rules['c1_above_zero'],
            'battery_capacity' => 'numeric|max:999999|' . $toggle_rules['c1'] . $toggle_rules['c1_above_zero'],
            'required_area' => 'max:999999|' . $toggle_rules['c1'],
            'parts_comment' => static::TEXT_AREA_RULE
        ];
    }

    /**
     * Returns rules for ses category network subcategory
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function network_rules(array $toggle_rules)
    {
        return [
            'photocell_power' => 'numeric|min:0|max:30|' . $toggle_rules['c1'] . $toggle_rules['c1_above_zero'],
            'installOnly_power' => 'numeric|max:999999|above_zero|nullable',
            'required_area' => 'numeric|min:0|max:210|' . $toggle_rules['c1'] . $toggle_rules['c1_above_zero'],
            'battery_capacity' => 'nullable',
//            'city' => [
//                $toggle_rules['c1'],
//                $toggle_rules['c3'],
//                'max:50',
//                "regex:/^[\p{L}\s-]+$/u"
//            ],
        ];
    }

    /**
     * Returns rules for ses category commercial subcategory
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function commercial_rules(array $toggle_rules)
    {
        return [
            'battery_capacity' => 'nullable'
        ];
    }

    /**
     * Returns rules for energy_audit category
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function energy_audit_rules($toggle_rules)
    {
        return [
            'area' => 'numeric|min:0|max:999999',
            'floors' => 'numeric|min:1|max:999',
            'exploitation' => 'required|boolean',
            'residents' => 'numeric|max:999999|' . $toggle_rules['exploitation'],
            'electricity_per_year' => 'numeric|min:0|max:999999|' . $toggle_rules['exploitation'],
            'water_per_year' => 'numeric|min:0|max:999999|' . $toggle_rules['exploitation'],
            'gas_per_year' => 'numeric|min:0|max:999999|' . $toggle_rules['exploitation'],
            'heating_type' => 'exists:energy_audit_heating_type,id|' . $toggle_rules['exploitation'],
            'high_costs' => 'boolean',
            'cold_winter' => 'boolean',
            'hot_summer' => 'boolean',
            'mold' => 'boolean',
            'draft' => 'boolean',
            'windows_fogging' => 'boolean',
            'disbalance' => 'boolean',
            'blackout' => 'boolean',
            'provider_problems' => 'boolean',
            'thermal_examination' => 'boolean',
            'blower_door_test' => 'boolean',
            'detailed_calculation' => 'boolean',
            'technical_supervision' => 'boolean',
            'region_id' => 'exists:regions,id|' . $toggle_rules['exploitation'],
            'city' => [
                $toggle_rules['exploitation'],
                'max:50',
                "regex:/^[\p{L}\s-]+$/u"
            ],
            'other_problems' => static::TEXT_AREA_RULE
        ];
    }

    /**
     * Returns rules for waste_sorting category
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function waste_sorting_rules($toggle_rules)
    {
        return [
            'size_id' => 'exists:waste_sorting_size,id|' . $toggle_rules['c1'],
            'pm_help' => 'boolean',
            'size_comment' => static::TEXT_AREA_RULE
        ];
    }

    /**
     * Returns common rules for all categories
     *
     * @param array $toggle_rules
     * @return array
     */
    protected function common_rules(array $toggle_rules)
    {
        return [
            'region_id' => 'exists:regions,id',
            'city_name' => [
                $toggle_rules['c2'],
                'max:50',
//                "regex:/^[\p{L}\s-]+$/u"
            ],
            'city' => [
                'required_if:city_name,=,nullable',
                'max:50',
                "regex:/^[\p{L}\s-]+$/u"
            ],
            'subcategory' => 'exists:order_subcategories,id',
            'about_comment' => static::TEXT_AREA_RULE,
            'house_comment' => static::TEXT_AREA_RULE,
            'client_comment' => static::TEXT_AREA_RULE,
        ];
    }
}