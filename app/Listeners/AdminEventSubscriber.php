<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 20.06.17
 * Time: 14:30
 */

namespace App\Listeners;


use App\Events\Admin\ClientCancelMailing;
use App\Events\Admin\OrderRejected as OrderRejectedEvent;
use App\Events\User\UserLeftEquipmentReview;
use App\Mail\Client\OrderRejected as OrderRejectedMail;
use App\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class AdminEventSubscriber
{
    public function onOrderRejected(OrderRejectedEvent $event)
    {
        $message = (new OrderRejectedMail($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user)->queue($message);
    }

    public function onClientCancelMailing(ClientCancelMailing $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();
        $message = (new \App\Mail\Admin\ClientCancelMailing($event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onUserLeftEquipmentReview(UserLeftEquipmentReview $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();
        $message =(new \App\Mail\Admin\UserLeftEquipmentReview($event->brand_title, $event->review_route, 'Новий відгук для бренду ' . $event->brand_title))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\Admin\OrderRejected',
            'App\Listeners\AdminEventSubscriber@onOrderRejected'
        );

        $events->listen(
            'App\Events\Admin\ClientCancelMailing',
            'App\Listeners\AdminEventSubscriber@onClientCancelMailing'
        );

        $events->listen(
            'App\Events\User\UserLeftEquipmentReview',
            'App\Listeners\AdminEventSubscriber@onUserLeftEquipmentReview'
        );
    }
}