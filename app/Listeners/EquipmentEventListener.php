<?php

namespace App\Listeners;

use App\Events\User\UserLeftEquipmentReview;
use App\Events\Equipment\EquipmentReviewStore;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentReview;
use App\Models\MasterStatus;
use App\Models\User;
use Illuminate\Events\Dispatcher;

class EquipmentEventListener
{
    /**
     * Handle the event.
     *
     * @param  EquipmentReviewStore  $event
     * @return bool
     */
    public function onEquipmentReviewStore(EquipmentReviewStore $event)
    {
        $brand_review = EquipmentReview::findOrFail($event->equipment_review_id);
        $brand_type = array_search($brand_review->equipment->type->slug, Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING);

        if ($event->user->can('update', $brand_review)) {
            $brand_review->fill(['user_id' => $event->user->id])->save();
          event(new UserLeftEquipmentReview($brand_review->equipment->title, route('admin.equipment.review.edit',
            ['equipment_type' => $brand_type, 'brand_slug' => $brand_review->equipment->slug, 'id' => $brand_review->id])));

            return true;
        }
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\Equipment\EquipmentReviewStore',
            'App\Listeners\EquipmentEventListener@onEquipmentReviewStore'
        );
    }
}
