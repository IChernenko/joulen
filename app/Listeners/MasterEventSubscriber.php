<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 19.06.17
 * Time: 19:00
 */

namespace App\Listeners;


use App\Events\Admin\MasterCreatePortfolio;
use App\Events\Admin\MasterVerificationRequest;
use App\Events\Event;
use App\Events\Master\MasterDeclined;
use App\Events\Master\MasterActivated;
use App\Events\Master\MasterReRegistered;
use App\Events\Master\Registered as MasterRegistered;
use App\Events\Master\OrderRequestMade;
use App\Events\MasterBlocked;
use App\Events\MasterCommissionPaid;
use App\Events\MasterOrderInProgress;
use App\Events\MasterOrderStatusСonsidering;
use App\Events\MasterProfileDeleted;
use App\Mail\MasterSelectedToOrder;
use App\Models\Master;
use App\Models\MasterStatus;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class MasterEventSubscriber
{
    public function onMasterDeleted(MasterProfileDeleted $event)
    {
        $master = $event->user->master;
        $master->deletePortfolios();
        $master->deleteStorageDirectory();
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $adminMessage = (new \App\Mail\MasterProfileDeleted($event->user))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($adminMessage);
    }

    public function onMasterBlocked(MasterBlocked $event)
    {
        $masterMessage = (new \App\Mail\MasterBlocked($event->user))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user)->queue($masterMessage);
    }

    public function onOrderStatusConsidering(MasterOrderStatusСonsidering $event)
    {
        $masters = Master::whereHas('subcategoriesWithMailingTrue', function ($category_query) use ($event) {
            $category_query->where(['subcategory_id' => $event->order->subcategory->id]);
        })->whereHas('user', function ($user_query) {
            $user_query->where(['blocked' => false, 'deleted' => false]);
        })->whereHas('status', function ($status_query) {
            $status_query->whereIn('slug', MasterStatus::$validStatuses);
        })->whereHas('regions', function ($region_query) use ($event) {
            $region_query->where('id', $event->order->region->id ?? null);
        })->get();


        $masters->each(function ($master) use ($event) {
            if(!$master->HasUnpaidCommission()){
                $masterMessage = (new \App\Mail\MasterOrderStatusСonsidering($event->order))
                    ->onConnection('database')
                    ->onQueue('emails');

                Mail::to($master->getActiveEmails())->queue($masterMessage);
            }
        });
    }

    public function onOrderStatusInProgress(MasterOrderInProgress $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $masterMessage = (new \App\Mail\MasterOrderInProgress($event->master, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        $adminMessage = (new MasterSelectedToOrder($event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->master->getActiveEmails())->queue($masterMessage);
        Mail::to($admins)->queue($adminMessage);
    }

    public function onCommissionPaid(MasterCommissionPaid $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();
        $event->order->update(['paid' => true]);

        $masterMessage = (new \App\Mail\MasterCommissionPaid($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        $adminMessage = (new \App\Mail\Admin\MasterCommissionPaid($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($adminMessage);
        Mail::to($event->user->master->getActiveEmails())->queue($masterMessage);
    }

    public function onMasterRegistered(MasterRegistered $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\Admin\MasterRegistered($event->master))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onMasterReRegistered(MasterReRegistered $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\Admin\MasterReRegistered($event->master))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onOrderRequestMade(OrderRequestMade $event)
    {
        $subject = 'Нова цінова пропозиція від компанії '.$event->order_request->master->user->name;
        if($event->order_request->order->client->mailing){
            $message = (new \App\Mail\Client\OrderRequestMade($event->order_request, $subject))
                ->onConnection('database')
                ->onQueue('emails');

            $user = $event->order_request->order->client->user;

            Mail::to($user)->queue($message);            
        }
    }

    public function onMasterDeclined(MasterDeclined $event)
    {
        $message = (new \App\Mail\MasterDeclined($event->user))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user->master->getActiveEmails())->queue($message);
    }

    public function onMasterActivated(MasterActivated $event)
    {
        $message = (new \App\Mail\MasterActivated($event->user))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user->master->getActiveEmails())->queue($message);
    }

    public function onMasterVerificationRequest(MasterVerificationRequest $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\Admin\MasterVerificationRequest($event->master))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onMasterCreatePortfolio(MasterCreatePortfolio $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\Admin\MasterCreatePortfolio($event->master, $event->route))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\MasterProfileDeleted',
            'App\Listeners\MasterEventSubscriber@onMasterDeleted'
        );

        $events->listen(
            'App\Events\MasterBlocked',
            'App\Listeners\MasterEventSubscriber@onMasterBlocked'
        );

        $events->listen(
            'App\Events\MasterOrderStatusСonsidering',
            'App\Listeners\MasterEventSubscriber@onOrderStatusConsidering'
        );

        $events->listen(
            'App\Events\MasterOrderInProgress',
            'App\Listeners\MasterEventSubscriber@onOrderStatusInProgress'
        );

        $events->listen(
            'App\Events\MasterCommissionPaid',
            'App\Listeners\MasterEventSubscriber@onCommissionPaid'
        );

        $events->listen(
            'App\Events\Master\OrderRequestMade',
            'App\Listeners\MasterEventSubscriber@onOrderRequestMade'
        );

        $events->listen(
            'App\Events\Master\Registered',
            'App\Listeners\MasterEventSubscriber@onMasterRegistered'
        );

        $events->listen(
            'App\Events\Master\MasterReRegistered',
            'App\Listeners\MasterEventSubscriber@onMasterReRegistered'
        );

        $events->listen(
            'App\Events\Master\MasterDeclined',
            'App\Listeners\MasterEventSubscriber@onMasterDeclined'
        );

        $events->listen(
            'App\Events\Master\MasterActivated',
            'App\Listeners\MasterEventSubscriber@onMasterActivated'
        );

        $events->listen(
            'App\Events\Admin\MasterVerificationRequest',
            'App\Listeners\MasterEventSubscriber@onMasterVerificationRequest'
        );

        $events->listen(
            'App\Events\Admin\MasterCreatePortfolio',
            'App\Listeners\MasterEventSubscriber@onMasterCreatePortfolio'
        );
    }
}