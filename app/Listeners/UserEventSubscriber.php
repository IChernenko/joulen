<?php

namespace App\Listeners;

use App\Events\Admin\AdminActivateEquipmentReview;
use App\Events\Admin\AdminRejectedEquipmentReview;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class UserEventSubscriber
{
    /**
     * @param AdminActivateEquipmentReview $event
     * @return void
     */
    public function onAdminActivateEquipmentReview(AdminActivateEquipmentReview $event)
    {
        $message =(new \App\Mail\AdminActivateEquipmentReview($event->brand_title, $event->brand_route,
            'Ваш відгук для бренду '. '“'. $event->brand_title .'”' . ' схвалено'))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user)->queue($message);
    }

    /**
     * @param AdminRejectedEquipmentReview $event
     * @return void
     */
    public function onAdminRejectedEquipmentReview(AdminRejectedEquipmentReview $event)
    {
        $message =(new \App\Mail\AdminRejectedEquipmentReview($event->review, $event->brand_route,
            'Ваш відгук для бренду '. '“'. $event->review->equipment->title .'”' . ' відхилено'))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user)->queue($message);
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\Admin\AdminActivateEquipmentReview',
            'App\Listeners\UserEventSubscriber@onAdminActivateEquipmentReview'
        );
        $events->listen(
            'App\Events\Admin\AdminRejectedEquipmentReview',
            'App\Listeners\UserEventSubscriber@onAdminRejectedEquipmentReview'
        );
    }
}