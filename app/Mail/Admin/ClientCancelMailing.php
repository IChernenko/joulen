<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 08.12.17
 * Time: 15:08
 */

namespace App\Mail\Admin;


use App\Mail\Mailable;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ClientCancelMailing extends Mailable
{
    use Queueable, SerializesModels;

    public $order;


    /**
     * Create a new message instance.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Клієнт відписався від розсилки')->view('emails.admin.client_cancel_mailing')->with([
            'order' => $this->order
        ]);
        return $view;
    }
}