<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 14:44
 */

namespace App\Mail\Admin;


use App\Models\Order;
use App\Models\Review;
use App\Models\User;
use Illuminate\Bus\Queueable;
use App\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientLeftReview extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $review;


    /**
     * Create a new message instance.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order, Review $review)
    {
        $this->user = $user;
        $this->order = $order;
        $this->review = $review;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->review->positive) {
            $view = $this->subject('Клієнт залишив позитивний відгук')->view('emails.admin.client_positive_review')->with([
                'user' => $this->user,
                'order' => $this->order,
                'review' => $this->review
            ]);
        } else {
            $view = $this->subject('Клієнт залишив негативний відгук')->view('emails.admin.client_negative_review')->with([
                'user' => $this->user,
                'order' => $this->order,
                'review' => $this->review
            ]);
        }
        return $view;
    }
}