<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientOrderConsulting extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    protected $name;
    protected $text;
    protected $phone;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @param $subject
     * @param $text
     * @param $data
     */
    public function __construct($subject, $text, $data)
    {
        $this->subject = $subject;
        $this->text = $text;
        $this->name = $data['name'];
        $this->phone = $data['phone'];
        $this->email = $data['email'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view('emails.admin.client_order_consulting')
            ->with([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'text' => $this->text
        ]);
    }
}
