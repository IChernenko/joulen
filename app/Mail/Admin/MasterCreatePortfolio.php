<?php

namespace App\Mail\Admin;

use App\Models\Master;
use Illuminate\Bus\Queueable;
use App\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MasterCreatePortfolio  extends Mailable {
    use Queueable, SerializesModels;

    public $master;
    public $route;


    /**
     * Create a new message instance.
     * @param Master $master
     * @param $route
     */
    public function __construct(Master $master, $route)
    {
        $this->master = $master;
        $this->route = $route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Компанія додала новий об’єкт у портфоліо')->view('emails.admin.master_create_portfolio')->with([
            'master' => $this->master,
            'route' => $this->route
        ]);
        return $view;
    }
}