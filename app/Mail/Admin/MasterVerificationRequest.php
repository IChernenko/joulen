<?php

namespace App\Mail\Admin;

use App\Models\Master;
use Illuminate\Bus\Queueable;
use App\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MasterVerificationRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $master;


    /**
     * Create a new message instance.
     * @param Master $master
     */
    public function __construct(Master $master)
    {
        $this->master = $master;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Компанія подала заявку на отримання статусу Перевіреної')->view('emails.admin.master_re-registered')->with([
            'user' => $this->master->user,
            'master' => $this->master,
        ]);
        return $view;
    }
}