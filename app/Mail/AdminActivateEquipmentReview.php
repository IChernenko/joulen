<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class AdminActivateEquipmentReview extends Mailable
{
    use Queueable, SerializesModels;

    public $brand_title;
    public $brand_route;
    public $subject;


    /**
     * Create a new message instance.
     * @param string $brand_title
     * @param string $brand_route
     * @param string $subject
     */
    public function __construct(string $brand_title, string $brand_route, $subject = 'Ваш відгук схвалено')
    {
        $this->brand_title = $brand_title;
        $this->brand_route = $brand_route;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->subject)->view('emails.user.admin_activate_equipment_review')->with([
            'brand_title' => $this->brand_title,
            'brand_route' => $this->brand_route
        ]);
        return $view;
    }
}