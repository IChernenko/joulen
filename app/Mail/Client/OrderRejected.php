<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 18:52
 */

namespace App\Mail\Client;


use App\Mail\Mailable;
use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class OrderRejected  extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;


    /**
     * Create a new message instance.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Ваш проект відхилено')->view('emails.client.order.rejected')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
        return $view;
    }
}