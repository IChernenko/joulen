<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 14:44
 */

namespace App\Mail\Client;


use App\Models\OrderRequest;
use Illuminate\Bus\Queueable;
use App\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderRequestMade extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $orderRequest;
    public $custom_subject;

    /**
     * Create a new message instance.
     * @param OrderRequest $orderRequest
     * @param $custom_subject
     */
    public function __construct(OrderRequest $orderRequest, $custom_subject)
    {
        $this->orderRequest = $orderRequest;
        $this->custom_subject = $custom_subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->custom_subject)->view('emails.client.order.request')->with([
            'orderRequest' => $this->orderRequest,
        ]);

        return $view;
    }
}