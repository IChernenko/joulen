<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:59
 */

namespace App\Mail;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ClientOrderCreated
 * @package App\Mail
 */
class ClientOrderAcceptedToReview extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;


    /**
     * Create a new message instance.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Ваша заявка прийнята до розгляду')->view('emails.client.order.accepted_to_review')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
        return $view;
    }
}