<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 19:32
 */

namespace App\Mail;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ClientOrderClosed extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;


    /**
     * Create a new message instance.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Клієнт закрив проект - ' . $this->order->title)->view('emails.client.order.closed')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
        return $view;
    }
}