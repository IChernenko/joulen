<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:59
 */

namespace App\Mail;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class ClientOrderCreated
 * @package App\Mail
 */
class ClientOrderCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $subject;


    /**
     * Create a new message instance.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order, $subject)
    {
        $this->user = $user;
        $this->order = $order;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->subject)->view('emails.client.order.new')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
        return $view;
    }


}