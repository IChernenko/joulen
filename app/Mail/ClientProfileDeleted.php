<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 20.06.17
 * Time: 14:31
 */

namespace App\Mail;


use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ClientProfileDeleted extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Кліент видалив свій профіль')->view('emails.client.profile.deleted')->with([
            'user' => $this->user
        ]);
    }
}