<?php

namespace App\Mail;

use Illuminate\Container\Container;
use Illuminate\Mail\Mailable as VendorMailable;
use Illuminate\Contracts\Mail\Mailer as MailerContract;

class Mailable extends VendorMailable
{
    /**
     * Send the message using the given mailer.
     *
     * @param  \Illuminate\Contracts\Mail\Mailer  $mailer
     * @return void
     */
    public function send(MailerContract $mailer)
    {
        Container::getInstance()->call([$this, 'build']);

        foreach ($this->to as $to){
            $mailer->send($this->buildView(), $this->buildViewData(), function ($message) {
                $this->buildFrom($message)
                    ->buildRecipients($message)
                    ->buildSubject($message)
                    ->buildAttachments($message)
                    ->runCallbacks($message);
            });
        }        
    }

    /**
     * Add all of the recipients to the message.
     *
     * @param  \Illuminate\Mail\Message  $message
     * @return $this
     */
    protected function buildRecipients($message)
    {
        foreach (['cc', 'bcc', 'replyTo'] as $type) {
            foreach ($this->{$type} as $recipient) {
                $message->{$type}($recipient['address'], $recipient['name']);
            }
        }

        $recipient = array_shift($this->to);
        $message->to($recipient['address'], $recipient['name']);

        return $this;
    }
}