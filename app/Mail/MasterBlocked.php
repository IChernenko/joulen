<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:08
 */

namespace App\Mail;


use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MasterBlocked extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ваш акаунт заблокований')->view('emails.master.blocked')->with([
            'user' => $this->user
        ]);
    }
}