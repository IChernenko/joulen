<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 16:51
 */

namespace App\Mail;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MasterOrderStatusСonsidering extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Нове замовлення')->view('emails.master.order.status.considering')->with([
            'order' => $this->order
        ]);
    }
}