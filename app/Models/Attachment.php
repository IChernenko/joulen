<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File as FileHelper;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Attachment extends Model
{
    const SMALL_PREFIX = 'small_';

    /*
     * size of thumbnails
     */

    const PORTFOLIO_PHOTO_WIDTH = 260;

    const PORTFOLIO_PHOTO_HEIGHT = 225;

    const MASTER_LOGO_HEIGHT = 150;

    const EQUIPMENT_LOGO_HEIGHT = 128;

    const EQUIPMENT_LOGO_WIDTH = 128;

    protected $fillable = [
        'path',
        'disk_name',
        'attachable_id',
        'attachment_type_id'
    ];

    /**
     * @var array The attributes that aren't mass assignable.
     */
    protected $guarded = ['disk_name'];

    /**
     * @var array Hidden fields from array/json access
     */
    protected $hidden = ['attachment_type', 'attachment_id'];

    public function attachable()
    {
        return $this->morphTo();
    }


    public function saveFromPost(\Illuminate\Http\UploadedFile $uploadedFile)
    {
        if ($uploadedFile === null)
            return;


        $this->file_name = $uploadedFile->getClientOriginalName();
        $this->disk_name = $this->getDiskName();

        $this->putFile($uploadedFile->getRealPath(), $this->disk_name);

        return $this;
    }

    public function getPath($is_small = false)
    {
        if(Storage::disk('public')->exists($this->disk_name))
            return asset('images/default_photo.jpg');

        $file_name = $is_small ? self::SMALL_PREFIX . $this->disk_name : $this->disk_name;

        return url('/storage'. $this->getPartitionDirectory() . $file_name);
    }

    /**
     * Generates a disk name from the supplied file name.
     */
    protected function getDiskName()
    {
        if ($this->disk_name !== null)
            return $this->disk_name;

        $ext = strtolower($this->getExtension());
        $name = str_replace('.', '', uniqid(null, true));

        return $this->disk_name = $ext !== null ? $name . '.' . $ext : $name;
    }

    protected function putFile($sourcePath, $destinationFileName = null)
    {
        $destinationPath = $this->getLocalRootPath() . '/' . $this->getStorageDirectory() . $this->getPartitionDirectory();

        if (!$destinationFileName) {
            $destinationFileName = $this->disk_name;
        }
        /*
        * Verify the directory exists, if not try to create it. If creation fails
        * because the directory was created by a concurrent process then proceed,
        * otherwise trigger the error.
        */
        if (
            !FileHelper::isDirectory($destinationPath) &&
            !FileHelper::makeDirectory($destinationPath, 0755, true, true) &&
            !FileHelper::isDirectory($destinationPath)
        ) {
            trigger_error(error_get_last(), E_USER_WARNING);
        }

        return FileHelper::copy($sourcePath, $destinationPath . $destinationFileName);
    }

    public function deleteStorageFile() {
        $destinationPath = $this->getLocalRootPath() . '/' . $this->getStorageDirectory() . $this->getPartitionDirectory();

        return FileHelper::delete($destinationPath.$this->getDiskName());
    }


    /**
     * Generates a partition for the file.
     * return /ABC/DE1/234 for an name of ABCDE1234.
     * @return mixed
     */
    public function getPartitionDirectory()
    {
        return '/' . $this->attachable_type . '/' . $this->attachable_id . '/';
    }

    protected function getStorageDirectory()
    {
        return 'public';
    }

    protected function getLocalRootPath()
    {
        return storage_path() . '/app';
    }

    /**
     * Returns the file extension.
     */
    protected function getExtension()
    {
        return FileHelper::extension($this->file_name);
    }


}
