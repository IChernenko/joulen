<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ClientEmail
 * 
 * @property int $id
 * @property int $client_id
 * @property int $master_id
 * @property int $order_id
 * @property string $text
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Client $client
 * @property \App\Models\Master $Master
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class ClientEmail extends Eloquent
{
    public $timestamps = true;

	protected $casts = [
		'client_id' => 'int',
		'master_id' => 'int',
		'order_id' => 'int'
	];

	protected $fillable = [
		'client_id',
		'master_id',
		'order_id',
		'text',
		'comment'
	];

	public function client()
	{
		return $this->belongsTo(\App\Models\Client::class);
	}

	public function master()
	{
		return $this->belongsTo(\App\Models\Master::class);
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}
}
