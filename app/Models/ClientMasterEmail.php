<?php

namespace App\Models;

use Reliese\Coders\Model\Model;
use Reliese\Database\Eloquent\Model as Eloquent;


class ClientMasterEmail extends Eloquent
{
    protected $table = 'client_master_email';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_email', 'email');
    }

    public function master()
    {
        return $this->belongsTo(\App\Models\Master::class);
    }

    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class);
    }
}
