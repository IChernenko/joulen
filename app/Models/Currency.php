<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 13.06.17
 * Time: 16:55
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Currency
 *
 * @property float $rate
 * @property string $txt
 * @property string $cc
 * @property string $exchangedate
 *
 * @package App\Models
 */
class Currency extends Eloquent
{
    protected $casts = [
        'rate' => 'float',
    ];

    protected $fillable = [
        'rate',
        'txt',
        'cc',
        'exchangedate'
    ];

    public function getRate()
    {
        return round($this->rate,2);
    }
}