<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EnergyAuditHeatingType
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $order_energy_audits
 *
 * @package App\Models
 */
class EnergyAuditHeatingType extends Eloquent
{
	protected $table = 'energy_audit_heating_type';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function order_energy_audits()
	{
		return $this->hasMany(\App\Models\OrderEnergyAudit::class, 'heating_type_id');
	}
}
