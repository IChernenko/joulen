<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderEnergyAudit
 * 
 * @property int $id
 * @property int $order_id
 * @property int $heating_type_id
 * @property int $floors
 * @property int $area
 * @property string $area_unit
 * @property bool $exploitation
 * @property int $residents
 * @property int $electricity_per_year
 * @property int $water_per_year
 * @property int $gas_per_year
 * @property string $house_comment
 * @property bool $high_costs
 * @property bool $cold_winter
 * @property bool $hot_summer
 * @property bool $mold
 * @property bool $draft
 * @property bool $windows_fogging
 * @property bool $disbalance
 * @property bool $blackout
 * @property bool $provider_problems
 * @property string $other_problems
 * @property bool $thermal_examination
 * @property bool $blower_door_test
 * @property bool $energy_passport
 * @property bool $detailed_calculation
 * @property bool $technical_supervision
 * 
 * @property \App\Models\EnergyAuditHeatingType $energy_audit_heating_type
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class OrderEnergyAudit extends Eloquent
{	
	protected $table = 'order_energy_audit';
	public $timestamps = false;

	protected $casts = [
		'order_id' => 'int',
		'heating_type_id' => 'int',
		'floors' => 'int',
		'area' => 'int',
		'area_unit' => 'string',
		'exploitation' => 'bool',
		'residents' => 'int',
		'electricity_per_year' => 'int',
		'water_per_year' => 'int',
		'gas_per_year' => 'int',
		'high_costs' => 'bool',
		'cold_winter' => 'bool',
		'hot_summer' => 'bool',
		'mold' => 'bool',
		'draft' => 'bool',
		'windows_fogging' => 'bool',
		'disbalance' => 'bool',
		'blackout' => 'bool',
		'provider_problems' => 'bool',
		'thermal_examination' => 'bool',
		'blower_door_test' => 'bool',
		'energy_passport' => 'bool',
		'detailed_calculation' => 'bool',
		'technical_supervision' => 'bool'
	];

	protected $fillable = [
		'order_id',
		'heating_type_id',
		'floors',
		'area',
		'area_unit',
		'exploitation',
		'residents',
		'electricity_per_year',
		'water_per_year',
		'gas_per_year',
		'house_comment',
		'high_costs',
		'cold_winter',
		'hot_summer',
		'mold',
		'draft',
		'windows_fogging',
		'disbalance',
		'blackout',
		'provider_problems',
		'other_problems',
		'thermal_examination',
		'blower_door_test',
		'energy_passport',
		'detailed_calculation',
		'technical_supervision'
	];

	public $with = ['energy_audit_heating_type'];

	protected $about_block;

	protected $problems_block;

	protected $measures_block;

	public function getAboutBlockAttribute()
	{
		$about_block =  false;

		$about_block = !empty($this->area) ?: $about_block;
		$about_block = !empty($this->floors) ?: $about_block;
		$about_block = count($this->order->attachments) ?: $about_block;
		$about_block = !empty($this->residents) ?: $about_block;
		$about_block = !empty($this->electricity_per_year) ?: $about_block;
		$about_block = !empty($this->water_per_year) ?: $about_block;
		$about_block = !empty($this->gas_per_year) ?: $about_block;
		$about_block = count($this->energy_audit_heating_type) ?: $about_block;
		$about_block = count($this->order->region) ?: $about_block;
		$about_block = !empty($this->order->city) ?: $about_block;
		$about_block = !empty($this->house_comment) ?: $about_block;		

		return $about_block;
	}

	public function getProblemsBlockAttribute()
	{
		$problems_block =  false;

		$problems_block = !empty($this->high_costs) ?: $problems_block;
		$problems_block = !empty($this->cold_winter) ?: $problems_block;
		$problems_block = !empty($this->hot_summer) ?: $problems_block;
		$problems_block = !empty($this->mold) ?: $problems_block;
		$problems_block = !empty($this->draft) ?: $problems_block;
		$problems_block = !empty($this->windows_fogging) ?: $problems_block;
		$problems_block = !empty($this->disbalance) ?: $problems_block;
		$problems_block = !empty($this->blackout) ?: $problems_block;
		$problems_block = !empty($this->provider_problems) ?: $problems_block;
		$problems_block = !empty($this->other_problems) ?: $problems_block;

		return $problems_block;
	}

	public function getMeasuresBlockAttribute()
	{
		$measures_block =  false;

		$measures_block = !empty($this->thermal_examination) ?: $measures_block;
		$measures_block = !empty($this->blower_door_test) ?: $measures_block;
		$measures_block = !empty($this->energy_passport) ?: $measures_block;
		$measures_block = !empty($this->detailed_calculation) ?: $measures_block;
		$measures_block = !empty($this->technical_supervision) ?: $measures_block;

		return $measures_block;
	}

	public function energy_audit_heating_type()
	{
		return $this->belongsTo(\App\Models\EnergyAuditHeatingType::class, 'heating_type_id');
	}

	public function order()
	{
		return $this->morphOne(\App\Models\Order::class, 'orderable');
	}
}
