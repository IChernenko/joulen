<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $request_price_types
 * @property \Illuminate\Database\Eloquent\Collection $subcategories
 * @property \Illuminate\Database\Eloquent\Collection $masters
 *
 * @package App\Models
 */
class OrderCategory extends Eloquent
{
    //TODO:: use this constants instead of string slugs. (replace existing)
    const SES = "ses";
    const ENERGY_AUDIT = "energy_audit";
    const WEST_SORTING = "waste_sorting";

	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function request_price_types()
	{
		return $this->hasMany(\App\Models\OrderRequestPriceType::class, 'category_id');
	}

	public function subcategories()
	{
		return $this->hasMany(\App\Models\OrderSubcategory::class, 'category_id');
	}

	public function masters()
	{
		return $this->belongsToMany(\App\Models\Master::class, 'master_order_category', 'category_id', 'master_id')->withPivot('mailing');
	}
}
