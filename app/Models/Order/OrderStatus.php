<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderStatus
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class OrderStatus extends Eloquent
{
	public $timestamps = false;

	const CONSIDERING = 3;
	const IN_PROGRESS = 'in_progress';

	protected $fillable = [
		'name',
		'slug'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'status_id');
	}
}
