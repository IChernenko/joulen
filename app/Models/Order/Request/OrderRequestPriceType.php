<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRequestPriceType
 * 
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $description
 * @property string $slug
 * 
 * @property \App\Models\OrderCategory $category
 * @property \Illuminate\Database\Eloquent\Collection $prices
 *
 * @package App\Models
 */
class OrderRequestPriceType extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'category_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'name',
		'description',
		'slug'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\OrderCategory::class, 'category_id');
	}

	public function prices()
	{
		return $this->hasMany(\App\Models\OrderRequestPrice::class, 'type_id');
	}

	public function orders()
    {
        return $this->belongsToMany(\App\Models\Order::class, 'order_price_type_pivot', 'order_id', 'price_type_id');
    }
}
