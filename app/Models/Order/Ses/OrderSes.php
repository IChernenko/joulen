<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderSes
 * 
 * @property int $id
 * @property int $order_id
 * @property int $price_category_id
 * @property float $photocell_power
 * @property int $battery_capacity
 * @property int $required_area
 * @property int $area
 * @property string $area_unit
 * @property string $parts_comment
 * @property float $lat
 * @property float $lng
 * @property int $zoom
 * @property string $house_comment
 * @property bool $green_tariff
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\SesPriceCategory $ses_price_category
 *
 * @package App\Models
 */
class OrderSes extends Eloquent
{    
	public $timestamps = false;

	protected $casts = [
		'order_id' => 'int',
		'price_category_id' => 'int',
		'photocell_power' => 'float',
		'battery_capacity' => 'float',
		'required_area' => 'int',
		'area' => 'int',
		'area_unit' => 'string',
		'lat' => 'float',
		'lng' => 'float',
		'zoom' => 'int',
		'green_tariff' => 'bool'
	];

	protected $fillable = [
		'order_id',
		'price_category_id',
		'photocell_power',
		'battery_capacity',
		'required_area',
		'area',
		'area_unit',
		'parts_comment',
		'lat',
		'lng',
		'zoom',
		'house_comment',
		'green_tariff'
	];


	protected $delivery_block;

	protected $installation_block;

	protected $legal_block;

	/* Accessors */

	/**
	 * Returns true if at least one value in delivery_block is not empty
	 *
	 * @return bool
	 */
	public function getDeliveryBlockAttribute()
	{
		$delivery_block =  false;

		$delivery_block = !empty($this->photocell_power) ?: $delivery_block;
		$delivery_block = !empty($this->required_area) ?: $delivery_block;
		$delivery_block = !empty($this->battery_capacity) ?: $delivery_block;
		$delivery_block = !empty($this->parts_comment) ?: $delivery_block;

		return $delivery_block;
	}

	/**
	 * Returns true if at least one value in installation_block is not empty
	 *
	 * @return bool
	 */
	public function getInstallationBlockAttribute()
	{
		$installation_block =  false;

		$installation_block = count($this->order->region) ?: $installation_block;
		$installation_block = !empty($this->order->city) ?: $installation_block;
		$installation_block = !empty($this->lat) ?: $installation_block;
		$installation_block = !empty($this->lng) ?: $installation_block;
		$installation_block = !empty($this->zoom) ?: $installation_block;
		$installation_block = !empty($this->area) ?: $installation_block;
		$installation_block = count($this->order->attachments) ?: $installation_block;
		$installation_block = !empty($this->house_comment) ?: $installation_block;

		return $installation_block;
	}

	/**
	 * Returns true if at least one value in legal_block is not empty
	 *
	 * @return bool
	 */
	public function getLegalBlockAttribute()
	{
		$legal_block =  false;

		$legal_block = !empty($this->green_tariff) ?: $legal_block;

		return $legal_block;
	}

	/* Relationships */
	
	public function order()
	{
		return $this->morphOne(\App\Models\Order::class, 'orderable');
	}

	public function ses_price_category()
	{
		return $this->belongsTo(\App\Models\SesPriceCategory::class, 'price_category_id');
	}
}
