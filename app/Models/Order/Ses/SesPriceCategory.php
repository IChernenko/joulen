<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SesPriceCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $order_ses
 *
 * @package App\Models
 */
class SesPriceCategory extends Eloquent
{
	protected $table = 'ses_price_category';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function order_ses()
	{
		return $this->hasMany(\App\Models\OrderSes::class, 'price_category_id');
	}
}
