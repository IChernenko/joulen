<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderWasteSorting
 * 
 * @property int $id
 * @property int $order_id
 * @property int $size_id
 * @property string $size_comment
 * @property string $about_comment
 * @property bool $pm_help
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\WasteSortingSize $waste_sorting_size
 *
 * @package App\Models
 */
class OrderWasteSorting extends Eloquent
{
	protected $table = 'order_waste_sorting';
	public $timestamps = false;

	protected $casts = [
		'order_id' => 'int',
		'size_id' => 'int',
		'pm_help' => 'bool'
	];

	protected $fillable = [
		'order_id',
		'size_id',
		'size_comment',
		'about_comment',
		'pm_help'
	];

	public $with = ['waste_sorting_size'];


	/**
	 * Returns true if at least one value in delivery_block is not empty
	 *
	 * @return bool
	 */
	public function getDeliveryBlockAttribute()
	{
		$delivery_block =  false;

		$delivery_block = count($this->waste_sorting_size) ?: $delivery_block;
		$delivery_block = !empty($this->size_comment) ?: $delivery_block;

		return $delivery_block;
	}


	/**
	 * Returns true if at least one value in installation_block is not empty
	 *
	 * @return bool
	 */
	public function getInstallationBlockAttribute()
	{
		$installation_block =  false;

		$installation_block = count($this->order->region) ?: $installation_block;
		$installation_block = !empty($this->order->city) ?: $installation_block;
		$installation_block = !empty($this->about_comment) ?: $installation_block;

		return $installation_block;
	}

	/**
	 * Returns true if at least one value in legal_block is not empty
	 *
	 * @return bool
	 */
	public function getLegalBlockAttribute()
	{
		$legal_block =  false;

		$legal_block = !empty($this->pm_help) ?: $legal_block;

		return $legal_block;
	}

	public function order()
	{
		return $this->morphOne(\App\Models\Order::class, 'orderable');
	}

	public function waste_sorting_size()
	{
		return $this->belongsTo(\App\Models\WasteSortingSize::class, 'size_id');
	}
}
