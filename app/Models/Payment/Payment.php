<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models\Payment;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $id
 * @property int $master_id
 * @property int $order_id
 * @property float $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Master $Master
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class Payment extends Eloquent
{
	protected $casts = [
		'master_id' => 'int',
		'order_id' => 'int',
        'transaction_id' => 'int',
		'amount' => 'float'
	];

	protected $fillable = [
		'master_id',
		'order_id',
		'amount',
        'transaction_id',
        'status_id'
	];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $dateFormat = 'Y-m-d H:i:s';


	public function master()
	{
		return $this->belongsTo(\App\Models\Master::class);
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'payment_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }


	public function getForrmatedDate()
    {
        return $this->updated_at->format('d-m-Y');
    }

    public function hasValidStatus()
    {
        return $this->status->name == Status::STATUS_SUCCESS || $this->status->name == Status::STATUS_MANUAL;
    }


    public function scopeOfMaster($query, $masterId)
    {
        return $query->where('master_id', $masterId);
    }
}
