<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Region
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $masters
 * @property \Illuminate\Database\Eloquent\Collection $mastersWork
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Region extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function masters()
	{
		return $this->hasMany(\App\Models\Master::class);
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}

	public function mastersWork()
	{
		return $this->belongsToMany(\App\Models\Master::class, 'master_region', 'region_id', 'master_id');
	}
}
