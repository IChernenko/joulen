<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderSubcategory
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 *
 * @property \App\Models\OrderCategory $category
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $partners
 *
 * @package App\Models
 */
//TODO: move this class to Order directory and Order namespace
class SubcategoryFactor extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'subcategory_id' => 'int'
    ];

    protected $fillable = [
        'subcategory_id',
        'factor'
    ];

    public function subcategory()
    {
        return $this->belongsTo(\App\Models\OrderSubcategory::class, 'subcategory_id');
    }
    
    public function getPercentsAttribute()
    {
        return $this->factor * 100;
    }
}
