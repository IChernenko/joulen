<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\User;
/**
 * Class Client
 * 
 * @property int $id
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $client_emails
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $reviews
 *
 * @package App\Models
 */
class Client extends User
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
        'status_id',
        'mailing'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function client_emails()
	{
		return $this->hasMany(\App\Models\ClientEmail::class);
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}
//
//	public function reviews()
//	{
//		return $this->hasMany(\App\Models\Review::class);
//	}
}
