<?php

namespace App;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    const MANAGER_ID = 1;
    const OWNER_ID = 2;

    protected $fillable = [
        'title',
        'slug',
    ];

    public function equipment()
    {
        return $this->hasMany(Employee::class);
    }
}
