<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EquipmentExchangeFund extends Model
{
    public $timestamps = false;

    public $table = 'equipment_exchange_fund';
}
