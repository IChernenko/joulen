<?php

namespace App\Models;

use App\Models\Equipment\Equipment;
use Illuminate\Database\Eloquent\Model;

class EquipmentGuaranteeRepair extends Model
{
    public $timestamps = false;

    public $table = 'equipment_guarantee_repair';
}
