<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterOffice extends Model
{
    public static $default_photo = '/images/default_photo.jpg';

    protected $casts = [
        'master_id' => 'int',
        'region_id' => 'int',
        'lat' => 'float',
        'lng' => 'float',
    ];

    public $timestamps = false;

    protected $fillable = [
        'master_id',
        'region_id',
        'city',
        'lat',
        'lng',
        'address',
        'index',
        'room'
    ];

    public function master()
    {
        return $this->hasOne(Master::class);
    }

    public function picture()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    public function getPictureSrc()
    {
        return $this->picture ? $this->picture->getPath() : null;
    }

}
