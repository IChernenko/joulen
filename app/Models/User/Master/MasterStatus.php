<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterStatus extends Model
{
    const INACTIVE = 'inactive';

    public $timestamps = false;

    public static $validStatuses = [
        'unverified',
        'verified',
    ];

    protected $fillable = [
        'name',
        'slug'
    ];

    public function masters()
    {
        return $this->hasMany(Master::class);
    }
}
