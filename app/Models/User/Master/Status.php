<?php

namespace App\Models\User\Master;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const VERIFIED_STATUS = 'verified';
    const CHECKING_STATUS = 'checking';
    const REJECTED_STATUS = 'rejected';

    public $timestamps = false;

    public function scopeVerifiedStatus($query)
    {
        $query->where('slug', self::VERIFIED_STATUS);
    }

    public function scopeRejectedStatus($query)
    {
        $query->where('slug', self::REJECTED_STATUS);
    }
}
