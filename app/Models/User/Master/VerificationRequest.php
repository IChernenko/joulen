<?php

namespace App\Models\User\Master;

use App\Models\Master;
use Illuminate\Database\Eloquent\Model;

class VerificationRequest extends Model
{

    protected $fillable = [
        'master_id',
        'status_id',
        'comment'
    ];

    public function master()
    {
        return $this->belongsTo(Master::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
