<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Partner
 * 
 * @property int $id
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $order_subcategories
 * @property \Illuminate\Database\Eloquent\Collection $preorder_categories
 * @property \Illuminate\Database\Eloquent\Collection $settings
 *
 * @package App\Models
 */
class Partner extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function order_subcategories()
	{
		return $this->belongsToMany(\App\Models\OrderSubcategory::class, 'order_subcategory_partner', 'partner_id', 'subcategory_id');
	}

	public function preorder_categories()
	{
		return $this->belongsToMany(\App\Models\PreorderCategory::class, 'partner_preorder_category', 'partner_id', 'category_id');
	}

	public function settings()
	{
		return $this->hasMany(\App\Models\PartnerSetting::class);
	}
}
