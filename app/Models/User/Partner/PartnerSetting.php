<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PartnerSetting
 * 
 * @property int $id
 * @property int $partner_id
 * @property string $key
 * @property string $value
 * 
 * @property \App\Models\Partner $partner
 *
 * @package App\Models
 */
class PartnerSetting extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'partner_id' => 'int'
	];

	protected $fillable = [
		'partner_id',
		'key',
		'value'
	];

	public function partner()
	{
		return $this->belongsTo(\App\Models\Partner::class);
	}
}
