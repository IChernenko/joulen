<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use App\EmployeeType;
use App\Models\User\Distributor;
use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\Auth\ResetPassword as ResetPasswordNotification;
/**
 * Class User
 * 
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $first_name
 * @property int $role_id
 * @property string $phone
 * @property bool $blocked
 * @property bool $deleted
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\UserRole $userRole
 * @property \App\Models\Admin admin
 * @property \App\Models\Client $client
 * @property \App\Models\Master $master
 * @property \App\Models\Partner $partner
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;

	protected $casts = [
		'role_id' => 'int',
		'blocked' => 'bool',
		'deleted' => 'bool'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'email',
		'password',
		'name',
		'role_id',
		'phone',
		'blocked',
		'deleted',
	];

    public function getNameWithStatusAttribute()
	{
		if ($this->deleted){
			return $this->attributes['name'] . ' (Видалився)';
		} elseif ($this->blocked){
			return $this->attributes['name'] . ' (Заблокований)';
		} else {
			return $this->attributes['name'];
		}
	}

    public function getFirstNameAttribute()
	{
		return explode(" ", $this->name)[0];
	}

    public function role()
    {
        return $this->belongsTo(\App\Models\UserRole::class, 'role_id');
    }

	public function admin()
	{
		return $this->hasOne(\App\Models\Admin::class);
	}

	public function client()
	{
		return $this->hasOne(\App\Models\Client::class);
	}

	public function distributor()
	{
		return $this->hasOne(Distributor::class);
	}

    public function orders()
    {
        return $this->hasManyThrough(Order::class, Client::class);
    }

    public function profileType()
    {
        return $this->hasOne(\App\Models\UserProfile::class)->withDefault();
    }

	public function master()
	{
		return $this->hasOne(\App\Models\Master::class)->with('user');
	}

	public function partner()
	{
		return $this->hasOne(\App\Models\Partner::class);
	}

	public function hasAnyRole($roles) {
		if(!is_array($roles))
			return false;

        $roles = array_map('trim', $roles);

        if ( in_array( $this->role->name, $roles ) ){
            return true;
        }
		return false;
    }

	public function isAdmin(){
		return $this->role->name === UserRole::ADMIN;
	}

	public function isClient(){
		return $this->role->name === UserRole::CLIENT;
	}

	public function isMaster(){
		return $this->role->name === UserRole::MASTER;
	}

	public function isPartner(){
		return $this->role->name === UserRole::PARTNER;
	}

	public function isDistributor(){
		return $this->role->name === UserRole::DISTRIBUTOR;
	}

	public function isTempMaster(){
		return $this->role->name === UserRole::TEMP_MASTER;
	}

	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetPasswordNotification($token, $this->email));
	}

	public function employees()
    {
        return $this->hasMany(Employee::class);
    }

	public function manager()
    {
        return $this->hasOne(Employee::class)->where('type_id', EmployeeType::MANAGER_ID)->withDefault();
    }

	public function owner()
    {
        return $this->hasOne(Employee::class)->where('type_id', EmployeeType::OWNER_ID)->withDefault();
    }

}
