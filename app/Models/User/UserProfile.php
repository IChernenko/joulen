<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserProfile extends Authenticatable
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
		'type_id',
		'profile_id',
		'remember_token',
		'name',
		'email',
		'phone',
        'profile_url',
        'token'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function type()
    {
        return $this->belongsTo(\App\Models\UserProfileType::class);
    }
}
