<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfileType extends Model
{

    public function profiles()
    {
        return $this->hasMany(\App\Models\UserProfile::class, 'type_id');
    }
}
