<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserRole
 * 
 * @property int $id
 * @property string $role
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class UserRole extends Eloquent
{
	public $timestamps = false;

	const CLIENT_ID = 2;
	const DISTRIBUTOR_ID = 6;
	const DISTRIBUTOR = 'distributor';
	const ADMIN = 'admin';
	const CLIENT = 'client';
	const MASTER = 'master';
	const TEMP_MASTER = 'temp_master';
	const PARTNER = 'partner';

	protected $fillable = [
		'name'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'role_id');
	}

	public function isMaster()
    {
        return $this->name == self::MASTER;
    }

	public function getDefaultPageRoute()
    {
        switch ($this->name) {
            case self::DISTRIBUTOR:
                return "{$this->name}.edit";
            default:
                return "{$this->name}.order.index";
        }
    }
}
