<?php

namespace App\Policies;

use App\Models\User;
use App\Models\User\Distributor;
use Illuminate\Auth\Access\HandlesAuthorization;

class DistributorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can edit distributor.
     *
     * @param  \App\Models\User $user
     * @param Distributor $distributor
     * @return boolean
     */
    public function edit(User $user, Distributor $distributor)
    {
        return $user->isAdmin();
    }
}
