<?php

namespace App\Policies;

use App\Models\Equipment\EquipmentReview;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EquipmentReviewPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the equipmentReview.
     *
     * @param  \App\Models\User $user
     * @param EquipmentReview $equipmentReview
     * @return mixed
     */
    public function view(User $user, EquipmentReview $equipmentReview)
    {
        //
    }

    /**
     * Determine whether the user can create equipmentReviews.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the equipmentReview.
     *
     * @param  \App\Models\User  $user
     * @param  EquipmentReview  $equipmentReview
     * @return mixed
     */
    public function update(User $user, EquipmentReview $equipmentReview)
    {
        if ($user->isTempMaster() || ($user->isMaster() && $user->master->isInactive())) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the equipmentReview.
     *
     * @param  \App\Models\User  $user
     * @param  EquipmentReview  $equipmentReview
     * @return mixed
     */
    public function delete(User $user, EquipmentReview $equipmentReview)
    {
        //
    }
}
