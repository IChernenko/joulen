<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        if ($user->isAdmin()) {
            return true;
        } elseif ($user->isPartner()) {
//            TODO: implement granting rights functionality
            return true;
        } elseif ($user->isClient()) {
            return $user->client->id === $order->client_id;
        } elseif ($user->isMaster()) {
            return $this->own($user, $order) ?: in_array($order->status->slug, ['considering']) ;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function update(User $user, Order $order)
    {
        if ($user->isAdmin()) {
            return true;
        } elseif ($user->isPartner()) {
//            TODO: implement granting rights functionality
            return true;
        } elseif ($user->isClient()) {
            return $user->client->id === $order->client_id && !in_array($order->status->slug, ['done', 'in_progress', 'deactivated']);
        } elseif ($user->isMaster()) {
            return false;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function delete(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can change status of order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function change_status(User $user, Order $order)
    {
        if ($user->isAdmin()) {
            return true;
        } elseif ($user->isPartner()) {
//            TODO: implement granting rights functionality
            return true;
        } elseif ($user->isClient()) {
            return $user->client->id === $order->client_id;
        } elseif ($user->isMaster()) {
            return false;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can leave review to the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function leave_review(User $user, Order $order)
    {
//        TODO: check case when request already exists
        if ( $user->isPartner() || $user->isMaster()) {
            return false;
        } elseif ($user->isAdmin() && is_null($order->review_by_master) && $order->status->slug == 'done') {
            return true;
        } elseif ($user->isClient() && $user->client->id == $order->client_id && is_null($order->review_by_master)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user own the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function own(User $user, Order $order)
    {
        if ($user->isAdmin() || $user->isPartner()) {
            return false;
        } elseif ($user->isClient()) {
            return $user->client->id === $order->client_id;
        } elseif ($user->isMaster()) {
            return $user->master->id === $order->master_id;
        } else {
            return false;
        }
    }
}
