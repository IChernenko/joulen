<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can restore users.
     *
     * @param  \App\Models\User $auth_user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function restore(User $auth_user, User $user)
    {
        if ($auth_user->isAdmin()) {
            return true;
        } else {
            return false;
        }
    }
}
