<?php

namespace App\Providers;

use App\Models\Equipment\EquipmentReview;
use App\Models\Master;
use App\Models\Order;
use App\Models\OrderRequest;
use App\Models\Review;
use App\Models\User;
use App\Models\User\Distributor;
use App\Policies\DistributorPolicy;
use App\Policies\EquipmentReviewPolicy;
use App\Policies\MasterPolicy;
use App\Policies\OrderPolicy;
use App\Policies\OrderRequestPolicy;
use App\Policies\ReviewPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Order::class => OrderPolicy::class,
        OrderRequest::class => OrderRequestPolicy::class,
        Review::class => ReviewPolicy::class,
        User::class => UserPolicy::class,
        Master::class => MasterPolicy::class,
        EquipmentReview::class => EquipmentReviewPolicy::class,
        Distributor::class => DistributorPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
