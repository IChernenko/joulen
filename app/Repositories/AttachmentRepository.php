<?php

namespace App\Repositories;

use App\Models\Attachment;
use App\Models\AttachmentType;
use Intervention\Image\Facades\Image;

class AttachmentRepository extends BaseRepository
{
    public function model()
    {
        return 'App\Models\Attachment';
    }

    public function resizePhoto($attachment, $width = null, $height = null)
    {
        $path = storage_path('app/public'.$attachment->getPartitionDirectory().$this->model::SMALL_PREFIX.$attachment->disk_name);
        $img = Image::make($attachment->getPath());
        if($width == null || $height == null) {
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        else {
            $img->resize($width, $height);
        }
        $img->save($path);
    }

    public function saveAttachment($model, string $attachable_type, $attachment_type_id, $photo, $public)
    {
        $attachment = new Attachment();
        $attachment->attachable_id = $model->id;
        $attachment->attachable_type = $attachable_type;
        $attachment->public = $public;
        $attachment->attachment_type_id = $attachment_type_id;
        $attachment->saveFromPost($photo);
        $attachment->save();

        return $attachment;
    }
}