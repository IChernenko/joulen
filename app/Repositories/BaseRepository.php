<?php

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Database\Query\Expression;

abstract class BaseRepository extends Repository
{

    protected $preventCriteriaOverwriting = false;

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
//    TODO: add this method to interface
    public function findOrFail($id, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * @param string $column
     * @param string $direction
     * @return mixed
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->applyCriteria();
        return $this->model->orderBy($column, $direction);
    }

    /**
     * @param $table
     * @param $first
     * @param null $operator
     * @param null $second
     * @return mixed
     */
    public function leftJoin($table, $first, $operator = null, $second = null)
    {
        $this->applyCriteria();
        return $this->model->join($table, $first, $operator, $second, 'left');
    }

    /**
     * @param $column
     * @return mixed
     */
    public function select($column)
    {
        $this->applyCriteria();
        return $this->model->select($column);
    }
}