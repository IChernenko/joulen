<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 29.06.17
 * Time: 14:39
 */

namespace App\Repositories;


class ClientEmailRepository extends BaseRepository
{
    public function model()
    {
        return 'App\Models\ClientEmail';
    }
}