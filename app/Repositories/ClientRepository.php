<?php

namespace App\Repositories;


use App\Models\Client;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Collection;

class ClientRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Client';
    }

    public function update(array $data, $id)
    {
        $data = new Collection($data);
        $client = Client::find($id);

        $client->user->name = $data->get('name', $client->user->name);
        $tel = str_replace(array('+', ' ', '(' , ')', '-'), '', $data->get('phone', $client->user->phone));
        if(strpos($tel, '38') === 0) {
            $tel = substr($tel, 2);
        }
        $client->user->phone = $tel;
        $client->user->email = $data->get('email', $client->user->email);
        if($data->get('password')) {
            $client->user->password = bcrypt($data->get('password', $client->user->password));
        }
        $client->user->blocked = $data->get('blocked', false);

        $client->mailing = $data->get('mailing', false);
        $client->push();
        
        return $client;
    }

    public function store(array $data)
    {
        $data = new Collection($data);

        $tel = str_replace(array('+', ' ', '(' , ')', '-'), '', $data->get('phone'));
        if(strpos($tel, '38') === 0) {
            $tel = substr($tel, 2);
        }

        $user = User::create([
            'name' => $data->get('name'),
            'phone' => $tel,
            'email' => $data->get('email'),
            'password' => bcrypt($data->get('password')),
            'role_id' => UserRole::CLIENT_ID
        ]);

        $client = $user->client()->create(['mailing' => $data->get('mailing', false)]);

        return $client;
    }
}