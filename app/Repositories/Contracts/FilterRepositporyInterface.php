<?php

namespace App\Repositories\Contracts;

interface FilterRepositoryInterface
{
    public function validateUrlFilter($raw_filter);

    public function decodeUrlFilter($raw_filter);
}