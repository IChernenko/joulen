<?php

namespace App\Repositories\Contracts;
use Illuminate\Http\Request;

interface OrderRepositoryInterface extends FilterRepositoryInterface
{    
    public function updateMeta(Request $request, $id);
}