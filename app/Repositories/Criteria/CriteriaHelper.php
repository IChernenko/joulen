<?php
/**
 * Created by PhpStorm.
 * User: Dancer
 * Date: 15.06.2017
 * Time: 18:52
 */

namespace App\Repositories\Criteria;


class CriteriaHelper
{
    public static function isAttribute($value, $attribute = null)
    {
        return is_numeric($value) && $value > 0 && $attribute != 'page';
    }
}