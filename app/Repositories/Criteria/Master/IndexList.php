<?php 

namespace App\Repositories\Criteria\Master;

use App\Models\MasterStatus;
use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\DB;

class IndexList extends Criteria {
    
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with('region')
            ->with('user')
            ->whereHas('user', function ($query){
                /**
                 * Filters masters with name 'undefined'.
                 * It is needed to hide masters that aren't fully registered
                 * But it can't be checked with user role = 'temp_master'
                 * Cuz in database there are masters that has role 'master' but undefined names.
                 */
                $query->where(['blocked' => false, 'deleted' => false, ['name', '!=', 'undefined'] ]);
            })
            ->whereHas('status', function ($query) {
                $query->whereIn('slug', MasterStatus::$validStatuses);
            })
            ->withCount([
                'reviews',
                'negative_reviews',
                'orders_in_progress',
                'order_requests as requests_considering' => function($request_query){
                    $request_query->whereHas('order', function($order_query){
                        $order_query->whereHas('status', function($status_query){
                            $status_query->where('slug', 'considering');
                        });
                    });
                },
            ])
            ->leftJoin(DB::raw('(SELECT
                master_id,
                SUM(amount) AS sum
            FROM
                payments
            LEFT OUTER JOIN payment_statuses ON payments.status_id = payment_statuses.id
            WHERE payment_statuses.name = \'success\' OR payment_statuses.name = \'manual\'
            GROUP BY
                master_id) AS payments'), 'payments.master_id', '=', 'masters.id')
            ->orderBy('payments.sum', 'desc')
            ->orderBy('requests_considering_count', 'desc');

        return $query;
    }
}