<?php 

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class AdminList extends Criteria {
    
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with('status', 'subcategory.category', 'orderable', 'master.user')
            ->withCount(['requests' => function ($query) {
                $query->whereHas('master.user', function ($query){
                    $query->where(['deleted' => false]);
                });
            }])
            ->orderBy('created_at', 'desc');
        
        return $query;
    }
}