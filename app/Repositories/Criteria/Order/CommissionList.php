<?php

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class CommissionList extends Criteria
{

    protected $master_id;

    public function __construct($master_id)
    {
        if (is_null($master_id))
            throw new InvalidArgumentException('master_id must be set');

        $this->master_id = $master_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
//        TODO: use whereHas status
        $masterId = $this->master_id;

        $query = $model->with([
            'request' => function ($request) use ($masterId) {
                $request->ofMaster($masterId);
            },
            'payment' => function ($payment) use ($masterId) {
                $payment->ofMaster($masterId);
            },
            'status'
        ])->whereHas('status', function ($query) {
            $query->where('slug', 'in_progress')->orWhere('slug', 'done');
        })->where('master_id', $this->master_id);

        return $query;
    }
}