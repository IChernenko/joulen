<?php

namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class RelationAttribute extends Criteria {

    protected $attribute;
    protected $value;
    protected $relation;
    protected $condition;

    public function __construct($relation, $attribute, $value, $condition = '=')
    {
        if(is_null($relation) || is_null($attribute) || is_null($value))
            throw new InvalidArgumentException('relation, attribute and value must be set');

        $this->attribute = $attribute;
        $this->value = $value;
        $this->relation = $relation;
        $this->condition = $condition;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with($this->relation)->whereHas($this->relation, function ($query){
            $query->where($this->attribute, $this->condition, $this->value);
        });

        return $query;
    }
}