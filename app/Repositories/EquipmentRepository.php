<?php

namespace App\Repositories;

use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Equipment\Equipment;
use Illuminate\Container\Container as App;
use Illuminate\Support\Collection;

class EquipmentRepository extends BaseRepository
{
    protected $attachmentRepository;

    public function __construct(App $app, Collection $collection, AttachmentRepository $attachmentRepository)
    {
        parent::__construct($app, $collection);
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Equipment::class;
    }

    public function storeImage($request, $brand)
    {
        if ($request->hasFile('equipment_logo')) {
            if ($brand->logo)
                $brand->logo->delete();
            $image = $request->file('equipment_logo');

            $attachment = $this->attachmentRepository->saveAttachment($brand, 'equipment', AttachmentType::where('name', AttachmentType::TYPE_EQUIPMENT_LOGO)->first()->id, $image, true);
            $this->attachmentRepository->resizePhoto($attachment, null, Attachment::EQUIPMENT_LOGO_HEIGHT);
        }
    }

}