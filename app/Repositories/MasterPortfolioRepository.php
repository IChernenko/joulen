<?php

namespace App\Repositories;

use App\Models\Equipment\Equipment;
use App\Models\MasterPortfolio;

class MasterPortfolioRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return MasterPortfolio::class;
    }

}