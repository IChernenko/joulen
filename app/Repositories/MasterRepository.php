<?php

namespace App\Repositories;

use App\EmployeeType;
use App\Events\Master\MasterReRegistered;
use App\Events\Master\Registered as MasterRegistered;
use App\Http\Requests\Master\ProfileRequest;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Employee;
use App\Models\Master;
use App\Models\MasterOffice;
use App\Models\MasterPortfolio;
use App\Models\MasterStatus;
use App\Models\OrderCategory;
use App\Models\OrderSubcategory;
use App\Models\Region;
use App\Models\User\Master\Status;
use App\Models\UserRole;
use App\Repositories\Contracts\MasterRepositoryInterface;
use \Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as ImageResize;
use Validator;
use File;
use Image;

class MasterRepository extends BaseRepository implements MasterRepositoryInterface
{

    protected $attachmentRepository;
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Master';
    }

    public function __construct(App $app, Collection $collection, AttachmentRepository $attachmentRepository)
    {
        parent::__construct($app, $collection);
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * Checks whether url filter correct or not.
     *
     * @param $raw_filter
     * @return bool
     */
    public function validateUrlFilter($raw_filter)
    {
        if (empty($raw_filter))
            return true;

        $verified = 'verified';

        $filter = explode('/', $raw_filter);

        if (!isset($this->category_slugs))
            $this->category_slugs = ['home_ses', 'commercial_ses'];

        if (!isset($this->region_slugs))
            $this->region_slugs = Region::all()->pluck('slug')->all();

        if (count($filter) == 1) {
            if (in_array($filter[0], $this->region_slugs) || in_array($filter[0], $this->category_slugs) || $filter[0] == $verified)
                return true;
        } elseif (count($filter) == 2) {
            if ((in_array($filter[0], $this->region_slugs) && in_array($filter[1], $this->category_slugs)) ||
                (in_array($filter[0], $this->region_slugs) && $filter[1] == $verified) ||
                (in_array($filter[0], $this->category_slugs) && $filter[1] == $verified))
                return true;
        } elseif (count($filter) == 3) {
            if ((in_array($filter[0], $this->region_slugs) && in_array($filter[1], $this->category_slugs) && $filter[2] == $verified))
                return true;
        }

        return false;
    }

    /**
     * Decodes raw url filter to array.
     *
     * @param $raw_filter
     * @return array
     */
    public function decodeUrlFilter($raw_filter)
    {
        $filter = explode('/', $raw_filter);

        $verified = 'verified';

        if (!isset($this->category_slugs))
            $this->category_slugs = ['home_ses', 'commercial_ses'];

        if (!isset($this->region_slugs))
            $this->region_slugs = Region::all()->pluck('slug')->all();

        if (count($filter) == 1) {
            if (in_array($filter[0], $this->region_slugs)) {
                $filter = ['region_slug' => $filter[0]];
            } elseif (in_array($filter[0], $this->category_slugs)) {
                $filter = ['subcategory' => $filter[0]];
            } elseif ($filter[0] == $verified) {
                $filter = ['verified_masters' => $filter[0]];
            }
        } elseif (count($filter) == 2) {
            if (in_array($filter[0], $this->region_slugs) && in_array($filter[1], $this->category_slugs) ) {
                $filter = ['region_slug' => $filter[0], 'subcategory' => $filter[1]];
            } elseif (in_array($filter[0], $this->category_slugs) && $filter[1] == $verified) {
                $filter = ['subcategory' => $filter[0], 'verified_masters' => $filter[1]];
            } elseif (in_array($filter[0], $this->region_slugs) && $filter[1] == $verified) {
                $filter = ['region_slug' => $filter[0], 'verified_masters' => $filter[1]];
            }
        } elseif (count($filter) == 3) {
            $filter = ['region_slug' => $filter[0], 'subcategory' => $filter[1], 'verified_masters' => $filter[2]];
        } else {
            $filter = [];
        }

        return $filter;
    }

    public function createFromRequest(ProfileRequest $request)
    {
        $user_id = Auth::user()->id;
        $master = Master::firstOrCreate(['user_id' => $user_id]);

        $role = UserRole::where('name', 'master')->first();
        if ($role) {
            $master->user->role_id = $role->id;
            $master->user->name = $request->input('company_name');
            $master->user->save();
            Auth::login($master->user);
        } else {
            return redirect('master created but role cant be changed', 401);
        }

        $this->updateProfileStageOne($request, $master);


    }

    public function portfolioDeleteFiles($portfolioId) {
        $result = File::deleteDirectory();
    }

    public function updateProfile(ProfileRequest $request, Master $master)
    {
        switch ($request->profile_stage) {
            case 2 : $this->updateProfileStageTwo($request, $master); break;
            case 3 : $this->updateProfileStageThree($request, $master); break;
            case 4 : $this->updateProfileStageFour($request, $master); break;
            case 5 : $this->updateProfileStageFive($request, $master); break;
            case 'all' : $this->updateAllProfile($request, $master); break;
        }
    }

    protected function updateProfileStageOne(ProfileRequest $request, Master $master)
    {
        if ($request->profile_stage != 'all')
            $master->profile_stage = $request->input('profile_stage', 0);

        $master->user->name = $request->input('company_name');
        $master->about = $request->input('about');
        $master->user->save();
        $master->save();
        if ($request->hasFile('master_avatar')) {
            if ($master->avatar)
                $master->avatar->delete();
            $image = $request->file('master_avatar');
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->attachment_type_id = AttachmentType::where('name', 'master_photo')->first()->id;
            $attachment->saveFromPost($image);
            $attachment->public = true;
            $attachment->save();

            $path = storage_path('app/public'.$attachment->getPartitionDirectory().Attachment::SMALL_PREFIX.$attachment->disk_name);
            $img = ImageResize::make($attachment->getPath());
            $img->resize(null, Attachment::MASTER_LOGO_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($path);
        }
    }

    protected function updateProfileStageTwo(ProfileRequest $request, Master $master)
    {
        $regions = Region::all()->pluck('name', 'id')->toArray();
        $main_region_id = Region::where('slug', 'kyiv')->first()->id;
        $second_region_id = Region::where('slug', 'kyiv')->first()->id;
        $third_region_id = Region::where('slug', 'kyiv')->first()->id;

        foreach ($regions as $region_id => $region) {
            $region_to_compare = explode(' ', $region)[0];
            similar_text($region_to_compare, $request->input('main_office_region'), $percentage);
            if ($percentage > 75)
                $main_region_id = $region_id;
            similar_text($region_to_compare, $request->input('second_office_region'), $percentage);
            if ($percentage > 75)
                $second_region_id = $region_id;
            similar_text($region_to_compare, $request->input('third_office_region'), $percentage);
            if ($percentage > 75)
                $third_region_id = $region_id;
        }

        if ($request->profile_stage != 'all')
            $master->profile_stage = $request->input('profile_stage', 0);

        $master->work_email = $request->input('work_email');
        $master->website = $request->input('website');
        $work_phone = str_replace(array('+', ' ', '(' , ')', '-'), '', $request->input('work_phone'));
        if(strpos($work_phone, '38') === 0) {
            $work_phone = substr($work_phone, 2);
        }
        $master->work_phone = $work_phone;
        $second_work_phone = str_replace(array('+', ' ', '(' , ')', '-'), '', $request->input('second_work_phone'));
        if(strpos($second_work_phone, '38') === 0) {
            $second_work_phone = substr($second_work_phone, 2);
        }
        $master->second_work_phone = $second_work_phone;
        $master->save();

        if ($request->input('main_office_added')) {
            $main_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 1
            ], [
                'lat' => $request->input('main_office_lat'),
                'lng' => $request->input('main_office_lng'),
                'address' => $request->input('main_office_address'),
                'region_id' => $main_region_id,
                'room' => $request->input('main_office_room'),
            ]);
            if ($request->hasFile('main_office_photo')) {
                if ($main_office->picture || $request->input('is_deleted_main_office_photo'))
                    $main_office->picture->delete();
                $image = $request->file('main_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $main_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->main_office->delete();
        }


        if ($request->input('second_office_added')) {
            $second_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 2
            ], [
                'lat' => $request->input('second_office_lat'),
                'lng' => $request->input('second_office_lng'),
                'address' => $request->input('second_office_address'),
                'region_id' => $second_region_id,
                'room' => $request->input('second_office_room'),
            ]);
            if ($request->hasFile('second_office_photo')) {
                if ($second_office->picture || $request->input('is_deleted_second_office_photo'))
                    $second_office->picture->delete();
                $image = $request->file('second_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $second_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->second_office->delete();
        }

        if ($request->input('third_office_added')) {
            $third_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 3
            ], [
                'lat' => $request->input('third_office_lat'),
                'lng' => $request->input('third_office_lng'),
                'address' => $request->input('third_office_address'),
                'region_id' => $third_region_id,
                'room' => $request->input('third_office_room'),
            ]);

            if ($request->hasFile('third_office_photo')) {
                if ($third_office->picture || $request->input('is_deleted_third_office_photo'))
                    $third_office->picture->delete();
                $image = $request->file('third_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $third_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->third_office->delete();
        }
    }

    protected function updateProfileStageThree(ProfileRequest $request, Master $master)
    {
        if ($request->profile_stage != 'all')
            $master->profile_stage = $request->input('profile_stage', 0);

        $master->edrpou = $request->input('edrpou');
        $master->ipn = $request->input('ipn');
        $master->save();

        $manager = Employee::updateOrCreate([
            'type_id' => EmployeeType::where('slug', 'manager')->first()->id,
            'user_id' => $master->user->id,
        ], [
            'name' => $request->input('manager_name'),
            'phone' => $request->input('manager_phone'),
            'mailing' => $request->input('manager_mailing', false),
            'email' => $request->input('manager_email'),
        ]);

        $owner = Employee::updateOrCreate([
            'type_id' => EmployeeType::where('slug', 'owner')->first()->id,
            'user_id' => $master->user->id,
        ], [
            'email' => $request->input('owner_email'),
            'name' => $request->input('owner_name'),
            'phone' => $request->input('owner_phone'),
            'mailing' => $request->input('owner_mailing', false),
        ]);
    }

    protected function updateProfileStageFour(ProfileRequest $request, Master $master)
    {
        if ($request->profile_stage != 'all')
            $master->profile_stage = $request->input('profile_stage', 0);

        $master->car = $request->input('car', false);
        $master->project_department = $request->input('project_department', 0);
        $master->construction_machinery = $request->input('machinery', false);
        $master->exchange_fund_bool = $request->input('exchange_fund', false);
        $master->exchange_fund = $request->input('exchange_fund_text');
        $master->eco_energy = $request->input('eco_energy', false);
        $master->year_started = $request->input('year_started');
        $master->save();

        $ses_category_id = OrderCategory::where('slug', 'ses')->first()->id;
        $ses_home_subcategories = OrderSubcategory::where(['home' => true, 'category_id' => $ses_category_id])->get()->pluck('id')->toArray();
        $ses_commercial_categories = OrderSubcategory::where(['home' => false, 'category_id' => $ses_category_id])->get()->pluck('id')->toArray();

        if ($request->input('home_ses')) {
            $master->subcategories()->syncWithoutDetaching($ses_home_subcategories);
        } else {
            $master->subcategories()->detach($ses_home_subcategories);
        }

        if ($request->input('commercial_ses')) {
            $master->subcategories()->syncWithoutDetaching($ses_commercial_categories);
        } else {
            $master->subcategories()->detach($ses_commercial_categories);
        }

        $work_locations = $request->input('work_location') ?? [];
        $master->regions()->detach();
        $master->regions()->sync($work_locations);
        $favorite_work_locations = array_filter($request->input('favorite_work_location')) ?? [];
        $favorite = array_fill(0, count($favorite_work_locations), ['favorite' => true]);
        $favorite_work_locations = array_combine($favorite_work_locations, $favorite);
        $master->regions()->syncWithoutDetaching($favorite_work_locations);


        $car_photo = $request->file('car_photo');

        if ($car_photo) {
            if ($master->car_photo)
                $master->car_photo->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->public = true;
            $attachment->attachment_type_id = AttachmentType::where('name', 'car_photo')->first()->id;
            $attachment->saveFromPost($car_photo);
            $attachment->save();
        }

        $machinery_photo = $request->file('machinery_photo');

        if ($machinery_photo) {
            if ($master->machinery_photo)
                $master->machinery_photo->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->public = true;
            $attachment->attachment_type_id = AttachmentType::where('name', 'machinery_photo')->first()->id;
            $attachment->saveFromPost($machinery_photo);
            $attachment->save();
        }

        $contract = $request->file('contract');

        if ($contract) {
            if ($master->contract)
                $master->contract->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->attachment_type_id = AttachmentType::where('name', 'contract')->first()->id;
            $attachment->saveFromPost($contract);
            $attachment->public = true;
            $attachment->save();
        }
    }

    protected function updateProfileStageFive(ProfileRequest $request, Master $master)
    {
        if ($request->profile_stage != 'all')
            $master->profile_stage = $request->input('profile_stage', 0);

        $canceled_status_id = MasterStatus::where('slug', 'canceled')->first()->id;

        if(Auth::user()->master) {
            if ($master->status_id == $canceled_status_id) {
                event(new MasterReRegistered($master));
            } else {
                event(new MasterRegistered($master));
            }
        }
        $wait_activation_status_id = MasterStatus::where('slug', 'wait_activation')->first()->id;
        $master->status_id = $wait_activation_status_id;

        $master->save();
    }

    public function portfolioStore(Request $request, $master)
    {
        $regions = Region::all()->pluck('name', 'id')->toArray();
        $portfolio_region_id = Region::where('slug', 'kyiv')->first()->id;

        foreach ($regions as $region_id => $region) {
            $region_to_compare = explode(' ', $region)[0];
            similar_text($region_to_compare, $request->input('portfolio_region'), $percentage);
            if ($percentage > 74)
                $portfolio_region_id = $region_id;
        }
        $power = (float) (str_replace(',', '.', $request->get('portfolio_power')));
        $status_id = Status::where('slug', 'checking')->first()->id;

        $portfolio = MasterPortfolio::create([
            'month' => $request->get('portfolio_month'),
            'year' => $request->get('portfolio_year'),
            'address' => $request->get('portfolio_address'),
            'lat' => $request->get('portfolio_lat'),
            'lng' => $request->get('portfolio_lng'),
            'power' => $power,
            'subcategory_id' => $request->input('portfolio_subcategory_id', OrderSubcategory::where('slug','commercial')->first()->id),
            'invertor_id' => $request->get('portfolio_invertor_id'),
            'panel_id' => $request->get('portfolio_panel_id'),
            'general_contractor' => $request->get('portfolio_contractor'),
            'designing' => $request->get('portfolio_designing'),
            'installation' => $request->get('portfolio_installation'),
            'delivery' => $request->get('portfolio_delivery'),
            'documentation' => $request->get('portfolio_documentation'),
            'region_id' => $portfolio_region_id,
            'master_id' => $master->id,
            'ground' => $request->input('portfolio_subcategory_type_id'),
            'status_id' => $status_id
        ]);

        $invertor_photo = $request->file('invertor_photo');

        if ($portfolio->invertor_photo)
            $portfolio->invertor_photo->delete();
        $attachment_type_id = AttachmentType::where('name', 'invertor_photo')->first()->id;
        $attachment = $this->attachmentRepository->saveAttachment($portfolio, 'master_portfolio', $attachment_type_id, $invertor_photo, $request->input('public_invertor_photo', false));
        $this->attachmentRepository->resizePhoto($attachment, Attachment::PORTFOLIO_PHOTO_WIDTH, Attachment::PORTFOLIO_PHOTO_HEIGHT);

        $panel_photo = $request->file('panel_photo');

        if ($portfolio->panel_photo)
            $portfolio->panel_photo->delete();
        $attachment_type_id = AttachmentType::where('name', 'panel_photo')->first()->id;
        $attachment = $this->attachmentRepository->saveAttachment($portfolio, 'master_portfolio', $attachment_type_id, $panel_photo, $request->input('public_panel_photo', false));
        $this->attachmentRepository->resizePhoto($attachment, Attachment::PORTFOLIO_PHOTO_WIDTH, Attachment::PORTFOLIO_PHOTO_HEIGHT);

        return $portfolio;
    }

    public function portfolioUpdate(Request $request, $master)
    {
        $portfolio = MasterPortfolio::find($request->get('portfolio_id'));
        $power = (float) (str_replace(',', '.', $request->get('portfolio_power')));
        $portfolio->fill([
            'month' => $request->get('portfolio_month'),
            'year' => $request->get('portfolio_year'),
            'address' => $request->get('portfolio_address'),
            'lat' => $request->get('portfolio_lat'),
            'lng' => $request->get('portfolio_lng'),
            'power' => $power,
            'subcategory_id' => $request->input('portfolio_subcategory_id', OrderSubcategory::where('slug','commercial')->first()->id),
            'invertor_id' => $request->get('portfolio_invertor_id'),
            'panel_id' => $request->get('portfolio_panel_id'),
            'general_contractor' => $request->get('portfolio_contractor'),
            'designing' => $request->get('portfolio_designing'),
            'installation' => $request->get('portfolio_installation'),
            'delivery' => $request->get('portfolio_delivery'),
            'documentation' => $request->get('portfolio_documentation'),
            'master_id' => $master->id,
            'ground' => $request->input('portfolio_subcategory_type_id')
        ])->save();

        $invertor_photo = $request->file('invertor_photo');

        if ($invertor_photo) {
            if ($portfolio->invertor_photo)
                $portfolio->invertor_photo->delete();
            $attachment_type_id = AttachmentType::where('name', 'invertor_photo')->first()->id;
            $attachment = $this->attachmentRepository->saveAttachment($portfolio, 'master_portfolio', $attachment_type_id, $invertor_photo, $request->input('public_invertor_photo', false));
            $this->attachmentRepository->resizePhoto($attachment, Attachment::PORTFOLIO_PHOTO_WIDTH, Attachment::PORTFOLIO_PHOTO_HEIGHT);
        }
        else {
            $portfolio->invertor_photo->public = $request->input('public_invertor_photo', false);
            $portfolio->invertor_photo->save();
        }

        $panel_photo = $request->file('panel_photo');

        if ($panel_photo) {
            if ($portfolio->panel_photo)
                $portfolio->panel_photo->delete();
            $attachment_type_id = AttachmentType::where('name', 'panel_photo')->first()->id;
            $attachment = $this->attachmentRepository->saveAttachment($portfolio, 'master_portfolio', $attachment_type_id, $panel_photo, $request->input('public_panel_photo', false));
            $this->attachmentRepository->resizePhoto($attachment, Attachment::PORTFOLIO_PHOTO_WIDTH, Attachment::PORTFOLIO_PHOTO_HEIGHT);
        }
        else {
            $portfolio->panel_photo->public = $request->input('public_panel_photo', false);
            $portfolio->panel_photo->save();
        }

        return $portfolio;
    }

    public function updateAllProfile(ProfileRequest $request, $master)
    {
        $this->updateProfileStageOne($request, $master);
        $this->updateProfileStageTwo($request, $master);
        $this->updateProfileStageThree($request, $master);
        $this->updateProfileStageFour($request, $master);
        $this->updateProfileStageFive($request, $master);

        $status_id = $request->input('company_status');

        if ($status_id == MasterStatus::where("slug", 'canceled')->first()->id || $status_id == MasterStatus::where("slug", 'inactive')->first()->id)
            $master->profile_stage = 0;

        $master->status_id = $status_id;
        $master->save();

        return $master;
    }
}