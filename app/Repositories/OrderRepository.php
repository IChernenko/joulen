<?php

namespace App\Repositories;

use App\Events\Admin\OrderRejected;
use App\Events\ClientProjectPublished;
use App\Events\MasterOrderInProgress;
use App\Events\MasterOrderStatusСonsidering;
use App\Models\Client;
use App\Models\Master;
use App\Models\Order;
use App\Models\OrderCategory;
use App\Models\OrderEnergyAudit;
use App\Models\OrderRequestPriceType;
use App\Models\OrderSes;
use App\Models\OrderStatus;
use App\Models\OrderSubcategory;
use App\Models\OrderWasteSorting;
use App\Models\Region;
use App\Models\User;
use App\Repositories\Contracts\OrderRepositoryInterface;
use Carbon\Carbon;
use \Illuminate\Http\Request;
use Illuminate\Support\Collection;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Order';
    }

    /**
     * Checks whether url filter correct or not.
     *
     * @param $raw_filter
     * @return bool
     */
    public function validateUrlFilter($raw_filter)
    {
        if (empty($raw_filter))
            return true;

        $filter = explode('/', $raw_filter);

        if (!isset($this->category_slugs))
            $this->category_slugs = OrderCategory::all()->pluck('slug')->all();

        if (!isset($this->subcategory_slugs))
            $this->subcategory_slugs = OrderSubcategory::all()->pluck('slug')->all();

        $valid_values = array_merge($this->category_slugs, $this->subcategory_slugs, ['region_filter_on', 'home']);

        foreach ($filter as $value){
            if (!in_array($value, $valid_values)){
                return false;
            }
        }

        return true;
    }

    /**
     * Decodes raw url filter to array.
     *
     * @param $raw_filter
     * @return array
     */
    public function decodeUrlFilter($raw_filter)
    {
        $raw_filter = explode('/', $raw_filter);
        $filter = [];

        if (!isset($this->category_slugs))
            $this->category_slugs = OrderCategory::all()->pluck('slug')->all();

        if (!isset($this->subcategory_slugs))
            $this->subcategory_slugs = OrderSubcategory::all()->pluck('slug')->all();

        foreach ($raw_filter as $value){
            if(in_array($value, $this->category_slugs)){
                $filter['category'] = $value;
            }
            if(in_array($value, $this->subcategory_slugs)){
                $filter['subcategory'] = $value;
            }
            if($value == 'region_filter_on'){
                $filter['region_filter_on'] = true;
            }
            if($value == 'home'){
                $filter['home'] = true;
            }
        }

        return $filter;
    }

    /**
     * Store newly created order.
     *
     * @param array $data
     * @return \App\Models\Order
     */
    public function create(array $data)
    {
//        TODO: remove price field from db
        $data = new Collection($data);

//        TODO: rename fields to name_id if it is id
        if(!empty($data->get('region_name'))) {
            $regions = Region::all()->pluck('name', 'id')->toArray();
            $region_id = Region::where('slug', 'kyiv')->first()->id;

            foreach ($regions as $id => $region) {
                $region_to_compare = explode(' ', $region)[0];
                similar_text($region_to_compare, $data->get('region_name'), $percentage);
                if ($percentage > 79) {
                    $region_id = $id;
                }
            }
        }
        else {
            $region_id = $data->get('region_id');
        }
//        dd($region_id);
        if(!empty($data->get('city_name'))) {
            $city = $data->get('city_name');
        }
        else {
            $city = $data->get('city');
        }
//        DB::transaction(function () use ($data) {
        $order = $this->model->create([
            'client_id' => $data->get('client_id'),
            'region_id' => $region_id,
            'status_id' => 1,
            'subcategory_id' => $data->get('subcategory'),
            'city' => $city,
            'price' => 0.00,
            'client_comment' => $data->get('client_comment'),
            'paid' => false,
            'ground' => $data->get('ground', null),
            'show_phone' => $data->get('show_phone', false)
        ]);

        if ($data->get('category') == 'ses') {

            $order_ses = OrderSes::create([
                'order_id' => $order->id,
                'price_category_id' => $data->get('ses_price_category'),
                'photocell_power' => $data->get('photocell_power')
                    ? $data->get('photocell_power')
                    : $data->get('installOnly_power'),
                'battery_capacity' => $data->get('battery_capacity'),
                'required_area' => $data->get('required_area'),
                'area' => $data->get('area'),
                'area_unit' => $data->get('area_unit'),
                'parts_comment' => $data->get('parts_comment'),
                'lat' => $data->get('lat'),
                'lng' => $data->get('lng'),
                'zoom' => $data->get('zoom'),
                'house_comment' => $data->get('house_comment'),
                'green_tariff' => $data->get('green_tariff', false)
            ]);

            $order->orderable_id = $order_ses->id;
            $order->orderable_type = $data->get('category');

            $order->price_types()->sync($data->get('price_types'));


        } elseif ($data->get('category') == 'energy_audit') {

            $order_energy_audit = OrderEnergyAudit::create([
                'order_id' => $order->id,
                'heating_type_id' => $data->get('heating_type'),
                'floors' => $data->get('floors'),
                'area' => $data->get('area'),
                'area_unit' => $data->get('area_unit'),
                'exploitation' => $data->get('exploitation'),
                'residents' => $data->get('residents'),
                'electricity_per_year' => $data->get('electricity_per_year'),
                'water_per_year' => $data->get('water_per_year'),
                'gas_per_year' => $data->get('gas_per_year'),
                'house_comment' => $data->get('house_comment'),
                'high_costs' => $data->get('high_costs'),
                'cold_winter' => $data->get('cold_winter'),
                'hot_summer' => $data->get('hot_summer'),
                'mold' => $data->get('mold'),
                'draft' => $data->get('draft'),
                'windows_fogging' => $data->get('windows_fogging'),
                'disbalance' => $data->get('disbalance'),
                'blackout' => $data->get('blackout'),
                'provider_problems' => $data->get('provider_problems'),
                'other_problems' => $data->get('other_problems'),
                'thermal_examination' => $data->get('thermal_examination'),
                'blower_door_test' => $data->get('blower_door_test'),
                'energy_passport' => $data->get('energy_passport'),
                'detailed_calculation' => $data->get('detailed_calculation'),
                'technical_supervision' => $data->get('technical_supervision'),
            ]);

            $order->orderable_id = $order_energy_audit->id;
            $order->price_types()->attach(OrderRequestPriceType::where('slug', 'energy_audit')->first()->id);
            $order->orderable_type = $data->get('category');

        } elseif ($data->get('category') == 'waste_sorting') {

            $order_waste_sorting = OrderWasteSorting::create([
                'order_id' => $order->id,
                'size_id' => $data->get('size_id'),
                'size_comment' => $data->get('size_comment'),
                'about_comment' => $data->get('about_comment'),
                'pm_help' => $data->get('pm_help', false),
            ]);

            $order->orderable_id = $order_waste_sorting->id;
            $order->orderable_type = $data->get('category');

        }

        $order->save();

//        });

        return $order;
    }

    /**
     * Update order.
     *
     * @param array $data
     * @param \App\Models\Order $order
     * @return \App\Models\Order
     */
    public function update(array $data, $order)
    {
        $data = new Collection($data);

        $regions = Region::all()->pluck('id')->toArray();
//        DB::transaction(function () use ($data) {
        if(!empty($data->get('region_name')) && !in_array($data->get('region_name'), $regions)) {
            $regions = Region::all()->pluck('name', 'id')->toArray();
            $region_id = Region::where('slug', 'kyiv')->first()->id;

            foreach ($regions as $id => $region) {
                $region_to_compare = explode(' ', $region)[0];
                similar_text($region_to_compare, $data->get('region_name'), $percentage);
                if ($percentage > 80) {
                    $region_id = $id;
                }
            }
        }
        else {
            $region_id = $data->get('region_id');
        }

        if(!empty($data->get('city_name'))) {
            $city = $data->get('city_name');
        }
        else {
            $city = $data->get('city');
        }
        $order->region_id = $region_id;
        $order->city = $city;
        $order->client_comment = $data->get('client_comment');
        $order->ground = $data->get('ground', null);
        $order->show_phone = $data->get('show_phone', false);

        if ($data->get('category') == 'ses') {

            $order->orderable->price_category_id = $data->get('ses_price_category');
            $order->orderable->photocell_power = $data->get('photocell_power')
                ? $data->get('photocell_power')
                : $data->get('installOnly_power');
            $order->orderable->battery_capacity = $data->get('battery_capacity');
            $order->orderable->required_area = $data->get('required_area');
            $order->orderable->area = $data->get('area');
            $order->orderable->area_unit = $data->get('area_unit', 'm');
            $order->orderable->parts_comment = $data->get('parts_comment');
            $order->orderable->lat = $data->get('lat');
            $order->orderable->lng = $data->get('lng');
            $order->orderable->zoom = $data->get('zoom');
            $order->orderable->house_comment = $data->get('house_comment');
            $order->orderable->green_tariff = $data->get('green_tariff');
            $order->price_types()->sync($data->get('price_types'));

        } elseif ($data->get('category') == 'energy_audit') {

            $order->orderable->heating_type_id = $data->get('heating_type');
            $order->orderable->floors = $data->get('floors');
            $order->orderable->area = $data->get('area');
            $order->orderable->exploitation = $data->get('exploitation');
            $order->orderable->residents = $data->get('residents');
            $order->orderable->electricity_per_year = $data->get('electricity_per_year');
            $order->orderable->water_per_year = $data->get('water_per_year');
            $order->orderable->gas_per_year = $data->get('gas_per_year');
            $order->orderable->house_comment = $data->get('house_comment');
            $order->orderable->high_costs = $data->get('high_costs');
            $order->orderable->cold_winter = $data->get('cold_winter');
            $order->orderable->hot_summer = $data->get('hot_summer');
            $order->orderable->mold = $data->get('mold');
            $order->orderable->draft = $data->get('draft');
            $order->orderable->windows_fogging = $data->get('windows_fogging');
            $order->orderable->disbalance = $data->get('disbalance');
            $order->orderable->blackout = $data->get('blackout');
            $order->orderable->provider_problems = $data->get('provider_problems');
            $order->orderable->other_problems = $data->get('other_problems');
            $order->orderable->thermal_examination = $data->get('thermal_examination');
            $order->orderable->blower_door_test = $data->get('blower_door_test');
            $order->orderable->energy_passport = $data->get('energy_passport');
            $order->orderable->detailed_calculation = $data->get('detailed_calculation');
            $order->orderable->technical_supervision = $data->get('technical_supervision');

        } elseif ($data->get('category') == 'waste_sorting') {

            $order->orderable->size_id = $data->get('size_id');
            $order->orderable->size_comment = $data->get('size_comment');
            $order->orderable->about_comment = $data->get('about_comment');
            $order->orderable->pm_help = $data->get('pm_help', false);

        }

        $order->push();

//        });

        return $order;
    }

    /**
     * Update the specified order's meta information (master_id, status, manager's comment, cancelation comment, commission) in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Order
     */
    public function updateMeta(Request $request, $id)
    {
        $order = $this->model->findOrFail($id);

        if (OrderStatus::find($request->input('status_id'))->slug == 'rejected' && $order->status->slug != 'rejected') {
            event(new OrderRejected(Client::find($order->client_id)->user, $order));
        }

        //TODO: resolve zero and null in master_id
        $order->master_id = $request->master_id == 0 ? null : $request->master_id;
        $order->paid = $request->input('paid', 0);
        $order->show_phone = $request->input('show_phone', false);

        if (OrderStatus::find($request->input('status_id'))->slug == 'in_progress' && $order->status->slug != 'in_progress'
            && ($order->status->slug = 'considering' || $order->status->slug = 'closed')) {
            event(new MasterOrderInProgress($order->master, $order));
        }
        
        $order->status_id = $request->input('status_id');
        $order->manager_comment = $request->input('manager_comment', '');

//        TODO: don't rely on status_id. 
        if ($request->input('status_id') == 2)
            $order->cancellation_comment = $request->input('cancellation_comment', '');

        $order->save();

        return $order;
    }

}