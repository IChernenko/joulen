<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 16.06.17
 * Time: 20:30
 */

namespace App\Repositories;


use App\Models\OrderRequest;
use Illuminate\Database\Eloquent\Collection;

class OrderRequestRepository extends BaseRepository
{
    public function model()
    {
        return OrderRequest::class;
    }

    /**
     * Updates order request.
     *
     * @param array $data
     * @param \App\Models\OrderRequest $order_request
     * @return \App\Models\OrderRequest
     */
    public function update(array $data, $order_request)
    {
        $data = new Collection($data);

        $order_request->final = $data->get('final', 0);
        $order_request->save();

        foreach ($order_request->prices as $price){
            if(isset($data->get('prices')[$price->type->id]))
                $price->price = $data->get('prices')[$price->type->id];

            if(isset($data->get('comments')[$price->type->id]))
                $price->comment = $data->get('comments')[$price->type->id];

            $price->save();
        }
    }

}