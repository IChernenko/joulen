<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 16.06.17
 * Time: 20:30
 */

namespace App\Repositories;


use App\Models\Payment\Payment;

class PaymentRepository extends BaseRepository
{
    public function model()
    {
        return Payment::class;
    }

}