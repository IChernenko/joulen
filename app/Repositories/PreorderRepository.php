<?php

namespace App\Repositories;

use App\Events\Client\PreorderCreated;
use App\Models\PreorderCategory;
use App\Models\PreorderWasteAudit;
use Illuminate\Support\Collection;

class PreorderRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Preorder';
    }

    /**
     * Store newly created order.
     *
     * @param array $data
     * @return \App\Models\Preorder
     */
    public function create(array $data)
    {
        $data = new Collection($data);

//        DB::transaction(function () use ($data) {
        $preorder = $this->model->create([
            'subcategory_id' => $data->get('subcategory_id'),
        ]);

        $category = PreorderCategory::whereHas('subcategories', function ($subcategory_query) use ($data) {
            $subcategory_query->where('id', $data->get('subcategory_id'));
        })->first();

        if($category->slug == 'waste_audit'){
            $preorderable = PreorderWasteAudit::create([
                'preorder_id' => $preorder->id,
                'name' => $data->get('name'),
                'phone' => $data->get('phone'),
                'email' => $data->get('email'),
                'comment' => $data->get('comment'),
            ]);

            $preorderable->preorder()->save($preorder);
        }
        
        event(new PreorderCreated($preorder));

//        });

        return $preorder;
    }

    /**
     * Update preorder.
     *
     * @param array $data
     * @param \App\Models\Preorder $preorder
     * @return \App\Models\Preorder
     */
    public function update(array $data, $preorder)
    {
        $data = new Collection($data);

//        DB::transaction(function () use ($data) {
        
        $preorder->preorderable->comment = $data['comment'];

        $preorder->push();

//        });

        return $preorder;
    }
}