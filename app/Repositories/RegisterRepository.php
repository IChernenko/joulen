<?php
/**
 * Created by PhpStorm.
 * User: Виктория
 * Date: 23.11.2017
 * Time: 12:21
 */

namespace App\Repositories;


use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileType;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class RegisterRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function sendSms($phone, $unique_code)
    {
        $sms_client = new \SmsClub\Client([
            'token'       => env('SMS_CLUB_TOKEN'), // Токен учетной записи пользователя (который можно найти в профиле пользователя);
            'username'    => env('SMS_CLUB_USERNAME'), // Логин учетной записи пользователя;
            'from'        => env('SMS_CLUB_FROM') // Альфа-имя, от которого идет отправка (до 11 английских символов, цифры, пробел);
        ]);

        $sms = $sms_client->setRecipient($phone)->setMessage('Код підтвердження: '.$unique_code)->send();

        return $sms['response'];
    }

    public function confirmPhone(Request $request)
    {
        if($request->confirm_code == $request->compare_code) {
            $name = $request->input('name');
            $email = $request->input('email');
            $tel = phone_format($request->input('tel'));

            $roleId = UserRole::where('name', 'client')->first()->id;

            if(isset($request->profile_id)) {
                $facebookProfileTypeId = UserProfileType::where('slug', 'facebook')->first()->id;

                $user = User::create(['email' => $email, 'name' => $name, 'role_id' => $roleId, 'phone' => $tel]);

                $profileUser = UserProfile::create([
                    'profile_id' => $request->input('profile_id'),
                    'user_id' => $user->id,
                    'type_id' => $facebookProfileTypeId,
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'profile_url' => $request->input('profile_url'),
                ]);
            }
            else {
                $password = bcrypt($request->input('password'));
                $user = User::create(['email' => $email, 'password' => $password, 'name' => $name, 'role_id' => $roleId, 'phone' => $tel]);
            }
            $client = Client::create(['user_id' => $user->id, 'mailing' => 1]);

            Auth::login($user);

            return true;
        }
        else {
            return false;
        }
    }
}