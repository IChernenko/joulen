<?php

namespace App\Repositories;

use App\Models\OrderSubcategory;
use App\Models\SubcategoryFactor;
use App\Repositories\Contracts\MasterRepositoryInterface;
use App\Models\Master;
use App\Models\Region;
use App\Models\OrderCategory;
use App\Repositories\Contracts\SubcategoryRepositoryInterface;
use DB;

class SubcategoryRepository implements SubcategoryRepositoryInterface
{

    protected $master;

    protected $model_class;

    protected $attr;
    protected $column;
    protected $active;
    protected $exists;
    protected $index_list;
    protected $service_type;
    protected $region_slug;
    protected $with;

    protected $category_slugs;
    protected $region_slugs;

    public function __construct(Master $master)
    {
        $this->master = $master;
        $this->model_class = get_class($this->master);
    }

    public function getAllWithFactors()
    {
        return OrderSubcategory::with('subCategoryFactor')->has('subCategoryFactor')->get();
    }

    public function getSesWithFactors()
    {
        return OrderSubcategory::with('subCategoryFactor')->has('subCategoryFactor')
            ->whereHas('category', function ($q) {
                $q->where('slug', 'ses');
            })
            ->get();
    }
}