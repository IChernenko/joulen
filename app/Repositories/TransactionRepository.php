<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 16.06.17
 * Time: 20:30
 */

namespace App\Repositories;


use App\Models\Payment\Payment;
use App\Models\Payment\Status;
use App\Models\Payment\Transaction;
use Bosnadev\Repositories\Eloquent\Repository;

class TransactionRepository extends BaseRepository
{
    public function model()
    {
        return Transaction::class;
    }
}