<?php

namespace App\Services;


use GuzzleHttp\Client;

class WPService
{
    const NEWS_SLUG = 'news';
    const FAQ_SLUG = 'faq';

    private function getClient()
    {
        static $client;

        if (is_null($client))
            $client = new Client(['base_uri' => route('blog')]);

        return $client;
    }

    private function getPostsByCategorySlug(string $category_slug, int $limit = 5)
    {
        if ($category_id = $this->getCategoryID($category_slug)) {
            try {
                $response = $this->getClient()->get(blog_route('wp-json/wp/v2/posts'), [
                    'query' => [
                        'categories' => $category_id,
                        'per_page' => $limit
                    ]
                ]);

                $content = collect(\GuzzleHttp\json_decode($response->getBody()->getContents()))->toArray();
            } catch (\Exception $e) {
                $content = [];
            }
        } else {
            $content = [];
        }

        return $content;
    }

    private function getCategoryUrlBySlug($category_slug)
    {
        if ($category_id = $this->getCategoryID($category_slug)) {
            try {
                $response = $this->getClient()->get(blog_route("wp-json/wp/v2/categories/$category_id"));

                $content = collect(\GuzzleHttp\json_decode($response->getBody()->getContents()));

                $link = $content->get('link');
            } catch (\Exception $e) {
                $link = '';
            }
        } else {
            $link = '';
        }

        return $link;
    }

    public function getLastNews(int $limit = 5)
    {
        return $this->getPostsByCategorySlug(self::NEWS_SLUG, $limit);
    }

    public function getLastFaqs(int $limit = 5)
    {
        return $this->getPostsByCategorySlug(self::FAQ_SLUG, $limit);
    }

    public function getNewsUrl()
    {
        return $this->getCategoryUrlBySlug(self::NEWS_SLUG);
    }

    public function getFaqUrl()
    {
        return $this->getCategoryUrlBySlug(self::FAQ_SLUG);
    }

    private function getCategoryID($slug)
    {
        try {
            $response = $this->getClient()->get(blog_route('wp-json/wp/v2/categories'), [
                'query' => [
                    'slug' => $slug
                ]
            ]);

            $content = collect(\GuzzleHttp\json_decode($response->getBody()->getContents()))->first();

            return $content->id;
        } catch (\Exception $e) {
            return false;
        }
    }
}