<?php

namespace App\Traits;


use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait RestoreUser
{
    public function restoreUser($id)
    {
        $user = User::find($id);
        
        if(Auth::user()->can('restore', $user)){
            $user->deleted = false;
            $user->save();
        }           
        
        return redirect(isset($this->redirect_after_restore) ? $this->redirect_after_restore : '/');
    }
}