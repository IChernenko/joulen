<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('type_id')->unsigned();
            $table->string('profile_id');
            $table->string('token')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('profile_url')->nullable();
        });
//        TODO rename field type and profile
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('no action')->onUpdate('cascade');
            $table->foreign('type_id')->references('id')->on('user_profile_types')->onDelete('no action')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->dropForeign('user_profiles_user_id_foreign');
            $table->dropForeign('user_profiles_type_id_foreign');
        });

        Schema::drop('user_profiles');
    }
}
