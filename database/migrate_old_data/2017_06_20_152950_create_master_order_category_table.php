<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterOrderCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_order_category', function (Blueprint $table) {
            $table->integer('master_id');
            $table->integer('category_id');
        });

        Schema::table('master_order_category', function (Blueprint $table) {
            $table->foreign('master_id')->references('id')->on('masters')->onDelete('no action')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('order_categories')->onDelete('no action')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_order_category', function (Blueprint $table) {
            $table->dropForeign('master_order_category_master_id_foreign');
            $table->dropForeign('master_order_category_category_id_foreign');
        });

        Schema::drop('master_order_category');
    }
}
