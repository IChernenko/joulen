<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPriceTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_price_type_pivot', function (Blueprint $table) {
            $table->integer('order_id');
            $table->integer('price_type_id');
        });

        Schema::table('order_price_type_pivot', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('no action')->onUpdate('cascade');
            $table->foreign('price_type_id')->references('id')->on('order_request_price_types')->onDelete('no action')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_price_type_pivot', function (Blueprint $table) {
            $table->dropForeign('order_price_type_pivot_order_id_foreign');
            $table->dropForeign('order_price_type_pivot_price_type_id_foreign');
        });

        Schema::drop('order_price_type_pivot');
    }
}
