CREATE TEMPORARY TABLE IF NOT EXISTS master_regions AS (SELECT id, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(regionsWorck, ',', ''), '"', ''), '{', ''), '}', ''), 'on', '') AS regionsWorck FROM joule.users);

delimiter $$

DROP PROCEDURE IF EXISTS split_value_into_multiple_rows $$
CREATE PROCEDURE split_value_into_multiple_rows(tablename VARCHAR(20),
    id_column VARCHAR(20), value_column VARCHAR(20), delim CHAR(1))
  BEGIN
    DECLARE id INT DEFAULT 0;
    DECLARE value VARCHAR(255);
    DECLARE occurrences INT DEFAULT 0;
    DECLARE i INT DEFAULT 0;
    DECLARE splitted_value VARCHAR(255);
    DECLARE done INT DEFAULT 0;
    DECLARE cur CURSOR FOR SELECT tmp_table1.id, tmp_table1.value FROM 
        tmp_table1 WHERE tmp_table1.value IS NOT NULL AND tmp_table1.value != '';
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

		DROP TEMPORARY TABLE IF EXISTS tmp_table1;
    SET @expr = CONCAT('CREATE TEMPORARY TABLE tmp_table1 (id INT NOT NULL, value VARCHAR(255)) ENGINE=Memory SELECT ',
        id_column,' id, ', value_column,' value FROM ',tablename);
    PREPARE stmt FROM @expr;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    DROP TEMPORARY TABLE IF EXISTS tmp_table2;
    CREATE TEMPORARY TABLE tmp_table2 (id INT NOT NULL, value VARCHAR(255) NOT NULL) ENGINE=Memory;

    OPEN cur;
      read_loop: LOOP
        FETCH cur INTO id, value;
        IF done THEN
          LEAVE read_loop;
        END IF;

        SET occurrences = (SELECT CHAR_LENGTH(value) -
                           CHAR_LENGTH(REPLACE(value, delim, '')) + 1);
        SET i=1;
        WHILE i <= occurrences DO
          SET splitted_value = (SELECT TRIM(SUBSTRING_INDEX(
              SUBSTRING_INDEX(value, delim, i), delim, -1)));
          INSERT INTO tmp_table2 VALUES (id, splitted_value);
          SET i = i + 1;
        END WHILE;
      END LOOP;

			/*SET foreign_key_checks = 0; 
			INSERT INTO joulen.*/
			SELECT 
				m_new.id AS master_id,
				r_new.id AS region_id
			FROM tmp_table2
			LEFT OUTER JOIN joule.regions AS r ON tmp_table2.`value` COLLATE utf8_general_ci = r.number
			LEFT OUTER JOIN joulen.regions AS r_new ON r_new.slug  COLLATE utf8_general_ci  = r.region_slug
			LEFT OUTER JOIN joulen.masters AS m_new ON m_new.user_id = tmp_table2.id
			WHERE r_new.id IS NOT NULL;
			/*SET foreign_key_checks = 1;*/

    CLOSE cur;
    DROP TEMPORARY TABLE tmp_table1;
  END; $$

delimiter ;

CALL split_value_into_multiple_rows('master_regions','id','regionsWorck',':');
DROP PROCEDURE IF EXISTS split_value_into_multiple_rows;