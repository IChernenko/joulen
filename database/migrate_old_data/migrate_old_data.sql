/*settings table*/
INSERT INTO joulen.settings
SELECT * FROM joule.settings;

/*users table*/
INSERT INTO joulen.users (id, email, password, name, role_id, phone, blocked, deleted, created_at, updated_at)
SELECT
	u.id,
	u.email,
	u.`password`,
	REPLACE(REPLACE(u.`name`, ' (Видалився)', ''), ' (Заблокований)', '') AS name,
	ur_new.id AS role_id,
	u.tel AS phone,
	u.block AS blocked,
	u.`delete` AS deleted,
	u.created_at,
	u.updated_at
FROM
	joule.users AS u
/* ? */INNER JOIN joulen.user_roles AS ur_new ON ur_new.role COLLATE utf8_general_ci = CASE
WHEN u.permission = 'user' THEN
	'client'
ELSE
	u.permission
END;

/*clients table*/
INSERT INTO joulen.clients (user_id, mailing)
SELECT
  u_new.id AS user_id,
  u.mailing AS mailing
FROM joulen.users AS u_new
  LEFT OUTER JOIN joule.users AS u ON u.id = u_new.id
WHERE u_new.role_id = 2
ORDER BY u_new.id;

/*admins table*/
INSERT INTO joulen.admins (user_id, mailing)
SELECT
  u_new.id AS user_id,
  u.mailing AS mailing
FROM joulen.users AS u_new
  LEFT OUTER JOIN joule.users AS u ON u.id = u_new.id
WHERE u_new.role_id = 1
ORDER BY u_new.id;

/*partners table*/
INSERT INTO joulen.partners
SELECT
    (@cnt := @cnt + 1) AS id,
    u.id AS user_id
FROM users AS u
  CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE u.role_id = 4
ORDER BY u.id;

/*masters table*/
INSERT INTO joulen.masters
SELECT
	(@cnt := @cnt + 1) AS id,
	u_new.id AS user_id,
CASE
	WHEN r_new.id IS NULL THEN 1
	ELSE r_new.id
END AS region_id,
	u.company,
	u.wmail AS work_email,
	u.website,
	u.aboutme AS about
FROM
	users AS u_new
CROSS JOIN (SELECT @cnt := 0) AS dummy
LEFT OUTER JOIN joule.users AS u ON u_new.id = u.id
LEFT OUTER JOIN joule.regions AS r ON u.regions = r.number
LEFT OUTER JOIN joulen.regions AS r_new ON r_new.slug  COLLATE utf8_general_ci  = r.region_slug
WHERE
	u_new.role_id = 3
ORDER BY
	u_new.id;

/*master_order_category table*/
INSERT INTO joulen.master_order_category
  SELECT
    m_new.id AS master_id,
    1 AS category_id,
    u.mailing AS mailing
  FROM
    joulen.masters AS m_new
    LEFT OUTER JOIN joule.users AS u ON m_new.user_id = u.id
  WHERE
    u.ses = 1
  ORDER BY
    master_id;

/*master_order_category table*/
INSERT INTO joulen.master_order_category
  SELECT
    m_new.id AS master_id,
    2 AS category_id,
    u.energoaudit_mailing AS mailing
  FROM
    joulen.masters AS m_new
    LEFT OUTER JOIN joule.users AS u ON m_new.user_id = u.id
  WHERE
    u.energoaudit = 1
  ORDER BY
    master_id;

/*master_order_category table*/
INSERT INTO joulen.master_order_category
  SELECT
    m_new.id AS master_id,
    3 AS category_id,
    u.waste_mailing AS mailing
  FROM
    joulen.masters AS m_new
    LEFT OUTER JOIN joule.users AS u ON m_new.user_id = u.id
  WHERE
    u.waste = 1
  ORDER BY
    master_id;
	
/*master_portfolios table*/	
INSERT INTO joulen.master_portfolios
SELECT 
p.id,
m_new.id AS master_id,
p.description,
p.created_at,
p.updated_at
FROM joule.portfolios AS p
INNER JOIN joulen.masters AS m_new ON m_new.user_id = p.master_id
WHERE p.photo != '';
	
/*orders table*/
INSERT INTO joulen.orders (id,client_id,master_id,region_id,status_id,subcategory_id,city,client_comment,manager_comment,cancellation_comment,paid,created_at,updated_at)
SELECT
	o.id,
	c_new.id AS client_id,
	m_new.id AS master_id,
	r_new.id AS region_id,
	o.`status`+1 AS status_id,
	o.nameorder AS subcategory_id,
	o.city,
	o.coments AS client_comment,
	o.coment_meneger AS manager_comment,
	o.coment_error AS cancellation_comment,
	o.payment AS paid,
	o.created_at,
	o.updated_at
FROM
	joule.orders AS o
INNER JOIN joulen.clients AS c_new ON c_new.user_id = o.user_id 
LEFT OUTER JOIN joulen.masters AS m_new ON m_new.user_id = o.master_id 
LEFT OUTER JOIN joule.regions AS r ON o.region = r.number
LEFT OUTER JOIN joulen.regions AS r_new ON r_new.slug COLLATE utf8_general_ci  = r.region_slug
LEFT OUTER JOIN joule.orders_wastesorting AS ow ON ow.order_id = o.id
/* ? */WHERE o.nameorder IN (SELECT id FROM joulen.order_subcategories) AND (ow.waste_power != 0 OR ow.waste_power IS NULL);

/*order_ses table*/
INSERT INTO joulen.order_ses
SELECT 
	(@cnt := @cnt + 1) AS id,
o.id AS order_id,
o.category AS price_category_id,
o.powerf AS photocell_power,
o.battery_capacity,
o.need_area AS required_area,
o.area_house AS area,
NULL AS area_unit,
o.description_mod AS parts_comment,
o.lat,
o.lng,
o.zoom,
o.about_house AS house_comment,
o.pre_calculation AS green_tariff
FROM joule.orders AS o
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE o.id IN (SELECT id FROM joulen.orders) AND o.nameorder IN(1,2,3,4);

/*order_energy_audit table*/
INSERT INTO joulen.order_energy_audit
SELECT 
	(@cnt := @cnt + 1) AS id,
o.id AS order_id,
oe.heating_type AS heating_type_id,
oe.floors,
o.area_house AS area,
NULL AS area_unit,
oe.exploitation - 1,
oe.residents,
oe.electricity_per_year,
oe.water_per_year,
oe.gas_per_year,
o.about_house AS house_comment,
oe.high_costs,
oe.cold_winter,
oe.hot_summer,
oe.mold,
oe.draft,
oe.windows_fogging,
oe.disbalance,
oe.blackout,
oe.provider_problems,
oe.other_problems,
oe.thermal_examination,
oe.blower_door_test,
oe.energy_passport,
oe.detailed_calculation,
oe.technical_supervision
FROM joule.orders AS o
LEFT OUTER JOIN joule.orders_energoaudit AS oe ON o.id = oe.order_id
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE o.id IN (SELECT id FROM joulen.orders) AND o.nameorder IN(5,6,7,8,9);

/*order_waste_sorting table*/
INSERT INTO joulen.order_waste_sorting
SELECT 
	(@cnt := @cnt + 1) AS id,
o.id AS order_id,
CASE
WHEN ow.waste_power = 5 THEN 1
WHEN ow.waste_power = 10 THEN 2
WHEN ow.waste_power = 20 THEN 3
WHEN ow.waste_power = 40 THEN 4
END AS size_id,
ow.waste_comment AS `size_comment`,
o.about_house AS `about_comment`,
ow.pm_help
FROM joule.orders AS o
LEFT OUTER JOIN joule.orders_wastesorting AS ow ON o.id = ow.order_id
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE o.id IN (SELECT id FROM joulen.orders) AND o.nameorder = 10 AND ow.waste_power != 0;

/*update orders table*/
UPDATE joulen.orders AS o, joulen.order_ses AS os
SET o.orderable_id = os.id, o.orderable_type = 'ses'
WHERE o.id = os.order_id AND o.subcategory_id IN (1,2,3,4);

UPDATE joulen.orders AS o, joulen.order_energy_audit AS oea
SET o.orderable_id = oea.id, o.orderable_type = 'energy_audit'
WHERE o.id = oea.order_id AND o.subcategory_id IN (5,6,7,8,9);

UPDATE joulen.orders AS o, joulen.order_waste_sorting AS ows
SET o.orderable_id = ows.id, o.orderable_type = 'waste_sorting'
WHERE o.id = ows.order_id AND o.subcategory_id = 10;

/*client_emails table*/
INSERT INTO joulen.client_emails
SELECT 
cm.id,
c_new.id AS client_id,
m_new.id AS master_id,
cm.order_id,
cm.text,
cm.`comment`,
cm.created_at,
cm.updated_at
FROM joule.client_mails AS cm
INNER JOIN joulen.clients AS c_new ON c_new.user_id = cm.user_id
INNER JOIN joulen.masters AS m_new ON m_new.user_id = cm.master_id;

/*reviews table*/
INSERT INTO joulen.reviews (id, client_id, master_id, order_id, text, positive, created_at, updated_at)
SELECT 
c.id,
c_new.id AS client_id,
m_new.id AS master_id,
c.order_id,
c.`comment` AS text,
c.voice AS positive,
c.created_at,
c.updated_at
FROM joule.comments AS c
INNER JOIN joulen.clients AS c_new ON c_new.user_id = c.user_id
INNER JOIN joulen.masters AS m_new ON m_new.user_id = c.master_id;

/*payments table*/
INSERT INTO joulen.payments (id, master_id, order_id, status_id, amount, created_at, updated_at)
SELECT 
p.id,
m_new.id AS master_id,
p.order_id,
  (SELECT id FROM joulen.payment_statuses WHERE `name` = 'manual') AS status_id,
p.amount,
p.created_at,
p.updated_at
FROM joule.payments AS p
INNER JOIN joulen.masters AS m_new ON m_new.user_id = p.master_id;

/*order_requests table*/
INSERT INTO joulen.order_requests
SELECT 
mm.id,
m_new.id AS master_id,
mm.order_id,
mm.type_order-1 AS final,
mm.created_at,
mm.updated_at
FROM joule.MasterMessages AS mm
INNER JOIN joulen.masters AS m_new ON m_new.user_id = mm.master_id;

/*preorders table*/
INSERT INTO preorders (id, subcategory_id, created_at, updated_at)
SELECT
  w.id,
	w.type AS subcategory_id,
	w.created_at,
	w.updated_at
FROM joule.wasteaudit AS w
WHERE w.type IN (SELECT id FROM joulen.preorder_subcategories);

/*preorder_waste_audit table*/
INSERT INTO joulen.preorder_waste_audit
SELECT
	(@cnt := @cnt + 1) AS id,
  w.id AS preorder_id,
	w.`name`,
	w.phone,
	w.email,
	w.`comment`
FROM joule.wasteaudit AS w
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE w.type IN (SELECT id FROM joulen.preorder_subcategories);

/*update preorders table*/
UPDATE joulen.preorders AS p, joulen.preorder_waste_audit AS pwa
SET p.preorderable_id = pwa.id, p.preorderable_type = 'waste_audit'
WHERE p.id = pwa.preorder_id AND p.subcategory_id IN (1,2,3);

/*order_request_prices table*/ /*нужно ли переносить нулевые цены??*/
INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	1 AS type_id,
	mm.devices_price AS price,
	mm.devices_coment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (1, 2, 3, 4);

INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	2 AS type_id,
	mm.installation_price AS price,
	mm.installation_coment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (1, 2, 3, 4);

INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	3 AS type_id,
	mm.legal_price AS price,
	mm.legal_coment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (1, 2, 3, 4);

INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	4 AS type_id,
	mm.energoaudit_price AS price,
	mm.energoaudit_comment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (5,6,7,8,9);

INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	5 AS type_id,
	mm.devices_price AS price,
	mm.devices_coment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (10);

INSERT INTO joulen.order_request_prices (request_id, type_id, price, `comment`)
SELECT
	mm.id AS request_id,
	6 AS type_id,
	mm.installation_price AS price,
	mm.installation_coment AS `comment`
FROM
	joule.MasterMessages AS mm
LEFT OUTER JOIN joule.orders AS o ON mm.order_id = o.id
WHERE
	mm.order_id IN (SELECT id FROM joulen.orders)
AND mm.master_id IN (SELECT id FROM joulen.users)
AND mm.id IN (SELECT id	FROM joulen.order_requests)
AND o.nameorder IN (10);

/*partner_settings table*/
INSERT INTO joulen.partner_settings (partner_id,`key`,`value`)
SELECT
	p_new.id AS partner_id,
	'banner' AS `key`,
	up.banner AS `value`
FROM
	joule.users_partners AS up
LEFT OUTER JOIN joulen.partners AS p_new ON p_new.user_id = up.user_id
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE up.banner_name != '' AND up.banner_phone != '';

INSERT INTO joulen.partner_settings (partner_id,`key`,`value`)
SELECT
	p_new.id AS partner_id,
	'banner_name' AS `key`,
	up.banner_name AS `value`
FROM
	joule.users_partners AS up
LEFT OUTER JOIN joulen.partners AS p_new ON p_new.user_id = up.user_id
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE up.banner_name != '' AND up.banner_phone != '';

INSERT INTO joulen.partner_settings (partner_id,`key`,`value`)
SELECT
	p_new.id AS partner_id,
	'banner_phone' AS `key`,
	up.banner_phone AS `value`
FROM
	joule.users_partners AS up
LEFT OUTER JOIN joulen.partners AS p_new ON p_new.user_id = up.user_id
CROSS JOIN (SELECT @cnt := 0) AS dummy
WHERE up.banner_name != '' AND up.banner_phone != '';

/*partner_preorder_category table*/
INSERT INTO joulen.partner_preorder_category (partner_id, category_id)
SELECT
	p_new.id AS partner_id,
	1 AS category_id
FROM
	joule.users_partners AS up
LEFT OUTER JOIN joulen.partners AS p_new ON p_new.user_id = up.user_id;

/*attachments table*/
INSERT INTO joulen.attachments (disk_name, file_name, attachable_id, attachable_type)
	SELECT
		SUBSTR(u.file, LENGTH(u.file) - LOCATE('/', REVERSE(u.file))+2, LENGTH(u.file)) AS disk_name,
		SUBSTR(u.file, LENGTH(u.file) - LOCATE('/', REVERSE(u.file))+2, LENGTH(u.file)) AS file_name,
		m_new.id AS attachable_id,
		'master' AS attachable_type
	FROM
		users AS u_new
		LEFT OUTER JOIN joule.users AS u ON u_new.id = u.id
		LEFT OUTER JOIN joulen.masters AS m_new ON u_new.id = m_new.user_id
	WHERE
		u_new.role_id = 3 AND u.file != ''
	ORDER BY
		m_new.id;

INSERT INTO joulen.attachments (disk_name, file_name, attachable_id, attachable_type)
	SELECT
		p.photo AS disk_name,
		p.photo AS file_name,
		p.id AS attachable_id,
		'master_portfolio' AS attachable_type
	FROM
		joule.portfolios AS p
		INNER JOIN joulen.masters AS m_new ON m_new.user_id = p.master_id
	WHERE
		p.photo != '';