<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id');
            $table->decimal('lat', 8, 4)->nullable();
            $table->decimal('lng', 8, 4)->nullable();
            $table->string('address');
            $table->unsignedTinyInteger('index');
        });

        Schema::table('master_offices', function (Blueprint $table) {
            $table->foreign('master_id')->references('id')->on('masters')->onDelete('no action')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_offices', function (Blueprint $table) {
            $table->dropForeign('master_offices_master_id_foreign');
        });
        Schema::dropIfExists('master_offices');
    }
}
