<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('masters', function (Blueprint $table) {
            $table->integer('status_id');
            $table->string('work_phone');
            $table->string('second_work_phone')->nullable();
            $table->string('edrpou', 30)->nullable();
            $table->string('ipn', 30)->nullable();
            $table->boolean('exchange_fund_bool')->nullable();
            $table->string('exchange_fund', 500)->nullable();
            $table->boolean('car')->default(0);
            $table->boolean('project_department')->default(0);
            $table->boolean('construction_machinery');
            $table->boolean('eco_energy')->default(0);
            $table->boolean('news_mailing')->default(1);
            $table->smallInteger('year_started')->nullable();
            $table->unsignedTinyInteger('profile_stage')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('masters', function (Blueprint $table) {
            $table->dropColumn(['second_phone']);
        });
    }
}
