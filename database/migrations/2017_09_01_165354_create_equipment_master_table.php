<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_master', function (Blueprint $table){
           $table->integer('master_id');
           $table->integer('equipment_id');
           $table->boolean('warranty_increase')->nullable();
           $table->unsignedSmallInteger('warranty_duration');
           $table->unsignedInteger('warranty_case');
           $table->unsignedInteger('exchange_fund_loc');
           $table->unsignedSmallInteger('built');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
