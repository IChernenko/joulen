<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterOrderSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_order_subcategory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id');
            $table->integer('subcategory_id');
            $table->boolean('mailing')->default(1);
        });

        Schema::table('master_order_subcategory', function (Blueprint $table) {
            $table->foreign('master_id')->references('id')->on('masters')->onDelete('no action')->onUpdate('cascade');
            $table->foreign('subcategory_id')->references('id')->on('order_subcategories')->onDelete('no action')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_order_subcategory', function (Blueprint $table) {
            $table->dropForeign('master_order_subcategory_master_id_foreign');
            $table->dropForeign('master_order_subcategory_subcategory_id_foreign');
        });
        Schema::dropIfExists('master_order_subcategory');
    }
}
