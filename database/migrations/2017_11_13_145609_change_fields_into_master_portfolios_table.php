<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsIntoMasterPortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('master_portfolios')) {
            Schema::table('master_portfolios', function (Blueprint $table) {
                $table->decimal('lat', 8, 5)->nullable()->change();
                $table->decimal('lng', 8, 5)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_portfolios', function (Blueprint $table) {
            //
        });
    }
}
