<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsIntoMasterOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('master_offices')) {
            Schema::table('master_offices', function (Blueprint $table) {
                $table->decimal('lat', 8, 5)->nullable()->change();
                $table->decimal('lng', 8, 5)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_offices', function (Blueprint $table) {
            //
        });
    }
}
