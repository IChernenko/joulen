<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipment', function (Blueprint $table) {
            $table->string('slug');
            $table->text('description')->nullable();
            $table->string('website')->nullable();
            $table->integer('foundation_year')->nullable();
            $table->string('headquarters')->nullable();
            $table->string('production')->nullable();
            $table->string('ukraine_office')->nullable();
        });

        Artisan::call('db:seed', [
            '--class' => SlugForEquipmentTable::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment', function (Blueprint $table) {
            $table->dropColumn(['slug', 'description', 'website', 'foundation_year', 'headquarters', 'production', 'ukraine_office']);
        });
    }
}
