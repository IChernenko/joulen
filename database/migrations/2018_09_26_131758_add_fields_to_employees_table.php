<?php

use App\Models\Employee;
use App\Models\Master;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('second_phone')->nullable()->after('email');
        });

        $masters = Master::all()->keyBy('id');

        foreach (Employee::all() as $employee) {
            $master = $masters->find($employee->master_id);

            if ($master) {
                $employee->update([
                    'master_id' => $master->user->id
                ]);
            } else {
                $employee->delete();
            }
        }

        Schema::table('employees', function (Blueprint $table) {
            $table->renameColumn('master_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('second_phone');
        });

        $delete_employee_ids = [];
        foreach (Employee::with('user')->get() as $employee) {
            if (!$employee->user->isMaster()) {
                $delete_employee_ids[] = $employee->id;
            } else {
                $user = $employee->user;

                if ($user) {
                    $employee->update([
                        'user_id' => $user->master->id
                    ]);
                }
            }
        }

        Employee::whereIn('id', $delete_employee_ids)->delete();

        Schema::table('employees', function (Blueprint $table) {
            $table->renameColumn('user_id', 'master_id');
        });
    }
}
