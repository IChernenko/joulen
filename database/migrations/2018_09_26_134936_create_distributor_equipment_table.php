<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_equipment', function (Blueprint $table) {
            $table->unsignedInteger('distributor_id');
            $table->unsignedInteger('equipment_id');

            $table->foreign('distributor_id')->references('id')->on('distributors')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipment_id')->references('id')->on('equipment')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_equipment');
    }
}
