<?php

use Illuminate\Database\Seeder;

class EmployeeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('employee_types')->truncate();
            DB::table('employee_types')->insert([
                'id' => 1,
                'title' => 'менеджер',
                'slug' => 'manager',
            ]);
            DB::table('employee_types')->insert([
                'id' => 2,
                'title' => 'керівник',
                'slug' => 'owner',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
