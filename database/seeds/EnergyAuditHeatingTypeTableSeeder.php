<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnergyAuditHeatingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('energy_audit_heating_type')->truncate();
            DB::table('energy_audit_heating_type')->insert([
                'id' => 1,
                'name' => 'газове опалення',
                'slug' => 'gas',
            ]);
            DB::table('energy_audit_heating_type')->insert([
                'id' => 2,
                'name' => 'електричне опалення',
                'slug' => 'electricity',
            ]);
            DB::table('energy_audit_heating_type')->insert([
                'id' => 3,
                'name' => 'твердопаливний котел / пічне опалення',
                'slug' => 'boiler',
            ]);
            DB::table('energy_audit_heating_type')->insert([
                'id' => 4,
                'name' => 'централізоване опалення',
                'slug' => 'central',
            ]);
            DB::table('energy_audit_heating_type')->insert([
                'id' => 5,
                'name' => 'інше',
                'slug' => 'other',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
