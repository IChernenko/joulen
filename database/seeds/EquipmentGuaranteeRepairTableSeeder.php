<?php

use Illuminate\Database\Seeder;

class EquipmentGuaranteeRepairTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('equipment_guarantee_repair')->truncate();
            DB::table('equipment_guarantee_repair')->insert([
                'id' => 1,
                'name' => 'Ваша компанія',
                'slug' => 'company',
            ]);
            DB::table('equipment_guarantee_repair')->insert([
                'id' => 2,
                'name' => 'Офіційний представник в Україні',
                'slug' => 'representative',
            ]);
            DB::table('equipment_guarantee_repair')->insert([
                'id' => 3,
                'name' => 'Клієнт',
                'slug' => 'client',
            ]);
        });
    }
}
