<?php

use Illuminate\Database\Seeder;

class EquipmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('equipment_types')->truncate();
            DB::table('equipment_types')->insert([
                'id' => 1,
                'title' => 'Инвертор',
                'slug' => 'inverter'
            ]);
            DB::table('equipment_types')->insert([
                'id' => 2,
                'title' => 'Панель',
                'slug' => 'solar_panel'
            ]);
        });
    }
}
