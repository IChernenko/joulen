<?php

use Illuminate\Database\Seeder;

class MarketDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('market_data')->truncate();
            DB::table('market_data')->insert([
                'id' => 1,
                'green_tariff_home_ses' => 0.16,
                'green_tariff_roof_ses' => 0.14,
                'green_tariff_ground_ses' => 0.14,
                'avg_cost_network_ses_5kW' => 5730,
                'avg_cost_network_ses_30kW' => 27000,
                'avg_cost_auto_ses_5kW' => 5000,
                'avg_cost_auto_ses_30kW' => 27000,
                'ratio_hybrid_station' => 1.5,
                'avg_cost_roof_ses_30kW' => 5730,
                'avg_cost_roof_ses_100kW' => 27000,
                'avg_cost_ground_ses_100kW' => 5730,
                'avg_cost_ground_ses_1000kW' => 27000,
                'avg_cost_gel_batteries' => 200
            ]);
        });
    }
}
