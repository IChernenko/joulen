<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('order_categories')->truncate();
            DB::table('order_categories')->insert([
                'id' => 1,
                'name' => 'СЕС',
                'slug' => 'ses',
            ]);
            DB::table('order_categories')->insert([
                'id' => 2,
                'name' => 'Енергоаудит',
                'slug' => 'energy_audit',
            ]);
            DB::table('order_categories')->insert([
                'id' => 3,
                'name' => 'Сміттєсортування',
                'slug' => 'waste_sorting',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
