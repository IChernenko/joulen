<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderRequestPriceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('order_request_price_types')->truncate();
            DB::table('order_request_price_types')->insert([
                'id' => 1,
                'category_id' => 1,
                'name' => 'Поставка обладнання та комплектуючих',
                'description' => 'Вкажіть виробника, модель, та параметри усіх одиниць обладнання яке ви пропонуєте',
                'slug' => 'ses_parts',
            ]);
            DB::table('order_request_price_types')->insert([
                'id' => 2,
                'category_id' => 1,
                'name' => 'Монтаж та налагодження',
                'description' => 'Коментар щодо монтажу та налаштування (необов\'язково)',
                'slug' => 'ses_installation',
            ]);
            DB::table('order_request_price_types')->insert([
                'id' => 3,
                'category_id' => 1,
                'name' => 'Документальний супровід',
                'description' => 'Коментар щодо підключення сес та отримання зеленого тарифу (необов\'язково)',
                'slug' => 'ses_legal_support',
            ]);
            DB::table('order_request_price_types')->insert([
                'id' => 4,
                'category_id' => 2,
                'name' => 'Проведення енергоаудиту згідно вимог клієнта',
                'description' => 'Напишіть детальніше про послуги, які ви можете надати клієнту',
                'slug' => 'energy_audit',
            ]);
            DB::table('order_request_price_types')->insert([
                'id' => 5,
                'category_id' => 3,
                'name' => 'Поставка сміттєсортувальної лінії',
                'description' => 'Коментар щодо поставки обладнання (необов\'язково)',
                'slug' => 'waste_sorting_parts',
            ]);
            DB::table('order_request_price_types')->insert([
                'id' => 6,
                'category_id' => 3,
                'name' => 'Загальнобудівельні роботи',
                'description' => 'Коментар щодо загальнобудівельних робіт (необов\'язково)',
                'slug' => 'waste_sorting_installation',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
