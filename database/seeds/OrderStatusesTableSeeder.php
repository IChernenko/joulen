<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('order_statuses')->truncate();
            DB::table('order_statuses')->insert([
                'id' => 1,
                'name' => 'На перевірці',
                'slug' => 'checking',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 2,
                'name' => 'Відхилено',
                'slug' => 'rejected',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 3,
                'name' => 'Розглядається',
                'slug' => 'considering',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 4,
                'name' => 'Виконується',
                'slug' => 'in_progress',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 5,
                'name' => 'Виконано',
                'slug' => 'done',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 6,
                'name' => 'Скасовано',
                'slug' => 'canceled',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 7,
                'name' => 'Деактивовано',
                'slug' => 'deactivated',
            ]);
            DB::table('order_statuses')->insert([
                'id' => 8,
                'name' => 'Закрито',
                'slug' => 'closed',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
