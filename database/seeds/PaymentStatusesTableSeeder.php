<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('payment_statuses')->truncate();
            DB::table('payment_statuses')->insert([
                'id' => 1,
                'name' => 'success',
            ]);
            DB::table('payment_statuses')->insert([
                'id' => 2,
                'name' => 'failure',
            ]);
            DB::table('payment_statuses')->insert([
                'id' => 3,
                'name' => 'in_progress',
            ]);
            DB::table('payment_statuses')->insert([
                'id' => 4,
                'name' => 'manual',
            ]);
            DB::table('payment_statuses')->insert([
                'id' => 5,
                'name' => 'canceled',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
