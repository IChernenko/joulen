<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreorderSubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('preorder_subcategories')->truncate();
            DB::table('preorder_subcategories')->insert([
                'id' => 1,
                'category_id' => 1,
                'name' => 'Експрес',
                'slug' => 'express',
            ]);
            DB::table('preorder_subcategories')->insert([
                'id' => 2,
                'category_id' => 1,
                'name' => 'Стандарт',
                'slug' => 'standard',
            ]);
            DB::table('preorder_subcategories')->insert([
                'id' => 3,
                'category_id' => 1,
                'name' => 'Розгорнутий',
                'slug' => 'advanced',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
