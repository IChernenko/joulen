<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('regions')->truncate();
            DB::table('regions')->insert([
                'id' => 1,
                'name' => 'Одеська обл.',
                'slug' => 'odesa',
            ]);
            DB::table('regions')->insert([
                'id' => 2,
                'name' => 'Дніпропетровська обл.',
                'slug' => 'dnipro',
            ]);
            DB::table('regions')->insert([
                'id' => 3,
                'name' => 'Чернігівська обл.',
                'slug' => 'chernihiv',
            ]);
            DB::table('regions')->insert([
                'id' => 4,
                'name' => 'Харківська обл.',
                'slug' => 'kharkiv',
            ]);
            DB::table('regions')->insert([
                'id' => 5,
                'name' => 'Житомирська обл.',
                'slug' => 'zhytomyr',
            ]);
            DB::table('regions')->insert([
                'id' => 6,
                'name' => 'Полтавська обл.',
                'slug' => 'poltava',
            ]);
            DB::table('regions')->insert([
                'id' => 7,
                'name' => 'Херсонська обл.',
                'slug' => 'kherson',
            ]);
            DB::table('regions')->insert([
                'id' => 8,
                'name' => 'Київська обл.',
                'slug' => 'kyiv',
            ]);
            DB::table('regions')->insert([
                'id' => 9,
                'name' => 'Запорізька обл.',
                'slug' => 'zaporizhzhia',
            ]);            
            DB::table('regions')->insert([
                'id' => 10,
                'name' => 'Луганська обл.',
                'slug' => 'lugansk',
            ]);
            DB::table('regions')->insert([
                'id' => 11,
                'name' => 'Донецька обл.',
                'slug' => 'donetsk',
            ]);
            DB::table('regions')->insert([
                'id' => 12,
                'name' => 'Вінницька обл.',
                'slug' => 'vinnytsya',
            ]);
            DB::table('regions')->insert([
                'id' => 13,
                'name' => 'АР Крим',
                'slug' => 'krym',
            ]);
            DB::table('regions')->insert([
                'id' => 14,
                'name' => 'Кiровоградська обл.',
                'slug' => 'kirovograd',
            ]);
            DB::table('regions')->insert([
                'id' => 15,
                'name' => 'Миколаївська обл.',
                'slug' => 'mykolaiv',
            ]);
            DB::table('regions')->insert([
                'id' => 16,
                'name' => 'Сумська обл.',
                'slug' => 'sumy',
            ]);
            DB::table('regions')->insert([
                'id' => 17,
                'name' => 'Львівська обл.',
                'slug' => 'lviv',
            ]);
            DB::table('regions')->insert([
                'id' => 18,
                'name' => 'Черкаська обл.',
                'slug' => 'cherkasy',
            ]);
            DB::table('regions')->insert([
                'id' => 19,
                'name' => 'Хмельницька обл.',
                'slug' => 'khmelnytskyi',
            ]);
            DB::table('regions')->insert([
                'id' => 20,
                'name' => 'Волинська обл.',
                'slug' => 'volyn',
            ]);
            DB::table('regions')->insert([
                'id' => 21,
                'name' => 'Рівненська обл.',
                'slug' => 'rivne',
            ]);
            DB::table('regions')->insert([
                'id' => 22,
                'name' => 'Івано-Франківська обл.',
                'slug' => 'ivano-frankivsk',
            ]);
            DB::table('regions')->insert([
                'id' => 23,
                'name' => 'Тернопільська обл.',
                'slug' => 'ternopil',
            ]);
            DB::table('regions')->insert([
                'id' => 24,
                'name' => 'Закарпатська обл.',
                'slug' => 'zakarpattja',
            ]);
            DB::table('regions')->insert([
                'id' => 25,
                'name' => 'Чернівецька обл.',
                'slug' => 'chernivtsi',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
