<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SesPriceCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('ses_price_category')->truncate();
            DB::table('ses_price_category')->insert([
                'id' => 1,
                'name' => 'Найдешевший варіант',
                'slug' => 'cheapest',
            ]);
            DB::table('ses_price_category')->insert([
                'id' => 2,
                'name' => 'Оптимальний варіант ціна/якість',
                'slug' => 'optimal',
            ]);
            DB::table('ses_price_category')->insert([
                'id' => 3,
                'name' => 'Найякісніше обладнання',
                'slug' => 'best_quality',
            ]);
            DB::table('ses_price_category')->insert([
                'id' => 4,
                'name' => 'Конкретні параметри',
                'slug' => 'custom',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
