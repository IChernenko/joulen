<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('statuses')->truncate();
            DB::table('statuses')->insert([
                'id' => 1,
                'name' => 'на перевірці',
                'slug' => 'checking',
            ]);
            DB::table('statuses')->insert([
                'id' => 2,
                'name' => 'підтверджена',
                'slug' => 'verified',
            ]);
            DB::table('statuses')->insert([
                'id' => 3,
                'name' => 'відхилена',
                'slug' => 'rejected',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
