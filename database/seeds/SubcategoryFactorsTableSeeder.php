<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubcategoryFactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('subcategory_factors')->truncate();
            for ($i = 1; $i <= 3; $i++) {
                DB::table('subcategory_factors')->insert([
                    'id' => $i,
                    'subcategory_id' => $i,
                    'factor' => '0.01',
                ]);
            }
            DB::table('subcategory_factors')->insert([
                'id' => 4,
                'subcategory_id' => 4,
                'factor' => '0.005',
            ]);

            for ($i = 5; $i <= 9; $i++) {
                DB::table('subcategory_factors')->insert([
                    'id' => $i,
                    'subcategory_id' => $i,
                    'factor' => '0.05',
                ]);
            }
            DB::table('subcategory_factors')->insert([
                'id' => 10,
                'subcategory_id' => 10,
                'factor' => '0.07',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
