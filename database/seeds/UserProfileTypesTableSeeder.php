<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserProfileTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('user_profile_types')->truncate();
            DB::table('user_profile_types')->insert([
                'id' => 1,
                'name' => 'facebook',
                'slug' => 'facebook',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
