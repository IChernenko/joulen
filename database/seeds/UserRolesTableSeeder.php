<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('user_roles')->truncate();
            DB::table('user_roles')->insert([
                'id' => 1,
                'name' => \App\Models\UserRole::ADMIN,
            ]);
            DB::table('user_roles')->insert([
                'id' => 2,
                'name' => \App\Models\UserRole::CLIENT,
            ]);
            DB::table('user_roles')->insert([
                'id' => 3,
                'name' => \App\Models\UserRole::MASTER,
            ]);
            DB::table('user_roles')->insert([
                'id' => 4,
                'name' => \App\Models\UserRole::PARTNER,
            ]);
            DB::table('user_roles')->insert([
                'id' => 5,
                'name' => \App\Models\UserRole::TEMP_MASTER,
            ]);
            DB::table('user_roles')->insert([
                'id' => 6,
                'name' => \App\Models\UserRole::DISTRIBUTOR,
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
