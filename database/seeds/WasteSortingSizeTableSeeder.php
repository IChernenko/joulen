<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WasteSortingSizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('waste_sorting_size')->truncate();
            DB::table('waste_sorting_size')->insert([
                'id' => 1,
                'power' => 5,
                'name' => 'Міні',
                'slug' => 'mini',
            ]);
            DB::table('waste_sorting_size')->insert([
                'id' => 2,
                'power' => 10,
                'name' => 'Малопотужна',
                'slug' => 'small',
            ]);
            DB::table('waste_sorting_size')->insert([
                'id' => 3,
                'power' => 20,
                'name' => 'Середня',
                'slug' => 'middle',
            ]);
            DB::table('waste_sorting_size')->insert([
                'id' => 4,
                'power' => 40,
                'name' => 'Велика',
                'slug' => 'big',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
