<?php

use Illuminate\Database\Seeder;

class DatabaseSeederDev extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('EquipmentTableSeeder');
        $this->call('EquipmentMasterTableSeeder');
    }
}
