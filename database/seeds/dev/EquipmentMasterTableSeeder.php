<?php

use Illuminate\Database\Seeder;

class EquipmentMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('equipment_master')->insert([
                'master_id' => 690,
                'equipment_id' => 1,
                'warranty_increase' => 1,
                'warranty_duration' => 1,
                'warranty_case' => 1,
                'exchange_fund_loc' => 1,
                'built' => 4
            ]);
            DB::table('equipment_master')->insert([
                'master_id' => 11,
                'equipment_id' => 1,
                'warranty_increase' => 1,
                'warranty_duration' => 1,
                'warranty_case' => 1,
                'exchange_fund_loc' => 1,
                'built' => 4
            ]);
            DB::table('equipment_master')->insert([
                'master_id' => 234,
                'equipment_id' => 2,
                'warranty_increase' => 1,
                'warranty_duration' => 1,
                'warranty_case' => 1,
                'exchange_fund_loc' => 1,
                'built' => 4
            ]);
            DB::table('equipment_master')->insert([
                'master_id' => 534,
                'equipment_id' => 4,
                'warranty_increase' => 1,
                'warranty_duration' => 1,
                'warranty_case' => 1,
                'exchange_fund_loc' => 1,
                'built' => 4
            ]);
            DB::table('equipment_master')->insert([
                'master_id' => 1,
                'equipment_id' => 5,
                'warranty_increase' => 1,
                'warranty_duration' => 1,
                'warranty_case' => 1,
                'exchange_fund_loc' => 1,
                'built' => 4
            ]);
        });
    }
}
