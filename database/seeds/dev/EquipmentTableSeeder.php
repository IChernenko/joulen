<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('equipment')->insert([
                'id' => 1,
                'type_id' => 1,
                'title' => 'ABC Solar',
                'comment' => ''
            ]);
            DB::table('equipment')->insert([
                'id' => 2,
                'type_id' => 2,
                'title' => 'Canadian Solar',
                'comment' => ''
            ]);
            DB::table('equipment')->insert([
                'id' => 3,
                'type_id' => 1,
                'title' => 'ABB',
                'comment' => ''
            ]);
            DB::table('equipment')->insert([
                'id' => 4,
                'type_id' => 2,
                'title' => 'SMA',
                'comment' => ''
            ]);
            DB::table('equipment')->insert([
                'id' => 5,
                'type_id' => 1,
                'title' => 'Blue Sun',
                'comment' => ''
            ]);
        });
    }
}
