1. composer install\update
2. php artisan migrate
3. php artisan db:seed

4. php artisan migrate --path="/vendor/jrean/laravel-user-verification/src/resources/migrations"
5. php artisan vendor:publish --provider="Jrean\UserVerification\UserVerificationServiceProvider" --tag="migrations"

// 6,7,8 is not necessary if project already has migration for creating tables: jobs,failed_jobs
6. php artisan queue:table
7. php artisan queue:failed-table
8. php artisan migrate

9. .env set 'QUEUE	failed_jobs_DRIVER=database'
10. php artisan up
11. configure supervisor https://laravel.com/docs/5.4/queues#supervisor-configuration ( see example joulen/queque.conf )
12. sudo supervisorctl reread
13. sudo supervisorctl update
14. supervisorctl start laravel-worker:*

15. php artisan storage:link
16. Add Collator extension (php intl)