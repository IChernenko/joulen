$(document).ready(function () {
    $(".select2").select2({
        placeholder: "Почніть вводити назву(лат)",
        width: '100%'
    });

    $('.pagination li:not(.active) a').on('click', function (e) {
        e.preventDefault();
        $('#admin_distributor_filter').attr('action', $(this).attr('href'));
        $('#admin_distributor_filter').submit();
    });

    $('.equipment-item').on('click', function () {
        var current_sortorder = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortorder');
        var current_sortfield = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortfield');
        var new_sortorder = current_sortorder != 'asc' && current_sortfield == $(this).attr('data-sortfield') ? 'asc' : 'desc';
        $('.equipment-item').removeClass('desc');
        $('.equipment-item').removeClass('asc');
        // $('.equipment-item').attr('data-sortorder', '');

        $(this).addClass(new_sortorder);
        $(this).attr('data-sortorder', new_sortorder);

        submit_admin_master_list();
    });

    $('select').on('change', function () {
        $('#admin_distributor_filter').submit();
    });

    function submit_admin_master_list(){
        var perpage = $('.amount_page_item.active').first().attr('data-perpage');
        var sortfield = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortfield');
        var sortorder = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortorder');
        $('input[name=perpage]').val(perpage);
        $('input[name=sortfield]').val(sortfield);
        $('input[name=sortorder]').val(sortorder);

        $('#admin_distributor_filter').submit();
    }
});