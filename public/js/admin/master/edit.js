$(document).ready(function () {
    var statusSelect = $('#company_status');

    $('.master-activate').on('click', function (e) {
       e.preventDefault();
       $.ajax({
           method: 'PUT',
           url: masterActivateURL,
           headers: {
               'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
           },
           success: function (master) {
               $('.master-activation-block').hide();
               statusSelect.val(master.status_id).niceSelect('update');
           }
       });
    });
    $('.master-reject').on('click', function (e) {
        e.preventDefault();
        var rejection_text = $('textarea[name=rejection_reason]').val();
        $.ajax({
            method: 'PUT',
            url: masterRejectURL,
            data: {rejection_reason: rejection_text},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (master) {
                $('.master-activation-block').hide();
                statusSelect.val(master.status_id).niceSelect('update');
            }
        });

    });
    $('.edit-master-form').on('submit', function (e) {
        var photos = [],
            attrs = [],
            visibleBlock = true;
        photos.push($('.edit-master-form').find('.logoFile img:visible'));
        for(var i = 0; i < photos[0].length; i++) {
            attrs.push($(photos[0][i]).attr('src'));
        }

        for(var i = 0; i < attrs.length; i++) {
            if(!attrs[i]) {
                visibleBlock = false;
            }
        }

        if(visibleBlock) {
            $('.edit-master-form').find('.show-error-hidden-file').hide();
        }
        else {
            $(this).find('.show-error-hidden-file').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            e.preventDefault();
        }
    });

    $('body').on('click', '.add-news-link', function () {
        $('.add-news-block').clone().removeClass().addClass('added-news').appendTo($('.news'));
        $('.add-news-block input').val('');
    });

    $('body').on('click', '.remove-news', function () {
        $(this).closest('.added-news').find('input').val('');
        $(this).closest('.added-news').remove();
    });
});