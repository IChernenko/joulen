$(document).ready(function () {

    $('.pagination li:not(.active) a').on('click', function (e) {
        e.preventDefault();
        $('#admin_master_list').attr('action', $(this).attr('href'));
        $('#admin_master_list').submit();
    });

    //Handeling masters exporting
    function set_emails_link(){
        var emails = [];
        $('input.styledChk:checked').not('#check_all1').each(function () {
            emails.push($(this).closest('tr').find('.wrap_email').html());
        });
        $('.export_base a').attr('href', window.URL.createObjectURL(new Blob([emails.join(', ')], {type: 'text/plain'})));
    }

    set_emails_link();

    $('#check_all1').on('change', function () {
        $('.styledChk').prop( "checked", $(this).prop('checked') );
    });

    $('.styledChk').on('change', set_emails_link);


    //TODO: fix this sedtion
    //Handeling form submitting
    $('.custom_select').on('change', function () {
        submit_admin_master_list();
    });

    $('.amount_page_item').on('click', function (e) {
        e.preventDefault();

        $('.amount_page_item').removeClass('active');
        $(this).addClass('active');

        submit_admin_master_list();
    });

    $('.masters_sort').on('click', function () {
        var current_sortorder = $('.masters_sort.desc,.masters_sort.asc').first().attr('data-sortorder');
        var current_sortfield = $('.masters_sort.desc,.masters_sort.asc').first().attr('data-sortfield');
        var new_sortorder = current_sortorder != 'asc' && current_sortfield == $(this).attr('data-sortfield') ? 'asc' : 'desc';
        console.log($('.masters_sort.desc,.masters_sort.asc').first());
        console.log(current_sortorder);
        console.log(current_sortfield);
        console.log(new_sortorder);
        $('.masters_sort').removeClass('desc');
        $('.masters_sort').removeClass('asc');
        $('.masters_sort').attr('data-sortorder', '');

        $(this).addClass(new_sortorder);
        $(this).attr('data-sortorder', new_sortorder);

        submit_admin_master_list();
    });

    function submit_admin_master_list(){
        var perpage = $('.amount_page_item.active').first().attr('data-perpage');
        var sortfield = $('.masters_sort.desc,.masters_sort.asc').first().attr('data-sortfield');
        var sortorder = $('.masters_sort.desc,.masters_sort.asc').first().attr('data-sortorder');
        $('input[name=perpage]').val(perpage);
        $('input[name=sortfield]').val(sortfield);
        $('input[name=sortorder]').val(sortorder);
        
        $('#admin_master_list').submit();
    }

});