$(document).ready(function () {
   $('.select2').niceSelect();
   $('.set-status').niceSelect();
   $('.portfolio_subcategory_type_id').niceSelect();
   $('.select-master').select2({
       width: '100%'
   });
   initialize();

    $('.pagination li:not(.active) a').on('click', function (e) {
        e.preventDefault();
        $('#admin_portfolio_filter').attr('action', $(this).attr('href'));
        $('#admin_portfolio_filter').submit();
    });

   $('body').on('click', '.get-portfolio', function () {
        var portfolio_id = $(this).attr('data-id');
        $.ajax({
            url: getPortfolioURL + '/' + portfolio_id,
            method: 'GET',
            success: function (response) {
                setPortfolioModalShow(response);
            }
        })
   });

    var master_id;
    $('body').on('click', '.update-commercial', function () {
        $('.modal-header h4').text('Редагувати об\'єкт (Комерційні СЕС)');
        $('.photo-invertor').text('Фото сонячної станції 1');
        $('.photo-sun-panel').text('Фото сонячної станції 2');
        $('.type-ses-block').hide();
        $('.designing-block').show();
        $('.contractor-block').show();
        $('.documentation-block').hide();
        $('.commercial-type-ses-block').show();
    });

    $('body').on('click', '.update-home', function () {
        $('.modal-header h4').text('Редагувати об\'єкт (Домашні СЕС)');
        $('.photo-invertor').text('Фото інвертора');
        $('.photo-sun-panel').text('Фото сонячної станції');
        $('.type-ses-block').show();
        $('.commercial-type-ses-block').hide();
    });

    $('body').on('click', '.open-update-ses', function () {
        var ses_id = $(this).attr('data-id');
        master_id = $(this).attr('data-master-id');
        var url = getPortfolioURL+'/'+ses_id;
        $('.portfolio_id').val(ses_id);
        $.ajax({
            url: url,
            method: 'GET',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                setPortfolioModal(response);
            }
        });
    });

    $(document).on('click', '#ses-confirm', function (e) {
        var portfolio_id = $('input[name=portfolio_id]').val();
        var validationBlock = $('.validation_errors_modal');
        $('input[name=master_id]').val(master_id);
        var subcategory_id = $('#subcategory-home-add').val();
        $('input[name=portfolio_subcategory_id]').val(subcategory_id);
        validationBlock.empty();
        e.preventDefault();
        var form = $('#ses-form'),
            formData = new FormData(form[0]);
        // Validation
        var panel_photo = $('.panel_photo_img').attr('src');
        var invertor_photo = $('.invertor_photo_img').attr('src');
        if( panel_photo == false && invertor_photo == '') {
            validationBlock.append(
                $('<li>').text('Поле "Фото інвертора" необхідно заповнити.'),
                $('<li>').text('Поле "Фото панелі" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
        }
        else if ( panel_photo == '' ) {
            validationBlock.append(
                $('<li>').text('Поле "Фото панелі" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
        }
        else if ( invertor_photo == '' ) {
            validationBlock.append(
                $('<li>').text('Поле "Фото інвертора" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
        }
        else {
            $.ajax({
                url: portfolioUpdateURL + '/' + portfolio_id,
                data: formData,
                async: true,
                method: 'POST',
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function (response) {
                    $('#hellopreloader').css('display', 'block');
                    $('#hellopreloader_preload').css('display', 'block').css('opacity', 1);
                },
                success: function (response) {
                    $('#hellopreloader').hide();
                    var index;
                    setPortfolioTable(response);
                    reloadMarkers(onlyMarkers);
                    if (portfolio_id == response.id) {
                        index = portfolios.findIndex(x => x.id == portfolio_id);
                        portfolios[index].id = response.id;
                        portfolios[index].lat = response.lat;
                        portfolios[index].lng = response.lng;
                    }
                    setMarkers(portfolios, response);
                    $('.home-ses-table').footable();

                    var modal = $('[data-remodal-id=add-ses]').remodal();
                    modal.close();
                },
                error: function (response) {
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('.validation_errors_modal');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }

                    $('.remodal-wrapper.remodal-is-opened').animate({
                        scrollTop: $('#add-ses').position().top
                    }, 800);
                }
            });
            $(validationBlock).children('li').remove();
        }
    });

    $('body').on('submit', '.set-portfolio .set-form', function (e) {
       e.preventDefault();
       var status_id = $('.set-status').val();
       var statusName;

       $.ajax({
           url: $(this).attr('action'),
           method: 'POST',
           headers: {
               'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
           },
           data: {status: status_id},
           success: function (portfolio) {
               if (portfolio.status.slug == 'verified') {
                   statusName = 'Активна';
               } else {
                   statusName = portfolio.status.name;
               }
               $('#' + portfolio.id + ' > td.application_status').text(statusName);
               var modal = $('#show-portfolio').remodal();
               modal.close();
           },
           error: function (response) {
               // implement when some developer will have more free time
           }

       })

    });

    $('body').on('click', '.delete_portfolio', function () {
        var portfolio_id = $(this).attr('data-id');
        $('body').on('click', '.confirm-delete', function (e) {
            e.preventDefault();
            $.ajax({
                url: portfolioDeleteURL + '/' + portfolio_id,
                method: 'POST',
                data: {_method: 'DELETE'},
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (portfolio) {
                    $('.home-ses-table tr[id=' + portfolio_id + ']').remove();
                },
                error: function (response) {
                    // implement when some developer will have more free time
                }

            })
        });
    });
});

var N = 50.4532, E = 30.5255;
var modal_map, marker_modal, marker_modal_2, onlyMarkers = [], map2, map, marker = [];
function initialize() {

    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(49.0383462,31.4489168),
        scrollwheel: false,
        zoom: 6,
        //Тип карты - обычная дорожная карта
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false
    };
    //Инициализируем карту
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var infowindow = new google.maps.InfoWindow({
        content: "Триває завантаження..."
    });
    // portfolio home ses map places
    var portfolioBounds = new google.maps.LatLngBounds(),
        markerStyle,
        myMarkers = [];
    for(var i = 0; i < portfolios.length; i++) {
        if(portfolios.length > 0) {
            map.setCenter(new google.maps.LatLng(portfolios[0].lat, portfolios[0].lng));
            if(portfolios[i].is_home) {
                markerStyle = homeMarkerSrc;
            }
            else {
                markerStyle = commercialMarkerSrc;
            }

            if(portfolios[i].lat != null && portfolios[i].lng != null) {
                myMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(portfolios[i].lat, portfolios[i].lng),
                    map: map,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    icon: markerStyle
                });
                myMarkers.push({marker: myMarker, info: portfolios[i]});
                onlyMarkers.push(myMarker);
                portfolioBounds.extend(myMarker.getPosition());
            }
        }
        map.fitBounds(portfolioBounds);
        google.maps.event.addListener(map, "idle", function () {
            if (map.getZoom() > 20) map.setZoom(16);
        });
    }

    $.each(myMarkers, function () {
        var contractor = '', designing = '', delivery = '',
            installation = '', documentation = '', string = [];
        if(this['info'].general_contractor == 1) {
            contractor = '<li>Генпідряд</li>';
        }
        if(this['info'].designing == 1) {
            designing = '<li>Проектування та документація</li>';
        }
        if(this['info'].delivery == 1) {
            delivery = '<li>Поставка обладнання</li>';
        }
        if(this['info'].installation == 1) {
            installation = '<li>Монтаж</li>';
        }
        if(this['info'].documentation == 1) {
            documentation = '<li>Документальний супровід</li>';
        }
        string.push('<div class="ses-info">' +
            '<div class="page-title">' +
            '<h1>' + this['info'].subcategory.name + ' СЕС, '
            + this['info'].power + ' кВт' + '</h1>' +
            '</div>' +
            '<p><a href="' + showMasterURL + '/' + this['info'].master_id + '">' + user[master[this['info'].master_id].user_id].name + '</a></p>' +
            '<ul>' + contractor +
            designing +
            delivery +
            installation +
            documentation + '</ul>' +
            '<button class="btn btn-primary get-portfolio" data-remodal-target="show-portfolio"' +
            ' data-id="' + this['info'].id + '">Детальніше</button></div>');
        google.maps.event.addListener(this.marker, 'click', function () {
            infowindow.setContent(string[0]);
            infowindow.open(map, this);
        });
    });

    modal_map = new google.maps.Map(document.getElementById('map-1'), {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(49.0383462,31.4489168),
        scrollwheel: false,
        zoom: 6,
        //Тип карты - обычная дорожная карта
        mapTypeId: 'hybrid',
        mapTypeControl: false,
        streetViewControl: false
    });

    var mapOptions2 = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(N, E),
        scrollwheel: false,
        //увеличение под которым будет карта, от 0 до 18
        // 0 - минимальное увеличение - карта мира
        // 18 - максимально детальный масштаб
        zoom: 8,
        //Тип карты - обычная дорожная карта
        mapTypeId: 'hybrid',
        mapTypeControl:false,
        streetViewControl: false
    };
    //Инициализируем карту
    map2 = new google.maps.Map(document.getElementById('gmap'), mapOptions2);
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    searchBox = new google.maps.places.SearchBox(input);
    map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
}

function setPortfolioModalShow(portfolio) {
    var arr = [];
    if(portfolio.general_contractor == 1) {
        arr.push('Генпідряд');
    }
    if(portfolio.designing == 1) {
        arr.push('Проектування та документація');
    }
    if(portfolio.delivery == 1) {
        arr.push('Поставка обладнання');
    }
    if(portfolio.installation == 1) {
        arr.push('Монтаж');
    }
    if(portfolio.documentation == 1) {
        arr.push('Документальний супровід');
    }
    $('.set-portfolio .modal-header h4').text(portfolio.subcategory.name + ' СЕС, '
        + portfolio.power + ' кВт');
    $('.set-portfolio .set-address p').text(portfolio.address);
    $('.set-portfolio .set-date').html('<span>Введено в експлуатацію:</span> ' + portfolio.month + ' ' + portfolio.year);
    $('.set-portfolio .set-panel-photo img').attr('src', portfolio.panel_photo_src);
    $('.set-portfolio .set-invertor-photo img').attr('src', portfolio.invertor_photo_src);
    $('.set-portfolio .set-invertor').html('<span>Інвертор:</span> ' + (invertorName[portfolio.invertor_id] ? invertorName[portfolio.invertor_id].title : 'даний бренд було видалено'));
    $('.set-portfolio .set-panel').html('<span>Панель:</span> ' + (panelName[portfolio.panel_id] ? panelName[portfolio.panel_id].title : 'даний бренд було видалено'));
    $('.set-portfolio .set-executor').html('<span>Виконавець:</span> ' + user[master[portfolio.master_id].user_id].name);
    for (var i = 0; i<arr.length; i++) {
        $('.set-portfolio .set-settings').append('<li>' + arr[i] + '</li>');
    }
    $('.set-portfolio .set-form').attr('action', portfolioStatusUpdateURL + '/' + portfolio.id);
    $('.set-portfolio .set-status').val(portfolio.status_id).niceSelect('update');
    $('select.set-status[value=' + portfolio.status_id + ']').prop('selected', true);
    $('.delete_portfolio').attr('data-id', portfolio.id);
    window.dispatchEvent(new Event('resize'));
    myLatlng = new google.maps.LatLng(portfolio.lat, portfolio.lng);
    modal_map.setCenter(myLatlng);
    marker_modal = new google.maps.Marker({
        //расположение на карте
        position: myLatlng,
        map: modal_map,
        //То что мы увидим при наведении мышкой на маркер
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
}

$(document).on('closed', '.remodal', function () {
    $('.set-portfolio .set-settings').empty();
    if(marker_modal) {
        marker_modal.setMap(null);
    }
    else if (marker_modal_2) {
        marker_modal_2.setMap(null);
    }
});
$(document).on('opened', '.remodal', function () {
    var subcategory_id = $(".portfolio_subcategory_id").val();
    if(subcategory_id == 1 || subcategory_id == 2) {
        $('.designing-block').hide();
        $('.contractor-block').hide();
        $('.documentation-block').show();
    }
    else if(subcategory_id == 3) {
        $('.designing-block').hide();
        $('.contractor-block').hide();
        $('.documentation-block').hide();
    }
    else if(subcategory_id == 4) {
        $('.portfolio_subcategory_id[value=4]').prop('selected', true);
        $('.designing-block').show();
        $('.contractor-block').show();
        $('.documentation-block').hide();
    }
    $(".portfolio_month").select2({
        placeholder: "Місяць",
        width: '100%'
    });
    $(".portfolio_year").select2({
        placeholder: "Рік",
        width: '100%'
    });
    $(".portfolio_subcategory_id").select2({
        placeholder: "Виберіть тип",
        width: '100%'
    });
    $(".portfolio_panel_id").select2({
        width: '100%'
    });
    $(".portfolio_invertor_id").select2({
        width: '100%'
    });

    window.dispatchEvent(new Event('resize'));
    lat = $('.geo_coords_lat-modal').val();
    lng = $('.geo_coords_lng-modal').val();
    if (lat == '' && lng == '') {
        lat = 50.4532;
        lng = 30.5255;
    }
    myLatlng = new google.maps.LatLng(lat, lng);
    map2.setCenter(myLatlng);
    marker_modal_2 = new google.maps.Marker({
        //расположение на карте
        position: myLatlng,
        //То что мы увидим при наведении мышкой на маркер
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    var geocoder = new google.maps.Geocoder;
    // Bias the SearchBox results towards current map's viewport.
    map2.addListener('bounds_changed', function () {
        searchBox.setBounds(map2.getBounds());
    });
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        var address = '', city = '', city_format = '', admin_region = '', admin_region_format = '', street = '', number = '';
        if (places.length == 0) {
            return;
        }
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            marker_modal_2.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map2.fitBounds(bounds);
        latlng = marker_modal_2.getPosition();
        $('.geo_coords_lat-modal').val(latlng.lat());
        $('.geo_coords_lng-modal').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();
        geocodeLatLng(geocoder, map2);
    });
    marker_modal_2.addListener('dragend',  function () {
        latlng = marker_modal_2.getPosition();
        $('.geo_coords_lat-modal').val(latlng.lat());
        $('.geo_coords_lng-modal').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();
        geocodeLatLng(geocoder, map2);
    });
    marker_modal_2.setMap(map2);
});

function geocodeLatLng(geocoder, map) {
    var latlng = {lat: parseFloat(N), lng: parseFloat(E)};
    var address = '', city = '', city_format = '', admin_region = '', admin_region_format = '', street = '', number = '';
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            for (var i = 0; i < results.length; i++) {
                if (results[i].types[0] === 'administrative_area_level_1') {
                    var region = results[i].formatted_address.split(' ')[0];
                    $('.portfolio-region').val(region);
                }
            }
            if (results[0]) {
                for (var i = 0; i < results[0].address_components.length; i++) {
                    if (results[0].address_components[i].types[0] == 'locality') {
                        city = results[0].address_components[i].long_name;
                        city_format = city + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'administrative_area_level_1') {
                        admin_region = results[0].address_components[i].long_name;
                        admin_region_format = admin_region + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'route') {
                        street = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'street_number') {
                        number = results[0].address_components[i].long_name;
                    }
                }
                if (street != '') {
                    if (city_format == '') {
                        address = admin_region_format + street + ' ' + number;
                    }
                    else {
                        address = city_format + street + ' ' + number;
                    }
                }
                else {
                    if (city_format == '') {
                        address = admin_region;
                    }
                    else {
                        address = city;
                    }
                }
                map.setCenter(latlng);
                $('#pac-input').val(address);
            } else {
                window.alert('Немає результатів');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}

function setPortfolioModal(response) {
    $('.portfolio_power').val(response.power);
    $('.panel_photo_img').attr('src', response.panel_photo_src);
    $('.invertor_photo_img').attr('src', response.invertor_photo_src);
    $('select.portfolio_subcategory_id').val(response.subcategory_id).select2({width: '100%'});
    $('select.portfolio_subcategory_id[value=' + response.subcategory_id + ']').prop('selected', true);
    $('.portfolio_subcategory_type_id').val(response.ground).niceSelect('update');
    $('select.portfolio_subcategory_type_id[value=' + response.ground + ']').prop('selected', true);
    $('select.portfolio_month').val(response.month).select2({width: '100%'});
    $('select.portfolio_month[value=' + response.month + ']').prop('selected', true);
    $('select.portfolio_year').val(response.year).select2({width: '100%'});
    $('select.portfolio_year[value=' + response.year + ']').prop('selected', true);
    $('.pac-input-modal').val(response.address);
    $('.geo_coords_lat-modal').val(response.lat);
    $('.geo_coords_lng-modal').val(response.lng);
    $('.portfolio-region').val(response.region_id);
    $('select.portfolio_invertor_id').val(response.invertor_id).select2({width: '100%'});
    $('select.portfolio_invertor_id[value=' + response.invertor_id + ']').prop('selected', true);
    $('select.portfolio_panel_id').val(response.panel_id).select2({width: '100%'});
    $('select.portfolio_panel_id[value=' + response.panel_id + ']').prop('selected', true);
    if($('.installation-true').val() == response.installation) {
        $('.installation-true').prop('checked', true);
    }
    else if($('.installation-false').val() == response.installation) {
        $('.installation-false').prop('checked', true);
    }
    if($('.delivery-true').val() == response.delivery) {
        $('.delivery-true').prop('checked', true);
    }
    else if($('.delivery-false').val() == response.delivery) {
        $('.delivery-false').prop('checked', true);
    }
    if($('.designing-true').val() == response.designing) {
        $('.designing-true').prop('checked', true);
    }
    else if($('.designing-false').val() == response.designing) {
        $('.designing-false').prop('checked', true);
    }
    if($('.contractor-true').val() == response.general_contractor) {
        $('.contractor-true').prop('checked', true);
    }
    else if($('.contractor-false').val() == response.general_contractor) {
        $('.contractor-false').prop('checked', true);
    }
    if (response.subcategory_id == 1 || response.subcategory_id == 2) {
        $('.documentation-block').show();
        if ($('.documentation-true').val() == response.documentation) {
            $('.documentation-true').prop('checked', true);
        }
        else if ($('.documentation-false').val() == response.documentation) {
            $('.documentation-false').prop('checked', true);
        }
    }

    if(response.invertor_photo.public == 1) {
        $('input[name=public_invertor_photo]').prop('checked', true);
    }
    else {
        $('input[name=public_invertor_photo]').prop('checked', false);
    }

    if(response.panel_photo.public == 1) {
        $('input[name=public_panel_photo]').prop('checked', true);
    }
    else {
        $('input[name=public_panel_photo]').prop('checked', false);
    }
}

function setPortfolioTable(response) {
    var old_id = $('.portfolio_id').val();
    var month = response.month;
    var year = response.year;
    var subcategory = subcategories[response.subcategory_id];
    var power = response.power;
    var address = response.address;
    var master_id = response.master_id;
    var master_name = user[master[master_id].user_id].name;
    var update_class, update, active, commercial_type, city;
    if (response.is_home == true) {
        update_class = 'update-home';
        commercial_type = '';
    }
    else {
        update_class = 'update-commercial';
        if(response.ground == 1) {
            commercial_type = ' наземна';
        }
        else if(response.ground == 0) {
            commercial_type = ' дахова';
        }
    }
    if (address) {
        city = address.split(',')[0];
    } else {
        city = address;
    }
    update = '<td data-title="Редагувати" data-breakpoints="xs sm md" class="edit_block" style="display: table-cell;">' +
        '<a class="open-update-ses ' + update_class + ' site-icon-edit" href="#add-ses" data-id="' + old_id + '" data-master-id="' + master_id + '"></a></td>';

    if(response.status_id == 2) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="active-portfolio">Активна</p></td>';
    }
    else if(response.status_id == 1) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="on-check-portfolio">На перевірці</p></td>';
    }
    else if(response.status_id == 3) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="rejected-request">Відхилено</p></td>';
    }

    var portfolioTable = '<tr id="' + old_id + '" class="tr_id">' +
    '<td data-title="Тип" data-breakpoints="xs sm md" style="display: table-cell;">' +
    '<a href="#show-portfolio" class="get-portfolio" data-id="' + old_id + '">' + subcategory + commercial_type + ' СЕС</a></td>' +
    '<td data-title="Потужність" data-breakpoints="xs sm md" style="display: table-cell;">' + power + 'кВт</td>' +
    '<td data-title="Адреса" data-breakpoints="xs sm md" style="display: table-cell;">' + city + '</td>' +
    '<td data-title="?" data-breakpoints="xs sm md" style="display: table-cell;">' + master_name + '</td>' +
    '<td data-title="Дата" data-breakpoints="xs sm md" class="footable-first-visible" style="display: table-cell;">' +
        month + ' ' + year + '</td>' +
    active + update +
    '</tr>';


    $('.home-ses-table tr[id=' + old_id + ']').replaceWith(function () {
        return portfolioTable;
    });
}

function setMarkers(markersArray) {
    var infowindow = new google.maps.InfoWindow({
        content: "Триває завантаження..."
    });
    var portfolioBounds = new google.maps.LatLngBounds(), markerStyle, myMarkers = [];
    for(var i = 0; i < markersArray.length; i++) {
        if(markersArray.length > 0) {
            map.setCenter(new google.maps.LatLng(markersArray[0].lat, markersArray[0].lng));
            if(markersArray[i].is_home) {
                markerStyle = homeMarkerSrc;
            }
            else {
                markerStyle = commercialMarkerSrc;
            }

            if(markersArray[i].lat != null && markersArray[i].lng != null) {
                myMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(markersArray[i].lat, markersArray[i].lng),
                    map: map,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    icon: markerStyle
                });
                myMarkers.push({marker: myMarker, info: markersArray[i]});
                portfolioBounds.extend(myMarker.getPosition());
            }
        }
        map.fitBounds(portfolioBounds);
        google.maps.event.addListener(map, "idle", function () {
            if (map.getZoom() > 20) map.setZoom(16);
        });
    }

    $.each(myMarkers, function () {
        var contractor = '', designing = '', delivery = '',
            installation = '', documentation = '', string = [];
        if(this['info'].general_contractor) {
            contractor = '<li>Генпідряд</li>';
        }
        if(this['info'].designing) {
            designing = '<li>Проектування та документація</li>';
        }
        if(this['info'].delivery) {
            delivery = '<li>Поставка обладнання</li>';
        }
        if(this['info'].installation) {
            installation = '<li>Монтаж</li>';
        }
        if(this['info'].documentation) {
            documentation = '<li>Документальний супровід</li>';
        }
        string.push('<div class="ses-info">' +
            '<div class="page-title">' +
            '<h1>' + this['info'].subcategory.name + ' СЕС, '
            + this['info'].power + ' кВт' + '</h1>' +
            '</div>' +
            '<p><a href="' + showMasterURL + '/' + this['info'].master_id + '">' + user[master[this['info'].master_id].user_id].name + '</a></p>' +
            '<ul>' + contractor +
            designing +
            delivery +
            installation +
            documentation + '</ul>' +
            '<button class="btn btn-primary get-portfolio" data-remodal-target="show-portfolio"' +
            ' data-id="' + this['info'].id + '">Детальніше</button></div>');
        google.maps.event.addListener(this.marker, 'click', function () {
            infowindow.setContent(string[0]);
            infowindow.open(map, this);
        });
    });
}

function reloadMarkers(markers) {

    // Loop through markers and set map to null for each
    for (var i=0; i<markers.length; i++) {
        markers[i].setMap(null);
    }

    // Reset the markers array
    markers = [];
}

//from input to photo block
function readURL(input, img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            img.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//input file changed
$(document).on('change', '.logoFile input',  function() {
    var img = $(this).closest('.logoFile').find('img');

    readURL(this, img);
});

//add photo
$(document).on('click','.site_icon-download', function() {
    $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
    var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
    hiddenInput.val('0');
});

//delete photo
$(document).on('click','.site_icon-delete', function(e) {
    e.preventDefault();

    var input = $(this).closest('.logoImgBlock').find('.logoFile input');
    var hiddenInput = $(this).next('input');
    hiddenInput.val('1');
    var img = $(this).closest('.logoImgBlock').find('.logoFile img');
    input.replaceWith(input.val('').clone(true));
    img.attr('src', '');
});

$('.open-modal').on('click', function () {
    $('.validation_errors_modal').empty();
});

//input[type=file]
$('.input-file input').on('change',function () {
    var filename = $(this).val();
    if (filename.substring(3,11) == 'fakepath') {
        filename = filename.substring(12);
    }
    $('.file-name').html(filename);
});

$('#company-contract-delete').on('click', function () {
    $('.file-name').html('');
    $('.input-file input').val('');
});
//end input[type=file]


$('body').on('click', function (event) {

    if( $(window).width() <= 992 && $(event.target).closest('.sidebar').length == 0){
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
        $('.sidebar').removeClass('hidden');
        if($(event.target).closest('.toggle-sidebar').length == 0){
            $('.cabinet-page').removeClass('sidebar-hidden');
        }
    }
});

$( window ).scroll(fixedSidebar());

fixedSidebar();
function fixedSidebar() {
    var scroll = $(document).scrollTop();
    if(scroll > 100){
        $('#mainMenu').css('position','fixed');
    }else{
        $('#mainMenu').css('position','absolute');
    }
}