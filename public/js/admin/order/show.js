$(document).ready(function () {
    var statusValue = $('#statuso').find(':selected').data('slug'),
        masterValue = $('#selectmaster').find(':selected').val();

    $('#selectmaster').on('change', function () {
        $('.specialist-price').html('$' + $(this).find(':selected').data('price'));
        $('#uahp').val($(this).find(':selected').data('commission'));
        masterValue = $(this).find(':selected').val();
    });

    $('#statuso').on('change', function () {
        statusValue = $(this).find(':selected').data('slug');
        if ($(this).find(':selected').data('slug') == 'rejected') {
            $('#coment_e').removeClass('hidden');
        } else {
            $('#coment_e').addClass('hidden');
        }
    });

    $('#button_submit').on('click', function (e) {
        e.preventDefault();

        if (masterValue == 0 && (statusValue === 'in_progress' || statusValue === 'done' )) {
            alert("Для встановлення даного статусу необхідно вибрати майстра!");

            return false;
        }

        this.closest('form').submit();
    });

    $('.add-commit').on('click', function (e) {
        e.preventDefault();

        $('#add-commit-form').submit();
    });
});