$(document).ready(function () {
    $('.create-client').on('click', function (e) {
            e.preventDefault();
            if($('#register-client').valid()) {
                var form = $('#register-client'),
                    formData = new FormData(form[0]);
                $.ajax({
                    url: $('#register-client').attr('action'),
                    data: formData,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function (response) {
                        $('#hellopreloader').css('display', 'block');
                        $('#hellopreloader_preload').css('display', 'block').css('opacity', 1);
                    },
                    success: function (response) {
                        $('#hellopreloader').hide();
                        $('.resend-confirm-code').hide();
                        if (response['errors']) {
                            var validationBlock = $('.backend_validation_errors');
                            validationBlock.empty();
                            validationBlock.append(
                                $('<li>').text(response['errors'])
                            );
                        }
                        else if (response['status'] == 'success') {
                            window.location.href = loginURL;
                        }
                        else if (response['redirect']) {
                            window.location.href = response['redirect'];
                        }
                        else if (response['status'] == 'failed_sms_send') {
                            $('.confirm-phone').show();
                            $('.resend-confirm-code').show();
                        }
                        else if ($.isNumeric(response)) {
                            $('.backend_validation_errors').empty();
                            $('.confirm-phone').show();
                            $('input[name=compare_code]').val(response);
                            var countdown = $('.message-sms-send span'),
                                startFrom = 60,
                                timer;

                            function startCountdown() {
                                timer = setInterval(function () {
                                    countdown.text(startFrom--);
                                    if (startFrom <= 0) {
                                        clearInterval(timer);
                                        $('.resend-confirm-code').show();
                                        $('input[name=compare_code]').val('');
                                        $('.message-sms-send').hide();
                                    }
                                }, 1000);
                            }

                            $('.message-sms-send').show(startCountdown());
                        }
                    },
                    error: function (response) {
                        var errors = response.responseJSON, errorField;
                        var validationBlock = $('.backend_validation_errors');
                        validationBlock.empty();
                        for (errorField in errors) {
                            validationBlock.append(
                                $('<li>').text(errors[errorField][0])
                            );
                        }
                    }
                });
            }
    });
});
