$(document).ready(function () {
    var data = {};

    $('.mail-to-master').on('click', function () {
        $('.company-name').html($(this).data('name'));
        $('.company-region').html($(this).data('region'));
        $('.company-img img').attr('src', $(this).data('img'));
        data.master_id = $(this).data('id');
    });

    $('.submit').on('click', function () {
        if(typeof window.url === 'undefined')
            window.url = '';

        data.order_id = $('[data-remodal-id="mail-to-master"] input[name="order_id"]').val();
        data.text = $('[data-remodal-id="mail-to-master"] [name="text"]').val();

        $.ajax({
            method: "POST",
            url: window.url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function() {
            var confirm_modal = $('div[data-remodal-id=confirm-mail-to-master]').remodal();
            confirm_modal.open();
            setTimeout(function(){confirm_modal.close();}, 3000);
        }).fail(function() {
            var fail_modal = $('div[data-remodal-id=fail-mail-to-master]').remodal();
            fail_modal.open();
            setTimeout(function(){fail_modal.close();}, 3000);
        })
    });

});