$(document).ready(function () {

    var is_order_created = false;

    $(document).on('submit', '#create-order-form',function (e) {

        e.preventDefault();

        var $form = $(this);
        var $formData = $(this).serializeArray();

        function checkPhoto(redirectTo) {
            if($("#trigger-upload").length == 0 || $('.uploaded-photo').length == 0){
                window.location.href = redirectTo;
            } else {
                $("#trigger-upload").trigger("click");
            }

        }

        if(is_order_created){
            checkPhoto();
        } else{
            $.ajax({
                url: $(this).attr('action'),
                data: $formData,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    response = JSON.parse(response);

                    is_order_created = true;

                    checkPhoto(response.redirectTo);

                },
                error: function (response) {
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('.backend_validation_errors ul');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }
                    $('html, body').animate({
                        scrollTop: 0
                    }, 800);
                }
            });
        }
    });
});