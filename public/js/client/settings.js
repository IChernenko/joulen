$(document).ready(function () {
    $(".delete-client").on('click', confirmdel);

    function confirmdel() {
        if (confirm("Ви дійсно впевнені що хочете видалити обліковий запис?")) {
            $("#delete-client").submit();
        }
    }
});

$(".validate_form").validate({
    lang: 'uk',
    errorElement: 'span'
});

$.validator.addClassRules({
    validate_required: {
        required: true
    }
});