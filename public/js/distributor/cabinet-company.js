$(document).ready(function () {
    $('.secondary_tel').on('change', function () {
        if ($(this).is(':checked')) {
            $(this).siblings('input[type=text]').prop('disabled', false);
        } else {
            $(this).siblings('input[type=text]').prop('disabled', true);
        }
    });
});