$('#main-filter select, #main-filter input').on('change', function () {
   $('#main-filter').submit();
});

$('.pagination li:not(.active) a').on('click', function (e) {
    e.preventDefault();
    $('#main-filter').attr('action', $(this).attr('href'));
    $('#main-filter').submit();
});