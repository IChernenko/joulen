$(document).ready(function () {
    // $("#joule-video--link").videoPopup();
    $("#joule-video--link").on('click', function () {
        autoPlayVideo('3ssA096HUfI');
        $(this).find('.joule-video--container').find('iframe').show();
    });

    function autoPlayVideo(vcode){
        "use strict";
        $(".joule-video--container").html('<iframe src="https://www.youtube.com/embed/'+vcode+'?autoplay=1&loop=1&rel=0&wmode=transparent" frameborder="0" allowfullscreen wmode="Opaque"></iframe>');
    }
});