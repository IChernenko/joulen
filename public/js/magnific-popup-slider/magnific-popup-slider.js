$(document).ready(function() {
    $('.image-link').magnificPopup({type:'image'});
});

var attachments = [];
for(var i = 0; i < portfolios.length; i++) {
    for(var j = 0; j < portfolios[i].attachments.length; j++) {
        if (portfolios[i].attachments[j].public == 1) {
            attachments.push(portfolios[i].attachments[j]);
        }
    }
}
$('.test-popup-link').magnificPopup({
    type: 'image',
    gallery: {
        enabled:true,
        navigateByImgClick: true,
        tCounter: '<span class="mfp-counter">%curr% of ' + attachments.length + '</span>'
    },
});

