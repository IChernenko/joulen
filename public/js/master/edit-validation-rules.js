$(document).ready(function(){
    // validation registration form master
function validateSpecificElement(error, element) {
    if (element.is(":checkbox")) {
        element.closest('.checkbox-validation-block').prepend(error);
    } else if(element.is('.hidden_validate:hidden')) {
        error.insertAfter(element.next('.custom_select'));
    }
    else {
        error.insertAfter(element);
    }
}

    $('.publicInfo').validate({ // initialize the plugin
        errorElement: 'span',
        focusInvalid: false,
        groups: {
            servicesChk: "services[ses] services[energy_audit] services[waste_sorting]"
        },
        ignore: ':hidden:not(:checkbox, .hidden_validate, .hidden_validate_file)',
        errorPlacement: function (error, element) {
            validateSpecificElement(error, element);
        },
        invalidHandler: function() {
            setTimeout(function () {
                var position = $('span.error').first().offset().top;
                $('html,body').animate({scrollTop: position - 300},'slow');
            },100);
        },
        rules: {
            'name': {
                required: true,
                minlength: 2
            },
            'tel': {
                required: true,
                minlength: 11
            },
            'email': {
                required: true,
                email:true
            },
            'work_email': {
                required: true,
                email:true
            },
            'location': {
                required: true
            },
            'services[ses]':{
                require_from_group: [1, '.servicesChk']
            },
            'services[energy_audit]':{
                require_from_group: [1, '.servicesChk']
            },
            'services[waste_sorting]':{
                require_from_group: [1, '.servicesChk']
            },
            'about':{
                required: true,
                maxlength: 2000
            },
            'validation-work-location':{
                required: true
            }


        },
        messages: {
            'name': {
                required: "Це поле необхідно заповнити.",
                minlength: "Будь ласка, введіть не менше {0} символів"
            },
            'tel': {
                required: "Це поле необхідно заповнити.",
                minlength: "Введіть коректний номер телефону"
            },
            'email': {
                required: "Це поле необхідно заповнити.",
                email: "Будь ласка, введіть коректну адресу електронної пошти."
            },
            'work_email': {
                required: "Це поле необхідно заповнити.",
                email: "Будь ласка, введіть коректну адресу електронної пошти."
            },
            'location': {
                required: "Обов'язково потрібно вибрати область!"
            },

            'services[ses]':{
                require_from_group: " 'Які послуги ви надаєте?' - мінімум 1 послугу потрібно вибрати!"
            },
            'services[energy_audit]':{
                require_from_group: " 'Які послуги ви надаєте?' - мінімум 1 послугу потрібно вибрати!"
            },
            'services[waste_sorting]':{
                require_from_group: " 'Які послуги ви надаєте?' - мінімум 1 послугу потрібно вибрати!"
            },
            'about':{
                required: "Це поле обов’язкове для заповнення!"
            },
            'validation-work-location':{
                required: " 'В яких областях вы надаєте свої послуги?' - мінімум 1 послугу потрібно вибрати!"
            }

        }

    });
    $('.validation-btn').on('click', function () {
        $('.regionChk').each(function () {
            if($('.regionChk:not(:checked)').length == 25){
                $('.validation-location').prop('checked',false);
            }else{
                $('.validation-location').prop('checked',true);
            }
        });
    });
    $.validator.addClassRules({
        validate_required: {
            required: true
        }
    });

    $('#master-avatar-delete').on('click', function () {
        $('#is_deleted_avatar').val('1');
    });

});
