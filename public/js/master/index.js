$("#filterWorkers").on('submit', function (e) {
    e.preventDefault();
});
$('.formsetfilter_type').on('click', function (e) {
    e.preventDefault();
    $('.formsetfilter_type').removeClass('active');
    $(this).addClass('active');
    applyFilter();
});
$('#filterRegion').on('change', function () {
    applyFilter();
});
$('#select-specialization').on('change', function () {
    applyFilter();
});
$('input[name=verified_masters]').on('change', function () {
    applyFilter();
});

function applyFilter(){
    var region = $('#filterRegion').val();
    var specialization = $('#select-specialization').val();
    var verified = $('input[name=verified_masters]').val();
    var url = '';
    if ( (typeof region != 'undefined' && region != 'all') && (typeof specialization != 'undefined' && specialization != 'all') && $('input[name=verified_masters]').is(':checked')){
        url = masterIndexRoute + '/' + region + '/' + specialization + '/' + verified;
    }
    else if (typeof region != 'undefined' && region != 'all'  && typeof specialization != 'undefined' && specialization != 'all'){
        url = masterIndexRoute + '/' + region + '/' + specialization;
    }
    else if ( typeof region != 'undefined' && region != 'all'  && $('input[name=verified_masters]').is(':checked')){
        url = masterIndexRoute + '/' + region + '/' + verified;
    }
    else if ( typeof specialization != 'undefined' && specialization != 'all'  && $('input[name=verified_masters]').is(':checked')){
        url = masterIndexRoute + '/' + specialization + '/' + verified;
    }
    else if (typeof region != 'undefined' && region != 'all') {
        url = masterIndexRoute + '/' + region;
    }
    else if (typeof specialization != 'undefined' && specialization != 'all') {
        url = masterIndexRoute + '/' + specialization;
    }
    else if ($('input[name=verified_masters]').is(':checked')) {
        url = masterIndexRoute + '/' + verified;
    }
    else {
        url = masterIndexRoute;
    }

    window.location.href = url;
}