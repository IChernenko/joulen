$(document).ready(function () {
    init_map();
    $(".portfolio_subcategory_type_id").niceSelect();

    $('body').on('change', 'select.portfolio_subcategory_id', function () {
        var subcategory_id = $(".portfolio_subcategory_id").val();
        if(subcategory_id == 1 || subcategory_id == 2) {
            $('.documentation-block').show();
            $('.designing-block').hide();
            $('.contractor-block').hide();
        }
        else if(subcategory_id == 3) {
            $('.documentation-block').hide();
            $('.designing-block').hide();
            $('.contractor-block').hide();
        }
    });

    $('body').on('change', 'select.portfolio_year', function () {
        var date = new Date();
        var currentYear = date.getFullYear();
        var currentMonth = date.getMonth();
        var selectedYear = $('#year-commercial-add').val();
        var selectedMonth = $('#month-commercial-add option:selected').attr('data-id');
        var listMonth = [];
        for(var i = 0; i < 12; i++) {
            listMonth.push(document.getElementById('month-' + i));
        }
        if(selectedYear == currentYear) {
            for(var i = 0; i < 12; i++) {
                if($(listMonth[i]).attr('data-id') > currentMonth) {
                    $(listMonth[i]).attr('disabled', true);
                }
            }
        }
        else if(currentYear > selectedYear) {
            for(var i = 0; i < 12; i++) {
                if($(listMonth[i]).attr('data-id') > currentMonth) {
                    $(listMonth[i]).attr('disabled', false);
                }
            }
        }
        $('select.portfolio_month').select2();
    });

    $('body').on('change', 'select.portfolio_month', function () {
        var date = new Date();
        var currentYear = date.getFullYear();
        var currentMonth = date.getMonth();
        var disYear = document.getElementById('year-'+currentYear);
        var selectedMonth = $('#month-commercial-add option:selected').attr('data-id');
        if(selectedMonth > currentMonth) {
            $(disYear).attr('disabled', true);
        }
        else if(currentMonth >= selectedMonth ) {
            $(disYear).attr('disabled', false);
        }
        $('select.portfolio_year').select2();
    });

    var action;
    $('body').on('click', '.update-commercial', function () {
        // $('.portfolio_subcategory_id option[value=0]').prop('selected', true);
        // $('#subcategory-home-add option:selected').val('4');
        $('.modal-header h4').text('Редагувати об\'єкт (Комерційні СЕС)');
        $('.photo-invertor').text('Фото сонячної станції 1');
        $('.photo-sun-panel').text('Фото сонячної станції 2');
        $('.type-ses-block').hide();
        $('.designing-block').show();
        $('.contractor-block').show();
        $('.documentation-block').hide();
        $('.commercial-type-ses-block').show();
    });

    $('body').on('click', '.update-home', function () {
        // $('.portfolio_subcategory_id option[value=4]').prop('selected', true);
        // $('#subcategory-home-add option:selected').val('0');
        $('.modal-header h4').text('Редагувати об\'єкт (Домашні СЕС)');
        $('.photo-invertor').text('Фото інвертора');
        $('.photo-sun-panel').text('Фото сонячної станції');
        $('.type-ses-block').show();
        $('.commercial-type-ses-block').hide();
    });

    $('body').on('click', '.add-commercial', function () {
        // $('.portfolio_subcategory_id option[value=0]').prop('selected', true);
        // $('#subcategory-home-add option:selected').val('4');
        $('#ses-form').attr('action', storePortfolioURL);
        action = storePortfolioURL;
        $('.modal-header h4').text('Додати об\'єкт у портфоліо (Комерційні СЕС)');
        $('.photo-invertor').text('Фото сонячної станції 1');
        $('.photo-sun-panel').text('Фото сонячної станції 2');
        $('.type-ses-block').hide();
        $('.designing-block').show();
        $('.contractor-block').show();
        $('.documentation-block').hide();
        $('.commercial-type-ses-block').show();
    });

    $('body').on('click', '.add-home', function () {
        // $('.portfolio_subcategory_id option[value=4]').prop('selected', true);
        // $('#subcategory-home-add option:selected').val('0');
        $('#ses-form').attr('action', storePortfolioURL);
        action = storePortfolioURL;
        $('.modal-header h4').text('Додати об\'єкт у портфоліо (Домашні СЕС)');
        $('.photo-invertor').text('Фото інвертора');
        $('.photo-sun-panel').text('Фото сонячної станції');
        $('.type-ses-block').show();
        $('.commercial-type-ses-block').hide();
    });

    $('body').on('click', '.open-update-ses', function () {
        var ses_id = $(this).attr('data-id');
        var master_id = $(this).attr('data-master-id');
        action = updatePortfolioURL;
        var url = getPortfolioURL+'/'+ses_id;
        $('.portfolio_id').val(ses_id);
        $.ajax({
            url: url,
            method: 'GET',
            data: { master_id: master_id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                setPortfolioModal(response);
            }
        });
    });

    $('.show-map-block').focusout(function () {
        var lat = $('.geo_coords_lat-modal').val();
        var lng = $('.geo_coords_lng-modal').val();
        if(lat == '' && lng == '') {
            $('.error-block-modal').show();
            $('#ses-confirm').prop('disabled', true);
        }
        else {
            $('.error-block-modal').hide();
            $('#ses-confirm').prop('disabled', false);

        }
    });

    $(document).on('click', '#ses-confirm', function (e) {
        var portfolio_id = $('input[name=portfolio_id]').val();
        var validationBlock = $('.validation_errors_modal');
        var subcategory_id = $('#subcategory-home-add').val();
        $('input[name=portfolio_subcategory_id]').val(subcategory_id);
        validationBlock.empty();
        e.preventDefault();
        var form = $('#ses-form'),
            formData = new FormData(form[0]);
        // Validation
        var panel_photo = $('.panel_photo_img').attr('src');
        var invertor_photo = $('.invertor_photo_img').attr('src');
       if( panel_photo == false && invertor_photo == '') {
            validationBlock.append(
                $('<li>').text('Поле "Фото інвертора" необхідно заповнити.'),
                $('<li>').text('Поле "Фото панелі" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
            }
        else if ( panel_photo == '' ) {
            validationBlock.append(
                $('<li>').text('Поле "Фото панелі" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
        }
        else if ( invertor_photo == '' ) {
            validationBlock.append(
                $('<li>').text('Поле "Фото інвертора" необхідно заповнити.')
            );
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#add-ses').position().top
            }, 800);
        }
        else {
            $.ajax({
                url: action,
                data: formData,
                async: true,
                method: 'POST',
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function (response) {
                    $('#hellopreloader').css('display', 'block');
                    $('#hellopreloader_preload').css('display', 'block').css('opacity', 1);
                },
                success: function (response) {
                    $('#hellopreloader').hide();
                    var index;
                    setPortfolioTable(response);
                    if(response.is_home != 0) {
                        reloadMarkers(myMarkers4);
                        if (portfolio_id == response.id) {
                            index = homePortfolioCoordinates.findIndex(x => x.id==portfolio_id);
                            homePortfolioCoordinates[index] = {
                                id: response.id,
                                lat: response.lat,
                                lng: response.lng
                            };
                        }
                        else {
                            homePortfolioCoordinates.push({id: response.id, lat: response.lat, lng: response.lng});
                        }
                        setMarkers(homePortfolioCoordinates, response);
                        $('.home-ses-table').footable();
                    }
                    else  {
                        reloadMarkers(myMarkers5);
                        if (portfolio_id == response.id) {
                            index = commercialPortfolioCoordinates.findIndex(x => x.id==portfolio_id);
                            commercialPortfolioCoordinates[index] = {
                                id: response.id,
                                lat: response.lat,
                                lng: response.lng
                            }
                        }
                        else {
                            commercialPortfolioCoordinates.push({lat: response.lat, lng: response.lng});
                        }
                        setMarkers(commercialPortfolioCoordinates, response);
                        $('.commercial-ses-table').footable();
                    }

                    var modal = $('[data-remodal-id=add-ses]').remodal();
                    modal.close();


                },
                error: function (response) {
                    $('#hellopreloader').hide();
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('.validation_errors_modal');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }

                    $('.remodal-wrapper.remodal-is-opened').animate({
                        scrollTop: $('#add-ses').position().top
                    }, 800);
                }
            });
            $(validationBlock).children('li').remove();
        }
    });

    $('body').on('click', '.delete_ses', function () {
        $('.modal-header h4').text('Ви дійсно бажаєте видалити цю СЕС з портфоліо?');
        var id = $(this).attr('data-id');
        $('body').on('click', '.delete_ses_confirm', function (e) {
                $.ajax({
                    url: deletePortfolioURL + '/' + id,
                    data: {
                        _method: 'DELETE'
                    },
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function (response) {
                        $('#hellopreloader').show();
                    },
                    success: function (response) {
                        $('#hellopreloader').hide();
                        var elem;
                        $('tr[id=' + id + ']').remove();
                        //portfolio home ses map places
                        if(response.is_home) {
                            reloadMarkers(myMarkers4);

                            elem = homePortfolioCoordinates.find(function () {
                                return new google.maps.LatLng(response.lat, response.lng);
                            });
                            homePortfolioCoordinates.remove(elem);
                            
                            setMarkers(homePortfolioCoordinates, response);
                        }
                        else {
                            reloadMarkers(myMarkers5);

                            elem = commercialPortfolioCoordinates.find(function () {
                                return new google.maps.LatLng(response.lat, response.lng);
                            });
                            commercialPortfolioCoordinates.remove(elem);

                            setMarkers(commercialPortfolioCoordinates, response);
                        }
                    },
                    error: function (response) {
                        $('#hellopreloader').hide();
                    }
                });
        });
    });

    $('body').on('click', '.get-portfolio', function () {
        var portfolio_id = $(this).attr('data-id');
        $.ajax({
            url: getPortfolioURL + '/' + portfolio_id,
            method: 'GET',
            success: function (response) {
                setPortfolioModalShow(response);
            }
        })
    });
});

var myMarker, searchBox, modal_map, marker_modal;
$(document).on('closed', '.remodal', function () {
    $('.set-portfolio .set-settings').empty();
    if(marker_modal) {
        marker_modal.setMap(null);
    }
    else if (myMarker) {
        myMarker.setMap(null);
    }
    $('.portfolio_power').val('');
    $('.panel_photo_img').attr('src', '');
    $('.invertor_photo_img').attr('src', '');
    $('.portfolio_subcategory_id option[value=4]').prop('selected', true);
    $(".portfolio_subcategory_id .select2-selection").text('Виберіть тип');
    $('.portfolio_subcategory_type_id').prop('selected', false);
    $(".portfolio_subcategory_type_id .current").text('Виберіть тип');
    $('.portfolio_month').val('');
    $('.portfolio_month .current').text('Місяць');
    $(".portfolio_month .list li").removeClass('selected');
    $('.portfolio_year').val('');
    $('.portfolio_year .current').text('Рік');
    $(".portfolio_year .list li").removeClass('selected');
    $('.pac-input-modal').val('');
    $('.geo_coords_lat-modal').val('');
    $('.geo_coords_lng-modal').val('');
    $('.portfolio_invertor_id').val('');
    $('.portfolio_invertor_id .select2-selection').text('Місяць');
    $('.portfolio_panel_id').val('');
    $('.portfolio_panel_id .select2-selection').text('Рік');
    $('input[name=portfolio_contractor]').prop('checked', false);
    $('input[name=portfolio_designing]').prop('checked', false);
    $('input[name=portfolio_delivery]').prop('checked', false);
    $('input[name=portfolio_installation]').prop('checked', false);
    $('input[name=portfolio_documentation]').prop('checked', false);
    $('.type-ses-block').hide();
    $('.designing-block').hide();
    $('.contractor-block').hide();
    $('.documentation-block').hide();
    $('.commercial-type-ses-block').hide();
});
$(document).on('opened', '.remodal', function () {
    var subcategory_id = $(".portfolio_subcategory_id").val();
    if(subcategory_id == 1 || subcategory_id == 2) {
        $('.designing-block').hide();
        $('.contractor-block').hide();
        $('.documentation-block').show();
    }
    else if(subcategory_id == 3) {
        $('.designing-block').hide();
        $('.contractor-block').hide();
        $('.documentation-block').hide();
    }
    else if(subcategory_id == 4) {
        $('.portfolio_subcategory_id[value=4]').prop('selected', true);
        $('.designing-block').show();
        $('.contractor-block').show();
        $('.documentation-block').hide();
    }
    $(".portfolio_month").select2({
        placeholder: "Місяць",
        width: '100%'
    });
    $(".portfolio_year").select2({
        placeholder: "Рік",
        width: '100%'
    });
    $(".portfolio_subcategory_id").select2({
        placeholder: "Виберіть тип",
        width: '100%'
    });
    $(".portfolio_panel_id").select2({
        width: '100%'
    });
    $(".portfolio_invertor_id").select2({
        width: '100%'
    });

    window.dispatchEvent(new Event('resize'));
    lat = $('.geo_coords_lat-modal').val();
    lng = $('.geo_coords_lng-modal').val();
    if (lat == '' && lng == '') {
        lat = 50.4532;
        lng = 30.5255;
    }
    myLatlng = new google.maps.LatLng(lat, lng);
    map.setCenter(myLatlng);
    myMarker = new google.maps.Marker({
        //расположение на карте
        position: myLatlng,
        //То что мы увидим при наведении мышкой на маркер
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    var geocoder = new google.maps.Geocoder;
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        $('.error-block-modal').hide();
        $('#ses-confirm').prop('disabled', false);

        if (places.length == 0) {
            return;
        }
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            myMarker.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
        latlng = myMarker.getPosition();
        $('.geo_coords_lat-modal').val(latlng.lat());
        $('.geo_coords_lng-modal').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();
        geocodeLatLngModal(geocoder, map);
    });
    myMarker.addListener('dragend',  function () {
        latlng = myMarker.getPosition();
        $('.geo_coords_lat-modal').val(latlng.lat());
        $('.geo_coords_lng-modal').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();
        geocodeLatLngModal(geocoder, map);
    });
    myMarker.setMap(map);
});

var N = 50.4532, E = 30.5255;
function init_map() {

    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(N, E),
        scrollwheel: false,
        //увеличение под которым будет карта, от 0 до 18
        // 0 - минимальное увеличение - карта мира
        // 18 - максимально детальный масштаб
        zoom: 8,
        //Тип карты - обычная дорожная карта
        mapTypeId: 'hybrid',
        mapTypeControl:false,
        streetViewControl: false
    };
    //Инициализируем карту
    map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    modal_map = new google.maps.Map(document.getElementById('map-1'), mapOptions);
}

function geocodeLatLngModal(geocoder, map) {
    var latlng = {lat: parseFloat(N), lng: parseFloat(E)};
    var address = '', city = '', city_format = '', admin_region = '', admin_region_format = '', street = '', number = '';
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            $('.error-block-modal').hide();
            $('#ses-confirm').prop('disabled', false);
            for (var i = 0; i < results.length; i++) {
                if (results[i].types[0] === 'administrative_area_level_1') {
                    var region = results[i].formatted_address.split(' ')[0];
                    $('.portfolio-region').val(region);
                }
            }
            if (results[0]) {
                for (var i = 0; i < results[0].address_components.length; i++) {
                    if (results[0].address_components[i].types[0] == 'locality') {
                        city = results[0].address_components[i].long_name;
                        city_format = city + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'administrative_area_level_1') {
                        admin_region = results[0].address_components[i].long_name;
                        admin_region_format = admin_region + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'route') {
                        street = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'street_number') {
                        number = results[0].address_components[i].long_name;
                    }
                }
                if (street != '') {
                    if (city_format == '') {
                        address = admin_region_format + street + ' ' + number;
                    }
                    else {
                        address = city_format + street + ' ' + number;
                    }
                }
                else {
                    if (city_format == '') {
                        address = admin_region;
                    }
                    else {
                        address = city;
                    }
                }
                map.setCenter(latlng);
                $('#pac-input').val(address);
            } else {
                window.alert('Немає результатів');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}

function setMarkers(markersArray, response) {
    //portfolio home ses map places
    if(response.is_home) {
        if(markersArray.length) {
            var homeBounds = new google.maps.LatLngBounds();
            $.each(markersArray, function (key, value) {
                if (value['lat'] != null && value['lng'] != null) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(value['lat'], value['lng']),
                        map: map4,
                        icon: markerSrc
                    });
                    homeBounds.extend(marker.getPosition());
                }

                myMarkers4.push(marker);
            });
            map4.fitBounds(homeBounds);

            var homeListener = google.maps.event.addListener(map4, "idle", function () {
                if (map4.getZoom() > 16) map4.setZoom(6);
                google.maps.event.removeListener(homeListener);
            });
        }
    }
    else {
        if(markersArray.length) {
            var comercialBounds = new google.maps.LatLngBounds();
            $.each(markersArray, function (key, value) {
                if (value['lat'] != null && value['lng'] != null) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(value['lat'], value['lng']),
                        map: map5,
                        icon: markerSrc
                    });
                    comercialBounds.extend(marker.getPosition());
                    myMarkers5.push(marker);
                }
            });
            map5.fitBounds(comercialBounds);

            var commercialListener = google.maps.event.addListener(map5, "idle", function () {
                if (map5.getZoom() > 16) map5.setZoom(6);
                google.maps.event.removeListener(commercialListener);
            });
        }
    }
}

function reloadMarkers(markers) {

    // Loop through markers and set map to null for each
    for (var i=0; i<markers.length; i++) {

        markers[i].setMap(null);
    }

    // Reset the markers array
    markers = [];
}

function setPortfolioModal(response) {
    $('.portfolio_power').val(response.power);
    $('.panel_photo_img').attr('src', response.panel_photo_src);
    $('.invertor_photo_img').attr('src', response.invertor_photo_src);
    $('select.portfolio_subcategory_id').val(response.subcategory_id).select2({width: '100%'});
    $('select.portfolio_subcategory_id[value=' + response.subcategory_id + ']').prop('selected', true);
    $('.portfolio_subcategory_type_id').val(response.ground).niceSelect('update');
    $('select.portfolio_subcategory_type_id[value=' + response.ground + ']').prop('selected', true);
    $('select.portfolio_month').val(response.month).select2({width: '100%'});
    $('select.portfolio_month[value=' + response.month + ']').prop('selected', true);
    $('select.portfolio_year').val(response.year).select2({width: '100%'});
    $('select.portfolio_year[value=' + response.year + ']').prop('selected', true);
    $('.pac-input-modal').val(response.address);
    $('.geo_coords_lat-modal').val(response.lat);
    $('.geo_coords_lng-modal').val(response.lng);
    $('select.portfolio_invertor_id').val(response.invertor_id).select2({width: '100%'});
    $('select.portfolio_invertor_id[value=' + response.invertor_id + ']').prop('selected', true);
    $('select.portfolio_panel_id').val(response.panel_id).select2({width: '100%'});
    $('select.portfolio_panel_id[value=' + response.panel_id + ']').prop('selected', true);
    if($('.installation-true').val() == response.installation) {
        $('.installation-true').prop('checked', true);
    }
    else if($('.installation-false').val() == response.installation) {
        $('.installation-false').prop('checked', true);
    }
    if($('.delivery-true').val() == response.delivery) {
        $('.delivery-true').prop('checked', true);
    }
    else if($('.delivery-false').val() == response.delivery) {
        $('.delivery-false').prop('checked', true);
    }
    if($('.designing-true').val() == response.designing) {
        $('.designing-true').prop('checked', true);
    }
    else if($('.designing-false').val() == response.designing) {
        $('.designing-false').prop('checked', true);
    }
    if($('.contractor-true').val() == response.general_contractor) {
        $('.contractor-true').prop('checked', true);
    }
    else if($('.contractor-false').val() == response.general_contractor) {
        $('.contractor-false').prop('checked', true);
    }
    if (response.subcategory_id == 1 || response.subcategory_id == 2) {
        $('.documentation-block').show();
        if ($('.documentation-true').val() == response.documentation) {
            $('.documentation-true').prop('checked', true);
        }
        else if ($('.documentation-false').val() == response.documentation) {
            $('.documentation-false').prop('checked', true);
        }
    }
    if(response.invertor_photo.public == 1) {
        $('input[name=public_invertor_photo]').prop('checked', true);
    }
    else {
        $('input[name=public_invertor_photo]').prop('checked', false);
    }

    if(response.panel_photo.public == 1) {
        $('input[name=public_panel_photo]').prop('checked', true);
    }
    else {
        $('input[name=public_panel_photo]').prop('checked', false);
    }
}

function setPortfolioTable(response) {
    var city, active = '';
    var new_id = response.id;
    var old_id = $('.portfolio_id').val();
    var month = response.month;
    var year = response.year;
    var subcategory = subcategories[response.subcategory_id];
    var subcategory_string = "";
    var commercial_type = '';
    var commercial_type_string = '';
    var power = response.power;
    var address = response.address;
    if (address) {
        city = address.split(',')[0];
    } else {
        city = address;
    }

    var contractor = '<img class="opacity-img" src="' + imgDirectoryPath + '/gen_contractor.png" alt="">',
        designing = '<img class="opacity-img" src="' + imgDirectoryPath + '/designing.png" alt="">',
        install = '<img class="opacity-img" src="' + imgDirectoryPath + '/installation.png" alt="">',
        delivery = '<img class="opacity-img" src="' + imgDirectoryPath + '/delivery.png" alt="">',
        documentation = '<img class="opacity-img" src="' + imgDirectoryPath + '/designing.png" alt="">',
        contractor_string = '',
        designing_string = '',
        documentation_string = '';
    var update_class, update;
    if (response.is_home == true) {
        update_class = 'update-home';
    }
    else {
        update_class = 'update-commercial';
    }
    update = '<td data-title="Редагувати" data-breakpoints="" class="edit_block" style="display: table-cell">' +
        '<a class="open-update-ses ' + update_class + ' site-icon-edit" data-id="' + new_id + '" href="#add-ses"></a>' +
        '</td>';
    if(response.is_home == true) {
        subcategory_string = '<td data-title="Тип" data-breakpoints="" style="display: table-cell">' +
            '<a href="#show-portfolio" class="get-portfolio" data-id="' + new_id + '">' + subcategory + '</a></td>';
    }
    if(response.ground == 1) {
        commercial_type = 'Наземна';
    }
    else {
        commercial_type = 'Дахова';
    }
    if(response.general_contractor == true) {
        contractor = '<img src="' + imgDirectoryPath + '/gen_contractor.png" alt="">';
    }
    if (response.designing == true) {
        designing = '<img src="' + imgDirectoryPath + '/designing.png" alt="">';
    }
    if(response.is_home == false) {
        designing_string = '<td data-title="генпіряд" data-breakpoints="xs" style="display: table-cell">' + designing + '</td>';
        contractor_string = '<td data-title="проект" data-breakpoints="xs" style="display: table-cell">' + contractor + '</td>';
        commercial_type_string = '<td data-title="Тип" data-breakpoints="xs" style="display: table-cell">' +
            '<a href="#show-portfolio" class="get-portfolio" data-id="' + new_id + '">' + commercial_type + '</a></td>';
    }
    if (subcategories[response.subcategory_id] != 'Автономна' && response.documentation == true) {
        documentation = '<img src="' + imgDirectoryPath + '/designing.png" alt="">';
    }
    if (response.is_home == true) {
        documentation_string = '<td data-title="док" data-breakpoints="xs" style="display: table-cell">' + documentation + '</td>';
    }
    if (response.installation == 1) {
        install = '<img src="' + imgDirectoryPath + '/installation.png" alt="">';
    }
    if (response.delivery == 1) {
        delivery = '<img src="' + imgDirectoryPath + '/delivery.png" alt="">';
    }
    if(response.status_id == 2) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="active-portfolio">Активна</p></td>';
    }
    else if(response.status_id == 1) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="on-check-portfolio">На перевірці</p></td>';
    }
    else if(response.status_id == 3) {
        active = '<td data-title="Статус" data-breakpoints="xs sm md" class="application_status footable-last-visible" ' +
            'style="display: table-cell;"><p class="rejected-request">Відхилено</p></td>';
    }

    var portfolioTable = ('<tr class="tr_id" id="' + new_id + '">' +
    '<td data-title="Дата" data-breakpoints="xs" style="display: table-cell">{Month} {Year}</td>' +
    '{Subcategory}' +
    '{CommercialType}' +
    '<td data-title="Потужність" data-breakpoints="" style="display: table-cell">{Power} кВт</td>' +
    '<td data-title="Адреса" data-breakpoints="xs sm md" style="display: table-cell">{City}</td>' +
    '{Contractor}' +
    '{Designing}' +
    '<td data-title="Обл" data-breakpoints="xs" style="display: table-cell">{Delivery}</td>' +
    '<td data-title="Монт" data-breakpoints="xs" style="display: table-cell">{Install}</td>' +
    '{Documentation}' +
    '{Update}' +
    '<td data-title="Видалити" data-breakpoints="" class="remove_block" style="display: table-cell">' +
    '<a class="delete_ses site_icon-delete" data-id="' + new_id + '" data-remodal-target="delete-ses"></a>' +
    '</td>' +
    '{Active}' +
    '</tr>').format({
        Month: month,
        Year: year,
        Subcategory: subcategory_string,
        CommercialType: commercial_type_string,
        Power: power,
        City: city,
        Contractor: contractor_string,
        Designing: designing_string,
        Install: install,
        Delivery: delivery,
        Documentation: documentation_string,
        Update: update,
        Active: active
    });

    if (response.is_home == true) {
        if (new_id != old_id) {
            $('.home-ses-table').append(portfolioTable);
        }
        else {
            $('.home-ses-table tr[id=' + old_id + ']').replaceWith(function () {
                return portfolioTable;
            });
        }
    }
    else {
        if (new_id != old_id) {
            $('.commercial-ses-table').append(portfolioTable);
        }
        else {
            $('.commercial-ses-table tr[id=' + old_id + ']').replaceWith(function () {
                return portfolioTable;
            });
        }
    }
}

String.prototype.format = function () {
    var formatted = this;
    for (var prop in arguments[0]) {
        var regexp = new RegExp('\\{' + prop + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[0][prop]);
    }
    return formatted;
};

Array.prototype.remove = function(value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        // Второй параметр - число элементов, которые необходимо удалить
        return this.splice(idx, 1);
    }
    return false;
};

function setPortfolioModalShow(portfolio) {
    var arr = [];
    if(portfolio.general_contractor == 1) {
        arr.push('Генпідряд');
    }
    if(portfolio.designing == 1) {
        arr.push('Проектування та документація');
    }
    if(portfolio.delivery == 1) {
        arr.push('Поставка обладнання');
    }
    if(portfolio.installation == 1) {
        arr.push('Монтаж');
    }
    if(portfolio.documentation == 1) {
        arr.push('Документальний супровід');
    }
    $('.set-portfolio .modal-header h4').text(portfolio.subcategory.name + ' СЕС, '
        + portfolio.power + ' кВт');
    $('.set-portfolio .set-date').html('<span>Введено в експлуатацію:</span> ' + portfolio.month + ' ' + portfolio.year);
    $('.set-portfolio .set-panel-photo img').attr('src', portfolio.panel_photo_src);
    $('.set-portfolio .set-invertor-photo img').attr('src', portfolio.invertor_photo_src);
    $('.set-portfolio .set-invertor').html('<span>Інвертор:</span> ' + (invertorName[portfolio.invertor_id] ? invertorName[portfolio.invertor_id].title : 'даний бренд було видалено'));
    $('.set-portfolio .set-panel').html('<span>Панель:</span> ' + (panelName[portfolio.panel_id] ? panelName[portfolio.panel_id].title : 'даний бренд було видалено'));
    $('.set-portfolio .set-executor').html('<span>Виконавець:</span> ' + master.user.name);
    for (var i = 0; i<arr.length; i++) {
        $('.set-portfolio .set-settings').append('<li>' + arr[i] + '</li>');
    }
    window.dispatchEvent(new Event('resize'));
    myLatlng = new google.maps.LatLng(portfolio.lat, portfolio.lng);
    modal_map.setCenter(myLatlng);
    marker_modal = new google.maps.Marker({
        //расположение на карте
        position: myLatlng,
        map: modal_map,
        //То что мы увидим при наведении мышкой на маркер
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
}