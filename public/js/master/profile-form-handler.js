$(document).ready(function () {

    $('#step-one-form').on('submit', function (e) {
        e.preventDefault();
        var photo, attr, visibleBlock = true;
        photo = $(this).find('.logoFile img');
        attr = $(photo).attr('src');

        if (attr == '') {
            visibleBlock = false;
        }

        if (visibleBlock) {
            $('#step-one-form').find('.show-error-hidden-file').hide();
            stepByStepAjax('#step-one-form', '#backend_validation_errors', '1', null);
        }
        else {
            $('#step-one-form').find('.show-error-hidden-file').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
    });
    $('#step-two-form').on('submit', function (e) {
        e.preventDefault();
        var blocks = [], attrs = [], photos = [], visibleBlock = true;
        blocks.push($(this).find('.logoFile:visible'));
        for (var i = 0; i < blocks[0].length; i++) {
            photos.push($(blocks[0][i]).find('img'));
            attrs.push($(photos[i]).attr('src'));
        }

        for (var i = 0; i < attrs.length; i++) {
            if (!attrs[i]) {
                visibleBlock = false;
            }
        }

        if (visibleBlock) {
            $('#step-two-form').find('.show-error-hidden-file').hide();
            stepByStepAjax('#step-two-form', '#backend_validation_errors', '1', null);
        }
        else {
            $('#step-two-form').find('.show-error-hidden-file').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
    });
    $('#step-three-form').on('submit', function (e) {
        e.preventDefault();
        stepByStepAjax('#step-three-form', '#backend_validation_errors', '1', null);
    });
    $('#step-four-form').on('submit', function (e) {
        e.preventDefault();
        var blocks = [],
            photos = [],
            attrs = [],
            carPhotoError = false,
            machineryPhotoError = false,
            regionsArr = [],
            regionsCheckedArr = [],
            checkedRegions = true,
            sesArr = [],
            sesCheckArr = [],
            sesChecked = true;
        $('.machinery-photo-error').hide();
        $('.car-photo-error').hide();
        blocks.push($(this).find('.logoFile:visible'));
        for (var i = 0; i < blocks[0].length; i++) {
            photos.push($(blocks[0][i]).find('img'));
            attrs.push($(photos[i]).attr('src'));
        }

        if ($('.photo-car').is(':visible') && !$('#car_photo_img').attr('src')) {
            carPhotoError = true;
            $('.car-photo-error').show();
            $('html, body').animate({
                scrollTop: $('.car-photo-error').offset().top -165
            }, 800);
        }

        if ($('.photo-machinery').is(':visible') && !$('#machinery_photo_img').attr('src')) {
            machineryPhotoError = true;
            $('.machinery-photo-error').show();
            $('html, body').animate({
                scrollTop: $('.machinery-photo-error').offset().top -165
            }, 800);
        }

        for (var i = 1; i < count_regions + 1; i++) {
            regionsArr[i] = document.getElementById(regions[i].slug);
            if ($(regionsArr[i]).prop('checked')) {
                regionsCheckedArr.push($(regionsArr[i]));
            }
        }

        if (regionsCheckedArr.length == 0) {
            checkedRegions = false;
        }

        sesArr.push(document.getElementById('commercial_ses'));
        sesArr.push(document.getElementById('home_ses'));
        for (var i = 0; i < 2; i++) {
            if ($(sesArr[i]).prop('checked')) {
                sesCheckArr.push($(sesArr[i]));
            }
        }

        if (sesCheckArr.length == 0) {
            sesChecked = false;
        }

        if (!carPhotoError && !machineryPhotoError && checkedRegions && sesChecked) {
            $(this).find('.error-block-region').hide();
            $(this).find('.show-error-hidden-file').hide();
            $(this).find('.error-block-ses').hide();
            stepByStepAjax('#step-four-form', '#backend_validation_errors', '1', null);
        }
        else if (!checkedRegions && !sesChecked) {
            $(this).find('.logoFile:visible').
            $(this).find('.error-block-region').show();
            $(this).find('.error-block-ses').show();
            $('html, body').animate({
                scrollTop: $('.show-error-hidden-file').offset().top -165
            }, 800);
        }
        else if (!checkedRegions) {
            $(this).find('.error-block-region').show();
            $(this).find('.show-error-hidden-file').hide();
            $(this).find('.error-block-ses').hide();
            $('html, body').animate({
                scrollTop: $('.error-block-region').offset().top -460
            }, 800);
        }
        else if (!sesChecked) {
            $('#step-four-form').find('.error-block-region').hide();
            $('#step-four-form').find('.show-error-hidden-file').hide();
            $(this).find('.error-block-ses').show();
            // $('html, body').animate({
            //     scrollTop: 500
            // }, 800);
            $('html, body').animate({
                scrollTop: $('.error-block-ses').offset().top -165
            }, 800);

}
})
;
$('#step-five-form').on('submit', function (e) {
    e.preventDefault();
    var home_count = $('.home-ses-table tbody tr').size();
    var commercial_count = $('.commercial-ses-table tbody tr').size();
    var check_home = $('input[name=home_ses]').prop('checked');
    var check_commercial = $('input[name=commercial_ses]').prop('checked');
    if ((home_count < 3 && check_home == true) && (commercial_count < 2 && check_commercial == true)) {
        $('.validation-home').show();
        $('.validation-commercial').show();
        $('html, body').animate({
            scrollTop: $('.home-ses-table').position().top
        }, 800);
    }
    else if (home_count < 3 && check_home == true) {
        $('.validation-home').show();
        $('.validation-commercial').hide();
        $('html, body').animate({
            scrollTop: $('.home-ses-table').position().top
        }, 800);
    }
    else if (commercial_count < 2 && check_commercial == true) {
        $('.validation-commercial').show();
        $('.validation-home').hide();
        $('html, body').animate({
            scrollTop: $('.commercial-ses-table').position().top
        }, 800);
    }
    else {
        stepByStepAjax('#step-five-form', '#backend_validation_errors', '1', null);
    }
});
// todo make this work

$('#add-invertor-confirm').on('click', function (e) {
    e.preventDefault();
    stepByStepAjax('#add-invertor-form', '.validation_errors_modal', '0', 'add-brand-invertor', 'addInvertor');
});
// todo make this work
$('#add-panel-confirm').on('click', function (e) {
    e.preventDefault();
    stepByStepAjax('#add-panel-form', '.validation_errors_modal', '0', 'add-brand-panel', 'addPanel');
});

$('#update-invertor-confirm').on('click', function (e) {
    e.preventDefault();
    updateAjax('#update-invertor-form', 'PUT', '.validation_errors_modal', 'update-brand-invertor', 'updateInvertor');
});
$('#update-panel-confirm').on('click', function (e) {
    e.preventDefault();
    updateAjax('#update-panel-form', 'PUT', '.validation_errors_modal', 'update-brand-panel', 'updatePanel');
});

//todo make this work
$('body').on('click', '.delete-equipment', function (e) {
    var id = $(this).attr('data-id');
    var link = $(this);
    $('body').on('click', '.delete-equipment-confirm', function () {
        var a = deleteEquipment(deleteEquipmentURL + '/' + id);
        if (a) {
            link.closest('.brand-list').find('.open-brand').show();
            link.parent('li').remove();
        }
    });
});
})
;

var goToNextStep = function (step) {
    $('.block.open').removeClass('open');
    $('#step-' + ++step).addClass('open');
    var stepPostion = $('.cabinet-step.open').offset();
    $("html, body").animate({scrollTop: stepPostion.top - 50}, 500);
};

var closeModal = function (modalBlock) {
    var modal = $('[data-remodal-id=' + modalBlock + ']').remodal();
    modal.close();
};

var step1SuccessHandler = function (response, step) {
    goToNextStep(step);
};

var step2SuccessHandler = function (response, step) {
    goToNextStep(step);
};

var step3SuccessHandler = function (response, step) {
    goToNextStep(step);
};

var step4SuccessHandler = function (response, step) {
    goToNextStep(step);
};

var step5SuccessHandler = function (response, step) {
    window.location.href = response;
};

var addInvertorSuccessHandler = function (response, modalBlock) {
    var id = $('#add_invertor_brand_invertor').val();
    var title = $('#add_invertor_brand_invertor option:selected').text();
    $('.invertor ul').append('<li>' +
        '<a href="#view-brands" data-id="' + id + '" class="brand-item">' + title + '</a> ' +
        '<a data-id="' + id + '" class="brand-edit edit open-update-invertor open-modal" ' +
        'href="#update-brand-invertor"></a> ' +
        '<a class="brand-delete delete delete-equipment" href="#modal-confirm-delete" data-id="' + id + '">' +
        '</a></li>');

    if ($("#add_invertor_brand_invertor option").size() == 1) {
        $('.open-add-invertor').hide();
    }
    closeModal(modalBlock);
};

var addPanelSuccessHandler = function (response, modalBlock) {
    var id = $('#add_panel_brand_panel').val();
    var title = $('#add_panel_brand_panel option:selected').text();
    $('.sun-panel ul').append('<li>' +
        '<a href="#view-brands" data-id="' + id + '" class="brand-item">' + title + '</a> ' +
        '<a data-id="' + id + '" class="brand-edit edit open-update-panel open-modal" ' +
        'href="#update-brand-panel"></a> ' +
        '<a class="brand-delete delete delete-equipment" href="#modal-confirm-delete" data-id="' + id + '">' +
        '</a></li>');

    if ($("#add_panel_brand_panel option").size() == 1) {
        $('.open-add-panel').hide();
    }
    closeModal(modalBlock);
};

var updateInvertorSuccessHandler = function (response, modalBlock) {
    closeModal(modalBlock);
};

var updatePanelSuccessHandler = function (response, modalBlock) {
    closeModal(modalBlock);
};

var stepByStepAjax = function (formSelector, errorsBlock, checkIfStep, modalBlock, successHandler = null) {
    var form = $(formSelector),
        formData = new FormData(form[0]),
        step = form.data('step'),
        success = false,
        stepOrBlock;

    if (successHandler) {
        successHandler = successHandler + 'SuccessHandler';
        stepOrBlock = modalBlock;
    } else {
        successHandler = 'step' + step + 'SuccessHandler';
        stepOrBlock = step;
    }

    formData.append('profile_stage', step);
    formData.append('master_id', master.id);
    $.ajax({
        url: form.attr('action'),
        data: formData,
        // async: false,
        method: 'POST',
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (response) {
            $('#hellopreloader').show();
        },
        success: function (response) {
            window[successHandler](response, stepOrBlock);
            $('#hellopreloader').hide();
            success = true;
            $(errorsBlock).children('li').remove();
        },
        error: function (response) {
            $('#hellopreloader').hide();
            var errors = response.responseJSON, errorField;
            var validationBlock = $(errorsBlock);
            validationBlock.empty();
            for (errorField in errors) {
                validationBlock.append(
                    $('<li>').text(errors[errorField][0])
                );
            }
            if (checkIfStep == 1) {
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
            } else {
                $('.remodal-wrapper.remodal-is-opened').animate({
                    scrollTop: 0
                }, 800);
            }
        }
    });

    return success;
};

$('body').on('click', '.open-update-invertor', function () {
    $('.modal-header h4').text('Редагувати бренд (Інвертори)');
    var equipment_id = $(this).attr('data-id');
    var url = getEquipmentURL + '/' + equipment_id;
    $.ajax({
        url: url,
        method: 'GET',
        data: {
            master_id: master.id
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('.equipment_title').val(equipment_id);
            $('.equipment_title_text').text(response.title);
            $('.guarantee').val(response.pivot.warranty_duration);
            if ($('.no_warranty_increase').val() == response.pivot.warranty_increase) {
                $('.no_warranty_increase').prop('checked', true);
            }
            else if ($('.warranty_increase').val() == response.pivot.warranty_increase) {
                $('.warranty_increase').prop('checked', true);
            }
            for (var i = 1; i < 4; i++) {
                if ($('.warranty_case' + i).val() == response.pivot.warranty_case)
                    $('.warranty_case' + i).prop('checked', true);
            }
            for (var i = 1; i < 5; i++) {
                if ($('.exchange_fund_loc' + i).val() == response.pivot.exchange_fund_loc)
                    $('.exchange_fund_loc' + i).prop('checked', true);
            }
            $('.built').val(response.pivot.built);
        }
    });
});

$('body').on('click', '.open-update-panel', function () {
    $('.modal-header h4').text('Редагувати бренд (Сонячні панелі)');
    var equipment_id = $(this).attr('data-id');
    $('.equipment_title').val(equipment_id);
    var url = getEquipmentURL + '/' + equipment_id;
    $.ajax({
        url: url,
        data: {
            master_id: master.id
        },
        method: 'GET',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('.equipment_title').val(equipment_id);
            $('.equipment_title_text').text(response.title);

            $('.guarantee').val(response.pivot.warranty_duration);
            for (var i = 1; i < 4; i++) {
                if ($('.warranty_case' + i).val() == response.pivot.warranty_case)
                    $('.warranty_case' + i).prop('checked', true);
            }
            for (var i = 1; i < 5; i++) {
                if ($('.exchange_fund_loc' + i).val() == response.pivot.exchange_fund_loc)
                    $('.exchange_fund_loc' + i).prop('checked', true);
            }
            $('.built').val(response.pivot.built);
        }
    });
});

$(document).on('closed', '.remodal', function () {
    $('.brand_invertor').val('');
    $(".brand_invertor .select2-selection").text('Виберіть тип');
    $('.warranty-input').val('');
    $('input[name=warranty_increase]').prop('checked', false);
    $('input[name=warranty_case]').prop('checked', false);
    $('input[name=exchange_fund_loc]').prop('checked', false);
    $('input[name=built]').val('');
});

$('body').on('click', '.brand-item', function () {
    var equipment_id = $(this).attr('data-id');
    var url = getEquipmentURL + '/' + equipment_id;
    $.ajax({
        url: url,
        method: 'GET',
        data: {
            master_id: master.id
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.type_id === 1) {
                $('.title-brand').text('Iнвертор - ' + response.title);
                $('.guarantee').text('Гарантія на інвертор: ' + response.pivot.warranty_duration + ' років.');
            }
            else {
                $('.title-brand').text('Cонячну панель - ' + response.title);
                $('.guarantee').text('Гарантія на сонячну панель: ' + response.pivot.warranty_duration + ' років.');
            }
            if (response.pivot.warranty_increase === 1) {
                $('.increase-text').show();
                $('.increase').text('Так').show();
            }
            else if (response.pivot.warranty_increase === 0) {
                $('.increase-text').show();
                $('.increase').text('Ні').show();
            }
            else {
                $('.increase-text').hide();
                $('.increase').hide();
            }
            $('.repair').text(guarantee_repairs[response.pivot.warranty_case]);
            $('.fund').text(exchange_funds[response.pivot.exchange_fund_loc]);
            $('.count_sun_station').text(response.pivot.built);
        }
    });
});

var updateAjax = function (formSelector, method, errorsBlock, modalBlock) {
    var form = $(formSelector),
        success = true,
        formData = form.serializeArray();
    formData.push({name: '_method', value: method}, {name: 'master_id', value: master.id});
    $.ajax({
        url: form.attr('action'),
        data: formData,
        method: 'POST',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function (response) {
            $('#hellopreloader').show();
        },
        success: function (response) {
            var modal = $('[data-remodal-id=' + modalBlock + ']').remodal();
            modal.close();
            $('#hellopreloader').hide();
        },
        error: function (response) {
            success = false;
            var errors = response.responseJSON, errorField;
            var validationBlock = $(errorsBlock);
            validationBlock.empty();
            for (errorField in errors) {
                validationBlock.append(
                    $('<li>').text(errors[errorField][0])
                );
            }
            $('.remodal-wrapper.remodal-is-opened').animate({
                scrollTop: $('#' + modalBlock).position().top
            }, 800);
        }
    });

    return success;
};

var deleteEquipment = function (url) {
    var success = false;
    $.ajax({
        url: url,
        async: false,
        data: {
            _method: 'DELETE',
            master_id: master.id
        },
        method: 'POST',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.status == 'success')
                success = true;
        },
        error: function (response) {
            // var errors = response.responseJSON, errorField;
            // var validationBlock = $(errorsBlock);
            // validationBlock.empty();
            // for (errorField in errors) {
            //     validationBlock.append(
            //         $('<li>').text(errors[errorField][0])
            //     );
            // }
            // $('html, body').animate({
            //     scrollTop: 0
            // }, 800);
        }
    });

    return success;
};




