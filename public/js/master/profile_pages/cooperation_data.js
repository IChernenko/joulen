$(document).ready(function () {
    $('body').on('click', 'input[name=legal]', function () {
        if ($('input[name="legal"]:checked').val() == 1) {
            $('.codeLegal').css('display', 'flex');
        }
        else {
            $('input[name="edrpou"]').val('');
            $('.codeLegal').hide();
        }
    });

    $('body').on('click', 'input[name=physical]', function () {
        if ($('input[name="physical"]:checked').val() == 1) {
            $('.codeFOP').css('display', 'flex');
        }
        else {
            $('input[name="ipn"]').val('');
            $('.codeFOP').hide();
        }
    });
});

$('body').on('click', function (event) {

    if( $(window).width() <= 992 && $(event.target).closest('.sidebar').length == 0){
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
        $('.sidebar').removeClass('hidden');
        if($(event.target).closest('.toggle-sidebar').length == 0){
            $('.cabinet-page').removeClass('sidebar-hidden');
        }
    }
});

$( window ).scroll(fixedSidebar());

fixedSidebar();
function fixedSidebar() {
    var scroll = $(document).scrollTop();
    if (scroll > 100) {
        $('#mainMenu').css('position', 'fixed');
    } else {
        $('#mainMenu').css('position', 'absolute');
    }
}
