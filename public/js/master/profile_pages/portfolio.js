$(document).ready(function () {
     initMap();
});

var myMarkers4 = [], myMarkers5 = [];
function initMap() {
    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        // Центр Украины
        center: new google.maps.LatLng(49.0383462, 31.4489168),
        scrollwheel: false,
        //увеличение под которым будет карта, от 0 до 18
        // 0 - минимальное увеличение - карта мира
        // 18 - максимально детальный масштаб
        zoom: 6,
        //Тип карты - обычная дорожная карта
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
    };
    map4 = new google.maps.Map(document.getElementById('gmap-4'), mapOptions);
    map5 = new google.maps.Map(document.getElementById('gmap-5'), mapOptions);

    if(homePortfolioCoordinates.length) {
        var homeBounds = new google.maps.LatLngBounds();
        $.each(homePortfolioCoordinates, function (key, value) {
            if (value['lat'] != null && value['lng'] != null) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(value['lat'], value['lng']),
                    map: map4,
                    icon: markerSrc
                });
                homeBounds.extend(marker.getPosition());
            }
            myMarkers4.push(marker);
        });
        map4.fitBounds(homeBounds);

        var homeListener = google.maps.event.addListener(map4, "idle", function () {
            if (map4.getZoom() > 16) map4.setZoom(6);
            google.maps.event.removeListener(homeListener);
        });
    }

    if(commercialPortfolioCoordinates.length)  {
        var comercialBounds = new google.maps.LatLngBounds();
        $.each(commercialPortfolioCoordinates, function (key, value) {
            if (value['lat'] != null && value['lng'] != null) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(value['lat'], value['lng']),
                    map: map5,
                    icon: markerSrc
                });
                comercialBounds.extend(marker.getPosition());
            }
            myMarkers5.push(marker);
        });
        map5.fitBounds(comercialBounds);

        var commercialListener = google.maps.event.addListener(map5, "idle", function () {
            if (map5.getZoom() > 16) map5.setZoom(6);
            google.maps.event.removeListener(commercialListener);
        });
    }
}

function readURL(input, img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            img.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//input file changed
$(document).on('change', '.logoFile input',  function() {
    var img = $(this).closest('.logoFile').find('img');

    readURL(this, img);
});

//add photo
$(document).on('click','.site_icon-download', function() {
    $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
    var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
    hiddenInput.val('0');
});

//delete photo
$(document).on('click','.site_icon-delete', function(e) {
    e.preventDefault();

    var input = $(this).closest('.logoImgBlock').find('.logoFile input');
    var hiddenInput = $(this).next('input');
    hiddenInput.val('1');
    var img = $(this).closest('.logoImgBlock').find('.logoFile img');
    input.replaceWith(input.val('').clone(true));
    img.attr('src', '');
});

$('.open-modal').on('click', function () {
    $('.validation_errors_modal').empty();
});

if($('.home-ses-table td').length != 0){
    $('.home-ses-table').footable();
}

if($('.commercial-ses-table td').length != 0){
    $('.commercial-ses-table').footable();
}