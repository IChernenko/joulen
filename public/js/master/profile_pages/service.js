$(document).ready(function () {
    $('.custom_select').niceSelect();

    $('#service').on('submit', function (e) {
        var photos = [],
            attrs = [],
            visibleBlock = true,
            regionsArr = [],
            regionsCheckedArr = [],
            checkedRegions = true;
        photos.push($(this).find('.logoFile img:visible'));
        for(var i = 0; i < photos[0].length; i++) {
            attrs.push($(photos[0][i]).attr('src'));
        }

        for(var i = 0; i < attrs.length; i++) {
            if(!attrs[i]) {
                visibleBlock = false;
            }
        }

        for(var i = 1; i < count_regions + 1; i++) {
            regionsArr[i] = document.getElementById(regions[i].slug);
            if($(regionsArr[i]).prop('checked')) {
                regionsCheckedArr.push($(regionsArr[i]));
            }
        }

        if(regionsCheckedArr.length == 0) {
            checkedRegions = false;
        }

        if(visibleBlock && checkedRegions) {
            $(this).find('.error-block-region').hide();
            $(this).find('.show-error-hidden-file').hide();
            $(this).find('.error-block-ses').hide();
        }
        else if(!visibleBlock && !checkedRegions) {
            e.preventDefault();
            $(this).find('.show-error-hidden-file').show();
            $(this).find('.error-block-region').show();
            $(this).find('.error-block-ses').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
        else if(!checkedRegions) {
            e.preventDefault();
            $(this).find('.error-block-region').show();
            $(this).find('.show-error-hidden-file').hide();
            $(this).find('.error-block-ses').hide();
            $('html, body').animate({
                scrollTop: $('.error-block-region').offset().top -460
            }, 800);
        }
        else if(!visibleBlock) {
            e.preventDefault();
            $(this).find('.error-block-region').hide();
            $(this).find('.show-error-hidden-file').show();
            $(this).find('.error-block-ses').hide();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
    });

    $('body').on('click', 'input[name="exchange_fund"]', function () {
        if ($('input[name="exchange_fund"]:checked').val() == 1) {
            $('.brandName').show();
        }
        else {
            $('textarea[name="exchange_fund_text"]').val('');
            $('.brandName').hide();
        }
    });

    $('body').on('click', 'input[name="car"]', function () {
        if ($('input[name="car"]:checked').val() == 1) {
            $('.photo-car').show();
        }
        else {
            var input = $('.photo-car').find('.logoFile input');
            var img = $('.photo-car').find('.logoFile img');
            input.replaceWith(input.val('').clone(true));
            img.attr('src', '');
            $('.photo-car').hide();
        }
    });

    $('body').on('click', 'input[name="machinery"]', function () {
        if ($('input[name="machinery"]:checked').val() == 1) {
            $('.photo-machinery').show();
        }
        else {
            var input = $('.photo-machinery').find('.logoFile input');
            var img = $('.photo-machinery').find('.logoFile img');
            input.replaceWith(input.val('').clone(true));
            img.attr('src', '');
            $('.photo-machinery').hide();
        }
    });

    $('body').on('click', 'input[name="home_ses"]', function () {
        if ($('input[name="home_ses"]').prop('checked') === true) {
            $('.contract').show();
        }
        else {
            $('.contract').hide();
        }
    });

    $('body').on('click', 'input[name="commercial_ses"]', function () {
        if ( $('input[name="commercial_ses"]').prop('checked') === true) {
            $('.projectInfo').show();
            $('.machinery-block').show();
        }
        else {
            $('.projectInfo').hide();
            $('.machinery-block').hide();
        }
    });

    if($('input[type=checkbox]').attr('disabled') == true) {
        $('.tooltip-checkbox').hide();
    }

    var listPanel = [],
        listInvertor = [];
    $('.sun-panel ul li a.brand-item').each(function(){
        listPanel.push($(this).text());
    });
    $('#add_panel_brand_panel option').each(function () {
        for(var i = 0; i < listPanel.length; i++) {
            if ($(this).text() == listPanel[i]) {
                $(this).remove();
            }
        }
    });
    if($('#add_panel_brand_panel option').size() == 0) {
        $('.open-add-panel').hide();
    }
    $('body').on('click', '.open-add-panel', function () {
        $('.modal-header h4').text('Додати бренд (Сонячні панелі)');
        type = $(this).attr('data-id');
        $('input[name=equipment_type]').val(type);
        var url = getBrandsURL+'/'+type;
        $.ajax({
            url: url,
            method: 'GET',
            data: {
                master_id: master.id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#add_panel_brand_panel').empty();
                $('#add_panel_brand_panel').append($('<option selected value="">Не вказано</option>'));
                for(var i = 0; i < response.length; i++) {
                    $('#add_panel_brand_panel').append($('<option value="' + response[i].id + '">' + response[i].title + '</option>'));
                }
                $("#add_panel_brand_panel").select2({
                    width: '50%'
                });
            }
        });
    });

    $('.invertor ul li a.brand-item').each(function(){
        listInvertor.push($(this).text());
    });
    $('#add_invertor_brand_invertor option').each(function () {
        for(var i = 0; i < listInvertor.length; i++) {
            if ($(this).text() == listInvertor[i]) {
                $(this.remove());
            }
        }
    });
    if($('#add_invertor_brand_invertor option').size() == 0) {
        $('.open-add-invertor').hide();
    }
    $('body').on('click', '.open-add-invertor', function () {
        $('.modal-header h4').text('Додати бренд (Інвертори)');
        type = $(this).attr('data-id');
        $('input[name=equipment_type]').val(type);
        var url = getBrandsURL+'/'+type;
        $.ajax({
            url: url,
            method: 'GET',
            data: {
                master_id: master.id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#add_invertor_brand_invertor').empty();
                $('#add_invertor_brand_invertor').append($('<option selected value="">Не вказано</option>'));
                for(var i = 0; i < response.length; i++) {
                    $('#add_invertor_brand_invertor').append($('<option value="' + response[i].id + '">' + response[i].title + '</option>'));
                }
                $("#add_invertor_brand_invertor").select2({
                    width: '50%'
                });
            }
        });
    });

    $('body').on('mouseover', '.tooltip-checkbox', function () {
        $('.tooltip-checkbox input').attr('disabled', true);
        $('select.disabled-year').attr('disabled', true).select2({
            width: '100%'
        });
        $('.tooltip-checkbox a').removeClass('remove-avatar');
        $('.tooltip-checkbox a').on('click', function (e) {
            e.preventDefault();
        });
    });
    $('body').on('mouseout', '.tooltip-checkbox', function () {
        $('.tooltip-checkbox input').attr('disabled', false);
        $('select.disabled-year').attr('disabled', false).select2({
            width: '100%'
        });
        $('.tooltip-checkbox a').addClass('remove-avatar');
    });
});

//Это класс для удобного манипулирования местами
function Place(name, latitude, longitude, description) {
    this.name = name;  // название
    this.latitude = latitude;  // широта
    this.longitude = longitude;  // долгота
    this.description = description;  // описание места
}

//from input to photo block
function readURL(input, img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            img.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//input file changed
$(document).on('change', '.logoFile input',  function() {
    var img = $(this).closest('.logoFile').find('img');

    readURL(this, img);
});

//add photo
$(document).on('click','.site_icon-download', function() {
    $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
    var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
    hiddenInput.val('0');
});

//delete photo
$(document).on('click','.remove-avatar', function(e) {
    e.preventDefault();

    var input = $(this).closest('.logoImgBlock').find('.logoFile input');
    var hiddenInput = $(this).next('input');
    hiddenInput.val('1');
    var img = $(this).closest('.logoImgBlock').find('.logoFile img');
    input.replaceWith(input.val('').clone(true));
    img.attr('src', '');
});

$('.open-modal').on('click', function () {
    $('.validation_errors_modal').empty();
});

//input[type=file]
$('.input-file input').on('change',function () {
    var filename = $(this).val();
    if (filename.substring(3,11) == 'fakepath') {
        filename = filename.substring(12);
    }
    $('.file-name').html(filename);
});

$('#company-contract-delete').on('click', function () {
    $('.file-name').html('');
    $('.input-file input').val('');
});
//end input[type=file]

$('body').on('click', function (event) {

    if( $(window).width() <= 992 && $(event.target).closest('.sidebar').length == 0){
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
        $('.sidebar').removeClass('hidden');
        if($(event.target).closest('.toggle-sidebar').length == 0){
            $('.cabinet-page').removeClass('sidebar-hidden');
        }
    }
});

$('.select2').select2({
    width: '100%'
}).on("select2:select", function(e) {
    $(this).validate({ validateHiddenInputs: false });//Selected - Hide errors message
});

$( window ).scroll(fixedSidebar());

fixedSidebar();
function fixedSidebar() {
    var scroll = $(document).scrollTop();
    if(scroll > 100){
        $('#mainMenu').css('position','fixed');
    }else{
        $('#mainMenu').css('position','absolute');
    }
}
