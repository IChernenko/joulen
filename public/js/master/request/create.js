$(document).ready(function ($) {
    $('.request_price').on('change', function () {
        var sum = 0;
        $('.request_price').each(function () {
            sum = isNaN(parseInt($(this).val())) ? sum : sum + parseInt($(this).val());
        });

        $('.request_price_sum').html(sum);
    });
});

$('.only-number').on('keypress', function() {
    var that = this;
    setTimeout(function() {
        var res = /[^01234567890]/g.exec(that.value);
        that.value = that.value.replace(res, '');
    }, 0);
});

$(".validate_form").validate({
    lang: 'uk',
    errorElement: 'span'
});

$.validator.addClassRules({

    validate_number:{
        required: true,
        number:true,
        maxlength: 7
    }

});