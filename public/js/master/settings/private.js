$(document).ready(function () {
    $(".delete-master").on('click', confirmdel);

    function confirmdel() {
        if (confirm("Ви дійсно впевнені що хочете видалити обліковий запис?")) {
            $("#delete-master").submit();
        }
    }
});