var swiper = new Swiper('.portfolio-slider', {
    pagination: '.swiper-pagination',
    slidesPerView: 'auto',
    centeredSlides: false,
    paginationClickable: true,
    spaceBetween: 3,
    nextButton: '.control-next',
    prevButton:'.control-prev',
    preloadImages: false,
    lazy: true
});

$(document).ready(function () {
    initialize();
    var data = {};

    var website = master.website;
    var cutWebsite = website.split('http://')[1];
    $('.cut-website').text(cutWebsite);
    $('.master-office-location').on('click', f_acc);
    function f_acc(){
        $('.master-office-location').not($(this).toggleClass('active'));
        $('.master-office-item').not($(this).next()).toggleClass('active');
        $(this).next().slideToggle(500, function () {
            if ($(this).is(':visible'))
                $(this).css('display','flex');
            window.dispatchEvent(new Event('resize'));
            if (masterOffices.length == 1) {
                map1.setCenter(Latlng1);
            }
            else if (masterOffices.length == 2) {
                map1.setCenter(Latlng1);
                map2.setCenter(Latlng2);
            }
            else if (masterOffices.length == 3) {
                map1.setCenter(Latlng1);
                map2.setCenter(Latlng2);
                map3.setCenter(Latlng3);
            }
        });
    }

    $('#client_master_mail').on('click', function () {
        $('.company-name').html($(this).data('name'));
        $('.company-region').html($(this).data('region'));
        $('.company-img img').attr('src', $(this).data('img'));
        data.master_id = $(this).data('id');
        $('.error_block > ul').empty();
    });

    $('#sendMaster').on('click', function () {
        if(typeof window.url === 'undefined')
            window.url = '';

        data.message = $('[data-remodal-id="client_master_mail"] [name="message"]').val();
        data.master_id = $('#client_master_mail').data('id');

        $.ajax({
            method: "POST",
            url: window.url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function(response) {
            var confirm_modal = $('div[data-remodal-id=confirm-mail-to-master]').remodal();
            confirm_modal.open();
            setTimeout(function(){confirm_modal.close();}, 3000);

            $('input:text:not(input#ignoreMe)');
            $(':input:not(input#master_id)').val(''); //clear all input
            $('.error_block > ul').empty();//clear <li> exception

        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            $('.error_block > ul').empty();

            for(value in response) {
                $('.error_block > ul').addClass('alert alert-danger').append(
                    $('<li>').text(response[value][0])
                )
            }
        })
    });
});

$(document).ready(function () {
    var data = {};

    $('#client_master_mail').on('click', function () {
        $('.company-name').html($(this).data('name'));
        $('.company-region').html($(this).data('region'));
        $('.company-img img').attr('src', $(this).data('img'));
        data.master_id = $(this).data('id');
    });

    $('#guestSendMaster').on('click', function () {
        if(typeof window.url === 'undefined')
            window.url = '';

        // if($('*').is($('.recaptcha-checkbox-checked'))) {
        //     $('[data-remodal-id="client_master_mail"] [name="g-recaptcha-response"]').val(1);
        // }

        data.message = $('[data-remodal-id="client_master_mail"] [name="message"]').val();
        data.master_id = $('#client_master_mail').data('id');
        data.user_phone = $('[data-remodal-id="client_master_mail"] [name="user_phone"]').val();
        data.user_email = $('[data-remodal-id="client_master_mail"] [name="user_email"]').val();
        data.user_name = $('[data-remodal-id="client_master_mail"] [name="user_name"]').val();
        data.captcha = $('[data-remodal-id="client_master_mail"] [name="g-recaptcha-response"]').val();

        $.ajax({
            method: "POST",
            url: window.url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function(response) {
            var confirm_modal = $('div[data-remodal-id=confirm-mail-to-master]').remodal();
            confirm_modal.open();
            setTimeout(function(){confirm_modal.close();}, 3000);

            $(':input:not(input#master_id)').val('');
            grecaptcha.reset();
            $('.error_block > ul').empty();

        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            $('.error_block > ul').empty();

            for(value in response) {
                $('.error_block > ul').append(
                    $('<li>').text(response[value][0])
                )
            }
            grecaptcha.reset();
        })
    });
});

function initialize() {
    $(document).ready(function () {
            mapOptions = {
                scrollwheel: false,
                zoom: 17
            };
            if(master.status.slug == 'verified') {
                if (masterOffices.length > 0) {
                    map1 = new google.maps.Map(document.getElementById('gmap-1'), mapOptions);
                    Latlng1 = new google.maps.LatLng(masterOffices[0].lat, masterOffices[0].lng);
                    map1.setCenter(Latlng1);
                    var myMarker1 = new google.maps.Marker({
                        position: Latlng1,
                        map: map1,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: markerSrc
                    });
                }
                if (masterOffices.length > 1) {
                    map2 = new google.maps.Map(document.getElementById('gmap-2'), mapOptions);
                    Latlng2 = new google.maps.LatLng(masterOffices[1].lat, masterOffices[1].lng);
                    map2.setCenter(Latlng2);
                    var myMarker2 = new google.maps.Marker({
                        position: Latlng2,
                        map: map2,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: markerSrc
                    });
                }
                if (masterOffices.length > 2) {
                    map3 = new google.maps.Map(document.getElementById('gmap-3'), mapOptions);
                    Latlng3 = new google.maps.LatLng(masterOffices[2].lat, masterOffices[2].lng);
                    map3.setCenter(Latlng3);
                    var myMarker3 = new google.maps.Marker({
                        position: Latlng3,
                        map: map3,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: markerSrc
                    });
                }
            }
        mapOptions2 = {
            scrollwheel: false,
            center: new google.maps.LatLng(49.0383462, 31.4489168),
            zoom: 5,
            disableDefaultUI: true
        };

        // portfolio map
        var portfolioBounds = new google.maps.LatLngBounds(),
            markerStyle;
        map5 = new google.maps.Map(document.getElementById('gmap-5'), mapOptions2);
        for(var i = 0; i < portfolios.length; i++) {
            if (portfolios.length > 0 && portfolios[i].status_id == 2) {
                map5.setCenter(new google.maps.LatLng(portfolios[0].lat, portfolios[0].lng));
                if (portfolios[i].is_home) {
                    markerStyle = homeMarkerSrc;
                }
                else {
                    markerStyle = commercialMarkerSrc;
                }

                if (portfolios[i].lat != null && portfolios[i].lng != null) {
                    myMarkers5 = new google.maps.Marker({
                        position: new google.maps.LatLng(portfolios[i].lat, portfolios[i].lng),
                        map: map5,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: markerStyle
                    });
                    portfolioBounds.extend(myMarkers5.getPosition());
                }
                map5.fitBounds(portfolioBounds);
                google.maps.event.addListener(map5, "idle", function () {
                    if (map5.getZoom() > 20) map5.setZoom(16);
                });
            }
        }
    });


    for (var i = 0; i < master.regions.length; i++) {
        var regionSlug = master.regions[i].slug;
        $('#' + regionSlug).addClass('active-region');
    }


    // $(".show-registry-document.document-fop").hover(function(){
    //     $('.visible-registry-document.document-fop').css("display", "block");
    // }, function(){
    //     $('.visible-registry-document.document-fop').css("display", "none");
    // });
    // $(".show-registry-document.document-edrpou").hover(function(){
    //     $('.visible-registry-document.document-edrpou').css("display", "block");
    // }, function(){
    //     $('.visible-registry-document.document-edrpou').css("display", "none");
    // });

    // var offset = $('.visible-photo').offset();
    // var topPadding = 0;
    // $(window).scroll(function() {
    //     if ($(window).scrollTop() > offset.top) {
    //         $('.visible-photo').stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
    //     }
    //     else {
    //         $('.visible-photo').stop().animate({marginTop: 0});
    //     }
    // });


}

