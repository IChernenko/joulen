var myLanguage = {
    errorTitle: "Помилка!",
    requiredField: "Обов'язкове поле",
    requiredFields: "Ви не заповнили всі обов*язкові поля",
    badTime: "Ви ввели не коректний час",
    badEmail: "Правильно заповніть це поле",
    badTelephone: "Ви ввели не коректний номер телефону",
    badSecurityAnswer: "Вы задали некорректный ответ на секретный вопрос",
    badDate: "Ви ввели некоректну дату",
    lengthBadStart: "Значение должно быть в диапазоне",
    lengthBadEnd: " символов",
    lengthTooLongStart: "Значение длинее, чем ",
    lengthTooShortStart: "Значение меньше, чем ",
    notConfirmed: "Введённые значения не могут быть подтверждены",
    badDomain: "Некорректное значение домена",
    badUrl: "Некорретный URL",
    badCustomVal: "Правильно заповніть це поле",
    andSpaces: " и пробелы ",
    badInt: "Ви ввели невірне значення",
    badSecurityNumber: "Введённый защитный номер - неправильный",
    badUKVatAnswer: "Некорректный UK VAT номер",
    badStrength: "Пароль не достаточно надёжен",
    badNumberOfSelectedOptionsStart: "Вы должны выбрать как минимум ",
    badNumberOfSelectedOptionsEnd: " ответов",
    badAlphaNumeric: "Значение должно содержать только числа и буквы ",
    badAlphaNumericExtra: " и ",
    wrongFileSize: "Загружаемый файл слишком велик (максимальный размер %s)",
    wrongFileType: "Принимаются файлы следующих типов %s",
    groupCheckedRangeStart: "Выберите между ",
    groupCheckedTooFewStart: "Выберите как минимум ",
    groupCheckedTooManyStart: "Выберите максимум из ",
    groupCheckedEnd: " элемент(ов)",
    badCreditCard: "Номер кредитной карты некорректен",
    badCVV: "CVV номер некорректно",
    wrongFileDim: "Неверные размеры графического файла,",
    imageTooTall: "изображение не может быть уже чем",
    imageTooWide: "изображение не может быть шире чем",
    imageTooSmall: "изображение слишком мало",
    min: "минимум",
    max: "максимум",
    imageRatioNotAccepted: "Изображение с таким соотношением сторон не принимается",
    badBrazilTelephoneAnswer: "Введённый номер телефона неправильный",
    badBrazilCEPAnswer: "CEP неправильный",
    badBrazilCPFAnswer: "CPF неправильный"
};

$.validate({
    language: myLanguage,
    modules: 'file',
    scrollToTopOnError:false,
    onError : function() {
        if($('.hidden-file').hasClass('has-error')) {
            $('.hidden-file.has-error').closest('form').find('.show-error-hidden-file').show();
        }
        setTimeout(function () {
            var errorPosition = $('span.help-block.form-error' || '.validation_errors_modal.backend_validation_errors ').offset();
            $("html, body").animate({ scrollTop: errorPosition.top - 300 }, 500);
        },50);
        // setTimeout(function () {
        //     var errorPosition = $('.validation_errors_modal.backend_validation_errors').offset();
        //     $("html, body").animate({ scrollTop: errorPosition.top - 120 }, 500);
        // },50);

    },
    onSuccess : function ($form) {
        if($('.hidden-file').hasClass('has-success')) {
            $('.hidden-file.has-error').closest('form').find('.show-error-hidden-file').hide();
        }
    }
});