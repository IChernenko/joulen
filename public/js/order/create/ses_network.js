$(function () {
    var handle = $("#custom-handle");
    var slideVal = $('.photocell_power_input').val();
    $("#slider").slider({
        min: 0,
        max: 30,
        step: .5,
        values: [0],
        create: function () {
            handle.text(slideVal);
            $("#slider_value").val(0);
            $(".required_area").text(0);
            $(".photocell_power").text(0);
            $("#custom-handle").text(0);
            $(".photocell_power").text(0);
        },

        slide: function (event, ui) {
            handle.text(ui.value);
            $("#slider_value").val(ui.value);
            $(".required_area").text(ui.value * 7);
            $(".required_area_input").val(ui.value * 7);
            $(".photocell_power").text(ui.value);
            $(".photocell_power_input").val(ui.value);
        }
    });

    $("#slider_value").on("keyup", function () {
        if ($(this).val() <= $("#slider").slider("option", "max") && $(this).val() >= $("#slider").slider("option", "min")) {
            $("#slider").slider({"value": $(this).val()});
            handle.text($(this).val());
            $("#sliderRes").html($(this).val() * 7);
        } else {
            $(this).val("0");
            $("#slider").slider({"value": "0"});
            handle.text("0");
            $("#sliderRes").html("0");
        }
    });

    $("#c1").change(function () {
        if (!this.checked) {
            $('.photocell_power_input').val('');
            $('.required_area_input').val('');
            $("#slider").slider({ values: [0]});
            $(".required_area").text(0);
            $(".photocell_power").text(0);
            $(".parts_comment_input").val('');
            $("#custom-handle").text(0);
        }
    });
});
