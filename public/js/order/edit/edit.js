$('.new_subcategory_id').on('change', function () {
    $('[name="new_subcategory_id"]').val($(this).val());

    $('#change_ses_subcategory').submit();
});

$(document).ready(function () {
    var qq_file_id = 15;
    $('.hidden-order-photos .hidden-order-photo').each(function (i,v) {
        $('.qq-upload-list').append(
            '<div class="uploaded-photo" qq-file-id="' + qq_file_id + '" attachment-id="' + $(this).attr('data-attachment_id') + '">' +
            '<img class="qq-thumbnail-selector" qq-max-size="400" qq-server-scale="" src="' + $(this).attr('data-src') + '">' +
            '<button type="button" class="qq-btn qq-upload-cancel remove-uploaded-photo site_icon-delete delete-uploaded-portfolio-image"></button>' +
            '<div class="status-info">' +
            '<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>' +
            '<span class="qq-upload-spinner-selector qq-upload-spinner qq-hide"></span>' +
            '<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry qq-hide">Повторити</button></div>' +
            '</div>');
        qq_file_id--;
    });

    if ($.find('.uploaded-photo').length >= 5)
        $('.upload_photo_buttons').hide();

    $(document).on("click", ".remove-uploaded-photo", function (e) {
        e.preventDefault();

        if ($.find('.uploaded-photo').length < 5)
            $('.upload_photo_buttons').show();

        var attachmentId = $(this).parent().attr('attachment-id');

        $(this).parent().remove();

        if (attachmentId) {
            $.ajax({
                url: remove_photo_url,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    attachment_id: attachmentId
                },
                success: function (attachmentId) {
                    $('[attachment-id='+ attachmentId +']').remove();

                },
                error: function (response) {

                }
            })
        }

    });

    var is_order_created = false;

    $(document).on('submit', '#update-order-form',function (e) {
        e.preventDefault();

        var $form = $(this);
        var $formData = $(this).serializeArray();
        var redirectUrl;

        function checkPhoto(redirectTo) {
            if($("#trigger-upload").length == 0 || $('.uploaded-photo').length == 0 || $("[class*='qq-file-id']").length == 0){
                $.ajax({
                    method: 'POST',
                    url: userUploadComplete,
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (redirectTo) {
                        window.location.href = redirectTo;
                    }
                })
            } else if ($(".qq-upload-fail").length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
            } else {
                $("#trigger-upload").trigger("click");
            }

        }

        if(is_order_created){
            checkPhoto();
        } else {
            $.ajax({
                url: $(this).attr('action'),
                data: $formData,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    response = JSON.parse(response);

                    is_order_created = true;

                    checkPhoto(response.redirectTo);

                },
                error: function (response) {
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('.backend_validation_errors ul');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }
                    $('html, body').animate({
                        scrollTop: 0
                    }, 800);
                }
            });
        }
    });

});






