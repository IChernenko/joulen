$(document).ready(function () {
    var selectedOption = $('.exploitation option:selected');
    if (selectedOption.val() == 1) {
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_repeat').slideDown("slow");
        $('.house_project').slideDown("slow");
        $('.site_button-orange.disabled').removeClass('disabled');
    }else if(selectedOption.val() == 0){
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_project').slideDown("slow");
        $('.house_repeat').hide();
        $('.site_button-orange.disabled').removeClass('disabled');
    }
});

function energyCategory(option) {
    if ($(option).data('value') == 1) {
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_repeat').slideDown("slow");
        $('.house_project').slideDown("slow");
        $('.site_button-orange.disabled').removeClass('disabled');
    }else if($(option).data('value') == 0){
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_project').slideDown("slow");
        $('.house_repeat').hide();
        $('.site_button-orange.disabled').removeClass('disabled');
    }

}

$('body').on('click', '.exploitation .option', function () {
    energyCategory(this);
});