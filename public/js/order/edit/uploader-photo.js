$('#fine-uploader-manual-trigger').fineUploader({
    template: 'qq-template-manual-trigger',
    request: {
        endpoint: userAttachmentHandler,
    },
    thumbnails: {
        placeholders: {
            waitingPath: waitingPath,
            notAvailablePath: notAvailablePath
        }
    },
    validation: {
        itemLimit: 5,
        sizeLimit: 15000000,
        stopOnFirstInvalidFile:false,
        allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
    },
    text: {
        formatProgress: "{percent}% з {total_size}",
        failUpload: "Збій при завантаженні",
        waitingForResponse: "Завантаження...",
        paused: "Пауза"
    },
    messages: {
        tooManyItemsError: "Завантажити можна не більше {itemLimit} фото.",
        noFilesError: "Немає файлів для завантаження",
        typeError: "{file} має невірний формат. Будь ласка, завантажте фото у форматі: {extensions}.",
        sizeError: "{file} занадто великий, максимальний розмір {sizeLimit}."
    },
    callbacks: {
        onCancel: function () {
            $('.upload_photo_buttons').show();
        },
        onStatusChange: function () {
            var uploadedFileCount = $.find('.uploaded-photo').length;
            if (uploadedFileCount >= 5) {
                $('.upload_photo_buttons').hide();
            }
        },
        onSubmit: function (id, fileName) {
            this.setParams({
                '_token': $('meta[name="csrf-token"]').attr('content')
            });
        },
        onAllComplete: function (successed,failed) {
            if ( failed.length > 0 ) {
                $('.preloader-order-wrap').hide();
            } else {
                $.ajax({
                    method: 'POST',
                    url: userUploadComplete,
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (redirectTo) {
                        window.location.href = redirectTo;

                    }
                })
            }
        },
        onComplete: function(){

        },
        onError: function(id, name, errorReason, xhrOrXdr) {
            if (xhrOrXdr) {
                var errors = JSON.parse(xhrOrXdr.response).qqfile, errorField;
                var validationBlock = $('.backend_validation_errors ul');
                validationBlock.empty();
                for (errorField in errors) {
                    validationBlock.append(
                        $('<li>').text(errors[errorField])
                    );
                }
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
                $('.qq-file-id-' + id).hide();
            }
        }
    },
    autoUpload: false
});

$('#trigger-upload').click(function () {
    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
    addOrderPreloader();
});

function addOrderPreloader() {
    $('body').append($('#preloader-order-template').html());
}


