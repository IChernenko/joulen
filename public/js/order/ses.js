
$(document).ready(function () {
    initialize();

    if($('#location_delivery').is(':hidden')) {
        $(this).find('input[name=city]').prop('disabled', true);
    }
    $('.gmap-block').focusout(function () {
        var lat = $('.geo_coords_lat').val();
        var lng = $('.geo_coords_lng').val();
        var city = $('input[name=city_name]').val();
        var region = $('input[name=region_name]').val();
        if(lat == '' && lng == '') {
            $('.error-input-map').show();
        }
        else {
            $('.error-input-map').hide();

        }
        if(city == '' || region == '') {
            $('.error-input-city').show();
            $('.error-input-city').fadeOut(3000);
        }
        else {
            $('.error-input-city').hide();

        }
    });

    if ($("#c1").is(':checked') || $("#c2").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
    } else {
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
        $('#comm').css('display', 'none');
    }

    if($("#c3").is(':checked')){
        $('#c150').prop('checked', true);
    }else{
        $('#c150').prop('checked', false);
    }

        if($("input[name='ses_price_category']:checked").val() == "4"){
            $("#paramsDetails").show();
        }else{
            $("#paramsDetails").hide();
        }

        $("input[name='ses_price_category']").change(function(){
            if($(this).val() == "4"){
                $("#paramsDetails").show();
            }else{
                $("#paramsDetails").hide();
            }
        });

    // if($('.geo_coords_lat').val('') != '' && $('.geo_coords_lng').val('') != '' && $("input[name='zoom']").val() != ''){
    //     $("#showgmap").trigger('click');
    // }
});


$("#c1").change(function () {
    if (this.checked || $("#c2").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });
    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
    }

    if (!this.checked) {
        $('.photocell_power_input').val('');
        $('.required_area_input').val('');
        $('#powerf').val('');
    }
});

$("#c2").change(function () {
    if (this.checked || $("#c1").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });

    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
    }

    if (!this.checked) {
        var selectedLi = $('.selectregion .list').find('.selected').removeClass('selected focus');
        selectedLi.removeClass('selected focus');
        var firstLi = $('.selectregion .list :first-child');
        var text = firstLi.text();
        firstLi.addClass('selected focus');
        $('#selectregion').val('');
        $('.current').text(text);
        $('#city').val('');
        $('#area_house').val('');
        $('#about_house').val('');
        $('.geo_coords_lat').val('');
        $('.geo_coords_lng').val('');
        clearCoordinates();
    }
});

$("#c3").change(function () {
    if (this.checked || $("#c2").is(':checked') || $("#c1").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });

    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
    }
});
$("#c3").change(function () {
    if($(this).is(':checked')){
        $('#c150').prop('checked', true);
    }else{
        $('#c150').prop('checked', false);
    }
});

///*******************************************


if($('.geo_coords_lat').val() == ''){
   var N = 50.4532;
   var E = 30.5255;
   var ZOOM = 7;

}else {
    var N = $('.geo_coords_lat').val();
    var E = $('.geo_coords_lng').val();
    var ZOOM = +$("input[name='zoom']").val();
}

// Слайдер карты-------------------------
// $("#showgmap").click(function () {
//     $(".gmap-block").slideToggle("slow", function () {
//         if( $('#gmap').html() == '' )
//             initialize();
//         $('.geo_coords_lat').val(N);
//         $('.geo_coords_lng').val(E);
//         $("input[name='zoom']").val(ZOOM);
//     });
// });

$("#clearcords").click(function () {
    clearCoordinates();
});
//Если указаные кординаты

//------------------------------------------
// гугл карта------------------------------------------

var map;
function initialize() {
    //получаем наш div куда будем карту добавлять

    var mapCanvas = document.getElementById('gmap');
    // задаем параметры карты
    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(N, E),
        scrollwheel: false,
        //увеличение под которым будет карта, от 0 до 18
        // 0 - минимальное увеличение - карта мира
        // 18 - максимально детальный масштаб
        zoom: ZOOM,
        //Тип карты - обычная дорожная карта
        mapTypeId: 'hybrid',
        mapTypeControl:false,
        streetViewControl: false
    };
    //Инициализируем карту
    map = new google.maps.Map(mapCanvas, mapOptions);

    //Создание места

    var myPlace = new Place('Киев', N, E, 'Столица Украины');
    //Создание маркера
    var myMarker = new google.maps.Marker({
        //расположение на карте
        position: new google.maps.LatLng(myPlace.latitude, myPlace.longitude),
        map:map,
        //То что мы увидим при наведении мышкой на маркер
        title: myPlace.name,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    //Добавим попап, который будет появляться при клике на маркер
    var infowindow = new google.maps.InfoWindow({
        content: '<h1>' + myPlace.name + '</h1><br/>' + myPlace.description
    });

    var geocoder = new google.maps.Geocoder;
    //makeInfoWindowEvent(map, infowindow, myMarker);

    map.addListener('zoom_changed',  function () {
        var userZoom = map.getZoom();
        console.log(userZoom);
        ZOOM = userZoom;
        $("input[name='zoom']").val(userZoom);
    });
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        $('.error-input-map').hide();

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            myMarker.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
        latlng = myMarker.getPosition();
        $('.geo_coords_lat').val(latlng.lat().toFixed(4));
        $('.geo_coords_lng').val(latlng.lng().toFixed(4));

        N = latlng.lat().toFixed(4);
        E = latlng.lng().toFixed(4);
        geocodeLatLngModal(geocoder, map);

        setCoords();
    });
    myMarker.addListener('dragend',  function () {
        latlng = myMarker.getPosition();
        $('.geo_coords_lat').val(latlng.lat().toFixed(4));
        $('.geo_coords_lng').val(latlng.lng().toFixed(4));

        N = latlng.lat().toFixed(4);
        E = latlng.lng().toFixed(4);
        geocodeLatLngModal(geocoder, map);

        setCoords();
    });
    function setCoords() {
        $('#clearcords').css('display', 'inline');
        $('.geo_coords').text(N + "N" + " " + E + "E");
    }
    setCoords();
}

function geocodeLatLngModal(geocoder, map) {
    var latlng = {lat: parseFloat(N), lng: parseFloat(E)};
    var address = '', city = '', city_format = '', admin_region = '', admin_region_format = '', street = '', number = '';

// set request options
    var responseStyle = 'short'; // the length of the response
    var citySize = 'cities1500'; // the minimal number of citizens a city must have
    var radius = 30; // the radius in KM
    var maxRows = 1; // the maximum number of rows to retrieve
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            $('.error-block-modal').hide();
            $('#ses-confirm').prop('disabled', false);
            if (results[0]) {
                for (var i = 0; i < results[0].address_components.length; i++) {
                    if (results[0].address_components[i].types[0] == 'locality') {
                        city = results[0].address_components[i].long_name;
                        $('input[name=city_name]').val(city);
                        city_format = city + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'administrative_area_level_1') {
                        admin_region = results[0].address_components[i].long_name;
                        var region = results[0].address_components[i].long_name.split(' ')[0];
                        $('input[name=region_name]').val(region);
                        admin_region_format = admin_region + ', ';
                    }
                    else if (results[0].address_components[i].types[0] == 'route') {
                        street = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'street_number') {
                        number = results[0].address_components[i].long_name;
                    }
                }
                if(city == '') {
                    $.ajax({
                        url: geonamesURL + '/findNearbyPlaceNameJSON?lat=' +
                        N + '&lng=' + E + '&style=' + responseStyle + '&cities=' + citySize +
                        '&radius=' + radius + '&maxRows=' +
                        maxRows + '&username=' + username,
                        method: 'POST',
                        success: function (response) {
                            city =  response.geonames[0].name;
                            $('input[name=city_name]').val(city);
                            if (street != '') {
                                address = city + ', ' + street + ' ' + number;
                            }
                            else {
                                address = city;
                            }
                            $('#pac-input').val(address);
                        },
                    });
                }
                if (street != '') {
                    if (city_format == '') {
                        address = admin_region_format + street + ' ' + number;
                    }
                    else {
                        address = city_format + street + ' ' + number;
                    }
                }
                else {
                    if (city_format == '') {
                        address = admin_region;
                    }
                    else {
                        address = city;
                    }
                }
                map.setCenter(latlng);
                $('#pac-input').val(address);
            } else {
                window.alert('Немає результатів');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}

function makeInfoWindowEvent(map, infowindow, marker) {
    //Привязываем событие КЛИК к маркеру
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}
//Это класс для удобного манипулирования местами
function Place(name, latitude, longitude, description) {
    this.name = name;  // название
    this.latitude = latitude;  // широта
    this.longitude = longitude;  // долгота
    this.description = description;  // описание места
}

//Когда документ загружен полностью - запускаем инициализацию карты.
//google.maps.event.addDomListener(window, 'load', initialize);

$('.autonomous-photosell').on('change', function () {
    var autonomousVal = $(this).val();
    $('.squad-info').text((autonomousVal * 7).toFixed(2));
    $('.input-squad').val((autonomousVal * 7).toFixed(2));
});

$('.commercial-photosell').on('change', function () {
    var autonomousVal = $(this).val();
    $('.squad-info').text((autonomousVal * 0.002).toFixed(3));
    $('.input-squad').val((autonomousVal * 0.005).toFixed(3));
});

$('.roof-photosell').on('change', function () {
    var autonomousVal = $(this).val();
    $('.squad-info').text(autonomousVal * 20);
    $('.input-squad').val(autonomousVal * 20);
});

function clearCoordinates() {
    // if (!$('.gmap-block').is(':hidden')) {
    //     $(".gmap-block").slideToggle("slow", function () {
    //     });
    // }
    $('.geo_coords').text('');
    $('#clearcords').css('display', 'none');
    $('.geo_coords_lat').val('');
    $('.geo_coords_lng').val('');
    $("input[name='zoom']").val('');
}