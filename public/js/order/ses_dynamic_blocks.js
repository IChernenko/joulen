$(document).ready(function () {
    var locationDivC1 = $('#location_delivery'),
        locationDivC3 = $('#location_legal'),
        installOnlyPowerBlock = $('#installOnly_power_block'),
        powerContent = installOnlyPowerBlock.clone(true).prop('id', 'power_block'),
        deliveryAppointment = $('#ses-delivery-appointment'),
        installationPowerBlockWrapper = $('#installOnly_power_block_wrapper'),
        legalDynamicDiv = $('#legal_dynamic_blocks'),
        c1WasChecked = false,
        c3WasChecked = false;

    // this block is only for edit, all of the rest code is common for both create and edit views
    if (!$('#c1').is(':checked') && $('#c2').is(':checked')) {
        $('#installOnly_power_block').show();
    }

    if (!$('#c2').is(':checked')) {
        if ($('#c1').is(':checked')) {
            // locationContent = locationContent.clone(true).prop('id', 'location_delivery_supply');
            // locationContent.prependTo(deliveryAppointment);
            // deliveryAppointment.find('select').niceSelect();
            // $('.site_icon-location').text(supplyLocationH3Text);
            // $('#location').detach();
            c1WasChecked = true;
            locationDivC1.show();
            locationDivC3.hide();
            locationDivC3.find('input[name=city]').prop('disabled', true);
            locationDivC1.find('input[name=city]').prop('disabled', false);
            locationDivC1.find('select').niceSelect();
        }

        if (!$('#c1').is(':checked') && $('#c3').is(':checked')) {
            // locationContent = locationContent.clone(true).prop('id', 'location_delivery_legal');
            // locationContent.prependTo(legalDynamicDiv);
            // legalDynamicDiv.find('select').niceSelect();
            powerContent.prependTo(legalDynamicDiv);
            $("#installOnly_power_block").detach();
            $('#power_block').show();
            c3WasChecked = true;
            locationDivC3.show();
            locationDivC3.find('input[name=city]').prop('disabled', false);
            locationDivC3.find('select').niceSelect();
        }

        if ($('#c3').is(':checked')) {
            c3WasChecked = true;
        }
    }
    // end only edit block

    $("#c1").change(function () {
        c1WasChecked = true;

        if (this.checked) {
            locationDivC1.find('input[name=city]').prop('disabled', false);
            if ($("#c2").is(':checked')) {
                $('#installOnly_power_block').hide();
                locationDivC1.hide();
                locationDivC3.hide();
                locationDivC3.find('input[name=city]').prop('disabled', true);
            }
            if (!$("#c2").is(':checked') && !$('#location_delivery_supply').length) {
                // locationContent = locationDiv.clone(true).prop('id', 'location_delivery_supply');
                // locationContent.prependTo(deliveryAppointment);
                // deliveryAppointment.find('select').niceSelect();
                // $('.site_icon-location').text(supplyLocationH3Text);
                // $('#location').detach();
                locationDivC1.show();
                locationDivC3.hide();
                locationDivC3.find('input[name=city]').prop('disabled', true);
                locationDivC1.find('select').niceSelect();
            }
            if (c3WasChecked) {
                $('#power_block').hide();
                locationDivC3.hide();
                locationDivC3.find('input[name=city]').prop('disabled', true);
                // $('#location_delivery_legal').detach();
            }

        }
        else {
            locationDivC1.find('input[name=city]').prop('disabled', true);
            if ($("#c2").is(':checked')) {
                $('#installOnly_power_block').show();
                locationDivC1.hide();
                locationDivC3.find('input[name=city]').prop('disabled', true);
            }
            if ($("#c3").is(':checked') && !$("#c2").is(':checked')) {
                // locationContent = locationDiv.clone(true).prop('id', 'location_delivery_legal');
                // locationContent.prependTo(legalDynamicDiv);
                // legalDynamicDiv.find('select').niceSelect();
                powerContent = $('#installOnly_power_block').clone(true).prop('id', 'power_block');
                powerContent.prependTo(legalDynamicDiv);
                $('#installOnly_power_block').detach();
                // $('#location_delivery_supply').detach();
                // $('#location').detach();
                // $('.site_icon-location').text(locationH3DefaultText);
                $('#power_block').show();
                locationDivC3.show();
                locationDivC3.find('input[name=city]').prop('disabled', false);
                locationDivC3.find('select').niceSelect();
            }
        }
    });

    $("#c2").change(function () {
        if (this.checked) {
            locationDivC1.find('input[name=city]').prop('disabled', true);
            locationDivC3.find('input[name=city]').prop('disabled', true);
            window.dispatchEvent(new Event('resize'));
            myLatlng = new google.maps.LatLng(N, E);
            map.setCenter(myLatlng);
            if (!$("#c1").is(':checked')) {
                $('#installOnly_power_block').show();
            }
            if (c3WasChecked) {
                if (!$('#location').length) {
                    // locationContent = locationContent.clone(true).prop('id', 'location');
                    // locationContent.prependTo(installationWrapper);
                    locationDivC3.hide();
                }
                if (!$('#installOnly_power_block').length) {
                    powerContent = $('#power_block').clone(true).prop('id','installOnly_power_block');
                    powerContent.prependTo(installationPowerBlockWrapper);
                }
                $('#power_block').detach();
                // $('#location_delivery_legal').detach();
            }
            if (c1WasChecked) {
                if (!$('#location').length) {
                    // locationContent = locationContent.clone(true).prop('id', 'location');
                    // locationContent.prependTo(installationWrapper);
                    // $('.site_icon-location').text(locationH3DefaultText);
                    locationDivC1.hide();
                }
                // $("#location_delivery_supply").detach();
            }

        } else {
            // locationContent = locationContent.clone(true).prop('id', 'location_delivery_supply');
            // locationContent.prependTo(deliveryAppointment);
            // deliveryAppointment.find('select').niceSelect();
            // $('.site_icon-location').text(supplyLocationH3Text);
            // $('#location').detach();
            locationDivC1.show();
            locationDivC1.find('select').niceSelect();
            locationDivC3.hide();
            locationDivC3.find('input[name=city]').prop('disabled', true);

            if ($("#c3").is(':checked') && !$("#c1").is(':checked')) {
                // locationContent = locationContent.clone(true).prop('id', 'location_delivery_legal');
                // locationContent.prependTo(legalDynamicDiv);
                // legalDynamicDiv.find('select').niceSelect();
                powerContent =  $('#installOnly_power_block').clone(true).prop('id', 'power_block');
                powerContent.prependTo(legalDynamicDiv);
                $("#installOnly_power_block").detach();
                // $('#location').detach();
                $('#power_block').show();
                locationDivC3.show();
                locationDivC3.find('input[name=city]').prop('disabled', false);
                locationDivC3.find('select').niceSelect();
            }
        }
    });

    $("#c3").change(function () {
        c3WasChecked = true;

        if (this.checked && !$("#c1").is(':checked')
            && !$("#c2").is(':checked') && !$('#location_delivery_legal').length) {
            locationDivC3.show();
            locationDivC3.find('input[name=city]').prop('disabled', false);
            locationDivC3.find('select').niceSelect();
            // locationContent = locationContent.clone(true).prop('id', 'location_delivery_legal');
            // locationContent.prependTo(legalDynamicDiv);
            // legalDynamicDiv.find('select').niceSelect();
            powerContent.prependTo(legalDynamicDiv);
            $("#installOnly_power_block").detach();
            // $('#location').detach();
            // $('.site_icon-location').text(locationH3DefaultText);
            $('#power_block').show();
       }
       else {
            locationDivC3.hide();
            locationDivC3.find('input[name=city]').prop('disabled', true);
        }
    });
});