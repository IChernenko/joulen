
$(".validate_form").validate({
    lang: 'uk',
    errorElement: 'span',
    ignore: ':hidden:not(.section-active .hidden_validate), .ignore',
    focusInvalid: false,
    errorPlacement: function(error, element) {
        if (element.is('select:hidden')) {
            error.insertAfter(element.next('.nice-select'));
        }else if(element.is('input[name=calc]')){
            error.insertAfter(element.parent('.calcData'));
        }else {
            error.insertAfter(element);
        }

        if (element.is('#pac-input')) {
            $('.error-input-map').show();
        }
    },
    invalidHandler: function() {
        setTimeout(function () {
            var position = $('span.error').first().offset().top;
            $('html,body').animate({scrollTop: position - 300},'slow');
        },100);
    },
    rules: {
        house_comment: {
            maxlength: 10000
        },
        parts_comment: {
            maxlength: 10000
        },
        client_comment: {
            maxlength: 10000
        }
    }
});

$.validator.addClassRules({
    validate_required: {
        required: true
    },
    hidden_validate: {
        required: true
    },
    photocell_power_input:{
        required: true,
        min:0.5,
        maxlength: 6
    },
    validate_number:{
        required: true,
        number:true,
        maxlength: 6
    },
    validate_number_3:{
        required: true,
        number:true,
        maxlength: 3
    },
    validate_ses_network:{
        required: true,
        number:true,
        min: 30
    },
    validate_power:{
        required: true,
        number:true,
        min: 1,
        maxlength: 5
    },
    validate_hours:{
        required: true,
        number:true,
        min: 1,
        max:24,
        maxlength: 2
    },
    validate_hours_week:{
        required: true,
        number:true,
        min: 1,
        max:168,
        maxlength: 3
    },
    validate_length:{
        maxlength: 10000
    }
});


    $('.only-letter').on('keypress', function() {
        var that = this;
        setTimeout(function() {
            var res = /[^а-яA-ЯїЇєЄіІёЁ ^a-zA-Z-]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        }, 0);
    });


    $('.only-number').on('keypress', function() {
        var that = this;
        setTimeout(function() {
            var res = /[^01234567890]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        }, 0);
    });
