$(window).scroll(function () {
    var test = $(".table_garbage").offset();
    var test1 = $(window).scrollTop();
    if (test.top - test1 <= 0 && $(window).width() <= 992){
        $(".table_garbage_header").css({"position":"fixed",
            "width":"96%"
        });
    }
    else {
        $(".table_garbage_header").css({"position":"relative",
            "width":"100%"
        });
    }
});
$(document).ready(function () {
    if(typeof open_modal === 'undefined')
        var open_modal = false;

    if(typeof subcategory_id === 'undefined')
        var subcategory_id = 1;

    if(open_modal){
        fill_modal_info($('.place_order[data-subcategory_id=' + subcategory_id + ']'));
        window.location.href = 'waste_audit#modal';
    }
    $('.place_order').on('click', function (e) {
        fill_modal_info($(this));
    });

    function fill_modal_info(obj){
        $('.packages_modal p span').html(obj.data('subcategory_name'));
        $('.packages_modal input[name=subcategory_id]').val(obj.data('subcategory_id'));
    }

    $('.submit').on('click', function (e) {
        e.preventDefault();
        var form = $('#wasteaudit_form');
        $.ajax({
            url: preorderStoreRoute,
            method: 'POST',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: form.serializeArray(),
            success: function (returnedUrl) {
                window.location.href = returnedUrl;
            },
            error: function (response) {
                var errors = response.responseJSON, errorField;
                var validationBlock = $('.backend_validation_errors ul');
                validationBlock.empty();
                for (errorField in errors) {
                    validationBlock.append(
                        $('<li>').text(errors[errorField][0])
                    );
                }
            }
        });
    });

    $(".modal-validate").validate({
        lang: 'uk',
        errorElement: 'span',
        ignore:[]

    });

    $.validator.addClassRules({
        validate_required: {
            required: true
        },
        name_required:{
            required: true,
            maxlength: 150
        },
        email_required:{
            email:true,
            required: true,
            maxlength: 150
        },
        tel_required:{
            required: true,
            minlength: 17
        },
        hidden_validate: {
            required: true
        },
        select_validate: {
            required: true
        },
        photocell_power_input:{
            required: true,
            min:0.5
        },
        password_required:{
            required: true,
            minlength: 6
        },
        pasword_equal:{
            equalTo: ".password_required"
        }

    });
    

});