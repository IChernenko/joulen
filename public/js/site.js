$(document).ready(function () {
    if ($("#reviewSlider").length > 0) {
        (function () {
            var ind, newInd;
            $(".reviewSlide").each(function () {
                $("#reviewSlider-pagination").append("<span />");
            });
            $(".reviewSlide").css("top", "-100%").eq(0).animate({
                "top": "0"
            }, 250);

            $("#reviewSlider-pagination span").eq(0).addClass("active");

            $("#reviewSlider-pagination span").click(function () {
                ind = $("#reviewSlider-pagination span.active").index();
                $("#reviewSlider-pagination span").removeClass("active");
                $(this).addClass("active");
                newInd = $(this).index();

                $(".reviewSlide").eq(ind).animate({"top": "100%"}, 150, function () {
                    $(".reviewSlide").eq(ind).css("top", "-100%");
                    $(".reviewSlide").eq(newInd).animate({
                        "top": "0"
                    }, 250);
                });

            });

            setInterval(function () {
                ind = $("#reviewSlider-pagination span.active").index();
                newInd = ind + 1;
                if (newInd >= $("#reviewSlider-pagination span").length) {
                    newInd = 0;
                }

                $("#reviewSlider-pagination span").removeClass("active").eq(newInd).addClass("active");

                $(".reviewSlide").eq(ind).animate({"top": "100%"}, 150, function () {
                    $(".reviewSlide").eq(ind).css("top", "-100%");
                    $(".reviewSlide").eq(newInd).animate({
                        "top": "0"
                    }, 250);
                });
            }, 4000);

        })();
    }

    (function () {
        if ($("#mainMenu > ul").children("li").length > 2) {
            $("<span id='hamburger' />").prependTo("#mainMenu");
            $("#header").addClass("withHamburger");

            $("#hamburger").click(function () {
                $("#mainMenu > ul").slideToggle(200);
            });
        }
        var url=document.location.href;
        $.each($('.menu-item a'),function(){
            if(this.href == url) {
                $(this).closest('.sub-menu').show();
                $(this).closest('.menu-item').addClass('active');
                $(this).closest('.sub-menu li').addClass('focus');
            }
        });
    })();

    $(".showSubMenu").click(function () {
        if (window.innerWidth < 1024) {
            $(this).parent().children("ul").slideToggle(150);
        }
    });

    $(window).resize(function () {
        if (window.innerWidth > 1024) {
            $("#mainMenu ul ul").removeAttr("style");
        }
    });

    if (typeof lightbox != "undefined") {
        lightbox.option({
            "showImageNumberLabel": false,
            "alwaysShowNavOnTouchDevices": true
        });
    }

    $(".showHideLabel").click(function () {
        $(this).next(".showHideContent").slideToggle(150);
        $(this).parents(".showHideBlock").toggleClass('displayed');
    });

    $(".togglePass").bind("mousedown", function () {
        $(this).addClass("show").prev("input").attr("type", "text");
    }).bind("mouseup", function () {
        $(this).removeClass("show").prev("input").attr("type", "password");
    });

    /* form calc */
    if ($(".powerCalc").length > 0) {
        $(".calcChk").each(function () {
            if ($(this).is(":checked")) {
                $(this).parents(".calcRow").removeClass("disabled").find("input[type=text],select").removeAttr("disabled");
            } else {
                $(this).parents(".calcRow").addClass("disabled").find("input[type=text],select").attr("disabled", "disabled");
            }
        });
        $(".calcRow .styledChk").on("change", function () {
            if ($(this).is(":checked")) {
                $(this).parents(".calcRow").removeClass("disabled").find("input[type=text],select").removeAttr("disabled");
            } else {
                $(this).parents(".calcRow").addClass("disabled").find("input[type=text],select").attr("disabled", "disabled");
            }
        });
    }
    /* end form calc */

    var is16 = false;
    var important_keys = [8, 37, 39, 46];
    var keyCodes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];


    $(".check-email").each(function () {
        $(this).bind("blur.check", function () {
            if ($(this).val().match(/^.{3,}\@.{1,}\..*$/) == null) {
                $(this).css({"border-bottom-color": "red"}).parent().prev().css({"color": "red"});
            } else {
                $(this).removeAttr("style").parent().prev().removeAttr("style");
            }
        });
    });
    if ($('.data-doc').hasClass('section-active')) {
        $('.data-doc').find('.styledChk').attr('checked', true);
    }
    // open hidden section on order create pages
    $("input[data-switchBlock]").each(function () {
        if (!$(this).is(":checked")) {
            $($(this).attr("data-switchblock")).hide().removeClass('section-active');
        } else {
            $($(this).attr("data-switchblock")).show().addClass('section-active');
        }

        if ($("input[data-switchBlock]:checked").length > 0) {
            $('.confirm-btn').removeClass('disabled');
        } else {
            $('.confirm-btn').addClass('disabled');
        }

    }).on("change", function () {
        if (!$(this).is(":checked")) {
            $($(this).attr("data-switchblock")).hide().removeClass('section-active');
        } else {
            $($(this).attr("data-switchblock")).show().addClass('section-active');
        }
        if ($("input[data-switchBlock]:checked").length > 0) {
            $('.confirm-btn').removeClass('disabled');
        } else {
            $('.confirm-btn').addClass('disabled');
        }
        if ($('.data-doc').hasClass('section-active')) {
            $('.data-doc').find('.styledChk').prop('checked', true);
        }
    });


    if (!$("#isCompany").is(":checked")) {
        $(".privateLabel").css("display", "inline-block");
        $(".companyLabel").hide();
    }

    $("#isCompany").change(function () {
        if ($(this).is(":checked")) {
            $(".privateLabel").hide();
            $(".companyLabel").css("display", "inline-block");

        }
        else {
            $(".companyLabel").hide();
            $(".privateLabel").css("display", "inline-block");
        }
    });

    $(".switchLink").bind("click.switch", function () {
        var me = $(this);
        $(this).parents(".contentBlock").fadeOut(150, function () {
            $(me.attr("href")).fadeIn(200);
        });
        return false;
    });

    // $('body').on('click', '.site_icon-download', function () {
    //     // $('#your_photo').trigger('click');
    // });


    // function readURL(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //
    //         reader.onload = function (e) {
    //             $('#invert_photo').attr('src', e.target.result);
    //         };
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }

    // load logo on master create page
    // $('#your_photo').on('change', function () {
    //     readURL(this);
    // });

    // var avatarInput = $('#your_photo');

    // function clearInput() {
    //     avatarInput.replaceWith(avatarInput.val('').clone(true));
    // }
    // $('body').on('click', '.remove-avatar', function (e) {
    //     e.preventDefault();
    //     clearInput();
    //     $('#avatar').attr('src', defaultAvatar);
    // });
    //END load photo


    //remove portfolio item
    $(document).on("click", ".remove-portfolio-item", function (e) {
        e.preventDefault();
        var current = $(this);
        var portfolio_id = $(this).parent().find('.portfolio-id').val();

        $.ajax({
            url: portfolio_delete,
            method: 'POST',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                portfolio_id: portfolio_id
            },
            success: function (response) {
                current.parent().parent().remove();
                if ($('.portfolio-item_new').length == 1) {
                    $('.addObjectPortfolio').removeClass('disabled');
                }
            },
            error: function (response) {

            }
        })
    });


    $(document).on("click", ".remove-uploaded-photo", function (e) {
        e.preventDefault();

        var attachmentId = $(this).parent().attr('attachment-id');

        $(this).parent().remove();
        if(portfolioPhoto === undefined){

        }else{
            $.ajax({
                url: delete_image,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    attachment_id: attachmentId
                },
                success: function (attachmentId) {
                    portfolioPhoto.find($('[attachment-id='+ attachmentId +']')).remove();
                },
                error: function (response) {

                }
            })
        }

    });

    //add object portfolio
    var addportfolioDescr;
    var addportfolioPhoto;

    function appendPortfolio(portfolio_id) {
        $('.addObjectPortfolio ').before(
            '<div class="portfolio-item portfolio-item_new">'
            + '<p class="portfolio-description">'+addportfolioDescr+'</p>'
            + '<div class="upload-photo-block"></div>'
            + '<div>'
            + '<input type="hidden" class="portfolio-id" value="' + portfolio_id + '">'
            + '<a href="javascript:" class="site_icon-edit edit-portfolio" data-modal="#addPortfolio">Редагувати об’єкт</a>'
            + '<a href="#" class="site_icon-delete remove-portfolio-item">Видалити обєкт</a>'
            + '</div>'
            + '</div>'
        );
    }


    function clearDescription() {
        $('.portfolio-descrioption').val('');
    }

    function initUploader(fineClass, onAllComplete, onComplete) {
        $(fineClass).fineUploader({
            template: 'qq-template-manual-trigger',
            request: {
                endpoint: portfolio_attach_image
            },
            validation: {
                itemLimit: 5,
                sizeLimit: 15000000,
                stopOnFirstInvalidFile:false,
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            },
            text: {
                formatProgress: "{percent}% з {total_size}",
                failUpload: "Збій при завантаженні",
                waitingForResponse: "Завантаження...",
                paused: "Пауза"
            },
            messages: {
                tooManyItemsError: "Завантажити можна не більше {itemLimit} фото.",
                noFilesError: "Немає файлів для завантаження",
                typeError: "{file} має невірний формат. Будь ласка, завантажте фото у форматі: {extensions}.",
                sizeError: "{file} занадто великий, максимальний розмір {sizeLimit}."
            },
            callbacks: {
                onError: function(id, name, errorReason, xhrOrXdr) {
                    alert(JSON.parse(xhrOrXdr.response).qqfile[0]);
                },
                onCancel: function () {
                    $(fineClass).find('.upload_photo_buttons').show();
                },
                onStatusChange: function () {
                    var submittedFileCount = this.getUploads({status: qq.status.SUBMITTED}).length;
                    if (submittedFileCount >= 5) {
                        $(fineClass).find('.upload_photo_buttons').hide();
                    }
                },
                onSubmit: function (id, fileName) {
                    this.setParams({
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    });
                    $(fineClass).parents('.modal-portfoilo').find('.error-photo').hide();
                },
                onAllComplete: onAllComplete,
                onComplete: onComplete
            },
            autoUpload: false
        });
    }
    var preloader = $('#preloader-template').html();
    function addUploader(button) {
        var uploadedPhoto = button.parents('.portfolioForm').find('.uploaded-photo');
        uploadedPhoto.each(function () {
            $(this).append(preloader);
        });
    }

    //save portfolio
    $('.addObjectPortfolio').on('click', function (e) {
        e.preventDefault();
        $(this).attr('data-modal', '#addPortfolio');
        var me = $(this);
        $('body').addClass('modal-open');
        $("#modalsWrapper").fadeIn(200, function () {
            $(me.attr("data-modal")).fadeIn(150,function () {
                $(this).find('.save-edit-portfolio').removeClass('save-edit-portfolio').addClass('save-portfolio');
            });
        });
        $('.portfolio-descrioption').next('.error').hide();
        clearDescription();

        var uploaderTrigger = '#fine-uploader-manual-trigger';
        initUploader(uploaderTrigger, function(succeeded, failed) {

            // if all uploaded successful close modal
            if ( failed.length > 0 ) {
                alert('Деякі з ваших фото не були завантаженні.')
            }
            $(".modal").fadeOut(150, function () {
                $("#modalsWrapper").fadeOut(150, function () {
                    $('body').removeClass('modal-open');
                });
            });

            $('.save-portfolio').removeClass('disabled');

        }, function(id, fileName, responseJSON) {
            // if current image downloaded add hidden field with attachment id
            // append only download success images
            if ( responseJSON['success'] === true ) {
                $('[qq-file-id='+ id +']').attr( 'attachment-id' ,responseJSON['attachment_id']);
                addportfolioPhoto.append( $('[qq-file-id='+ id +']').parents('.modal-portfoilo').find('[qq-file-id='+ id +']').clone() );
            }
            $('[qq-file-id='+ id +']').find('.lds-css').remove();
        });


    });
    function modalPortfolioValidate() {
        $( ".portfolio-descrioption" ).keyup(function() {
            if($(this).val() == ''){
                $(this).parents('.modal-portfoilo').find('.error').show();
            }else{
                $(this).parents('.modal-portfoilo').find('.error').hide();
            }
        });
    }
    modalPortfolioValidate();
    $(document).on('click', '.save-portfolio', function (e) {
        e.preventDefault();
        var current = $(this);
        var descr = $('.portfolio-descrioption').val();
        var checkPhoto = $(this).parents('.modal-portfoilo').find('.uploaded-photo').length;
        if (descr == '') {

            $(this).parents('.modal-portfoilo').find('.error-descr').show();

        }else if(checkPhoto == 0){
            $(this).parents('.modal-portfoilo').find('.error-photo').show();
        }else {

            var form = $('.portfolioForm');
            var context = $(this);
            context.addClass('disabled');
            addUploader(current);
            $.ajax({
                url: portfolio_save,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: form.serializeArray(),
                success: function (portfolioId) {
                    var currentDescription = current.parents('.portfolioForm').find('.portfolio-descrioption').val();
                    //add portfolio_id with new block
                    // deleteUploader(context);
                    appendPortfolio(portfolioId);
                    $('input[value='+ portfolioId +']').parents('.portfolio-item').find('.portfolio-description').html(currentDescription);
                    addportfolioPhoto = $('input[value='+ portfolioId +']').parents('.portfolio-item').find('.upload-photo-block');
                    context.parents('.modal-portfoilo').find('.fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                },
                error: function (errpr) {

                }
            });
        }
    });


    //edit portfolio

    var portfolioDescr;
    var portfolioPhoto;
    var array;

    $('body').on('click', '.edit-portfolio', function (e) {
        e.preventDefault();

        portfolioDescr = $(this).parents('.portfolio-item').find('.portfolio-description');
        portfolioPhoto = $(this).parents('.portfolio-item').find('.upload-photo-block');
        // var portfolioIndex = $(this).parents('.portfolio-item').index();
        var me = $(this);
        $('body').addClass('modal-open');
        $("#modalsWrapper").fadeIn(200, function () {
            $(me.attr("data-modal")).fadeIn(150, function () {
                $(this).find('.save-portfolio').removeClass('save-portfolio').addClass('save-edit-portfolio');
            });
        });

        var portfolioId = $(this).parent().find('.portfolio-id').val();

        $("input[name='portfolio_id']").val(portfolioId);
        $('.portfolio-descrioption').val(portfolioDescr.text());
        $('.portfolio-descrioption').next('.error').hide();
        var uploaderTrigger = '#fine-uploader-manual-trigger';

        initUploader(uploaderTrigger, function(succeeded, failed) {
            //if all uploaded successful close modal
            if ( failed.length > 0 ) {
                alert('Деякі з ваших фото не були завантаженні.')
            }
            $(".modal").fadeOut(150, function () {
                $("#modalsWrapper").fadeOut(150, function () {
                    $('body').removeClass('modal-open');
                });
            });
            $('.save-edit-portfolio').removeClass('disabled');

        }, function(id, fileName, responseJSON) {
            //if current image downloaded add hidden field with attachment id
            // append only download success images
            if ( responseJSON['success'] === true ) {
                var portfolioIndex = portfolioDescr.parent().index();
                $('[qq-file-id='+ id +']').attr( 'attachment-id' ,responseJSON['attachment_id']);
                portfolioPhoto.append( $('[qq-file-id='+ id +']').parents('.modal-portfoilo').find('[qq-file-id='+ id +']').clone() );
            }
            $('[qq-file-id='+ id +']').find('.lds-css').remove();
        });

        var qq_file_id = 15;

        $(this).parent().parent().find('.upload-photo-block .uploaded-photo').each(function () {

            var src = $(this).find('img').attr('src');
            var attachmentId = $(this).attr('attachment-id');
            // var uniqueId = portfolioIndex + '' + qq_file_id;
            $('.qq-upload-list').append(
                '<div class="uploaded-photo qq-file-id-' + qq_file_id + '" qq-file-id="' + qq_file_id + '" attachment-id="' + attachmentId + '">' +
                '<img class="qq-thumbnail-selector" qq-max-size="400" qq-server-scale="" src="' + src + '">' +
                '<button type="button" class="qq-btn qq-upload-cancel remove-uploaded-photo site_icon-delete delete-uploaded-portfolio-image"></button>' +
                '<div class="status-info">' +
                '<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>' +
                '<span class="qq-upload-spinner-selector qq-upload-spinner qq-hide"></span>' +
                '<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry qq-hide">Повторити</button></div>' +
                '</div>');

            qq_file_id--;
        });
        var element = $('.qq-upload-list').find('.uploaded-photo');
        array = [];
        element.each(function () {
            array.push($(this).attr('qq-file-id'))
        });
    });

    $('body').on('click', '.save-edit-portfolio', function (e) {

        e.preventDefault();
        var form = $(this).parents('.portfolioForm');
        var context = $(this);
        var checkPhoto = $(this).parents('.modal-portfoilo').find('.uploaded-photo').length;
        var descr = $('.portfolio-descrioption').val();
        if (descr == '') {
            $(this).parents('.modal-portfoilo').find('.error').show();
        }else if(checkPhoto == 0){
            $(this).parents('.modal-portfoilo').find('.error-photo').show();
        }else{
            context.addClass('disabled');
            addUploader(context);
            form.find('.uploaded-photo').each(function () {
                if($(this).attr('qq-file-id') >= 6){
                    $(this).find('.lds-css').remove();
                }
            });
            $.ajax({
                url: portfolio_edit,
                method: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: form.serializeArray(),
                success: function (response) {
                    var hasNew;

                    form.find('.uploaded-photo').each(function () {
                        hasNew = $.inArray( $(this).attr('qq-file-id'), array);
                    });
                    //if uploaded has new foto, upload them
                    if ( hasNew == -1 ) {
                        context.parents('.modal-portfoilo').find('.fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                    } else {
                        $(".modal").fadeOut(150, function () {
                            $("#modalsWrapper").fadeOut(150, function () {
                                $('body').removeClass('modal-open');
                            });
                        });
                        $('.save-edit-portfolio').removeClass('disabled');
                    }
                    portfolioDescr.text(context.parents('.modal-portfoilo').find('.portfolio-descrioption').val());
                    //portfolioPhoto.html(context.parents('.modal-portfoilo').find('.uploaded_photos').html());
                },
                error: function (data) {
                    var response = JSON.parse(data.responseText);
                    $('.save-edit-portfolio').removeClass('disabled');
                    alert(response.description[0]);
                }
            });
        }

    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-top-btn').fadeIn();
        } else {
            $('.scroll-top-btn').fadeOut();
        }
    });



    //Click event to scroll to top
    $('.scroll-top-btn').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });


//open modal

    $(".showModal").bind("click.modal", function () {
        var me = $(this);
        $('body').addClass('modal-open');
        $("#modalsWrapper").fadeIn(200, function () {
            $(me.attr("data-modal")).fadeIn(150, function () {
                var modalHeight = $(me.attr("data-modal")).outerHeight();
                if(modalHeight > $(window).height()){
                    $('.modal').addClass('modal-top');
                }
            });
        });

        return false;
    });

    $('body').on('click', '.closeModal', function () {
        $(this).parents(".modal").fadeOut(150, function () {
            $("#modalsWrapper").fadeOut(150);
            $('body').removeClass('modal-open');
        });
    });

    $('body').on('click', '.closewind', function (e) {
        e.preventDefault();

        $(this).parents(".modal").fadeOut(150, function () {
            $("#modalsWrapper").fadeOut(150, function () {
                $('body').removeClass('modal-open');
            });
        });

    });

    $('body').on('click', '#modalsWrapper', function (event) {
        var modal = $('#modalsWrapper')[0];
        if (event.target == modal) {
            $(".modal").fadeOut(150, function () {
                $("#modalsWrapper").fadeOut(150, function () {
                    $('body').removeClass('modal-open');
                });
            });
        }
    });

    var dialog = $('dialog')[0];
    window.onclick = function (event) {
        if (event.target == dialog) {
            $(".qq-cancel-button-selector").trigger('click');
        }
    };
    //end open modal
    $('body').on('click', '.nice-select .option', function () {
        $(this).parents('.nice-select').prev('select.error').removeClass('error');
        $(this).parents('.nice-select').next('span.error').remove();
    });

    $('#reset-password-link').on('click', function (e) {
        $('.backend_validation_errors ul').empty();
        $('#idmemail').val('');
    });

    $('#password_recovery').on('submit', function (e) {
        e.preventDefault();

        if (!$('#idmemail').val()) {
            $(this).find('.error').show();
        } else {
            var form = $('#password_recovery');
            $.ajax({
                method: "POST",
                url: url,
                data: form.serializeArray(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (response) {
                $('#error_block ul').empty();
                $('#error_block ul').append(
                    $('<li>').text('Посилання для відновлення паролю надіслане на вашу пошту.')
                );
                window.location.href = '#';

            }).fail(function (response) {
                var errors = response.responseJSON, errorField;
                var validationBlock = $('.backend_validation_errors ul');
                validationBlock.empty();
                for (errorField in errors) {
                    validationBlock.append(
                        $('<li>').text(errors[errorField][0])
                    );
                }
            })
        }
    });

    // MAKSIM
    $('.menu-item > a').click(function(){
        $(this).parent().toggleClass('active');
        $(this).parent().find('ul').slideToggle(200);
    });

    $('.toggle-sidebar').click(function() {
        $('.sidebar').toggleClass('hidden');
        $('.cabinet-page').toggleClass('sidebar-hidden');
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
    });

    $('body').on('mouseleave', '.sidebar.hidden', function () {
        $('.sub-menu').each(function () {
           $(this).hide();
        });
        $('.menu-item').each(function () {
           $(this).removeClass('active');
        });

    });


    $('#verification_request_submit').on('click', function (e) {
        e.preventDefault();
        $('#modal-master-verification').show();
    });

    $('#master-verification-confirmed').on('click', function (e) {
       e.preventDefault();
       $('#master-verification-form').submit();
    });
    $('#master-verification-canceled').on('click', function (e) {
        e.preventDefault();
        $('#modal-master-verification').hide();
    });

    $('.open-table').on('click' , function(){
        $(this).toggleClass('arrow-transform');
            $(this).parent().parent().toggleClass('active');
    });

});