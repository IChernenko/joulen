<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'sent' => 'Посилання для відновлення паролю надіслано на вашу пошту',
    'reset' => 'Новий пароль успішно встановлений!',
    'user' => "Користувача з таким email не виявлено",

];
