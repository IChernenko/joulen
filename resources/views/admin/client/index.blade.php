@extends('layouts.master.app')

@section('content')

    <div class="cabinet-page-wrap equipment-wrap">
        {{--<div class="container">--}}
        <div class="add-brand-equipment">
            <h1 class="main-title">
                Список клієнтів
            </h1>
            <!-- /.main-title -->
            <a href="{{ route('admin.client.create') }}" class="add-brand">
                + Додати клієнта
            </a>
            <!-- /.add-brand -->
        </div>
        <!-- /.add-brand -->
        <div class="wrapper-page">
            {{--<div class="search-items">--}}
            <form class="equipment-list search-items" enctype="multipart/form-data" method="POST"
                  action="{{ route('admin.customer.index') }}" id="admin_equipment_filter">
                {{ csrf_field() }}

                <input type="hidden" name="perpage" value="{{ old('perpage', 20) }}">
                <input type="hidden" name="sortfield" value="{{ old('sortfield', 'name') }}">
                <input type="hidden" name="sortorder" value="{{ old('sortorder', 'asc') }}">

                <div class="search-brand-type">
                    <label for="search-brand-type">ПІБ:</label>
                    <select class="select2" name="name" id="search-brand-type">
                        <option value="0" {{ !old('name') ? 'selected' : '' }}>Всі</option>
                        @foreach($names as $name)
                            <option value="{{ $name }}" {{ old('name') == $name ? 'selected' : '' }}>
                                {{ $name }}
                            </option>
                        @endforeach
                    </select>
                    <!-- /# -->
                </div>
                <!-- /.search-brand-type -->
                <div class="search-brand-equipment">
                    <label for="search-brand-equipment">E-mail:</label>
                    <select class="select2" name="email" id="search-brand-equipment">
                        <option value="0" {{ !old('email') ? 'selected' : '' }}>Почніть вводити назву (лат)</option>
                        @foreach($emails as $email)
                            <option value="{{ $email }}" {{ old('email') == $email ? 'selected' : '' }}>{{ $email }}</option>
                        @endforeach
                    </select>
                    <!-- /# -->
                </div>
                <div class="search-brand-equipment">
                    <label for="search-brand-equipment">Телефон:</label>
                    <select class="select2" name="phone" id="search-brand-equipment">
                        <option value="0" {{ !old('phone') ? 'selected' : '' }}>Почніть вводити</option>
                        @foreach($phones as $phone)
                            <option value="{{ $phone }}" {{ old('phone') === $phone ? 'selected' : '' }}>{{ $phone }}</option>
                        @endforeach
                    </select>
                    <!-- /# -->
                </div>
                <!-- /.search-brand-equipment -->
                <div class="accept-btn">
                    {{--<a href="#">Ок</a>--}}
                    <input type="submit" class="btn btn-primary" value="Ок" />
                </div>
            </form>
            <!-- /.accept-btn -->
            {{--</div>--}}
        <!-- /.search-items -->
            @if(count($clients) > 0)
                <table id="myTable" class="equipment-table">
                    <thead class="equipment-header-table">
                    <tr style="width: 100%;">
                        <th class="company-name equipment-item {{ old('sortfield') == 'name' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="name" style="width: 25%">
                            <p>ПІБ: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.company-name -->
                        <th class="equipment-item {{ old('sortfield') == 'email' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="email" style="width: 35%">
                            <p>E-mail: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>

                        <!-- /.number-dealers -->
                        <th class="equipment-item {{ old('sortfield') == 'phone' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="phone" style="width: 25%">
                            <p>Телефон: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.quantity-photo -->
                        <th class="equipment-item {{ old('sortfield') == 'orders_count' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="orders_count" style="width: 15%">
                            <p>Замовлення: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.quantity-reviews -->
                    </tr>
                    </thead>
                    <!-- /.equipment-header-table -->
                    <tbody>
                    @foreach($clients as $client)
                        <tr class="view-equipment">
                            <td class="equipment-item" style="width: 25%">
                                <p class="company-name-title">
                                    ПІБ: <i class="grade demo-icon icon-down-dir"></i>
                                </p>
                                <!-- /.company-name-title -->
                                <a href="{{ route('admin.client.edit', ['id' => $client->client->id]) }}">
                                    {{ $client->name }}</a>
                            </td>
                            <!-- /.company-name -->
                            <td class="equipment-item" style="width: 35%; cursor: inherit;">
                                <p class="dealers-quantity-title">
                                    E-mail: <i class="grade demo-icon icon-down-dir"></i>
                                </p>
                                <!-- /.dealers-quantity-title -->
                                <span>{{ $client->email }}</span>
                            </td>
                            <!-- /.number-dealers -->
                            <td class="equipment-item" style="width: 25%; cursor: inherit;">
                                <p class="quantity-photo-title">
                                    Телефон: <i class="grade demo-icon icon-down-dir"></i>
                                </p>
                                <!-- /.quantity-photo-title -->
                                <span>{{ $client->phone }}</span>
                            </td>
                            <!-- /.quantity-photo -->
                            <td class="equipment-item" style="width: 15%">
                                <p class="quantity-reviews-title">
                                    Замовлення: <i class="grade demo-icon icon-down-dir"></i>
                                </p>
                                <!-- /.quantity-reviews-title -->
                                <a href="{{ route('admin.order.index', ['email' => $client->email]) }}">
                                    {{ $client->client->orders->count() }}</a>
                            </td>
                            <!-- /.quantity-reviews -->
                        </tr>
                    @endforeach
                    <!-- /.view-equipment -->

                    </tbody>
                </table>
            @else
                <span>Клієнтів не знайдено</span>
        @endif
        <!-- /.equipment-table -->
            <div class="pagination-container"> {{ $clients->links() }} </div>
            <!-- /.pagination -->
        </div>
        <!-- /.wrapper-page -->
    {{--</div>--}}
    <!-- /.wrapper-page -->
    </div>

@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/jquery.tablesorter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.nice-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/equipment/index.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
@endpush
@push('styles')
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}" />
@endpush