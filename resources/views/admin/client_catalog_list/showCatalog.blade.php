@extends('layouts.old.app')

@section('site-title', 'Повідомлення (каталог)')

@section('content')
    <div class="container">
        <div class="table_page">
            <h1 class="title">Повідомлення (каталог)</h1>

            <div class="table_wrap">
                <table width="100%" class="tablesorter tablesorter_new admin-table" id="Table">
                    <thead>
                    <tr class="thead_style">
                        <th class="date small_title" rowspan="2"><span>Дата</span> </th>
                        <th class="contact small_title" rowspan="3"><span>Контакти клієнта</span></th>
                        <th class="package small_title" rowspan="2"><span>Отримувач</span></th>
                        <th class="package client-message small_title" rowspan="2">Текст повідомлення</th>
                        <th class="comment small_title" colspan="8">Коментар</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($client_master_emails as $email)
                        <tr>
                            <td>
                                <div class="wrap_date">{{ $email->created_at }}</div>
                            </td>
                            <td>
                                <div class="wrap_name"><h4>{{ $email->user['name']}}</h4></div>
                                <div class="wrap_phone">{{ $email->user_phone }}</div>
                                <div class="wrap_email"><a href="">{{ $email->user_email }}</a></div>
                            </td>
                            <td>
                                <div class="wrap_package">
                                    <a href="{{ route('master.show', ['id' => $email->master->id]) }}">{{ $email->master->user->name }}</a>
                                </div>
                            </td>
                            <td>
                                <div class="wrap_comment">{!! nl2br($email->message) !!}</div>
                            </td>
                            <td>
                                <div class="wrap_comment"><span class="comment">{!! nl2br($email->comment) !!}</span><a href="#modal" class="edit_comment" data-update_route="{{ route('admin.update_catalog', ['id' => $email->id]) }}"></a></div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="control_table_block center">
            <div class="pagesNav textCenter">
                {{ $client_master_emails->links() }}
            </div>
        </div>

    </div>

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush

    @push('footer_scripts')
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script src="{{ asset('js/client_email/index.js') }}"></script>
    @endpush

    @push('modals')
    <div class="remodal audit_modal" data-remodal-id="modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h1>Коментар до листа</h1>
        </p>
        <form method="POST" action="" class="modal-validate" id="comment_form">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <textarea name="comment" class="validate_required" id="comment"></textarea>
            <div class="remodal_footer">
                <input type="submit" value="Зберегти" class="site_button-orange submit">
                <button data-remodal-action="cancel" class="site_button-transperent">Скасувати</button>
            </div>
        </form>
    </div>
    @endpush
@endsection