@extends('layouts.master.app')

@section('content')

    <div class="cabinet-page-wrap">
        <h1 class="main-title">Новий дистриб'ютор</h1>
        <!-- /.main-title -->
        <div class="wrapper-page">
            <form class="form-container" action="{{ route('admin.distributors.store') }}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                @if (count($errors) > 0)
                    <div class="error" id="error_block">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="color: red; list-style: none;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-container-group border-gray">
                    <p class="form-title">Інформація для вашої публічної сторінки</p>
                    <div class="form-item">
                        <label for="name_company">Назва компанії:</label>
                        <input id="name_company" class="border-gray" type="text" name="company_name" value="{{ old('company_name') }}">
                    </div>
                    <div class="form-item">
                        <label for="avatar">Ваш логотип файли jpg та png розміром не менше
                            100х100px</label>
                        <div class="logoImgBlock">
                            <div class="logoFile">
                                <input type="file" class="file-upload hidden_validate_file" id="avatar"
                                       name="avatar" style="display:none;"><br>
                                <img src="{{ asset('images/default_photo.jpg') }}" alt="avatar"
                                     id="master_avatar_img">
                            </div>
                            <div>
                                <div id="master_avatar_download" class="site_icon-download">Завантажити</div>
                            </div>
                            <div>
                                <a href="" id="master-avatar-delete" class="site_icon-delete remove-avatar">Видалити</a>
                                <input type="checkbox" checked id="is_deleted_master_avatar" value=""
                                       name="is_deleted_master_avatar"
                                       style="display:none;">
                            </div>
                        </div>
                    </div>
                    <div class="form-item">
                        <label for="url">URL:</label>
                        <input id="url" class="border-gray" type="text" name="slug" value="{{ old('slug') }}">
                    </div>
                    <div class="form-item">
                        <label for="description-company">Опис компанії:</label>
                        <textarea id="description-company" class="border-gray" rows="6" name="description">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-item">
                        <label for="site">Сайт:</label>
                        <input id="site" class="border-gray" type="text" name="website" value="{{ old('website') }}">
                    </div>
                    <div class="form-item">
                        <label for="corp-email">Корпоративний е-мейл:</label>
                        <input id="corp-email" class="border-gray" type="text"
                               name="corporate_email" value="{{ old('corporate_email') }}">
                    </div>
                    <div class="form-item">
                        <label for="tel">Телефон:</label>
                        <input id="tel" class="border-gray mask-phone" type="text" name="phone" value="{{ old('phone') }}">
                    </div>
                    <div class="form-item">
                        <input id="tel_2" class="secondary_tel" type="checkbox" {{ old('second_phone') ? 'checked' : '' }}>
                        <label class="custom-checkbox border-gray" for="tel_2"></label>
                        <label for="tel_2" class="with-checkbox">Телефон 2:</label>
                        <input id="tel_2" class="border-gray mask-phone" type="text"
                               disabled name="second_phone" value="{{ old('second_phone') }}">
                    </div>
                </div>

                <div class="form-container-group border-gray">
                    <p class="form-title">Контакти менеджера, по роботі з дилерами</p>
                    <div class="form-item">
                        <label for="full_name">ПІБ:</label>
                        <input id="full_name" class="border-gray" type="text" name="manager_name" value="{{ old('manager_name') }}">
                    </div>
                    <div class="form-item">
                        <label for="email">Е-мейл:</label>
                        <input id="email" class="border-gray" type="text" name="manager_email" value="{{ old('manager_email') }}">
                    </div>
                    <div class="form-item">
                        <label for="number">Телефон:</label>
                        <input id="number" class="border-gray mask-phone" type="text"
                               name="manager_phone" value="{{ old('manager_phone') }}">
                    </div>
                    <div class="form-item">
                        <input id="tel_3" class="secondary_tel" type="checkbox" {{ old('manager_second_phone') ? 'checked' : '' }}>
                        <label class="custom-checkbox border-gray" for="tel_3"></label>
                        <label for="tel_3" class="with-checkbox">Телефон 2:</label>
                        <input id="tel_3" class="border-gray mask-phone" type="text"
                               disabled name="manager_second_phone" value="{{ old('manager_second_phone') }}">
                    </div>
                </div>

                <div class="form-container-group border-gray">
                    <p class="form-title">Реєстраційні дані</p>
                    <div class="form-item">
                        <label for="full_name_2">Е-мейл:</label>
                        <input id="full_name_2" class="border-gray" type="text" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-item">
                        <label for="password">Пароль:</label>
                        <input id="password" class="border-gray" type="text" name="password" value="{{ old('password') }}">
                    </div>
                </div>

                <div class="form-container-distributor">

                    <h3>Офіційний дистрибютор брендів:</h3>

                    <div class="dealers">
                        <p class="dealer_type-system">Інертори</p>
                        <ul class="dealers-list border-gray">
                            @foreach($inverters as $inverter)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="inverter{{$loop->index}}" name="equipment[]" value="{{ $inverter->id }}">
                                    <label class="custom-checkbox border-gray" for="inverter{{$loop->index}}"></label>

                                    <label for="inverter{{$loop->index}}">{{ $inverter->title }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="dealers">
                        <p class="dealer_type-system">Панелі</p>
                        <ul class="dealers-list border-gray">
                            @foreach($panels as $panel)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="panel{{$loop->index}}" name="equipment[]" value="{{ $panel->id }}">
                                    <label class="custom-checkbox border-gray" for="panel{{$loop->index}}"></label>

                                    <label for="panel{{$loop->index}}">{{ $panel->title }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <h3>Регіональні дилери:</h3>

                    <div class="dealers">
                        <ul class="dealers-list border-gray">
                            @foreach($masters as $master)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="dealer{{ $loop->index }}" name="masters[]" value="{{ $master->id }}">
                                    <label class="custom-checkbox border-gray" for="dealer{{ $loop->index }}"></label>

                                    <label for="dealer{{ $loop->index }}">{{ $master->user->name }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="dealer-status">
                        <h3>Баланс</h3>
                        <label for="balance">Залишок на балансі:</label>
                        <div class="dealer-status__input">
                            <input id="balance" class="border-gray" name="balance" type="text" value="{{ old('balance', 0) }}">
                            <p class="dealer-status__currency">USD</p>
                        </div>
                        <!-- /.dealer-status__input -->
                    </div>
                    <!-- /.dealer-status -->
                    <div class="dealer-status">
                        <h3>Статус</h3>
                        <div class="dealers-select">
                            <select name="active" id="active" class="select2">
                                <option value="0" {{ old('active', 0) === 0 ? 'selected' : '' }}>Неактивний</option>
                                <option value="1" {{ old('active') === 1 ? 'selected' : '' }}>Активний</option>
                            </select>
                            <!-- /#.select2 -->
                        </div>
                        <!-- /.dealers-select -->
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Зберегти</button>
            </form>

        </div>
        <!-- /.wrapper-page -->
        <!-- /.container -->
    </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/distributor/cabinet-company.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/load-avatar.js') }}"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endpush

@section('site-title')
    Додати дистриб'ютора
@endsection