@extends('layouts.master.app')

@section('content')

    <div class="cabinet-page-wrap">
        <h1 class="main-title">Редагувати дистриб'ютора</h1>
        <!-- /.main-title -->
        <div class="wrapper-page">
            <form class="form-container" action="{{ route('admin.distributors.update', ['id' => $distributor->id]) }}"
                  method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                @if (count($errors) > 0)
                    <div class="error" id="error_block">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="color: red; list-style: none;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @include('layouts.distributor.company-info', ['distributor' => $distributor])

                <div class="form-container-group border-gray">
                    <p class="form-title">Реєстраційні дані</p>
                    <div class="form-item">
                        <label for="full_name_2">Е-мейл:</label>
                        <input type="hidden" name="user_id" value="{{ $distributor->user->id }}">
                        <input id="full_name_2" class="border-gray" type="text" name="email" value="{{ $distributor->user->email }}">
                    </div>
                    <div class="form-item">
                        <label for="password">Новий пароль:</label>
                        <input id="password" class="border-gray" type="text" name="password" value="">
                    </div>
                </div>

                <div class="form-container-distributor">

                    <h3>Офіційний дистрибютор брендів:</h3>

                    <div class="dealers">
                        <p class="dealer_type-system">Інертори</p>
                        <ul class="dealers-list border-gray">
                            @foreach($inverters as $inverter)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="inverter{{$loop->index}}"
                                           name="equipment[]" value="{{ $inverter->id }}"
                                            {{ $distributor->inverters->pluck('id')->contains($inverter->id) ? 'checked' : '' }}>
                                    <label class="custom-checkbox border-gray" for="inverter{{$loop->index}}"></label>

                                    <label for="inverter{{$loop->index}}">{{ $inverter->title }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="dealers">
                        <p class="dealer_type-system">Панелі</p>
                        <ul class="dealers-list border-gray">
                            @foreach($panels as $panel)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="panel{{$loop->index}}"
                                           name="equipment[]" value="{{ $panel->id }}"
                                            {{ $distributor->panels->pluck('id')->contains($panel->id) ? 'checked' : '' }}>
                                    <label class="custom-checkbox border-gray" for="panel{{$loop->index}}"></label>

                                    <label for="panel{{$loop->index}}">{{ $panel->title }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <h3>Регіональні дилери:</h3>

                    <div class="dealers">
                        <ul class="dealers-list border-gray">
                            @foreach($masters as $master)
                                <li class="dealers-list_item">
                                    <input type="checkbox" id="dealer{{ $loop->index }}"
                                           name="masters[]" value="{{ $master->id }}"
                                            {{ $distributor->masters->pluck('id')->contains($master->id) ? 'checked' : '' }}>
                                    <label class="custom-checkbox border-gray" for="dealer{{ $loop->index }}"></label>

                                    <label for="dealer{{ $loop->index }}">{{ $master->user->name }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="dealer-status">
                        <h3>Баланс</h3>
                        <label for="balance">Залишок на балансі:</label>
                        <div class="dealer-status__input">
                            <input id="balance" class="border-gray" name="balance" type="text" value="{{ $distributor->balance }}">
                            <p class="dealer-status__currency">USD</p>
                        </div>
                        <!-- /.dealer-status__input -->
                    </div>
                    <!-- /.dealer-status -->
                    <div class="dealer-status">
                        <h3>Статус</h3>
                        <div class="dealers-select">
                            <select name="active" id="active" class="select2">
                                <option value="0" {{ $distributor->active ? 'selected' : '' }}>Неактивний</option>
                                <option value="1" {{ $distributor->active ? 'selected' : '' }}>Активний</option>
                            </select>
                            <!-- /#.select2 -->
                        </div>
                        <!-- /.dealers-select -->
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Зберегти</button>
            </form>

        </div>
        <!-- /.wrapper-page -->
        <!-- /.container -->
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/distributor/cabinet-company.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/load-avatar.js') }}"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endpush

@section('site-title')
    Редагувати дистриб'ютора
@endsection