@extends('layouts.master.app')

@section('content')

    <div class="cabinet-page-wrap equipment-wrap">
        {{--<div class="container">--}}
            <div class="add-brand-equipment">
                <h1 class="main-title">
                    Обладнання
                </h1>
                <!-- /.main-title -->
                {{--TODO: set page link--}}
                <a href="{{ route('admin.equipment.create') }}" class="add-brand">
                    + Додати бренд
                </a>
                <!-- /.add-brand -->
            </div>
            <!-- /.add-brand -->
            <div class="wrapper-page">
                {{--<div class="search-items">--}}
                    <form class="equipment-list search-items" enctype="multipart/form-data" method="POST"
                          action="{{ route('admin.equipment.index.filter') }}" id="admin_equipment_filter">
                        {{ csrf_field() }}

                        <input type="hidden" name="perpage" value="{{ old('perpage', 20) }}">
                        <input type="hidden" name="sortfield" value="{{ old('sortfield', 'title') }}">
                        <input type="hidden" name="sortorder" value="{{ old('sortorder', 'asc') }}">

                        <div class="search-brand-type">
                            <label for="search-brand-type">Тип:</label>
                            <select class="select2" name="types" id="search-brand-type">
                                <option value="0" {{ !old('types') ? 'selected' : '' }}>Всі</option>
                                @foreach($equipment_types as $type)
                                    <option value="{{ $type->id }}" {{ old('types') == $type->id ? 'selected' : '' }}>
                                        @if($type->slug == 'inverter')
                                            Інвертори
                                        @elseif($type->slug == 'solar_panel')
                                            Панелі
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                            <!-- /# -->
                        </div>
                        <!-- /.search-brand-type -->
                        <div class="search-brand-equipment">
                            <label for="search-brand-equipment">Бренд:</label>
                            <select class="select2" name="brand" id="search-brand-equipment">
                                <option value="0" {{ !old('brand') ? 'selected' : '' }}>Почні вводити назву (лат)</option>
                                @foreach($equipment_brand as $brand)
                                    <option value="{{ $brand->id }}" {{ old('brand') == $brand->id ? 'selected' : '' }}>{{ $brand->title }}</option>
                                @endforeach
                            </select>
                            <!-- /# -->
                        </div>
                        <!-- /.search-brand-equipment -->
                        <div class="accept-btn">
                            {{--<a href="#">Ок</a>--}}
                            <input type="submit" class="btn btn-primary" value="Ок" />
                        </div>
                    </form>
                    <!-- /.accept-btn -->
                {{--</div>--}}
                <!-- /.search-items -->
                @if(count($equipment) > 0)
                <table id="myTable" class="equipment-table">
                    <thead class="equipment-header-table">
                    <tr>
                        <th class="company-name equipment-item {{ old('sortfield') == 'title' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="title">
                            <p>Назва: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.company-name -->
                        <th class="quantity-dealers equipment-item {{ old('sortfield') == 'masters_count' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="masters_count">
                            <p>Дилери: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>

                        <!-- /.number-dealers -->
                        <th class="quantity-photo equipment-item {{ old('sortfield') == 'portfolios_count' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="portfolios_count">
                            <p>Фото: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.quantity-photo -->
                        <th class="quantity-reviews equipment-item {{ old('sortfield') == 'reviews_count' ? old('sortorder', 'desc') : ''}}"
                            data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="reviews_count">
                            <p>Відгуки: <i class="grade demo-icon icon-down-dir"></i></p>
                        </th>
                        <!-- /.quantity-reviews -->
                    </tr>
                    </thead>
                    <!-- /.equipment-header-table -->
                    <tbody>
                    @foreach($equipment as $brand)
                    <tr class="view-equipment ">
                        <td class="company-name equipment-item">
                            <p class="company-name-title">
                                Назва: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.company-name-title -->
                            <a href="{{ route('brand.show', ['equipment_type' => array_search($brand->type->slug, $equipment_slug_mapping), 'brand_slug' => $brand->slug]) }}">
                                {{ $brand->title }}</a>
                        </td>
                        <!-- /.company-name -->
                        <td class="quantity-dealers equipment-item">
                            <p class="dealers-quantity-title">
                                Дилери: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.dealers-quantity-title -->
                            <a href="{{ route('brand.show', ['equipment_type' => array_search($brand->type->slug, $equipment_slug_mapping), 'brand_slug' => $brand->slug]) . '#equipment-dealers' }}">
                                {{ $brand->masters_count }}</a>
                        </td>
                        <!-- /.number-dealers -->
                        <td class="quantity-photo equipment-item">
                            <p class="quantity-photo-title">
                                Фото: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.quantity-photo-title -->
                            <a href="{{ route('brand.show', ['equipment_type' => array_search($brand->type->slug, $equipment_slug_mapping), 'brand_slug' => $brand->slug]) . '#equipment-photos' }}">
                                {{ count($brand->portfolios) }}</a>
                        </td>
                        <!-- /.quantity-photo -->
                        <td class="quantity-reviews equipment-item">
                            <p class="quantity-reviews-title">
                                Відгуки: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.quantity-reviews-title -->
                            <a href="{{ route('brand.show', ['equipment_type' => array_search($brand->type->slug, $equipment_slug_mapping), 'brand_slug' => $brand->slug]) . '#equipment-reviews' }}">
                                {{ $brand->reviews_count }}</a>
                        </td>
                        <!-- /.quantity-reviews -->
                    </tr>
                    @endforeach
                    <!-- /.view-equipment -->

                    </tbody>
                </table>
                @else
                    <span>Обладнання не знайдено</span>
                @endif
                <!-- /.equipment-table -->
                <div class="pagination-container"> {{ $equipment->links() }} </div>
                <!-- /.pagination -->
            </div>
            <!-- /.wrapper-page -->
        {{--</div>--}}
        <!-- /.wrapper-page -->
    </div>

@endsection

@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.nice-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/equipment/index.js') }}"></script>
<script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
@endpush
@push('styles')
{{--<link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>--}}
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}" />
@endpush