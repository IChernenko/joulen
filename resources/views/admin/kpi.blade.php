@extends('layouts.master.app')

@section('site-title', 'Ключові показники ефективності')

@section('content')

    {{--<div class="wrapper efficiency-table-page">--}}
    <div class="cabinet-page-wrap efficiency-table-page">
    <!-- /.cabinet-page-wrap -->
        <div class="container">
            <h1 class="main-title">
                Ключові показники ефективності
            </h1>
            <!-- /.main-title -->
            <div class="wrapper-page">
                <div id="scrollbarX" class="">

                @foreach($statistics as $ym => $data)
                        <table class="efficiency-table-wrap statistic">
                            <thead>
                            <tr>
                                <td class="efficiency-thead-item"></td>
                                <td class="efficiency-thead-item new-orders">
                                    Нові замовлення:
                                </td>
                                <!-- /.new-orders -->
                                <td class="efficiency-thead-item price-offers">
                                    Цінові пропозиції:
                                </td>
                                <!-- /.price-offers -->
                                <td class="efficiency-thead-item messages">
                                    Повідомлення:
                                </td>
                                <!-- /.messages -->
                                <td class="efficiency-thead-item installers">
                                    Інсталятори:
                                </td>
                            </tr>
                            <!-- /.efficiency-thead-item -->
                            </thead>
                            <!-- /.efficiency-table-thead -->
                            <tbody>
                            <tr>
                                <td class="efficiency-table-item">
                                    <p class="month-order">{{ $data['month'] }} <span class="year-order">{{ $data['year'] }}</span></p>
                                </td>
                                <!-- /.table-item -->
                                <td class="efficiency-table-item new-order-item">
                                    <p>Активовано:
                                        <span class="active-order">{{ $data->has('count_orders_in_valid_statuses') ? $data['count_orders_in_valid_statuses'] : 0 }}</span>
                                        із
                                        <a href="{{ route('admin.order.index', ['created_at' => $data['date']]) }}" class="total-orders">
                                            {{ $data->has('count_all_orders') ? $data['count_all_orders'] : 0 }}
                                        </a>
                                        <span class="percentage-orders">
                                            {{ $data->has('percentage_orders_in_valid_statuses') ? $data['percentage_orders_in_valid_statuses'] : 0 }}%
                                        </span></p>
                                    <div class="efficiency-table-info">
                                        <div class="efficiency-info-item">
                                            <p>Розглядається: <a href="{{ route('admin.order.index', ['status_id' => 3, 'created_at' => $data['date']]) }}">
                                                    {{ $data->has('3') ? $data['3']['count_by_status'] : 0 }}
                                                </a></p>
                                            <span class="percentage-orders">{{ $data->has('3') ? $data['3']['percentage_by_status'] : 0 }}%</span>
                                        </div>
                                        <div class="efficiency-info-item">
                                            <p>Закрито: <a href="{{ route('admin.order.index', ['status_id' => 8, 'created_at' => $data['date']]) }}">
                                                    {{ $data->has('8') ? $data['8']['count_by_status'] : 0 }}
                                                </a></p>
                                            <span class="percentage-orders">{{ $data->has('8') ? $data['8']['percentage_by_status'] : 0 }}%</span>
                                        </div>
                                        <div class="efficiency-info-item green">
                                            <p>Виконується: <a href="{{ route('admin.order.index', ['status_id' => 4, 'created_at' => $data['date']]) }}">
                                                    {{ $data->has('4') ? $data['4']['count_by_status'] : 0 }}
                                                </a></p>
                                            <span class="percentage-orders">{{ $data->has('4') ? $data['4']['percentage_by_status'] : 0 }}%</span>
                                        </div>
                                        <div class="efficiency-info-item green">
                                            <p>Виконано: <a href="{{ route('admin.order.index', ['status_id' => 5, 'created_at' => $data['date']]) }}">
                                                    {{ $data->has('5') ? $data['5']['count_by_status'] : 0 }}
                                                </a></p>
                                            <span class="percentage-orders">{{ $data->has('5') ? $data['5']['percentage_by_status'] : 0 }}%</span>
                                        </div>
                                    </div>
                                    <!-- /.order-info -->
                                    <p class="count-all">Дохід: <span href="#">{{ $data['payment_sum'] }} грн</span></p>
                                </td>
                                <td class="efficiency-table-item price-offers-item">
                                    <p class="count-all">Всього: <span href="#">{{ $data['requests_count'] }}</span></p>
                                    <div class="efficiency-table-info">
                                        <p>В середньому: <span href="#">{{ $data['avg_requests_count'] }}</span></p>
                                    </div>
                                </td>
                                <td class="efficiency-table-item message-item">
                                    <p class="count-all">Всього: <span href="#">{{ $data['all_emails'] }}</span></p>
                                    <div class="efficiency-table-info">
                                        <p>По замовленнях: <span href="#">{{ $data['client_emails_count'] }}</span></p>
                                        <p>Із каталогу: <span href="#">{{ $data['master_emails_count'] }}</span></p>
                                    </div>
                                </td>
                                <td class="efficiency-table-item installers-item">
                                    <p class="count-all">Всього: <span href="#">{{ $data['active_masters'] }}</span>
                                        <span href="#" class="new-installers">+{{ $data['active_masters_by_month'] }}</span></p>
                                    <div class="efficiency-table-info">
                                        <p>Портфоліо: <span>{{ $data['active_master_portfolios'] }}</span>
                                            <span href="#" class="new-installers">+{{ $data['active_masters_portfolios_by_month'] }}</span></p>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <!-- /.efficiency-table -->
                        </table>
                        <!-- /.efficiency-table -->
                @endforeach
                </div>
                <!-- /#scrollbarY -->
                <div class="accept-btn">
                    <a href="#">Показати ще</a>
                </div>
                <!-- /.accept-btn -->
            </div>
            <!-- /.wrapper-page -->
        </div>
        <!-- /.add-brand -->

    </div>
    <!-- /.wrapper -->

@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/custom-scrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/kpi.js') }}"></script>
@endpush

@push('styles')
    <style>
        .hide-block {
            display: none;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/custom-scrollbar/jquery.mCustomScrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/kpi.css') }}"/>
@endpush
