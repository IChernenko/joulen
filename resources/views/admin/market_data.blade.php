@extends('layouts.old.app')

@section('content')
    <div class="container pageTitle">
        <h1>Стан ринку</h1>
    </div>

    <div class="contentBlock userData">
        <div class="container">
            <form action="{{ route('admin.update.market_condition', ['id' => 1]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <ol>
                    <li>
                        Зелений тариф після оподаткування для домашнціх (Z):
                        <input type="text" name="green_tariff_home_ses" value="{{ $market->find(1)->green_tariff_home_ses }}">
                    </li>
                    <li>
                        Зелений тариф для дахових (R):
                        <input type="text" name="green_tariff_roof_ses" value="{{ $market->find(1)->green_tariff_roof_ses }}">
                    </li>
                    <li>
                        Зелений тариф для наземних (G):
                        <input type="text" name="green_tariff_ground_ses" value="{{ $market->find(1)->green_tariff_ground_ses }}">
                    </li>
                    <li>
                        Cередня вартість мережевої станції 5 кВт(X1):
                        <input type="text" name="avg_cost_network_ses_5kW" value="{{ $market->find(1)->avg_cost_network_ses_5kW }}">
                    </li>
                    <li>
                        Cередня вартість мережевої станції 30 кВт(X2):
                        <input type="text" name="avg_cost_network_ses_30kW" value="{{  $market->find(1)->avg_cost_network_ses_30kW}}">
                    </li>
                    <li>
                        Cередня вартість автономної станції 5 кВт(B2):
                        <input type="text" name="avg_cost_auto_ses_5kW" value="{{  $market->find(1)->avg_cost_auto_ses_5kW}}">
                    </li>
                    <li>
                        Cередня вартість автономної станції 30 кВт(B1):
                        <input type="text" name="avg_cost_auto_ses_30kW" value="{{  $market->find(1)->avg_cost_auto_ses_30kW}}">
                    </li>
                    <li>
                        Коефіцієнт здорожчання гібридної станції (H):
                        <input type="text" name="ratio_hybrid_station" value="{{  $market->find(1)->ratio_hybrid_station}}">
                    </li>
                    <li>
                        Cередня вартість дахової комерційної станції 60 кВт(Y1):
                        <input type="text" name="avg_cost_roof_ses_30kW" value="{{ $market->find(1)->avg_cost_roof_ses_30kW }}">
                    </li>
                    <li>
                        Cередня вартість дахової комерційної станції 430 кВт(Y2):
                        <input type="text" name="avg_cost_roof_ses_100kW" value="{{ $market->find(1)->avg_cost_roof_ses_100kW }}">
                    </li>
                    <li>
                        Cередня вартість наземної комерційної станції 200 кВт(L1):
                        <input type="text" name="avg_cost_ground_ses_100kW" value="{{ $market->find(1)->avg_cost_ground_ses_100kW }}">
                    </li>
                    <li>
                        Cередня вартість наземної комерційної станції 5000 кВт(L2):
                        <input type="text" name="avg_cost_ground_ses_1000kW" value="{{ $market->find(1)->avg_cost_ground_ses_1000kW }}">
                    </li>
                    <li>
                        Середня вартість гелевих акумуляторів(A1):
                        <input type="text" name="avg_cost_gel_batteries" value="{{ $market->find(1)->avg_cost_gel_batteries }}">
                    </li>
                </ol>

                <div class="submitBtns">
                    <input type="submit" value="Зберегти" class="site_button-orange button-small">
                </div>
            </form>
        </div>
    </div>
@endsection