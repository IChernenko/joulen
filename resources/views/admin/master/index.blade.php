@extends('layouts.old.app')

@section('content')
    <div class="container">
        <div class="table_page">
            <h1 class="title">Список спеціалістів</h1>
            <form name="FSelect" class="filterWorkers" method="POST" action="{{ route('admin.master.index.filter') }}" id="admin_master_list">

                {{ csrf_field() }}

                <div class="filter_block">

                    <input type="hidden" name="perpage" value="{{ old('perpage', 20) }}">
                    <input type="hidden" name="sortfield" value="{{ old('sortfield', 'updated_at') }}">
                    <input type="hidden" name="sortorder" value="{{ old('sortorder', 'desc') }}">

                    <div class="select_wrap status">
                        <label>Статус</label>
                        <select class="custom_select" name="status_id">
                            <option value="0" {{ !old('status_id') ? 'selected' : '' }} >Показати всіх</option>
                            @foreach($master_statuses as $status)
                                <option value="{{ $status->id }}" {{ old('status_id') == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                            @endforeach
                            <option value="deleted" {{ old('status_id') == 'deleted' ? 'selected' : '' }}>Видалився</option>
                            <option value="blocked" {{ old('status_id') == 'blocked' ? 'selected' : '' }}>Заблоковано</option>
                        </select>
                    </div>
                    <div class="select_wrap specialization">
                        <label>Спеціалізація</label>
                        <select class="custom_select" name="category_id">
                            <option value="0" {{ !old('category_id') ? 'selected' : '' }}>Всі спеціалізації</option>
                            @foreach($order_subcategories as $category)
                                {{--TODO: create and use $category->master_name--}}
                                @if($category->id == 1)
                                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>Домашні</option>
                                @endif
                                @if($category->id == 4)
                                <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>Комерційні</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="select_wrap region">
                        <label>Область</label>
                        <select class="custom_select" name="regions_id" id="filterRegion">
                            <option value="0" {{ !old('regions_id') ? 'selected' : '' }}>Вся Україна</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ old('regions_id') == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select_wrap userregion">
                        <label>Звідки</label>
                        <select class="custom_select" name="region_id" id="filterUserRegion">
                            <option value="0" {{ !old('region_id') ? 'selected' : '' }}>Вся Україна</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ old('region_id') == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select_wrap useremail">
                        <label>E-mail спеціаліста</label>
                        <select class="custom_select" name="master_email">
                            <option value="0" {{ !old('master_email') ? 'selected' : '' }}>Всi</option>
                            @foreach($masters_name as $master)
                                <option value="{{ $master->user->email }}" {{ old('master_email') == $master->user->email ? 'selected' : '' }}>{{ $master->user->email }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select_wrap companyname">
                        <label>Назва компанії</label>
                        <select class="custom_select" name="company_name">
                            <option value="0" {{ !old('company_name') ? 'selected' : '' }}>Всi</option>
                            @foreach($masters_name as $master)
                                <option value="{{ $master->user->name }}" {{ old('company_name') == $master->user->name ? 'selected' : '' }}>{{ $master->user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
            <div class="table_wrap">
                <table width="100%" class="tablesorter" id="Table">
                    <thead>
                    <tr class="thead_style">
                        <th class="checkbox_table" rowspan="2"><input class="styledChk" type="checkbox" id="check_all1">
                            <label class="check_all" for="check_all1"></label>
                        </th>
                        <th class="name masters_sort {{ old('sortfield') == 'name' ? old('sortorder', 'desc') : ''}}" rowspan="2" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="name">
                            <span>Ім'я<span class="arrow"></span></span>
                        </th>
                        <th class="registration_date masters_sort {{ old('sortfield') == 'created_at' ? old('sortorder', 'desc') : ''}}" rowspan="2" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="created_at">
                            <span>Реєстрація<span class="arrow"></span></span>
                        </th>
                        <th class="edit_date masters_sort {{ !old('sortfield') || old('sortfield') == 'updated_at' ? old('sortorder', 'desc') : ''}}" rowspan="2" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="updated_at">
                            <span>Зміна<span class="arrow"></span></span>
                        </th>
                        <th class="sending" colspan="2">Розсилки</th>
                        <th>Статус</th>
                        <th class="request" colspan="4">Заявки</th>
                    </tr>
                    <tr>
                        <th class="masters_sort {{ old('sortfield') == $category->slug ? old('sortorder', 'desc') : ''}}" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="{{ $category->slug }}">
                            <span class="small_title">Домашні<span class="arrow"></span></span>
                        </th>
                        <th class="masters_sort {{ old('sortfield') == $category->slug ? old('sortorder', 'desc') : ''}}" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="{{ $category->slug }}">
                            <span class="small_title">Коммерційні<span class="arrow"></span></span>
                        </th>
                        <th class="masters_sort border-left"></th>
                        <th class="masters_sort border-left {{ old('sortfield') == 'order_requests_count' ? old('sortorder', 'desc') : ''}}"  data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="order_requests_count">
                            <span>
                                <span class="arrow"></span>
                                <img src="{{ asset('images/summ_icon.png') }}" alt="image">
                            </span>
                        </th>
                        <th class="masters_sort {{ old('sortfield') == 'order_requests_considering_count' ? old('sortorder', 'desc') : ''}}" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="order_requests_considering_count">
                            <span>
                                <span class="arrow"></span>
                               <img src="{{ asset('images/a_icon.png') }}" alt="image">
                            </span>
                        </th>
                        <th class="masters_sort {{ old('sortfield') == 'orders_in_progress_count' ? old('sortorder', 'desc') : ''}}" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="orders_in_progress_count">
                            <span>
                                <span class="arrow"></span>
                                <img src="{{ asset('images/fix_icon.png') }}" alt="image">
                            </span>
                        </th>
                        <th class="masters_sort {{ old('sortfield') == 'orders_done_count' ? old('sortorder', 'desc') : ''}}" data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="orders_done_count">
                            <span>
                                <span class="arrow"></span>
                                  <img src="{{ asset('images/check_icon.png') }}" alt="image">
                            </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($masters as $key => $master)
                        <tr>
                            <td> <div class="checkbox_wrap"><input class="styledChk" type="checkbox" id="check_{{$key}}"><label for="check_{{$key}}"></label></div></td>
                            <td>
                                {{--TODOD: change route to admin.master.edit--}}
                                <div class="wrap_name">
                                    <a href="{{ route('master.show', ['id' => $master->id]) }}">
                                        {{ $master->user->name_with_status }}
                                    </a>
                                    <a href="{{ route('admin.master.edit', ['id' => $master->id]) }}">(редагувати)</a>
                                </div>
                                <div class="wrap_email">{{ $master->user->email }}</div>
                            </td>
                            <td>
                                <div class="wrap_date">{{ $master->user->created_at }}</div>
                                {{--TODO: change this to time--}}
                                <div class="wrap_time">{{ $master->user->created_at_time }}</div>
                            </td>
                            <td>
                                <div class="wrap_date">{{ $master->user->updated_at }}</div>
                                <div class="wrap_time">{{ $master->user->updated_at_time }}</div>
                            </td>
                            <td>
                                <div class="order_category ses {{ $master->home_mailing ? 'active' : ''}}"></div>
                            </td>
                            <td>
                                <div class="order_category energy_audit {{ $master->commercial_mailing ? 'active' : ''}}"></div>
                            </td>
                            <td>
                                <div>{{ $master->status->name }}</div>
                            </td>
                            <td>
                                <form enctype="multipart/form-data" class="ordersFilter clr" method="post" action="{{ route('admin.order.index.filter') }}" id="admin_order_filter">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $master->id }}" name="master_id_requests"/>
                                    <input type="submit" value="{{ $master->order_requests_count }}" class="admin-master-list-requests-count"/>
                                </form>
                            </td>
                            <td>
                                <form enctype="multipart/form-data" class="ordersFilter clr" method="post" action="{{ route('admin.order.index.filter') }}" id="admin_order_filter">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $master->id }}" name="master_id_requests"/>
                                    <input type="hidden" value="{{ 3 }}" name="status_id"/>
                                    <input type="submit" value="{{ $master->order_requests_considering_count }}" class="admin-master-list-requests-count"/>
                                </form>
                            </td>
                            <td>
                                <form enctype="multipart/form-data" class="ordersFilter clr" method="post" action="{{ route('admin.order.index.filter') }}" id="admin_order_filter">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $master->id }}" name="master_id_requests"/>
                                    <input type="hidden" value="{{ 4 }}" name="status_id"/>
                                    <input type="submit" value="{{ $master->orders_in_progress_count }}" class="admin-master-list-requests-count"/>
                                </form>
                            </td>
                            <td>
                                <form enctype="multipart/form-data" class="ordersFilter clr" method="post" action="{{ route('admin.order.index.filter') }}" id="admin_order_filter">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $master->id }}" name="master_id_requests"/>
                                    <input type="hidden" value="{{ 5 }}" name="status_id"/>
                                    <input type="submit" value="{{ $master->orders_done_count }}" class="admin-master-list-requests-count"/>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="control_table_block">
            <div class="export_base">
                <a href="" download="emails.txt"><svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: sketchtool 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
                        <title>05A71C24-4A06-477E-B3F8-E68EB3B676D8</title>
                        <desc>Created with sketchtool.</desc>
                        <defs></defs>
                        <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Icons/ic_export" fill="#3B7D9C">
                                <path d="M14.0567179,21.9399487 C16.2658462,21.9399487 18.3629744,21.7773333 20.2102564,21.488359 C21.336,19.0606154 23.786359,17.3727179 26.6341026,17.3727179 C27.0469231,17.3727179 27.4331795,17.4082564 27.8355897,17.4764615 L27.8355897,14.1742564 C27.8355897,13.7212308 27.5925641,13.3338974 27.2048718,12.8797949 C25.4581026,14.6272821 20.2231795,15.9217436 14.0753846,15.9217436 C7.92866667,15.9217436 2.66394872,14.627641 0.852205128,12.8797949 C0.529128205,13.3338974 0.247333333,13.7215897 0.247333333,14.1742564 L0.247333333,17.6046154 C0.246974359,19.9989744 6.48558974,21.9399487 14.0567179,21.9399487 Z M14.0405641,14.1742564 C21.6770256,14.1742564 27.8355897,12.2329231 27.8355897,9.90317949 L27.8355897,6.4085641 C27.8355897,4.07953846 21.6773846,2.13748718 14.0405641,2.13748718 C6.46979487,2.13748718 0.246974359,4.07989744 0.246974359,6.4085641 L0.246974359,9.90317949 C0.246974359,12.2329231 6.46979487,14.1742564 14.0405641,14.1742564 Z M19.5702051,24.4588718 C19.5702051,24.0683077 19.6025128,23.6852821 19.6638974,23.3112308 C17.9558974,23.5524615 16.0666154,23.6874359 14.0832821,23.6874359 C7.9365641,23.6874359 2.66789744,22.3929744 0.856153846,20.6458462 C0.532717949,21.0985128 0.247333333,21.4872821 0.247333333,21.9399487 L0.247333333,25.3695897 C0.247333333,27.7646667 6.48594872,29.706 14.0570769,29.706 C16.6987692,29.706 19.1749744,29.4733846 21.2677949,29.0702564 C20.201641,27.830359 19.5702051,26.2185641 19.5702051,24.4588718 Z M21,24.5 C21,27.5375316 23.4624684,30 26.4996346,30 C29.5371662,30 32,27.5375316 32,24.5 C32,21.4628338 29.5375316,19 26.4996346,19 C23.4624684,19 21,21.4624684 21,24.5 Z M22.7869337,25.4827953 L22.7902219,23.4952836 L26.6753687,23.495649 L26.6753687,20.9783778 L30.2083167,24.5109605 L26.7024047,28.0208915 L26.6790222,25.4824299 L22.7869337,25.4827953 Z" id="ic_export"></path>
                            </g>
                        </g>
                    </svg>Експортувати базу</a>
            </div>
            <div class="pagesNav textCenter">
                {{ $masters->links() }}
            </div>
            <div class="show_in_page">
                Показувати на стронці:
                <div class="amount_page">
                    <a href="#" class="amount_page_item {{ !old('perpage') || old('perpage') == '20' ? 'active' : '' }}" data-perpage="20">20</a>
                    <a href="#" class="amount_page_item {{ old('perpage') == '50' ? 'active' : '' }}"  data-perpage="50">50</a>
                    <a href="#" class="amount_page_item {{ old('perpage') == '100' ? 'active' : '' }}"  data-perpage="100">100</a>
                    <a href="#" class="amount_page_item {{ old('perpage') == '500' ? 'active' : '' }}"  data-perpage="500">500</a>
                </div>
            </div>
        </div>

    </div>

    @push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("select").select2();
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/admin/master/index.js') }}"></script>

    @endpush
    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"/>
    @endpush
@endsection