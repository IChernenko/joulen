@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">

        <div class="page-title">
            <h1>Портфоліо</h1>
        </div>

        <!--# contentBlock #-->
        <div class="cabinet-portfolio">
            <div class="portfolio-map img-border">
                <div id="map" class="map" style="height: 348px"></div>
            </div>
            <div class="portfolio-list">
                <p>Список</p>
                <form class="portfolio-select" enctype="multipart/form-data" method="post"
                      action="{{ route('admin.portfolio.index.filter') }}" id="admin_portfolio_filter">
                    {{ csrf_field() }}

                    {{--TODO: old request values not working--}}
                    <div class="portfolio-select-item">
                        <label>Область:</label>
                        <select name="region_id" class="nice-select select-master">
                            <option value="0" {{ !old('region_id') ? 'selected' : '' }}>Всі</option>
                            {{--TODO: resolve region parsing from portfolio input map--}}
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ old('region_id') == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Тип СЕС:</label>
                        <select name="subcategory_id" class="select2">
                            <option value="0" {{ !old('subcategory_id') ? 'selected' : '' }}>Всі</option>
                            @foreach($order_subcategories as $subcategory)
                                @if($subcategory->slug != 'commercial' )
                                <option value="{{ $subcategory->id }}" {{ old('subcategory_id') == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                @endif
                            @endforeach
                            <option value="ground" {{ old('subcategory_id') == 'ground' ? 'selected' : '' }}>Наземна</option>
                            <option value="roof" {{ old('subcategory_id') == 'roof' ? 'selected' : '' }}>Дахова</option>
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Виконавець:</label>
                        <select name="master_id" class="nice-select select-master">
                            <option value="0" {{ !old('master_id') ? 'selected' : '' }}>Всі</option>
                            @foreach($masters as $master)
                                <option value="{{ $master->id }}" {{ old('master_id') == $master->id ? 'selected' : '' }}>{{ $master->user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Інвертор:</label>
                        <select name="invertor_id" class="select2">
                            <option value="0" {{ !old('invertor_id') ? 'selected' : '' }}>Всі</option>
                            @foreach($invertors as $invertor)
                                <option value="{{ $invertor->id }}" {{ old('invertor_id') == $invertor->id ? 'selected' : '' }}>{{ $invertor->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Панелі:</label>
                        <select name="panel_id" class="select2">
                            <option value="0" {{ !old('panel_id') ? 'selected' : '' }}>Всі</option>
                            @foreach($panels as $panel)
                                <option value="{{ $panel->id }}" {{ old('panel_id') == $panel->id ? 'selected' : '' }}>{{ $panel->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Рік:</label>
                        @php
                            $currentYear = date("Y");
                            $yearArray = range($currentYear, 1991);
                        @endphp
                        <select name="year" class="select2">
                            <option value="0" {{ !old('year') ? 'selected' : '' }}>Рік</option>

                            @foreach ($yearArray as $year)
                                <option {{ old('year') == $year ? 'selected' : '' }} value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="portfolio-select-item">
                        <label>Статус:</label>
                        <select name="status_id" class="select2" id="select-status">
                            <option value="0" {{ !old('status_id') ? 'selected' : '' }}>Всі</option>
                            @foreach($statuses as $status)
                                @if($status->slug == 'verified')
                                    <option value="{{ $status->id }}" {{ old('status_id') == $status->id ? 'selected' : '' }}>Активна</option>
                                @else
                                    <option value="{{ $status->id }}" {{ old('status_id') == $status->id ? 'selected' : '' }}>{{ ucfirst($status->name) }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="center-block">
                        <input type="submit" class="btn btn-primary" value="Відфільтрувати"/>
                    </div>
                </form>
            </div>
            <div class="portfolio-table">
                <div class="table-block">
                    <table class="table home-ses-table footable footable-1 breakpoint-lg table-portfolio" style="display: table;">
                        @foreach($portfolios as $portfolio)
                            <tr id="{{ $portfolio->id }}" class="tr_id">
                                <td data-title="Тип" data-breakpoints="xs sm md" style="display: table-cell;">
                                    @if($portfolio->subcategory_id)
                                        <a href="#show-portfolio" class="get-portfolio" data-id="{{ $portfolio->id }}">
                                            {{ $order_subcategories->find($portfolio->subcategory_id)->name }}
                                            {{ $portfolio->is_home == false ? $portfolio->ground == true ? 'наземна ' : 'дахова ' : ''}}СЕС</a>
                                    @endif
                                        <img class='open-table' src="{{ asset('images/arrow-prev.png') }}">
                                </td>
                                <td data-title="Потужність" data-breakpoints="xs sm md"
                                    style="display: table-cell;">
                                    {{ $portfolio->power }} кВт
                                </td>
                                <td data-title="Адреса" data-breakpoints="xs sm md" style="display: table-cell;">
                                    {{ $portfolio->getCity() }}
                                </td>
                                <td data-title="?" data-breakpoints="xs sm md" style="display: table-cell;">
                                    {{ $masters->find($portfolio->master_id)->user->name }}
                                </td>
                                <td data-title="Дата" data-breakpoints="xs sm md" class="footable-first-visible"
                                    style="display: table-cell;">
                                    {{ $portfolio->month }} {{ $portfolio->year }}
                                </td>
                                <td data-title="Статус" data-breakpoints="xs sm md"
                                    class="application_status footable-last-visible" style="display: table-cell;">
                                    @if($portfolio->status->slug == 'verified')
                                        <p class="active-portfolio">Aктивна</p>
                                    @elseif($portfolio->status->slug == 'checking')
                                        <p class="on-check-portfolio">{{ ucfirst($portfolio->status->name) }}</p>
                                        @else
                                        <p class="rejected-request">{{ ucfirst($portfolio->status->name) }}</p>
                                    @endif
                                </td>
                                <td data-title="Редагувати" data-breakpoints="xs sm md" class="edit_block"
                                    style="display: table-cell;">
                                    <a class="open-update-ses {{ $portfolio->is_home ? 'update-home' : 'update-commercial' }} site-icon-edit"
                                       data-remodal-target="add-ses" data-id="{{ $portfolio->id }}" data-master-id="{{ $portfolio->master_id }}"></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

                        {{ $portfolios->links() }}
        </div>

        <div id="hellopreloader" style="display:none">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
    @push('footer_scripts')
    <script>
        var portfolios = {!! $portfolios_all !!},
            commercialMarkerSrc = '{{ asset('images/map-marker-green.png') }}',
            homeMarkerSrc = '{{ asset('images/map-marker-orange.png') }}',
            markerSrc = '{{ asset('images/map-marker.png') }}',
            getPortfolioURL = '{{ route('admin.portfolio.get') }}',
            portfolioUpdateURL = '{{ route('admin.portfolio.update', ['id' => null]) }}',
            portfolioStatusUpdateURL = '{{ route('admin.portfolio.status.update', ['id' => null]) }}',
            portfolioDeleteURL = '{{ route('admin.portfolio.destroy', ['portfolio' => null]) }}',
            user = {!! $users !!},
            master = {!! $masters->keyBy('id') !!},
            subcategories = {!! $subcategories !!},
            imgDirectoryPath = '{{ asset('images/') }}',
            showMasterURL = '{{ route('master.show', ['id' => null]) }}',
            invertorName = {!! $invertor_type_equipment->keyBy('id') !!},
            panelName = {!! $panel_type_equipment->keyBy('id') !!};
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
            defer></script>
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/master/portfolio/index.js') }}"></script>

    <script src="{{ asset('libs/footable/js/footable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/profile-form-handler.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/master/mask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
    @endpush

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/footable/css/footable.standalone.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
    @endpush

    @push('modals')
    <div class="remodal set-portfolio" id="show-portfolio" data-remodal-id="show-portfolio">
        <div class="modal-header">
            <h4></h4>
        </div>

        <!--# contentBlock #-->
        <div class="cabinet-portfolio">
            <div class="set-address">
                <p></p>
            </div>
            <div id="map-1" class="map"></div>
            <div class="form-block form-portfolio">
                <div class="set-panel-photo">
                    <img src="" alt="image">
                </div>
                <div class="set-invertor-photo">
                    <img src="" alt="image">
                </div>
            </div>
            <div class="set-date"></div>
            <div class="set-invertor"></div>
            <div class="set-panel"></div>
            <div class="set-executor"></div>
            <ul class="set-settings"></ul>
            <form class="form-block set-form" action="" method="POST">
                {{ csrf_field() }}
                <div><p>Статус:</p>
                    <div class="field">
                        <select class="select-modal set-status" name="status">
                            @foreach($statuses as $status)
                                @if($status->slug == 'verified')
                                <option value="{{ $status->id }}">Активна</option>
                                @else
                                <option value="{{ $status->id }}">{{ ucfirst($status->name) }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div><a class="delete_portfolio site_icon-delete" data-id='' data-remodal-target="portfolio-confirm-delete">Видалити
                        об'єкт</a></div>

                <div class='space-top'>
                    <input type="submit" value="Зберегти" class='btn btn-primary portfolio-status-update'>
                    <button class='btn btn-secondary' data-remodal-action="cancel">Скасувати</button>
                </div>
            </form>
        </div>
    </div>

    @component('layouts.master.profile.ses_portfolio', [
    'subcategories' => $subcategories,
    'invertor_type_equipment' => $invertor_type_equipment,
    'panel_type_equipment' => $panel_type_equipment,
    'master' => $master
    ])
    @endcomponent

    <div class="remodal" data-remodal-id="portfolio-confirm-delete" id="portfolio-confirm-delete">
        <div class="modal_header delete-ses">
            <p>Ви дійсно бажаєте видалити цю СЕС з портфоліо?</p>
        </div>
        <form action="">
        <div class='space-top button-remove'>
            <button type="submit" class='btn btn-primary confirm-delete' data-remodal-action="confirm">Так</button>
            <button class='btn btn-secondary' data-remodal-target="show-portfolio">Ні</button>
        </div>
        </form>
    </div>
    @endpush
@endsection

@section('site-title')
    Портфоліо
@endsection