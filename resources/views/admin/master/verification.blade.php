@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Заявки на статус "Перевірена компанія"</h1>
        </div>
        <div class="cabinet-profile">
            <div class="cabinet-item">
                <div class="table-block">
                <table class="table home-ses-table footable footable-1 breakpoint-lg status-table"
                       style="display: table;">
                    <thead>
                    <tr>
                        <td>Дата</td>
                        <td>Компанія</td>
                        <td>Статус заявки</td>
                        <td></td>
                        <td></td>
                        <td>Коментар адміна</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($requests as $request)
                        <tr data-id="{{ $request->id }}" class="tr_id">
                            <td data-title="Дата" data-breakpoints="xs sm md" style="display: table-cell;">
                                {{ $request->created_at }}
                            </td>
                            <td data-title="Компанія" data-breakpoints="xs sm md"
                                style="display: table-cell;">
                                {{ $request->master->user->name }}
                                <img class='open-table' src="<?php echo e(asset('images/arrow-prev.png')); ?>">
                            </td>
                            <td data-title="Статус заявки" data-breakpoints="xs sm md" style="display: table-cell;">
                                @if($request->status->slug == 'verified')
                                    <div class="verified-request">{{ $request->status->name }}</div>
                                @elseif($request->status->slug == 'checking')
                                    <div class="cheking-request">{{ $request->status->name }}</div>
                                @elseif($request->status->slug == 'rejected')
                                    <div class="rejected-request">{{ $request->status->name }}</div>
                                @endif
                            </td>
                            <td data-title="Підтвердити" data-breakpoints="xs sm md" style="display: table-cell;" class="accept-application">
                                @if($request->status->slug != 'verified')
                                    <form method="POST" action="{{ route('admin.verification.update', $request->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <input type="hidden" value="{{ $request_statuses['verified'] }}" name="request_status_id">
                                        <input type="submit" value="" class="site_icon-check">
                                    </form>
                                @endif
                            </td>
                            <td data-title="Видалити" data-breakpoints="xs sm md" style="display: table-cell;">
                                @if($request->status->slug != 'rejected')
                                    <form method="POST" action="{{ route('admin.verification.update', $request->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <input type="hidden" value="{{ $request_statuses['rejected'] }}" name="request_status_id">
                                        <input type="submit" value="" class="delete_ses site_icon-delete">
                                    </form>
                                @endif
                            </td>
                            <td  data-title="Коментар адміна" data-breakpoints="xs sm md" class="comment_block"
                                 style="display: table-cell;">
                                @if($request->comment)
                                    <p class="comment-admin">{{ $request->comment }}&nbsp;</p><a href="#comment" class="comment update-comment site-icon-edit" data-id="{{ $request->id }}"></a>
                                @else
                                    <a href="#comment" class="comment add-comment" data-id="{{ $request->id }}">Додати коментар</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                {{ $requests->links() }}
            </div>
        </div>
    </div>
@endsection

@push('modals')
<div class="remodal admin-comment" id="add-comment" data-remodal-id="comment">
    <div class="page-title">
        <h1>Коментар до заявки</h1>
    </div>

    @component('layouts.validation_errors')
    @endcomponent

    <form id="comment-form">
        {{ csrf_field() }}
        <div class="form-block" >
            <textarea name="comment"></textarea>
        </div>
        <div class="form-block" >
            <a class="btn btn-primary comment-confirm">Зберегти</a>
            <button class='btn btn-secondary' data-remodal-action="cancel">Скасувати</button>
        </div>
    </form>
</div>
@endpush
@push('footer_scripts')
<script>
    var commentStoreURL = '{{ route('admin.master.comment.store') }}',
        commentGetURL = '{{ route('admin.master.comment.get') }}';
</script>
<script type="text/javascript" src="{{ asset('js/admin/master/verification.js') }}"></script>
<script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
@endpush
