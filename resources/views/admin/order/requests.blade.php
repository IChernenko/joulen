@extends('layouts.old.app')

@section('content')
    <div id="content">

        <div id="applicationsList" class="listOfApp">
            <div class="container">
                <h1>Заявки спеціалістів на замовлення: <a href="{{ route('admin.order.show', ['id' => $order->id]) }}">{{ $order->title }}</a></h1>
                <div>

                    @foreach($order->requests as $request)
                        @component('layouts.order.request', ['request' => $request])
                        @slot('final')
                            {{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }}
                        @endslot

                        @slot('buttons')
                            <a href="{{ route('admin.request.edit', ['id' => $request->id]) }}" class="site_button-orange button-small leaveFeedback">Редагувати заявку</a>
                        @endslot
                        @endcomponent
                    @endforeach

                </div>
            </div>
        </div>

    </div>
@endsection