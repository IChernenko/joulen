@extends('layouts.old.app')

@section('content')
    {{--TODO: FRONT remove html breaks--}}
    <div class="container pageTitle">
        <h1>Параметри облікового запису</h1>
    </div>

    <div class="contentBlock userData">
        <div class="container">
            <div class="changePassLink">
                <a href="{{ route('password.change') }}" class="site_icon-pass">Змінити пароль</a>
    {{--       <a href="#" class="delete-admin">Видалити обліковий запис</a>--}}
                {{--<form action="{{ route('admin.delete') }}" method="POST" id="delete-admin">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--{{ method_field('DELETE') }}--}}
                {{--</form>--}}
            </div>

            <form id="formparams" method="POST" action="{{ route('admin.update') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div>
                    <div class="site_icon-person">
                        <div class="label">Ім'я</div>
                        <div class="field">
                            <input name="name" class="validate_required" type="text" value="{{ $user->name }}">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="site_icon-mail_gray">
                        <div class="label">E-mail</div>
                        <div class="field">
                            <input name="" type="text" value="{{ $user->email }}" disabled="">
                        </div>
                    </div>
                </div>

                <div style="display:block;">
                    <div class="site_icon-phone_gray">
                        <div class="label">Телефон</div>
                        <div class="field">
                            <input name="phone" class="mask-phone" type="text" value="{{ $user->phone }}">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="spacing-top">

                        <input value="1" name="mailing" type="checkbox" class="styledChk" id="mailing" {{  $user->admin->mailing ? 'checked' : '' }}>

                        <label for="mailing">Отримувати розсилку про нові заявки від клієнтів</label>


                    </div>
                </div>
                <div class="submitBtns">
                    <a href="{{ route('admin.order.index') }}" class="site_button-orange button-small onlyBorder">Відмінити</a>
                    <input type="submit" value="Зберегти" class="site_button-orange button-small">
                </div>
            </form>
        </div>
    </div>

    @push('footer_scripts')
        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/admin/settings.js') }}"></script>--}}
        <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
        <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    @endpush

@endsection

@section('site-title')
    Параметри облікового запису
@endsection