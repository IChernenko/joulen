@if ( !Auth::user()->profileType->profile_id )
    <br>
    <br>
    <a href="{{ route('facebook.attach') }}" class="btn btn-primary">Приєднати Facebook
        акаунт</a>
@else
    <br>
    <br>
    У вас є приєднанні аккаунти:
    <br>
    <a href="{{ route('facebook.detach') }}" class="btn btn-primary">

        Від'єднати
        {{ Auth::user()->profileType->type->name }}
    </a>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif