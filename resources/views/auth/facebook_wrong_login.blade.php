@extends('layouts.app')

@section('content')
    <div class="auth-items-wrap select-category">
        <div class="container">

            <div class="auth-items">
                <div class="register-items">
                    <!-- /.register-item -->
                    <div class="authorized-items">
                        <!-- /.authorized-item -->
                                <div class="info-register">
                                    <div class="unregistered">
                                        <span>Ще не зареєстровані?</span> <span>Зареєструйтеся:
                                    <a href="{{ route('client.register') }}">як клієнт</a>/
                                    <a href="{{ route('master.register') }}">як монтажник</a></span>
                                    </div>
                                </div>
                            <!-- /.come-in -->

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('site-title')
    Реєстрація у сервісі {{ env('DOMAIN') }}
@endsection