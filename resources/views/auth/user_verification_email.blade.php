<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Реєстрація у сервісі Джоуль</title>
</head>
<body>
<div>
    Для завершення реєстрації вам потрібно перейти по  <a href="{{ $link = route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) }}">цьому посиланню</a> і заповнити просту анкету

</div>
</body>
</html>

