<div class="banner">
    <div class="banner_head">Безкоштовна консультація <span class="close_baner"></span></div>
    <div class="banner_body">
        <div class="name">(050) 074-54-65</div>
        <div class="phone">Роман</div>
    </div>
</div>

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/banner.js') }}"></script>
@endpush