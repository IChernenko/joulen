@extends('layouts.app')

@section('content')
    <!--# contentWrapper #-->
    <div class="auth-items-wrap select-category">
        <div class="container">

            <div class="auth-items">
                <div class="register-items">
                    <!-- /.register-item -->
                    <!--<h2>Зареєструватися</h2>-->
                    <div class="authorized-items">

                        <div class="auth-facebook">
                            <h2>Зареєструватися</h2>
                            <a id="facebook_client_register_modal" class="login-facebook"
                               href="{{ route('facebook.client') }}">
                                <img src="{{ asset('images/facebook.svg') }}" alt="image">
                                <span>Зареєструватися через Facebook</span>
                            </a>
                        </div>
                        <div class="login-email">
                            <form method="post" action="{{ route('client.register') }}"
                                  class="register-validate" id="register-client">
                                {{ csrf_field() }}
                                @component('layouts.validation_errors')
                                @endcomponent
                            <h3>Зареєструватися через e-mail:</h3>
                                <label for="name">Ім'я:</label>
                                <input type="text" id="name" value="{{ old('name') }}"
                                       name="name" class="name_required">

                            <label for="email">E-mail:</label>
                            <input type="text" id="email" value="{{ old('email') }}"
                                   class="check-email email_required" name="email" >

                            <label for="number-phone">Телефон:</label>
                            <input type="text" id="number-phone" value="{{ old('tel') }}"
                                   class="check-phone mask-phone tel_required" name="tel">

                            <div class="repeat-password">
                                    <label for="password1">Придумайте пароль:</label>
                                    <input type="password" id="password1" name="password"
                                           class="password_required" value="" >
                            </div>

                            <div class="code-verification confirm-phone" style="display: none">
                                <label for="enter-code">Введіть код, який вам прийшов у СМС:</label>
                                <div class="enter-code">
                                    <input id="enter-code" type="text" name="confirm_code">
                                    <input type="hidden" name="compare_code"/>
                                    {{--<a href="#">ОК</a>--}}
                                </div>
                                <div style="display: none" class="resend-confirm-code clr">
                                    <input type="submit" class="resend-code create-client"
                                           value="Відправити SMS повторно"/>
                                </div>
                                <div class="message-sms-send" style="display: none">
                                    <div class="field">Код підтвердження буде доступний: <span></span></div>
                                </div>
                            </div>

                                <input type="submit" class="btn btn-default create-client" value="Зареєструватися" />
                            </form>
                            <!-- /.repeat-password -->

                            <div class="info-register">
                                <div class="come-in">
                                    <span>Я вже зареєстрований. <a href="{{ route('login') }}">Ввійти</a></span>
                                </div>
                            </div>

                            <!-- /.come-in -->

                        </div>
                    </div>
                </div>
            </div>

            <div id="hellopreloader" style="display:none">
                <div id="hellopreloader_preload">
                    <p class="preloader">Зачекайте, надсилається SMS</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('site-title')
    Реєстрація клієнта у сервісі {{ env('DOMAIN') }}
@endsection

@push('footer_scripts')
<script type="text/javascript">
    var loginURL = '{{ route('client.order.index') }}',
        fbRegisterURL = '{{ route("client.facebook.register") }}';
</script>
<script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>

<script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
<script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/client-register/client-register.js') }}"></script>
<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
{{--<script src="{{ asset('js/client-register/open-fb-modal-registration.js') }}"></script>--}}

@if ( isset($profile_id) )
    <script>
        $(document).ready(function () {
//            $("#facebook_client_register_modal").trigger('click');
            if($('input[name=profile_id]').val() != '') {
                var modal = $('[data-remodal-id=modal]').remodal();
                modal.open();
            }

        $('.fb-create-client').on('click', function (e) {
            e.preventDefault();
            var form = $('#client_facebook_register_form'),
                formData = new FormData(form[0]);
            $.ajax({
                url: $('#client_facebook_register_form').attr('action'),
                data: formData,
                method: 'POST',
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function (response) {
                    $('#hellopreloader').css('display', 'block');
                    $('#hellopreloader_preload').css('display', 'block').css('opacity', 1);
                },
                success: function (response) {
                    $('#hellopreloader').hide();
                    $('.fb-resend-confirm-code').hide();
                    if(response['errors']) {
                        var validationBlock = $('#fb-errors');
                        validationBlock.empty();
                        validationBlock.append(
                            $('<li>').text(response['errors'])
                        );
                    }
                    else if(response['status'] == 'success') {
                        window.location.href = loginURL;
                    }
                    else if(response['redirect']) {
                        window.location.href = response['redirect'];
                    }
                    else if(response['status'] == 'failed_sms_send') {
                        $('.fb-confirm-phone').show();
                        $('.fb-resend-confirm-code').show();
                    }
                    else if($.isNumeric(response)) {
                        $('#fb-errors').empty();
                        $('.fb-confirm-phone').show();
                        $('input[name=compare_code]').val(response);
                        var countdown = $('.fb-message-sms-send span'),
                            startFrom = 60,
                            timer;
                        function startCountdown(){
                            timer = setInterval(function(){
                                countdown.text(startFrom--);
                                if(startFrom <= 0) {
                                    clearInterval(timer);
                                    $('.fb-resend-confirm-code').show();
                                    $('input[name=compare_code]').val('');
                                    $('.fb-message-sms-send').hide();
                                }
                            },1000);
                        }
                        $('.fb-message-sms-send').show(startCountdown());
                    }
                },
                error: function (response) {
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('#fb-errors');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }
                }
            });
        });
        });

    </script>

@endif
@endpush

@push('modals')
<div class="remodal registration-facebook" data-remodal-id="modal">

    <button data-remodal-action="close" class="remodal-close close-btn"></button>

    <h3>Реєстрація через Facebook</h3>
    <div class="enter-email enter-data">
        <form class="registration-modal" id="client_facebook_register_form" method="POST"
              action="{{ route('client.facebook.register') }}">
            {{ csrf_field() }}
            <ul class="contentBlock backend_validation_errors" id="fb-errors" style="color:red; text-align: left;">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                @endif
            </ul>
            <label for="name">Ім'я:</label>
            <input type="text" id="name" value="{{ $name ?? '' }}" name="name">

            <label for="number-phone">Телефон:</label>
            <input type="text" class="mask-phone" id="number-phone" name="tel">

            <label for="email">E-mail:</label>
            <input type="text" placeholder="E-mail" id="email" name="email">

            <div class="code-verification fb-confirm-phone" style="display: none">
                <label for="enter-code">Введіть код, який вам прийшов у СМС:</label>
                    <input id="enter-code" type="text" name="confirm_code" style="margin-bottom: 0">
                    <input type="hidden" name="compare_code"/>
                    {{--<a href="#">ОК</a>--}}
                <div style="display: none" class="fb-resend-confirm-code clr">
                    <input type="submit" class="fb-resend-code resend-code fb-create-client"
                           value="Відправити SMS повторно"/>
                </div>
                <div class="fb-message-sms-send" style="display: none">
                    <div class="field">Код підтвердження буде доступний: <span></span></div>
                </div>
            </div>

            <input type="hidden" name="profile_id" value="{{ $profile_id ?? '' }}">
            <input type="hidden" name="profile_url" value="{{ $profile_url ?? '' }}">
            <div class="forgot-password-btn">
                <input type="submit" class="accept-btn remodal-confirm fb-create-client" value="Зареєструватися" />
                <a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>
            </div>
        </form>
    </div>
</div>

@endpush