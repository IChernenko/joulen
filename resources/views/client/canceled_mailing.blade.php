@extends('layouts.app')

@section('content')
    <div class="select-category">
        <div class="container">
            <div class="auth-items subscription-client">
                <h2>Ви відписалися від розсилки про нові цінові пропозиції</h2>
                <p>Тепер вам не будуть приходити повідомлення про нові цінові пропозиції від спеціалістів.</p>
                <div class="choice-client">
                    <div class="choice-client-item close-project">
                        <p>Якщо ви передумали будувати станцію,
                            або вже знайшли спеціаліста ви можете </p>
                        <a href="{{ route('client.order.show', ['id' => $order->id]) }}" class="close-project-btn">Закрити проект</a>
                    </div>
                    <div class="choice-client-item subscription-again">
                        <p>Щоб бути в курсі нових пропозицій, ви можете</p>
                        <a href="{{ $order->client->user->remember_token ? route('confirm_mailing', ['order' => $order->id, 'token' => $order->client->user->remember_token]) : route('login') }}" class="main-btn">Підписатися знову</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('site-title')
    Відписка
@endsection