@extends('layouts.app')

@section('content')
    <div class="select-category subscription-info">
        <div class="container">
            <div class="auth-items">
                <h2>Дякуємо! Ви знову підписалися на розсилку!</h2>
                <p>Тепер вам будуть приходити повідомлення про нові цінові пропозиції від спеціалістів.</p>
                <span>Ой, це я випадково...  <a href="{{ $order->client->user->remember_token ? route('cancel_mailing', ['order' => $order->id, 'token' => $order->client->user->remember_token]) : route('login') }}">Відписатися</a></span>
            </div>
            <!-- /.auth-items -->
        </div>
        <!-- /.container -->
    </div>

@endsection

@section('site-title')
    Підписка
@endsection