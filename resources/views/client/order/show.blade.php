@extends('order.show')

@section('user-section')
    @yield('buttons')

    @yield('selected-master-request')

    @yield('selected-master-review')

    @yield('requests')
@endsection