@extends('client.order.show')

@section('buttons')
    @component('layouts.client.order.buttons', ['order' => $order])
        @slot('button')
            <a href="{{ route('client.order.run.checking', ['id' => $order->id]) }}" class="site_button-orange">Відновити проект</a>
        @endslot
    @endcomponent
@endsection