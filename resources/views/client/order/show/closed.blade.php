@extends('client.order.show')

@section('buttons')
    @component('layouts.client.order.buttons', ['order' => $order])
        @slot('button')
            <a href="{{ route('client.order.run.considering', ['id' => $order->id]) }}" class="site_button-orange">Відновити прийом заявок</a>
        @endslot
    @endcomponent
@endsection

@section('requests')
    @foreach($order->requests_from_active_masters as $request)
        @component('layouts.order.request', ['request' => $request])
            @slot('final')
                {{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }}
            @endslot

            @slot('buttons')
                <div class="master-message-buttons">
                    <a href="#mail-to-master" class="mail-to-master site_button-orange onlyBorder button-small" data-name="{{ $request->master->user->name }}" data-region="{{ $request->master->region->name }}" data-img="{{ $request->master->getAvatarSrc() }}" data-id="{{ $request->master->id }}">Написати повідомлення</a>
                </div>
            @endslot
        @endcomponent
    @endforeach

    {{--TODO: move email sending functionality to external file if it will be imlplemented on master page--}}
    @push('footer_scripts')
        <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
        <script type="text/javascript">
            var url = '{{ route('client.email.send') }}';
        </script>
        <script type="text/javascript" src="{{ asset('js/client/order/client_email.js') }}"></script>
    @endpush

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush

    @push('modals')
        <div  class="remodal modal-client-mail" data-remodal-id="mail-to-master">
            <div class="modal_header">
                <p>Написати повідомлення</p>
            </div>
            <div class="modal_body">
                {{--<form method="POST" action="{{ route('client.email.send') }}" id="message-to-master-form">--}}
                {{ csrf_field() }}
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <input type="hidden" name="master_id" value="" class="company-id">
                <div class="form_block">
                    <label for="">Кому</label>
                    <div class="user_block">
                        <div class="company-img user_img">
                            <img src="" alt="image">
                        </div>
                        <p class="company-name name"></p>
                        <p class="company-region region"></p>
                    </div>
                </div>
                <div class="form_block">
                    <label for="">Проект</label>
                    <input type="text" placeholder="{{ $order->title }}" disabled>
                </div>
                <div class="form_block">
                    <label for="">Повідомлення</label>
                    <textarea name="text" id="message-to-master-text"></textarea>
                </div>
                <p class="info">
                    <span>УВАГА!</span> Спеціаліст надішле Вам відповдь на Ваш е-мейл: <a href="#">{{ Auth::user()->email }}</a>
                </p>
                <div class="btn_block">
                    <button class='btn_cancel' data-remodal-action="cancel" >Скасувати</button>
                    <button class='btn_confirm submit'>Надіслати</button>
                </div>
                {{--</form>--}}
            </div>
        </div>

        <div class="remodal modal-client-mail" data-remodal-id="confirm-mail-to-master">
            <div class="modal_header">
                <p>Ваш лист надіслано</p>
            </div>
        </div>

        <div class="remodal modal-client-mail" data-remodal-id="fail-mail-to-master">
            <div class="modal_header">
                <p>Виникла помилка. Спробуйте надіслати лист пізніше.</p>
            </div>
        </div>
    @endpush
@endsection