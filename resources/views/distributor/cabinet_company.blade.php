@extends('layouts.master.app')

@section('content')

    <div class="cabinet-page-wrap">
        <h1 class="main-title">Про компанію</h1>
        <!-- /.main-title -->
        <div class="wrapper-page">
            <form class="form-container" action="{{ route('distributor.update') }}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                @if (count($errors) > 0)
                    <div class="error" id="error_block">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="color: red; list-style: none;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <input type="hidden" name="user_id" value="{{ $distributor->user_id }}">
                @include('layouts.distributor.company-info', ['distributor' => $distributor])
                <button class="btn btn-primary" type="submit">Зберегти</button>
            </form>
        </div>
        <!-- /.wrapper-page -->
        <!-- /.container -->
    </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/distributor/cabinet-company.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/load-avatar.js') }}"></script>
@endpush