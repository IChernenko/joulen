<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Клієнт залишив негативний відгук</title>
</head>
<body>
<h2>Клієнт залишив негативний відгук</h2>
<p>Для замовлення (ID:{{$order->id}}) “{{$order->title}}”. Клієнт залишив негативний відгук</p>
<p><a href="{{route('admin.order.show', ['id' => $order->id])}}">Переглянути деталі</a></p>
</body>
</html>