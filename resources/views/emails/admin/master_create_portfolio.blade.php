<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hовий об’єкт у портфоліо</title>
</head>
<body>
<p>Компанія «{{ $master->user->name }}» додала новий об‘єкт у портфоліо.</p>
<p><a href="{{ $route }}">Детальніше</a></p>
</body>
</html>