<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ 'Клієнт закрив проект - ' . $order->title }}</title>
</head>
<body>
<p>Клієнт щойно закрив проект {{ $order->title }} <a href="{{ route('admin.order.show', ['id' => $order->id]) }}">Переглянути деталі</a></p>
</body>
</html>
