<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Нове замовлення на {{ $preorder->title }}</title>
</head>
<body>
<h2>Нове замовлення на {{ $preorder->title }}</h2>
<p>Отримано нове замовлення на {{ $preorder->title }}: Ім'я: {{ $preorder->preorderable->name }}, Телефон: {{ $preorder->preorderable->phone }}, Е-мейл: {{ $preorder->preorderable->email }}.</p>
</body>
</html>