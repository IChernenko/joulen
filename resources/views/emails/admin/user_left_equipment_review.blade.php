<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новий відгук для бренду {{ $brand_title }}</title>
</head>
<body>
<p>Щойно додали новий відгук для бренду {{ $brand_title }}. Деталі <a href="{{ $review_route }}">ТУТ</a></p>
</body>
</html>