<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ env('DOMAIN') }}</title>
</head>
<body>
<p>Ваша заявка "<a href="{{ route('client.order.index') . '/' . $order->id }}">{{ $order->title }}</a>" прийнята до розгляду.</p>
<p>Найближчим часом ми з вами зв’яжемося по контактному телефону, який ви вказали у заявці.</p>
</body>
</html>