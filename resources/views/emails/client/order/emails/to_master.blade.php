<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Нове повідомлення від клієнта щодо проекту: {{$order->title}}</title>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
       style="background:#F2F2F2;margin:0; padding:0; font-family: 'Open Sans', sans-serif">
    <tr>
        <td style="background:#346F8A;"><p style="padding:30px; text-align:center; color:#fff"> ШАНОВНИЙ СПЕЦІАЛІСТ!</p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="420" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;">
                <tr>
                    <td colspan="2" style="text-align:center; color:#808080; padding:30px 40px; font-size:13px">

                        Замовник проекту <a href="{{ route('master.order.show', ['id' => $order->id] ) }}"
                                            style="color:#346F8A">“{{ $order->title }}, м.{{ $order->city }}”</a>
                        щойно надіслав вам повідомлення:

                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0 40px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0"
                               style="background:#fff; font-size:12px; color:#4d4d4d;">
                            <tr>
                                <td style="padding:15px">{!! nl2br($clientEmail->text) !!}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 1px 40px 10px 40px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0"
                               style="background:#fff; font-size:12px; text-align:center; color:#666">
                            <tr>
                                <td style="border-right:1px solid #f2f2f2; padding:12px">{{ $client->user->name }}</td>
                                <td style="padding:12px">{{ $client->user->email }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center; color:#4d4d4d; padding:20px 40px 30px; font-size:13px">
                        УВАГА! Не натискайте на кнопку “Відповісти”!<br>Щоб відповісти клієнту, напишіть лист на його
                        е-мейл: <a href="#" style="color:#4D4D4D;"> {{ $client->user->email }}</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>