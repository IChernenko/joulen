<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ваш проект опублікованний</title>
</head>
<body>
<p>Ваша проект <a  href="{{ route('client.order.show', ['id' => $order->id]) }}">{{$order->title}}</a>" пройшов модерацію.</p>
<p>Порядок роботи в сервісі:</p>
<p>1. Отримуйте заявки від спеціалістів</p>
<p>2. Вкажіть спеціаліста перед початком співпраці</p>
<p>3. Залишіть відгук про співпрацю</p>
</body>
</html>