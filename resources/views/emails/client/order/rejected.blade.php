<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ваш проект відхилено</title>
</head>
<body>
<div>Ваш проект {{$order->title}} відхилено модератором.
<a href="{{route('client.order.show', ['id' => $order->id ])}}">Деталі</a>
</div>
</body>
</html>