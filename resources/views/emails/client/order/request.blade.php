<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Нова цінова пропозиція від компанії {{ $orderRequest->master->user->name }}</title>
</head>
<body>
<h2>Вітаємо, {{ $orderRequest->order->client->user->name }}</h2>
<p>Компанія “{{ $orderRequest->master->user->name }}” ознайомилася із вашою заявкою і підготувала для вас цінову пропозицію.
    Ви можете переглянути деталі цінової пропозиції та порівняти її з пропозиціями інших компаній у вашому кабінеті:</p>
<a href="{{ route('client.order.show', ['id' => $orderRequest->order->id]) }}">Переглянути цінові пропозиції</a>

<p>Якщо вам потрібна консультація щодо вибору обладнання або спеціаліста - ви завжди можете звернутися в підтримку сервісу Джоуль.
    Ми надаємо тільки об’єктивну консультацію.</p>

<p>Набридла розсилка? <a href="{{ $orderRequest->order->client->user->remember_token ? route('cancel_mailing', ['order' => $orderRequest->order, 'token' => $orderRequest->order->client->user->remember_token]) : route('login') }}">Відписатися</a></p>
</body>
</html>
