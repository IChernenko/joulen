<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $order->title }}</title>
</head>
<body>
<h2>Нове замовлення - {{ $order->title }}</h2>
@yield('restored')
@yield('new')
<p>Тип {{$order->category->name}}: {{$order->subcategory->name}}</p>
{{--<p>{{ $order->email_title }}</p>--}}

@if(count($order->category->subcategories) == 1)
    @include("emails.client.order.show.{$order->category->slug}", ['order' => $order, $order->category->slug => $order->orderable])
@else
    @include("emails.client.order.show.{$order->category->slug}.{$order->subcategory->slug}", ['order' => $order, $order->category->slug => $order->orderable])
@endif

@if($order->client_comment != '')
    <p>Коментар</p>
    <p>{!! nl2br(e($order->client_comment)) !!}</p>
@endif

<p><b>Клієнт:</b></p>
<p>Ім’я: {{ $user->first_name }}</p>
<p>Телефон: {{ $user->phone }}</p>
<p>Е-мейл: {{ $user->email }}</p>
<p><a href="{{ route('admin.order.show', ['id' => $order->id]) }}">Переглянути деталі</a></p>
</body>
</html>