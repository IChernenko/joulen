@if($order->orderable->{'delivery_block'})
    <p><b>Поставка обладнання та комплектуючих</b></p>

    <p>Необхідна потужність СЕС: {{ $ses->photocell_power }} кВт</p>
    @yield('ses-battery-capacity')
    <p>Цінова категорія обладнання : <br>
        {{ $ses->ses_price_category->name }}
        @if($ses->ses_price_category->slug == 'custom')
            {{ ': ' . $ses->parts_comment}}
        @endif
    </p>
@endif

@if($order->orderable->{'installation_block'})
    <p><b>Монтаж та налагодження</b></p>

    <p>Область: {{ $order->region->name }}</p>
    <p>Населений пункт: {{ $order->city }}</p>

    @yield('ses-area')
    <p>@yield('ses-house-comment-title') {{ $ses->house_comment }}</p>
@endif

@if($order->orderable->{'legal_block'})
    @if(!empty($ses->green_tariff))
        <p>Потрібне підключення до енергомережі та отримання “зеленого” тарифу.</p>
    @endif
@endif