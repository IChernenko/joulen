@extends('emails.client.order.show.ses')

@section('ses-area')
    {{--TODO: use area_unit here--}}
    <p>Площа ділянки: {{$ses->area}} </p>
@endsection

@section('ses-house-comment-title')
    Інша інформація по ділянці:
@endsection