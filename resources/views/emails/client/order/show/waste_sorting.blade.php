@if($order->orderable->{'delivery_block'})
    <p><b>Поставка сміттєсортувальної лінії</b></p>

    <p>Необхідна потужність сміттєсортувальної лінії: {{ $waste_sorting->waste_sorting_size->power }} тон/год</p>
    <p>Коментар клієнта: {!! nl2br(e($waste_sorting->size_comment)) !!}</p>
@endif

@if($order->orderable->{'installation_block'})
    <p><b>Загальнобудівельні роботи</b></p>

    <p>Необхідно провести загальнобудівельні роботи «під ключ»</p>
    <p>Область: {{ count($order->region) > 0 ? $order->region->name : ''}}</p>
    <p>Район або населений пункт: {{ $order->city }}</p>
    <p>Інша інформація щодо будівництва: {!! nl2br(e($waste_sorting->about_comment)) !!}</p>
@endif