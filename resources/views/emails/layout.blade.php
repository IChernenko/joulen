<!DOCTYPE HTML>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8;"/>
    <meta charset="utf-8"/>
    <title>{{ isset($title) ? $title : '' }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/site.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}"/>
    @stack('styles')
</head>
<body>

<div id="wrapper">
    @yield('content')
</div>

</body>
</html>