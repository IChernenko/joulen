<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ваша компанія не пройшла перевірку</title>
</head>
<body>
<h2>На жаль, ваша компанія не пройшла перевірку на сервісі Джоуль.</h2>
<p>Причина відхилення:</p><br>
<p style="word-wrap: break-word; white-space: pre-wrap;">{{ $user->master->rejection_reason }}</p>
</body>
</html>