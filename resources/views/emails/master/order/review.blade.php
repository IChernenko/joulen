<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новий відгук</title>
</head>
<body>
<p>Вам щойно залишили відгук щодо проекту "{{$order->title}}".</p>
<p><a href="{{ route('master.order.show', ['id' => $order->id]) }}">Переглянути деталі</a></p>

<h3>Звертаємо увагу!</h3>
<p>Для перегляду нових замовлень від клієнтів, вам потрібно <a href="{{ route('master.commission') }}">сплатити комісію</a> сервісу за виконані проекти.</p>
</body>
</html>