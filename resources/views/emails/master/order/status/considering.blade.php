<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Нове замовлення</title>
</head>
<body>
<h2> Нове замовлення</h2>
Ознайомтеся із новим проектом “{{$order->title}}”.
<a href="{{route('master.order.show', ['id' => $order->id])}}">Переглянути деталі</a><br>
Відключити розсилку можна у <a href="{{route('master.profile_pages.subscribe')}}">вашому профілі.</a>
</body>
</html>
