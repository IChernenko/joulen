<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Вас обрали виконавцем</title>
</head>
<body>
<h2>Вас обрали виконавцем</h2>
Вас обрали виконавцем для замовлення “{{$order->title}}”.<br>
<a href="{{route('master.order.show', ['id' => $order->id])}}">Переглянути деталі</a><br>
</body>
</html>
