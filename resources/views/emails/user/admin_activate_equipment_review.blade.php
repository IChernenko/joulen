<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ваш відгук для бренду “{{ $brand_title }}” схвалено</title>
</head>
<body>
<p>Ваш відгук для бренду <a href="{{ $brand_route }}">“{{ $brand_title }}”</a> схвалено адміністратором і він тепер доступний для перегляду іншими користувачами.</p>
<p>Переглянути ваш відгук можна <a href="{{ $brand_route }}#equipment-reviews">ТУТ</a></p>
</body>
</html>