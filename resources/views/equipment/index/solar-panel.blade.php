@extends('equipment.index')

@section('page-title', 'сонячних панелей')

@section('filter-title', 'панелей')

@section('brand-name-select-options')
    @foreach($panels as $panel)
        <option value="{{ $panel->id }}" {{ old('id') == $panel->id ? 'selected' : '' }}>{{ $panel->title }}</option>
    @endforeach
@endsection

@section('site-title', 'Каталог виробників сонячних панелей: рейтинг, відгуки, купити в Україні')

@section('site-meta-description', 'Список всіх виробників сонячних панелей, батарей, доступних в Україні: рейтинг, відгуки, де купити, отзывы')