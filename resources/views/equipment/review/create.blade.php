@extends('layouts.app')

@section('content')
    <div class="wrapper write-review-page">
        <div class="container">
            <div class="wrapper-page">
                <h2 class="main-title">Написати відгук</h2>
                <div class="review-requirements">
                    <p>Залишаючи чесний відгук, ви робите ЗНАЧНИЙ ВНЕСОК у розвиток "зеленої" енергетики і покращення
                        екології.</p>
                    <p>Ми віримо у чесність, адекватність та щирість українців. І що разом ми зможемо зробити ринок
                        сонячної
                        енергетики прозорим та цивілізованим.</p>

                    <div class="condition-comments">
                        <p>Зверніть увагу:</p>
                        <ul>
                            <li><span>1.</span> Всі відгуки проходять поперденю модерацію. Тобто ваш відгук з'явиться на
                                сайті лише після перевірки адміном.
                            </li>
                            <li><span>2.</span> Вам потрібно буде авторизуватися/зареєструватися на сайті. Це зроблено для
                                того, щоб відсіяти неякісні відгуки
                            </li>
                            <li><span>3.</span> Відгук буде опубліковано від імені клієнта або від імені спеціаліста -
                                в залежності від того, під чиїм іменем ви зареструєтеся (авторизуєтеся)
                            </li>
                        </ul>
                    </div>
                </div>

                <form enctype="multipart/form-data" id="review-form" class="add-brand-form" action="{{ route('brand.review.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="validation_errors_modal backend_validation_errors">
                        @component('layouts.validation_errors')
                        @endcomponent
                    </div>

                    <div class="form-block review-company-name">
                        <label for="">Відгук про компанію:</label>
                        <input type="hidden" name="equipment_id" value="{{ $brand->id }}">
                        <p>
                            {{ $brand->title }} <span>(Виробник {{ $brand->type_id == $invertor_type_id
                            ? 'інверторів' : 'сонячних панелей'}})</span>
                        </p>
                    </div>
                    <!-- /.form-block -->

                    <div class="form-block">
                        <label for="">Чи рекомендуєте ви обладнання цього бренду?</label>
                        <div class="styled-radio wrap-radio">
                            <input data-validation="required" data-validation-error-msg="Обов'язкове поле"
                                   type="radio" name="positive" value="1" id="yes">
                            <label for="yes" class="selectItem">Так</label>
                            <input data-validation="required" type="radio" name="positive" value="0" id="no">
                            <label for="no" class="selectItem">Ні</label>
                        </div>
                    </div>

                    <div class="form-block">
                        <label for="review-title" class="selectItem">Заголовок відгуку:</label>
                        <div class="counter-symbol-item">
                            <p class="counter-symbol"> Максимум 45 символів <span id="max-review-title">45</span></p>
                            <!--Максимум 45 символів (залишилося 45)-->

                            <input data-validation="required" data-validation-error-msg="Обов'язкове поле, введіть не більше 45 символів"
                                   id="review-title" name="title">
                            <p>Придумайте короткий заголовок, який у декількох словах підсумує ваш досвід</p>
                        </div>
                        <!-- /.counter-symbol -->
                    </div>

                    <div class="form-block">
                        <label for="name" class="selectItem">Оцініть наступні характеристики:</label>
                        <div class="rating-company" style="flex-flow: inherit">
                            <div class="overal-quality">
                                <span>Якість</span>
                                <span>Ціна</span>
                                <span>Сервіс</span>
                            </div>
                            <div class="stars-wrap">
                                <fieldset class="rating">
                                    <input data-validation="required" data-validation-error-msg="Обов'язкове поле"
                                           type="radio" id="star5" name="quality_rating" value="5" />
                                    <label class = "full" for="star5" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star4" name="quality_rating" value="4" />
                                    <label class = "full" for="star4" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star3" name="quality_rating" value="3" />
                                    <label class = "full" for="star3" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star2" name="quality_rating" value="2" />
                                    <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star1" name="quality_rating" value="1" />
                                    <label class = "full" for="star1" title="Sucks big time - 1 star"></label>

                                </fieldset>
                                <fieldset class="rating">
                                    <input data-validation="required" data-validation-error-msg="Обов'язкове поле"
                                           type="radio" id="star10" name="price_rating" value="5" />
                                    <label class = "full" for="star10" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star9" name="price_rating" value="4" />
                                    <label class = "full" for="star9" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star8" name="price_rating" value="3" />
                                    <label class = "full" for="star8" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star7" name="price_rating" value="2" />
                                    <label class = "full" for="star7" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star6" name="price_rating" value="1" />
                                    <label class = "full" for="star6" title="Sucks big time - 1 star"></label>

                                </fieldset>
                                <fieldset class="rating">
                                    <input data-validation="required" data-validation-error-msg="Обов'язкове поле"
                                           type="radio" id="star15" name="service_rating" value="5" />
                                    <label class = "full" for="star15" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star14" name="service_rating" value="4" />
                                    <label class = "full" for="star14" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star13" name="service_rating" value="3" />
                                    <label class = "full" for="star13" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star12" name="service_rating" value="2" />
                                    <label class = "full" for="star12" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star11" name="service_rating" value="1" />
                                    <label class = "full" for="star11" title="Sucks big time - 1 star"></label>

                                </fieldset>
                            </div>
                            <!-- /. -->
                        </div>
                        <!-- /.rating-company -->
                    </div>


                    <div class="form-block">
                        <label for="your-review" class="selectItem">Ваш відгук:</label>
                        <div class="counter-symbol-item">
                            <p class="counter-symbol">Необхідно ввести ще <span id="min-your-review">200</span> символів</p>
                            <textarea  id="your-review" name="body" data-validation="length"
                                       data-validation-error-msg="Введіть не менше ніж 200 символів" data-validation-length="min200"> </textarea>
                            <p>Вкажіть як переваги, так і недоліки цього обладнання. <br>
                                Також напишіть як на вашу думку можна покращити сервіс/якість.</p>
                        </div>
                        <!-- /.counter-symbol -->
                    </div>
                    <div class="form-block agreement-conditions">
                        <div class="agreement-conditions-item" >
                            <div class="styled-checkbox">
                                <input type='checkbox' data-validation="required" id="agree">
                                <label for="agree"></label>
                            </div>
                            <!-- /.custom-checkbox -->
                            <p>Я залишаю цей відгук для того щоб допомогти іншим людям робити правильний вибір.</p>
                            <p>Я не отримую ніякої вигоди від цієї чи іншої компанії за те, що залишив відгук.</p>
                            <p>Я розумію, що сервіс {{ env('DOMAIN') }} негативно відноситься до фейкових відгуків.</p>
                        </div>
                        <!-- /.agreement-conditions -->
                    </div>
                    <div class="add-review-btn">
                        <button type="submit" class="main-btn">Додати відгук</button>
                        <p>Ви завжди зможете відредагувати чи видалити ваш відгук</p>
                        <!-- /.main-btn -->
                    </div>
                    <!-- /.add-review-btn -->

                </form>

            </div>
        </div>
        <!-- /.review-requirements -->

    </div>
@endsection

@push('footer_scripts')
<script src="{{ asset('js/jquery.form-validator.min.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/equipment/equipment-review/create.js') }}"></script>
@endpush