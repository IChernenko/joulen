@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="find-out-cost">
                <h3>Порівняйте ціни на cонячні електростанції
                    від кращих монтажників України</h3>
                <a href="{{ route('temp.step0') }}" class="main-btn">
                    Дізнатися вартість
                    <img src="{{ asset('images/arrow.png') }}" alt="image">
                </a>
                <!-- /.main-btn -->
            </div>
            <!-- /.find-out-cost -->
            <div class="wrapper-page">
                <div class="container-sm">
                    <div class="about-proven-companies">
                        <div class="general-data-companies">
                            <div class="logo-company">
                                <img src="{{ $brand->logo ? $brand->logo->getPath(true) : asset('images/default_photo.jpg') }}" alt="image">
                            </div>
                            <!-- /.logo-company -->
                            <div class="general-info-companies-view">
                                <p class="company-specialization">@yield('brand-type')</p>
                                <h4 class="name-company">{{ $brand->title }}
                                    @if(Auth::check() && Auth::user()->isAdmin())
                                        <a class="site-icon-edit" href="{{ route('admin.equipment.edit', $brand->id)}}"></a>
                                    @endif
                                </h4>
                                <!-- /.name-company -->
                                <div class="total-rating">
                                    @if($brand->reviews_count)
                                    <span class="value-evaluation">{{ number_format($brand->sum_rating * 5 / 100, 2) }}</span>
                                    @endif
                                    <div class="stars-wrapper">
                                        <div class="stars-off stars">
                                            <div class="stars-on stars" style="width:{{ $brand->sum_rating }}%"></div>
                                        </div>
                                    </div>
                                    <!-- /. -->
                                    <p>Відгуків <a href="#equipment-reviews">{{ $brand->reviews_count }}</a></p>

                                </div>

                                @if ($brand->description)
                                    <div class="text-block">
                                        <p>{{ $brand->description }}</p>
                                    </div>

                                    <a href="#" class="read-more">Читати більше <img
                                                src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                                @else
                                    <div class="text-block no-text">
                                        Інформація про бренд відсутня
                                    </div>
                            @endif
                            <!-- /.read-more -->
                                <!-- /.text-block -->
                            </div>
                            <!-- /.general-info-companies -->
                        <div class="rating-company" style="flex-flow: wrap row">
                            <div class="stars-container">
                                <div class="stars-wrapper">
                                    <div class="stars-off stars">
                                        <div class="stars-on stars" style="width:{{ $brand->quality_rating }}%"></div>
                                    </div>
                                </div>
                                <div class="stars-wrapper">
                                    <div class="stars-off stars">
                                        <div class="stars-on stars" style="width:{{ $brand->price_rating }}%"></div>
                                    </div>
                                </div>
                                <div class="stars-wrapper">
                                    <div class="stars-off stars">
                                        <div class="stars-on stars" style="width:{{ $brand->service_rating }}%"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.stars-wrap -->
                            <div class="overal-quality">
                                <span>Якість</span>
                                <span>Ціна</span>
                                <span>Сервіс</span>
                            </div>
                            <!-- /.quality -->
                        </div>
                        <!-- /.rating-company -->
                            <div class="main-info-company">
                                @if ($brand->website)
                                    <div>
                                        <p>Оф.сайт:</p>
                                        <a target="_blank" rel="nofollow" href="{{ $brand->website }}">{{ $brand->website }}</a>
                                    </div>
                                @endif
                                @if ($brand->foundation_year)
                                    <div>
                                        <p>Рік заснування:</p>
                                        <span>{{ $brand->foundation_year }}</span>
                                    </div>
                                @endif
                                @if ($brand->headquarters)
                                    <div>
                                        <p>Штаб-квартира</p>
                                        <span>{{ $brand->headquarters }}</span>
                                    </div>
                                @endif
                                @if ($brand->production)
                                    <div>
                                        <p>Виробництво:</p>
                                        <span>{{ $brand->production }}</span>
                                    </div>
                                @endif
                                @if ($brand->ukraine_office)
                                    <div>
                                        <p>Офіс в Україні:</p>
                                        <span>{{ $brand->ukraine_office }}</span>
                                    </div>
                                @endif
                            </div>
                            <!-- /.main-info-company -->
                        </div>
                        <!-- /.general-data-companies -->
                        <div class="general-info-companies" itemscope itemtype="http://schema.org/Product">
                            <p class="company-specialization">@yield('brand-type')</p>
                            <h4 class="name-company" itemprop="name">{{ $brand->title }}
                                @if(Auth::check() && Auth::user()->isAdmin())
                                    <a class="site-icon-edit" href="{{ route('admin.equipment.edit', $brand->id)}}"></a>
                                @endif
                            </h4>
                            <!-- /.name-company -->
                            <div class="total-rating" itemprop="aggregateRating"
                                 itemscope itemtype="http://schema.org/AggregateRating">
                                @if($brand->reviews_count)
                                <span class="value-evaluation" itemprop="ratingValue">{{ number_format($brand->sum_rating * 5 / 100, 2) }}</span>
                                @endif

                                <div class="stars-wrapper">
                                    <div class="stars-off stars" itemscope itemtype="http://schema.org/Rating">
                                        <meta itemprop="worstRating" content = "0">
                                        <div class="stars-on stars" style="width:{{ $brand->sum_rating }}%"></div>
                                        <span style="display: none" itemprop="ratingValue">{{ number_format($brand->sum_rating * 5 / 100, 2) }}</span>
                                        <span style="display: none" itemprop="bestRating">5</span>
                                    </div>
                                </div>

                                <p>Відгуків <a href="#equipment-reviews">{{ $brand->reviews_count }}</a></p>
                                    <span style="display: none" itemprop="reviewCount">{{ $brand->reviews_count }}</span>
                            </div>
                            @if ($brand->description)
                                <div class="text-block" itemprop="description">
                                    <p>{{ $brand->description }}</p>
                                </div>

                                <a href="#" class="read-more">Читати більше <img
                                            src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                            @else
                                <div class="text-block">
                                    Інформація про бренд відсутня
                                </div>
                            @endif

                        </div>
                        <!-- /.info-companies -->
                    </div>
                    {{--<!-- /.about-proven-companies -->--}}

                    <div class="general-list-companies view-sm" id="equipment-dealers">
                        @if ($brand->distributors_count > 0)
                            <div class="distributors">
                            <div class="group-company">
                            <p>Офіційні дистриб’ютори:</p>
                            <div class="tooltip-btn">
                                ?
                                <p class="tooltip">
                                    <span>Офіційні дистриб’ютори</span> - це компанії, які офіційно ввозять обладнання на
                                    територію України і несуть гарантійні зобов’язання перед покупцями.
                                    <br><br>
                                    Сервіс “Джоуль” отримав підтвердження від виробників обладнання про те, що ці компанії
                                    є офіційними дистриб’юторами на території країни.
                                </p>
                            </div>
                            <ul>
                                @foreach($brand->distributors as $distributor)
                                    <li>
                                        <a href="{{ route('distributor.show', ['slug' => $distributor->slug]) }}">{{ $distributor->company_name }}</a>
                                        <span class="verified-company">
                                            <img src="{{ asset('images/check.svg') }}" alt="image">
                                        </span>
                                    </li>
                                @endforeach
                                <li class="view-more-item">
                                    <a href="#" class="view-more">Показати решту <img
                                                src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                                </li>
                            </ul>
                        </div>
                        </div>
                        @endif
                        @if ($official_dealers->isNotEmpty())
                                <div class="official-dealers">
                            <div class="group-company">
                            <p>Офіційні дилери:</p>
                            <div class="tooltip-btn">
                                ?
                                <p class="tooltip">
                                    <span>Офіційні дилери</span> - це регіональні дилери дистриб’юторів, які продають офіційно
                                    ввезене обладнання із гарантією виробника/дистриб’ютора.
                                    <br><br>
                                    Сервіс “Джоуль” отримав підтвердження від дистриб’юторів, що ці компанії є їхніми регіональними дилерами.
                                    <br><br>
                                    Рекомендуємо купувати обладнання лише у офіційних дилерів.
                                </p>
                            </div>
                            <ul>
                                @foreach($official_dealers as $dealer)
                                    <li>
                                        <a href="{{ route('master.show', ['id' => $dealer->id]) }}">{{ $dealer->user->name }}</a>
                                        @if($dealer->status->slug == 'verified')
                                            <span class="verified-company">
                                        <img src="{{ asset('images/check.svg') }}" alt="image">
                                    </span>
                                        @endif
                                    </li>
                                @endforeach
                                <li class="view-more-item">
                                    <a href="#" class="view-more">Показати решту <img
                                                src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                                </li>
                            </ul>
                        </div>
                        </div>
                        @endif
                        @if ($brand->masters_count > 0)
                                <div class="dealers">
                            <div class="group-company dealers">
                            <p>Дилери:</p>
                            <div class="tooltip-btn">
                                ?
                                <p class="tooltip">
                                    <span>Дилери</span> - це компанії, які можуть встановити (або продати) сонячну станцію
                                    із
                                    обладнанням цього бренду.
                                    Дилером може бути будь-яка компанія-інсталятор.
                                    <br><br>
                                    Статус “дилера” не означає, що ця компанія є офіційним представником бренду.</p>
                            </div>
                            <ul>
                                @foreach($dealers as $master)
                                    <li>
                                        <a href="{{ route('master.show', ['id' => $master->id]) }}">{{ $master->user->name }}</a>
                                        @if($master->status->slug == 'verified')
                                            <span class="verified-company">
                                        <img src="{{ asset('images/check.svg') }}" alt="image">
                                    </span>
                                        @endif
                                    </li>
                                @endforeach
                                <li class="view-more-item">
                                    <a href="#" class="view-more">Показати решту <img
                                                src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                                </li>
                            </ul>
                        </div>
                        </div>
                        @endif
                        <!-- /.group-company -->
                    </div>
                    <!-- /.list-companies -->


                    <h3 class="implemented-projects" id="equipment-photos">Фото реалізовиних проектів в Україні (<span>{{ count($brand->portfolios) }}</span> фото)</h3>
                    @if (count($brand->portfolios) == 0)
                        <span class="information-absent">Фото відсутні</span>
                    @else
                        <div class="swiper-completed-projects swiper-container">
                            <div class="swiper-wrapper">
                                @foreach($brand->portfolios as $portfolio)
                                    {{-- TODO: think how do this check not in view --}}
                                    @if( $equipment_type == 'inverter' ? (!$portfolio->invertor_photo ?: $portfolio->invertor_photo->public == true)
                                    : (!$portfolio->panel_photo ?: $portfolio->panel_photo->public == true))
                                    <div class="swiper-slide photo-completed-project">
                                        <a class="popup-link photo-project" title="" href="{{ $equipment_type == 'inverter' ? $portfolio->invertor_photo_src : $portfolio->panel_photo_src }}"
                                           data-popup="
                            <h4>Фото {{ $equipment_type == 'inverter' ? 'інверторів' : 'панелей' }}: <b>{{ $brand->title }}</b></h4>
                                   <ul>
                                      <li><span>Потужність станції:</span>{{ $portfolio->power }} кВт</li>
                                      <li><span>Рік введення в експлуатацію:</span>{{ $portfolio->year }}</li>
                                      <li><span>Населений пункт:</span>{{ $portfolio->getCity() }}</li>
                                      <li><span>Виконавець:</span><a href='{{ route('master.show', ['id' => $portfolio->master->id ]) }}'>{{ $portfolio->master->user->name }}</a></li>
                                  </ul>">
                                            <img src="{{ $equipment_type == 'inverter' ? !$portfolio->invertor_photo ?:
                                            $portfolio->invertor_photo->getPath(true) : !$portfolio->panel_photo ?: $portfolio->panel_photo->getPath(true)}}" alt="image">

                                            <span class="to-enlarge ">
                                <img src="{{ asset('images/loupe.png') }}" alt="image">
                                <span>Збільшити</span>
                            </span></a>


                                        <div class="location-completed-project">
                                            <p><img src="{{ asset('images/company-location.png') }}" alt="image">{{ $portfolio->getCity() }}</p>
                                            <span><img src="{{ asset('images/company-specialization.png') }}" alt="image">
                               <a href="{{ route('master.show', ['id' => $portfolio->master->id ]) }}">{{ $portfolio->master->user->name }}</a></span>
                                        </div>
                                        <!-- /.location-completed-project -->
                                        <!-- /.to-enlarge -->
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                            <!-- /.swiper-srapper -->
                            <div class="swiper-button swiper-button-prev"><img
                                        src="{{ asset('images/swiper-arrow-left.png') }}" alt="image">
                            </div>
                            <div class="swiper-button swiper-button-next"><img
                                        src="{{ asset('images/swiper-arrow-right.png') }}" alt="image">
                            </div>
                        </div>
                @endif
                <!-- /.swiper-completed-projects -->
                    <div class="reviews-container" id="equipment-reviews">
                        <div class="write-review">
                            <h3 class="reviews-company">Відгуки</h3>
                            @if(!Auth::check() || Auth::user()->isClient() || Auth::user()->isMaster())
                            <a href="{{ route('brand.review.create', ['equipment_type' => $equipment_type, 'brand_slug' => $brand->slug]) }}" class="main-btn">
                                + Написати відгук
                            </a>
                            @endif
                            <!-- /.main-btn -->
                        </div>
                        <div class="reviews-wrapper reviews-wrapper-company">
                            <p>Відгуки від компаній-інсталяторів (<span>{{ $brand->master_reviews_count }}</span>)</p>
                            @if($auth_user_review && $auth_user_review->user->master && $auth_user_review->status->slug != 'verified')
                                @include('layouts.equipment.user_review', ['equipment_type' => $equipment_type, 'review' => $auth_user_review])
                            @endif
                            @foreach($brand->master_reviews as $review)
                                @include('layouts.equipment.user_review', ['equipment_type' => $equipment_type, 'review' => $review])
                            @endforeach
                            <!-- /.review-company -->

                            <!-- /.review-company -->
                            @if($brand->master_reviews_count > 5)
                            <div class="show-more-btn">
                                <a href="#" class="show-more">Показати старіші</a>
                                <!-- /.show-more -->
                            </div>
                            @endif
                            <!-- /.show-more-btn -->

                        </div>


                        <div class="reviews-wrapper reviews-wrapper-customer">
                            <p>Відгуки від клієнтів (<span>{{ $brand->client_reviews_count }}</span>)</p>
                            @if($auth_user_review && $auth_user_review->user->client && $auth_user_review->status->slug != 'verified')
                                @include('layouts.equipment.user_review', ['equipment_type' => $equipment_type, 'review' => $auth_user_review])
                            @endif
                            @foreach($brand->client_reviews as $review)
                                @include('layouts.equipment.user_review', ['equipment_type' => $equipment_type, 'review' => $review])
                            @endforeach

                            @if ($brand->client_reviews_count > 5)
                            <div class="show-more-btn">
                                <a href="#" class="show-more">Показати старіші</a>
                                <!-- /.show-more -->
                            </div>
                            @endif
                        </div>

                    </div>
                    <!-- /.reviews-container -->
                </div>
                <!-- /.reviews-wrapper -->
                <div class="general-list-companies view" id="equipment-dealers">
                    @if ($brand->distributors_count > 0)
                        <div class="distributors">
                        <div class="group-company distributors">
                        <p>Офіційні дистриб’ютори:</p>
                        <div class="tooltip-btn">
                            ?
                            <p class="tooltip">
                                <span>Офіційні дистриб’ютори</span> - це компанії, які офіційно ввозять обладнання на
                                територію України і несуть гарантійні зобов’язання перед покупцями.
                                <br><br>
                                Сервіс “Джоуль” отримав підтвердження від виробників обладнання про те, що ці компанії
                                є офіційними дистриб’юторами на території країни.
                            </p>
                        </div>
                        <ul>
                            @foreach($brand->distributors as $distributor)
                                <li>
                                    <a href="{{ route('distributor.show', ['id' => $distributor->slug]) }}">{{ $distributor->company_name }}</a>
                                    <span class="verified-company">
                                        <img src="{{ asset('images/check.svg') }}" alt="Компанія перевірена">
                                        <span class="help-title">Компанія перевірена</span>
                                    </span>
                                </li>
                            @endforeach
                            <li class="view-more-item">
                                <a href="#" class="view-more">Показати решту <img
                                            src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                            </li>
                        </ul>
                    </div>
                    </div>
                    @endif
                    @if ($official_dealers->isNotEmpty())
                        <div class="official-dealers">
                        <div class="group-company official-dealers">
                        <p>Офіційні дилери:</p>
                        <div class="tooltip-btn">
                            ?
                            <p class="tooltip">
                                <span>Офіційні дилери</span> - це регіональні дилери дистриб’юторів, які продають офіційно
                                ввезене обладнання із гарантією виробника/дистриб’ютора.
                                <br><br>
                                Сервіс “Джоуль” отримав підтвердження від дистриб’юторів, що ці компанії є їхніми регіональними дилерами.
                                <br><br>
                                Рекомендуємо купувати обладнання лише у офіційних дилерів.
                            </p>
                        </div>
                        <ul>
                            @foreach($official_dealers as $dealer)
                                <li>
                                    <a href="{{ route('master.show', ['id' => $dealer->id]) }}">{{ $dealer->user->name }}</a>
                                    @if($dealer->status->slug == 'verified')
                                        <span class="verified-company">
                                        <img src="{{ asset('images/check.svg') }}" alt="Компанія перевірена">
                                            <span class="help-title">Компанія перевірена</span>
                                    </span>
                                    @endif
                                </li>
                            @endforeach
                            <li class="view-more-item">
                                <a href="#" class="view-more">Показати решту <img
                                            src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                            </li>
                        </ul>
                    </div>
                    </div>
                    @endif
                    @if ($brand->masters_count > 0)
                        <div class="dealers">
                        <div class="group-company dealers">
                        <p>Дилери:</p>
                        <div class="tooltip-btn">
                            ?
                            <p class="tooltip">
                                <span>Дилери</span> - це компанії, які можуть встановити (або продати) сонячну станцію
                                із
                                обладнанням цього бренду.
                                Дилером може бути будь-яка компанія-інсталятор.
                                <br><br>
                                Статус “дилера” не означає, що ця компанія є офіційним представником бренду.</p>
                        </div>
                        <ul>
                            @foreach($dealers as $master)
                                <li>
                                    <a href="{{ route('master.show', ['id' => $master->id]) }}">{{ $master->user->name }}</a>
                                    @if($master->status->slug == 'verified')
                                        <span class="verified-company">
                                        <img src="{{ asset('images/check.svg') }}" alt="Компанія перевірена">
                                            <span class="help-title">Компанія перевірена</span>
                                    </span>
                                    @endif
                                </li>
                            @endforeach
                            <li class="view-more-item">
                                <a href="#" class="view-more">Показати решту <img
                                            src="{{ asset('images/blue-arrow.png') }}" alt="image"></a>
                            </li>
                        </ul>
                    </div>
                    @endif
                    <!-- /.group-company -->
                </div>
                <!-- /.list-companies -->
                <!-- /.about-proven-companies -->

            </div>
        </div>
        <!-- /.container -->
    </div>
    </div>
@endsection

@push('modals')
<div class="remodal" data-remodal-id="delete-review" id="delete" style="width: 450px">
    <div class="modal-header">
        <h4>Ви впевнені?</h4>
    </div>

    <div class="modal-body">
        <form action="" method="POST" id="form-delete-review">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="btn-modal">
                <button type="submit" class="btn btn-confirm">Так</button>
                <button class="btn btn-cancel" data-remodal-action="cancel">Скасувати</button>
            </div>
        </form>
    </div>
</div>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal-default-theme.css') }}"/>
@endpush
@push('footer_scripts')
<script type="text/javascript">
    var left_comment = {{ $left_comment ?: 'undefined' }};
    var deleteReviewURL = '{{ route('brand.review.destroy', ['id' => null]) }}';
</script>
<script type="text/javascript" src="{{ asset('js/magnific-popup-slider/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/equipment/show.js') }}"></script>
<script src="{{ asset('libs/remodal/remodal.min.js') }}"></script>
@endpush