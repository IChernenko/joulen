@extends('equipment.show')

@section('site-title')
    Сонячні панелі {{ $brand->title }}: відгуки, фото, рейтинг, дилери, інформація, де купити в Україні
@endsection

@section('site-meta-description')
    Детальна інформація про сонячні панелі {{ $brand->title }}: відгуки, фото, список дилерів, модельний ряд,
    де купити, отзывы, сравнение, купить инвертор
@endsection

@section('brand-type', 'Сонячні панелі')