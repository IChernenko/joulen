@extends('layouts.app')

@section('content')
    <div class="not-found-wrap">
        <div class="container">
            <div class="page-not-found">
                <div class="not-found-info">
                    <h1>404 <span>Сторінку не знайдено</span></h1>

                    <p>Що тепер робити?</p>
                    <ul class="styled-list">
                        <li><span class="list-number">1</span>Перейти на <a href="/">головну сторінку</a></li>
                        <li><span class="list-number">2</span>Написати нам все, що ви про нас думаеєте на скриньку <a
                                    href="mailto:{{ env('SUPPORT_MAIL') }}" target="_blank">{{ env('SUPPORT_MAIL') }}</a></li>
                        <li><span class="list-number">3</span>Продовжуйте дивитись в монітор та через 30 секунд вас буде автоматично переадресовано на головну сторінку</li>
                    </ul>
                </div>
                <div class="not-found-img">
                    <img src="{{ asset('images/404.png') }}" alt="image">
                </div>
                <!-- /.not-found-img -->
                <!-- /.not-found-info -->

            </div>
            <!-- /.page-not-found -->

        </div>

    </div>

    @push('footer_scripts')
        <script type="text/javascript">
            setTimeout(function() { window.location.href = '{{ route('home') }}'; }, 30000);
        </script>
    @endpush
@endsection