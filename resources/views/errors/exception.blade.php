@extends('layouts.old.app')

@section('content')
    <div id="content">

        <!--# 404 #-->
        <div class="container">
            <div id="err404">
                <div>
                    <h4>Що тепер робити?</h4>
                    <ul>
                        <li>Перейти на <a href="{{ route('home') }}"> головну сторінку</a></li>
                        <li>Написати нам все, що ви про нас думаеєте на скриньку {{ env('SUPPORT_MAIL') }}</li>
                        <li>Продовжуйте дивитись в монітор та через 30 секунд вас буде автоматично переадресовано на головну сторінку</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--# end 404 #-->

    </div>

    @push('footer_scripts')
    <script type="text/javascript">
        setTimeout(function() { window.location.href = '{{ route('home') }}'; }, 30000);
    </script>
    @endpush
@endsection