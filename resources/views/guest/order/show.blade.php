@extends('order.show')

@section('user-section')
    @if(!empty($order->request_by_master))
        @component('layouts.order.request', ['request' => $order->request_by_master])
            @slot('final')
            @endslot

            @slot('buttons')
            @endslot
        @endcomponent
    @endif
@endsection