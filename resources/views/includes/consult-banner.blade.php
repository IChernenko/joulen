<div class="service-consultant-container service-consultant-container_main">
    <div class="service-consultant-wrap">
        <div class="service-consultant">
            <div class="service-consultant__item service-consultant__icon-wrap">
                <img src="{{asset('/images/banner-consultant.png')}}" class="service-consultant__icon"/>
            </div>
            <div class="service-consultant__item service-consultant__content">
                <a href="{{ route('services.consulting') }}" class="service-consultant__link">
                    Замовити послуги персонального консультанта
                </a>
                <p class="service-consultant__text">
                    Персональний консультант допоможе зекономити ваші кошти та уникнути типових
                    помилок при будівництві сонячної станції.
                </p>
            </div>
        </div>
    </div>
</div>