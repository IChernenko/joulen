<div class="main-info main-info-faq">
    <h1 class="main-info--title gray">Це потрібно знати</h1>
    <p class="main-info--text">Дізнайтеся, чи доцільно встановлювати сонячну станцію у вашому випадку:</p>
    <div class="main-info--items">
        <a href="{{ route('assistant.step2_1') }}" class="main-info--item left">
            <img src="{{ asset('images/main-info-1.png') }}" alt="image">
            <span class="main-info--link">Сонячна станція для приватного будинку</span>
        </a>
        <a href="{{ route('assistant.step2_2') }}" class="main-info--item center">
            <img src="{{ asset('images/main-info-2.png') }}" alt="image">
            <span class="main-info--link">Сонячна станція для багатоквартирного будинку</span>
        </a>
        <a href="{{ route('assistant.step2_3') }}" class="main-info--item right">
            <img src="{{ asset('images/main-info-3.png') }}" alt="image">
            <span class="main-info--link">Сонячна станція на земельній ділянці</span>
        </a>
        <a href="{{ route('assistant.step2_4') }}" class="main-info--item left">
            <img src="{{ asset('images/main-info-4.png') }}" alt="image">
            <span class="main-info--link">Сонячна станція для бізнесу</span>
        </a>
        <a href="{{ route('assistant.step2_6') }}" class="main-info--item center">
            <img src="{{ asset('images/main-info-5.png') }}" alt="image">
            <span class="main-info--link">Сонячна станція для шкіл, лікарень, ОТГ</span>
        </a>
        <a href="{{ route('assistant.step2_5') }}" class="main-info--item right">
            <img src="{{ asset('images/main-info-6.png') }}" alt="image">
            <span class="main-info--link" >Інвестиції у сонячну енергетику</span>
        </a>
    </div>
</div>
