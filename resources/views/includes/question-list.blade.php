<div class="question-list--wrap left">
    <h3 class="question-list--title">Часті питання</h3>
    <ul class="question-list--items">
        @foreach($blog_faqs as $faq)
            <li class="question-list--item"><a href="{{ $faq->link }}">{{ $faq->title->rendered }}</a></li>
        @endforeach
    </ul>
    <a class="question-list--link" href="{{ $blog_faq_url }}">Наша база знань</a>
</div>

<div class="question-list--wrap right">
    <h3 class="question-list--title">Новини</h3>
    <ul class="question-list--items">
        @foreach($blog_news as $news)
            <li class="question-list--item"><a href="{{ $news->link }}">{{ $news->title->rendered }}</a></li>
        @endforeach
    </ul>
    <a class="question-list--link" href="{{ $blog_news_url }}">Всі новини</a>
</div>