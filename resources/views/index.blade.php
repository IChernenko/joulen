@extends('layouts.app')

@section('content')
    <div class="main-content-wrap">
        <div class="container">
            <div class="main-content">
                <div class="main-content--info">
                    <h2 class="main-content--title">ОТРИМАЙТЕ ДО <span>10 ПРОПОЗИЦІЙ</span></h2>
                    <p class="main-content--text">від монтажників сонячних станцій із вашого регіону вже сьогодні</p>
                    @if( !Auth::check() || (Auth::check() && Auth::user()->isClient()) )
                        <a href="{{ route('temp.step0') }}" class="main-content--link main-btn">Отримати пропозиції
                            <img src="{{ asset('images/arrow.png') }}">
                        </a>
                    @endif
                </div>
                <div class="main-content--video">
                    <div class="joule-video">
                        <a id="joule-video--link" class="joule-video--preview">
                            <h3>Як працює Джоуль? </h3>
                            <img src="{{ asset('images/joule-preview.jpg') }}" alt="image">
                            <div class="joule-video--container"></div>
                            <span class="joule-video--play"></span>
                        </a>
                    </div>
                    <!-- /.joule_video -->
                </div>
                <!-- /.main-content-photo -->
            </div>
            <!-- /.main-content -->
        </div>
        <!-- /.container -->
    </div>


    <div class="advantages-wrap">
        <div class="container">
            <div class="work-joule-wrap">
                <div class="work-joule">
                    <h2>ВСЕ ДУЖЕ ПРОСТО</h2>
                    <div class="stage-works">
                        <div class="stage-item stage-item-1">
                            <div class="stage-item-info">
                                <!-- /.stage-item-info -->
                                <img src="{{ asset('images/application.png') }}" alt="image">
                                <h3>Залишіть заявку</h3>
                            </div>
                            <p>Реєстрація та заповнення анкети про ваш будинок чи ділянку займає 5хв.</p>
                        </div>
                        <div class="stage-item stage-item-2">
                            <div class="stage-item-info">
                                <img src="{{ asset('images/proposition.png') }}" alt="image">
                                <h3>Отримайте цінові пропозиції</h3>
                            </div>
                            <p>Монтажники оцінять вашу заявку і запропонують свою ціну.</p>
                        </div>
                        <div class="stage-item stage-item-3">
                            <div class="stage-item-info">
                                <img src="{{ asset('images/choose-best.png') }}" alt="image">
                                <h3>Оберіть кращого!</h3>
                            </div>
                            <p>Порівняйте досвід монтажників, їхні цінові пропозиції та зробіть свій вибір.</p>
                        </div>
                        <!-- /.stage-item -->
                    </div>
                    <!-- /.stage-works -->
                </div>
                <!-- /.work-joule -->
                <!-- /.container -->
            </div>


            <div class="advantages">
                <h2 class="gray">Чому потрібно користуватися сервісом Джоуль?</h2>
            </div>
            <div class="advantages-item-wrap">
                <div class="advantages-item">
                    <span class="advantages-numbering">1</span>
                    <div class="advantages-item-info">
                        <img src="{{ asset('images/installers.png') }}" alt="image">
                        <h4>Ми особисто перевіряємо монтажників</h4>
                    </div>
                    <p>І ми ручаємося своєю репутацією за монтажників зі статусом «Перевірені»</p>
                    <!-- /.advantages-item-info -->
                </div>
                <div class="advantages-item">
                    <span class="advantages-numbering">2</span>
                    <div class="advantages-item-info">
                        <img src="{{ asset('images/price.png') }}" alt="image">
                        <h4>Ціни будуть нижчі на <br> 10-30%</h4>
                    </div>
                    <p>Монтажники конкурують між собою за ваше замовлення і готові зробити хорошу знижку</p>
                    <!-- /.advantages-item-info -->
                </div>
                <div class="advantages-item">
                    <span class="advantages-numbering">3</span>
                    <div class="advantages-item-info">
                        <img src="{{ asset('images/time.png') }}" alt="image">
                        <h4>Економія вашого <br> часу</h4>
                    </div>
                    <p>Вам потрібно витратити лише 5 хв, і ви отримаєте конкурентні пропозиції від кращих монтажників
                        України</p>
                    <!-- /.advantages-item-info -->
                </div>
                <div class="advantages-item">
                    <span class="advantages-numbering">4</span>
                    <div class="advantages-item-info">
                        <img src="{{ asset('images/choose-help.png') }}" alt="image">
                        <h4>Допомога із вибором</h4>
                    </div>
                    <p>Можемо надати вам об‘єктивну та неупереджену консультацію щодо вибору обладнання або
                        монтажника.</p>
                    <!-- /.advantages-item-info -->
                </div>
                <!-- /.advantages-item -->
            </div>

            <div class="company-info">
                <div class="company-info-wrap">
                    <!-- /.company-item-wrap -->
                    <div class="company-info-item quantity-company">
                        <span>{{ $all_masters }}</span>
                        <p>компаній зареєстровано на сервісі Джоуль</p>
                    </div>
                    <!-- /. -->
                    <div class="company-info-item quantity-company">
                        <span>{{ $masters_verified }}</span>
                        <p>компаній ми перевірили особисто</p>
                    </div>
                    <!-- /. -->
                    <div class="company-info-item quantity-company">
                        <span>{{ $orders_with_selected_master }}</span>
                        <p>клієнтів вже знайшли монтажника через Джоуль</p>
                    </div>
                </div>
                <!-- /.quantity -->
            </div>
            @include('includes.knowledge')
            @include('includes.consult-banner')

            <div class="question-list">
                @include('includes.question-list')
            </div>

        </div>
        <!-- /.container -->
    </div>

    <div class="implemented-projects-wrap">
        <div class="container">
            <div class="implemented-projects">
                <h2 class="gray">Реалізовані проекти</h2>
            </div>
            <div class="swiper-container swiper-implemented-projects ">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="implemented-projects-img">
                            <!-- /.implemented-projects-img -->
                            <div class="img-border">
                                <img src="{{ asset('images/Dragomyrchany.jpg') }}" alt="image">
                            </div>
                            <!-- /.img-border -->

                        </div>
                        <div class="implemented-projects-info">
                            <h3>Мережева сонячна електростанція 12 кВт</h3>
                            <p>Особисто для мене, сервіс Джоуль став поштовхом для встановлення сонячної
                                електростанції.</p>
                            <!--<img src="img/quotes-tops.svg" alt="image">-->
                            <span>Максим, с.Драгормирчани</span>
                        </div>
                        <!-- /.implemented-projects-info -->
                    </div>

                    <div class="swiper-slide">
                        <div class="implemented-projects-img">
                            <!-- /.implemented-projects-img -->
                            <div class="img-border">
                                <img src="{{ asset('images/Varash.jpg') }}" alt="image">
                            </div>
                            <!-- /.img-border -->

                        </div>
                        <div class="implemented-projects-info">
                            <h3>Автономна сонячна електростанція 1 кВт</h3>
                            <p>На мою заявку відгукнулося більше десяти спеціалістів, що дозволило мені відразу зробити
                                оптимальний вибір.
                                Рекомендую cервіс {{ env('DOMAIN') }}&nbsp;майбутнім власникам СЕС.</p>
                            <!--<img src="img/quotes-tops.svg" alt="image">-->
                            <span>Владислав, с.Бабка</span>
                        </div>
                        <!-- /.implemented-projects-info -->
                    </div>

                    <div class="swiper-slide">
                        <div class="implemented-projects-img">
                            <!-- /.implemented-projects-img -->
                            <div class="img-border">
                                <img src="{{ asset('images/3f.jpg') }}" alt="image">
                            </div>
                            <!-- /.img-border -->

                        </div>
                        <div class="implemented-projects-info">
                            <h3>Мережева сонячна електростанція 10 кВт</h3>
                            <p>Велика подяка сервісу ДЖОУЛЬ! В Інтернеті багато пропозицій про монтаж, але на жаль не
                                всі викликають
                                довіру. З ДЖОУЛЕМ все простіше. Написав свою заявку, розглянув пропозиції, вибрав
                                монтажника, і “вуаля”.</p>
                            <!--<img src="img/quotes-tops.svg" alt="image">-->
                            <span>Ервін, м.Рахів</span>
                        </div>
                        <!-- /.implemented-projects-info -->
                    </div>
                </div>
                <div class="swiper-pagination">
                </div>
            </div>
            <!-- /.implemented-projects -->
        </div>
        <!-- /.container -->
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/youtube-popup/video.popup.css') }}"/>
@endpush


@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('libs/youtube-popup/video.popup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/home/index.js') }}"></script>
@endpush


