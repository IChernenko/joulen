<!--# footer #-->
<footer class="page-footer-wrap" role="contentinfo">
    <div class="container">
        <div class="page-footer">
            <!-- /.page-footer -->
            <p class="copyright">
                <a href="{{ route('home') }}">© {{ env('DOMAIN') }}</a>
            </p>
            <div class="footer-contacts">
                <p class="contact-phone">
                    <a href="tel:+380500745465"><img src="{{ asset('images/icon-phone.svg') }}" alt="image">+38 (050) 074-54-65</a>
                </p>
                <p class="contact-mail">
                    <a href="mailto:{{ env('SUPPORT_MAIL') }}"><img src="{{ asset('images/icon-mail.svg') }}" alt="image">{{ env('SUPPORT_MAIL') }}</a>
                </p>
            </div>
            <p class="installer-company">
                <a href="{{ route('privacy') }}">Політика конфіденційності</a>
            </p>
            <p class="installer-company"><a href="{{ route('master.register') }}">Компаніям-інсталяторам</a></p>
            <!-- /.contact-mail -->
            <!-- /.contact-phone -->
        </div>
    </div>
    <!-- /.footer-contacts -->
    <!-- /.copyright -->

    <!-- /.company-social -->
</footer>

