@if($requests->count())
<div class="container textCenter another_masters_requests_button">
    <a href="javascript:" class="site_button-orange onlyBorder another_masters_requests_button">
            <span class="mobileHide">
                Показати решту спеціалістів, які відгукувалися на ваш проект
            </span>
        <span class="mobileShow">
            Інші заявки
        </span>
    </a>
</div>

<div class="another_masters_requests" style="display: none">
    @foreach($requests as $request)
        @component('layouts.order.request', ['request' => $request])
            @slot('final')
            @endslot
            @slot('buttons')
                <a href="{{ route('admin.request.edit', ['id' => $request->id]) }}" class="site_button-orange button-small leaveFeedback">Редагувати заявку</a>
            @endslot
        @endcomponent
    @endforeach
</div>

@push('footer_scripts')
<script src="{{ asset('js/client/order/show.js') }}"></script>
@endpush
@endif