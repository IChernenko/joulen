<!DOCTYPE HTML>
<html>
<head>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2461444-16"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-2461444-16');
    </script>
    <title>@yield('site-title', 'Сервіс пошуку спеціалістів. Монтаж сонячних електростанцій')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8;" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('site-meta-description', 'Каталог компаній-інсталяторів сонячних електростанцій.
    Залиште одну заявку, і отримайте пропозиції від кращих монтажників України')">
    @stack('styles')
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/site.css') }}" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/preloader.css') }}"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="format-detection" content="telephone=no">
</head>
<body>

@include('layouts.header')
{{--@include('layouts.new.home-page.menu')--}}
@yield('content')

@include('layouts.footer')



</body>
</html>