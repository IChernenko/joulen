<div class="form-container-group border-gray">
    <p class="form-title">Інформація для вашої публічної сторінки</p>
    <div class="form-item">
        <label for="name_company">Назва компанії:</label>
        <input id="name_company" class="border-gray" type="text" name="company_name" value="{{ $distributor->company_name }}">
    </div>
    <div class="form-block">
        <label for="master_avatar">Ваш логотип файли jpg та png розміром не менше
            100х100px</label>
        <div class="logoImgBlock">
            <div class="logoFile">
                <input type="file" class="file-upload hidden_validate_file" id="master_avatar"
                       name="avatar" style="display:none;"><br>
                <img src="{{ $distributor->getAvatarSrc() }}" alt="avatar"
                     id="master_avatar_img">
            </div>
            <div>
                <div id="master_avatar_download" class="site_icon-download">Завантажити</div>
            </div>
            <div>
                <a href="" id="master-avatar-delete" class="site_icon-delete remove-avatar">Видалити</a>
                <input type="checkbox" checked id="is_deleted_master_avatar" value=""
                       name="is_deleted_master_avatar"
                       style="display:none;">
            </div>
        </div>
    </div>
    @if (Auth::user()->isAdmin())
        <div class="form-item">
            <label for="url">URL:</label>
            <input id="url" class="border-gray" type="text" name="slug" value="{{ $distributor->slug }}">
        </div>
    @endif
    <div class="form-item">
        <label for="description-company">Опис компанії:</label>
        <textarea id="description-company" class="border-gray" rows="6" name="description">{{ $distributor->description }}</textarea>
    </div>
    <div class="form-item">
        <label for="site">Сайт:</label>
        <input id="site" class="border-gray" type="text" name="website" value="{{ $distributor->website }}">
    </div>
    <div class="form-item">
        <label for="corp-email">Корпоративний е-мейл:</label>
        <input id="corp-email" class="border-gray" type="text"
               name="corporate_email" value="{{ $distributor->corporate_email }}">
    </div>
    <div class="form-item">
        <label for="tel">Телефон:</label>
        <input id="tel" class="border-gray mask-phone" type="text" name="phone" value="{{ $distributor->phone }}">
    </div>
    <div class="form-item">
        <input id="tel_2" class="secondary_tel" type="checkbox" {{ is_null($distributor->second_phone) ? '' : 'checked' }}>
        <label class="custom-checkbox border-gray" for="tel_2"></label>
        <label for="tel_2" class="with-checkbox">Телефон 2:</label>
        <input id="tel_2" class="border-gray mask-phone" type="text"
               {{ is_null($distributor->second_phone) ? 'disabled' : '' }}
               name="second_phone" value="{{ $distributor->second_phone }}">
    </div>
</div>

<div class="form-container-group border-gray">
    <p class="form-title">Контакти менеджера, по роботі з дилерами</p>
    <div class="form-item">
        <label for="full_name">ПІБ:</label>
        <input id="full_name" class="border-gray" type="text" name="manager_name" value="{{ $distributor->user->manager->name }}">
    </div>
    <div class="form-item">
        <label for="email">Е-мейл:</label>
        <input id="email" class="border-gray" type="text" name="manager_email" value="{{ $distributor->user->manager->email }}">
    </div>
    <div class="form-item">
        <label for="number">Телефон:</label>
        <input id="number" class="border-gray mask-phone" type="text"
               name="manager_phone" value="{{ $distributor->user->manager->phone }}">
    </div>
    <div class="form-item">
        <input id="tel_3" class="secondary_tel" type="checkbox"
                {{ is_null($distributor->user->manager->second_phone) ? '' : 'checked' }}>
        <label class="custom-checkbox border-gray" for="tel_3"></label>
        <label for="tel_3" class="with-checkbox">Телефон 2:</label>
        <input id="tel_3" class="border-gray mask-phone" type="text"
               {{ is_null($distributor->user->manager->second_phone) ? 'disabled' : '' }}
               name="manager_second_phone" value="{{ $distributor->user->manager->second_phone }}">
    </div>
</div>