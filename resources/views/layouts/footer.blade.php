@include('layouts._footer')

<!--# end footer #-->
<script src="{{ asset('js/cookie.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>
{{--<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>--}}
{{--<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript">
    var market = {!! \App\Models\MarketData::find(1) !!};
</script>
<script src="{{ asset('js/new-style/script.min.js') }}"></script>
@stack('footer_scripts')

@stack('modals')

