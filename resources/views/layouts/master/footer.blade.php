<!--# footer #-->
<footer id="footer" class="main-footer">

    <div class="copyright">
        &copy; {{ env('DOMAIN') }}
    </div>
    <div class="footer-contacts">
        <div class="site_icon-phone-white">
            <a href="tel:+38(050)0745465">+38 (050) 074-54-65</a>
        </div>
        <div class="site_icon-mail-white">
            <a href="mailto:{{ env('SUPPORT_MAIL') }}">{{ env('SUPPORT_MAIL') }}</a>
        </div>
    </div>
    <div class="copyright">
        <a href="{{ route('privacy') }}">Політика конфіденційності</a>
    </div>

</footer>
</div>
</div>


<div class="scroll-top-btn">
    <img src="{{ asset('images/up-arrow.png') }}" alt="image">
</div>
<!--# end footer #-->
<script src="{{ asset('js/cookie.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>

<script src="{{ asset('js/site.js') }}"></script>
@stack('footer_scripts')

@stack('modals')

