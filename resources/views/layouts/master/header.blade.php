<!--# header #-->
<div class="container-fluid">
<!-- /.container -->
    <header id="header" class="main-header">
        {{--<div class="container">--}}
        <div class="logo-wrap">
            <a href="{{ route('home') }}" id="logo" class="main-logo" title="Джоуль - Головна сторінка">
                <img src="{{ asset('images/logo_joulen.png') }}" alt="Джоуль - Знайдіть профессіоналів для встановлення сонячної електростанції без посередників" />
            </a>
            <a class="toggle-sidebar"><img src="{{ asset('images/burger.png') }}" alt=""></a>
        </div>
        <div class="header-wrap">
            @if (Auth::user()->isDistributor())
                <p class="balance">
                    <img src="{{asset('images/money.png')}}" alt="image">
                    Баланс: <span>${{ Auth::user()->distributor->balance }}</span>
                </p>
            @endif
            <a class="logout-link hidden-sm icon-logout" href="{{ route('logout') }}">Вийти</a>
        </div>
        <!-- /.header-wrap -->

            {{--@include('layouts.menu')--}}
        {{--</div>--}}

    </header>
</div>
<div class="container-fluid">
<div class="cabinet-page">





<!--# end header #-->