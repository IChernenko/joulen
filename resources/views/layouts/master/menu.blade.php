<div class="sidebar" id="mainMenu">
    <ul>
        @if(is_null(Auth::user()))
            <li class="menu-item"><a href="{{ route('master.index') }}">Каталог спеціалстів</a></li>
            <li class="menu-item"><a href="{{ route('login') }}">Вхід</a></li>
        @else
            @include('layouts.master.menu.' . Auth::user()->role->name)
        @endif
        <li class="logout-item hidden-md"><a class="logout-link" href="{{ route('logout') }}">Вийти</a></li>
    </ul>
</div>