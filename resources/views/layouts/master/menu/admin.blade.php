<li class="menu-item"><a href="{{ route('master.index') }}">Каталог спеціалістів</a></li>
<li class="menu-item">
    <a href="#">Адміністрування</a>
    <span class="showSubMenu"></span>
    <ul class="sub-menu">
        @include('layouts.admin_menu_list')
    </ul>
</li>
<li class="menu-item">
    <a href="#">Мій профіль</a>
    <span class="showSubMenu"></span>
    <ul class="sub-menu">
        <li>
            <a href="{{ route('admin.settings') }}">Параметри облікового запису</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>