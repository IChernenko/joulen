<li class="menu-item">
    <a href="{{ route('client.order.index') }}">Мої проекти</a>
</li>
<li class="menu-item"><a href="{{ route('master.index') }}">Каталог спеціалістів</a></li>
<li class="menu-item">
    <a href="#">Мій профіль</a>
    <span class="showSubMenu"></span>
    <ul class="sub-menu">
        <li>
            <a href="{{ route('client.settings') }}">Параметри облікового запису</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>