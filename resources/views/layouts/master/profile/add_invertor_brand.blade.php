<div class="remodal" data-remodal-id="add-brand-invertor" id="invertors">
    <div class="modal-header">
        <h4>Додати бренд (Інвертори)</h4>
    </div>

    <div class="validation_errors_modal backend_validation_errors">
        @component('layouts.validation_errors')
        @endcomponent
    </div>

    <div class="modal-body">
        <form class="validate_form" data-step='brand' action="{{ Auth::user()->isAdmin() ? route('admin.master.store.equipment') : route('master.store.equipment') }}" method="POST"
              id="add-invertor-form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $master->id }}" name="master_id">
            <input type="hidden" name="equipment_type">
            <div class="brand-invertor">
                <div class="modal-form-block">
                    <label for="add_invertor_brand_invertor">Назва бренду:</label>

                    <select name="equipment_id" class="select-modal brand_invertor brands-list" id="add_invertor_brand_invertor">
                        @foreach($invertor_type_equipment as $invertor)
                            <option value="{{ $invertor->id }}">{{ $invertor->title }}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-form-block">
                    <label for="add_invertor_warranty_duration">Базова гарантія на інвертор: </label>
                    <input type="text" class="warranty-input" name="warranty_duration" id="add_invertor_warranty_duration"> років.
                </div>
                <div class="modal-form-block">
                    <p>Чи є можливість збільшити/продовжити гарантійний строк?</p>
                    <div class="styled-radio">
                        <input type="radio" name="warranty_increase" id="add_invertor_warranty_increase" value="1">
                        <label for="add_invertor_warranty_increase">Так</label>
                    </div>
                    <!-- /.styled-radio -->
                    <div class="styled-radio">
                        <input type="radio" name="warranty_increase" id="add_invertor_no_warranty_increase" value="0">
                        <label for="add_invertor_no_warranty_increase">Ні</label>
                    </div>
                    <!-- /.styled-radio -->
                </div>
                <!-- /.modal-form-block -->
                <div class="modal-form-block">
                    <p>Хто буде займатися ремонтом/заміною обладнання при настанні гарантійного випадку?</p>
                    @foreach($guarantee_repairs as $id=>$repair)
                        <div class="styled-radio">
                            <input type="radio" name="warranty_case" id="add_invertor_warranty_case{{ $id }}" value="{{ $id }}">
                            <label for="add_invertor_warranty_case{{ $id }}">{{ $repair }}</label>
                        </div>
                @endforeach

                <!-- /.styled-radio -->
                </div>
                <!-- /.modal-form-block -->
                <div class="form-help-wrap">
                    <div class="modal-form-block">
                        <p>Де знаходиться <span class="tooltip">обмінний фонд
                                @component('layouts.profile_help')
                                @endcomponent</span> обладнання/комплектуючих?</p>
                        @foreach($exchange_funds as $id => $fund)
                            <div class="styled-radio">
                                <input type="radio" name="exchange_fund_loc" id="add_invertor_exchange_fund_loc{{ $id }}" value="{{ $id }}">
                                <label for="add_invertor_exchange_fund_loc{{ $id }}">{{ $fund }}</label>
                            </div>
                        @endforeach
                    </div>

                </div>
                <!-- /.form-help-wrap -->

                <!-- /.modal-form-block -->
                <div class="modal-form-block">
                    <label for="add_invertor_built">Скільки сонячних станцій із використанням інверторів цього бренду ви збудували?</label>
                    <input type="text" name="built" id="add_invertor_built">
                    <span class="form-info">Вказане число станцій повинно збігатися із кількістю об'єктів, доданих у вашому портфоліо</span>
                </div>
                <!-- /.modal-form-block -->
            </div>
            <div class="btn-modal">
                <button class="btn btn-confirm" id="add-invertor-confirm">Зберегти</button>
                <button class="btn btn-cancel" data-remodal-action="cancel">Скасувати</button>
            </div>
        </form>
    </div>
</div>