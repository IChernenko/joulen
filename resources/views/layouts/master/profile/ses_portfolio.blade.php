<div class="remodal" data-remodal-id="add-ses" id="add-ses" data-remodal-options="hashTracking: false">
    <div class="modal-header">
        <h4>Додати об'єкт у портфоліо (Домашні СЕС)</h4>
    </div>
    <ul class="validation_errors_modal backend_validation_errors">
        @component('layouts.validation_errors')
        @endcomponent
    </ul>
    <div class="modal-body">
        <div class="alert alert-warning">
            <p>Увага!</p>
            <p> Додавайте станції лише тих клієнтів, які зможуть підтвердити, що це ваша компанія
                займалася будівництвом.</p>
        </div>
        <form class="validate_form" id="ses-form" action="" method="POST">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $master->id }}" name="master_id">
            <input type="hidden" class="portfolio_id" name="portfolio_id"/>
            <div class="add-ses">
                <div class="modal-form-block">
                    <label for="power-commercial-add">Потужність станції, кВт:</label>
                    <input id="power-commercial-add" type="text" name="portfolio_power" class="portfolio_power">
                </div>
                <!-- /.modal-form-block -->

                <div class="modal-form-block type-ses-block" style="display: none;">
                    <label for="subcategory-home-add">Тип станції:</label>
                    <select id='subcategory-home-add'
                            class="select-modal portfolio_subcategory_id">
                        <option selected value="4">Виберіть тип</option>
                        @foreach($subcategories as $key => $subcategory)
                            @if ($key < 4)
                                <option value="{{ $key }}">{{ $subcategory }}</option>
                            @endif
                        @endforeach
                    </select>
                    <input name="portfolio_subcategory_id" type="hidden" value="">
                </div>

                <div class="modal-form-block commercial-type-ses-block" style="display: none;">
                    <label for="subcategory-commercial-add">Тип станції:</label>
                    <select name="portfolio_subcategory_type_id" id='subcategory-commercial-add'
                            class="nice-select portfolio_subcategory_type_id">
                        <option selected disabled>Виберіть тип</option>
                        <option value="0">Дахова</option>
                        <option value="1">Наземна</option>
                    </select>

                </div>

                <div class="modal-form-block">
                    <label>Дата введення в експлуатацію:</label>
                    <div class="select-modal-wrap">
                        @php
                        $monthArray = array(
                            "0" => "Січень", "1" => "Лютий", "2" => "Березень",
                            "3" => "Квітень", "4" => "Травень", "5" => "Червень",
                            "6" => "Липень", "7" => "Серпень",
                            "8" => "Вересень", "9" => "Жовтень",
                            "10" => "Листопад", "11" => "Грудень"
                        );
                        @endphp
                        <select name="portfolio_month" id='month-commercial-add' class="select-modal portfolio_month">
                            <option value="">Місяць</option>
                            @php
                            foreach ($monthArray as $key => $month) {
                                echo '<option id="month-' . $key . '" data-id="' . $key . '" value="' . $month . '">' . $month . '</option>';
                            }
                            @endphp
                        </select>


                        @php
                        $currentYear = date("Y");
                        $yearArray = range($currentYear, 1991);
                        @endphp
                        <select name="portfolio_year" id='year-commercial-add' class="select-modal portfolio_year">
                            <option value="">Рік</option>
                            @php
                            foreach ($yearArray as $year) {
                                echo '<option id="year-' . $year . '" value="' . $year . '">' . $year . '</option>';
                            }
                            @endphp
                        </select>

                    </div>
                    <!-- /.select-modal-wrap -->

                </div>
                <div class="form-block station-location-wrap">
                    <h5>Розміщення станції:</h5>
                    <div class="station-location">
                        <p>Вкажіть точне розміщення станції на карті:</p>
                        <span class="error-block-modal" style="display:none;color:red; text-align: left;">
                            Будь-ласка, вкажіть точне розміщення станції, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                        </span>
                        <div class="show-map-block" style="margin-top: 15px;">
                            <div class="gmap-block">
                                <!-- /.marker-info -->
                                <input name="portfolio_address" autocomplete="on" id="pac-input" value=""
                                       class="controls pac-input-modal search-map-input" type="text" placeholder="Введіть адресу тут ...">
                                <input name="portfolio_region" class="portfolio-region" value="" type="hidden">
                                <div id="gmap" class="map portfolio-map"></div>
                            </div>

                            <input type="hidden" class="geo_coords_lat-modal" name="portfolio_lat" value="">
                            <input type="hidden" class="geo_coords_lng-modal" name="portfolio_lng" value="">
                        </div>

                    </div>
                </div>
                <!-- /.form-block -->
                <div class="form-block">
                    <h5>Фото:</h5>
                    <div class="photo-ses">
                        <div>
                            <label class="photo-invertor">Фото інвертора</label>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="invertor_photo_commersial"
                                           name="invertor_photo" style="display:none;"><br>
                                    <img src="" alt="" class="foto_img invertor_photo_img">
                                </div>
                                <div>
                                    <div id="invertor_photo_download" class="site_icon-download">Завантажити</div>
                                </div>
                                <div>
                                    <a href="" id="invertor_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_invertor_photo" value="0"
                                           name="is_deleted_invertor_photo"
                                           style="display:none;">
                                </div>
                            </div>
                            <div class="styled-checkbox">
                                <input type="checkbox" value="1" checked name="public_invertor_photo"
                                       id="public_invertor_photo">
                                <label for="public_invertor_photo" class="under-photo">Додати це фото у ваше
                                    портфоліо</label>
                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                        <div>
                            <label class="photo-sun-panel">Фото сонячної станції</label>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="panel_photo" name="panel_photo"
                                           style="display:none;"><br>
                                    <img src="" alt="" class="panel_photo_img">
                                </div>
                                <div>
                                    <div id="panel_photo_download" class="site_icon-download">Завантажити</div>
                                </div>
                                <div>
                                    <a href="" id="panel_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_panel_photo" value="0"
                                           name="is_deleted_photo"
                                           style="display:none;">
                                </div>
                            </div>
                            <div class="styled-checkbox">
                                <input type="checkbox" checked value="1" id="public_panel_photo"
                                       name="public_panel_photo">
                                <label for="public_panel_photo" class="under-photo">Додати
                                    це фото у ваше портфоліо</label>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.form-block -->
                <div class="modal-form-block">
                    <h5>Обладнання</h5>

                    <label>Який бренд інвертора використаний для даної станції?</label>

                    <select name="portfolio_invertor_id" class="select-modal portfolio_invertor_id">
                        <option selected value="">Не активно</option>
                        @foreach($invertor_type_equipment as $invertor)
                            <option value="{{ $invertor->id }}">{{ $invertor->title }}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-form-block">
                    <label>Який бренд панелей використаний для даної станції?</label>

                    <select name="portfolio_panel_id" class="select-modal portfolio_panel_id">
                        <option selected value="">Не активно</option>
                        @foreach($panel_type_equipment as $panel)
                            <option value="{{ $panel->id }}">{{ $panel->title }}</option>
                        @endforeach
                    </select>
                </div>
                <!-- /.modal-form-block -->
                <div class="form-block">
                    <h5>Сервіс та гарантія</h5>
                    <div class="service-modal">
                        <div class="contractor-block" style="display: none;">
                            <p>Ви були генеральним підрядником при будівництві цієї станції?</p>
                            <div class="styled-radio">
                                <input type="radio" class="contractor-true" id="contractor-true"
                                       name="portfolio_contractor"
                                       value="1">
                                <label for="contractor-true">Так</label>
                            </div>
                            <!-- /.styled-radio -->
                            <div class="styled-radio">
                                <input type="radio" class="contractor-false" id="contractor-false"
                                       name="portfolio_contractor"
                                       value="0">
                                <label for="contractor-false">Ні</label>
                            </div>
                            <!-- /.styled-radio -->
                        </div>
                    </div>
                    <div class="designing-block" style="display: none;">
                        <p>Ви займалися проектуванням та отриманням дозвільної документації для цієї
                            СЕС?
                        </p>
                        <div class="styled-radio">
                            <input type="radio" class="designing-true" id="designing-true" name="portfolio_designing"
                                   value="1">
                            <label for="designing-true">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" class="designing-false" id="designing-false" name="portfolio_designing"
                                   value="0">
                            <label for="designing-false">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                    </div>
                    <div class="delivery-block">

                        <p>Ви займалися поставкою обладнання для даної станції?</p>
                        <div class="styled-radio">
                            <input type="radio" class="delivery-true" id="delivery-true" name="portfolio_delivery"
                                   value="1">
                            <label for="delivery-true">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" class="delivery-false" id="delivery-false" name="portfolio_delivery"
                                   value="0">
                            <label for="delivery-false">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                    </div>
                    <div class="install-block">
                        <p>Ви займалися монтажем даної станції?</p>
                        <div class="styled-radio">
                            <input type="radio" class="installation-true" id="installation-true"
                                   name="portfolio_installation" value="1">
                            <label for="installation-true">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" class="installation-false" id="installation-false"
                                   name="portfolio_installation" value="0">
                            <label for="installation-false">Ні</label>
                        </div>
                        <!-- /.styled-radio -->
                    </div>
                    <div class="documentation-block" style="display: none;">
                        <p>Ваша компанія займалася документальним супроводом для даної станції?</p>
                        <div class="styled-radio">
                            <input type="radio" class="documentation-true" id="documentation-true"
                                   name="portfolio_documentation" value="1">
                            <label for="documentation-true">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" class="documentation-false" id="documentation-false"
                                   name="portfolio_documentation" value="0">
                            <label for="documentation-false">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                    </div>
                </div>
            </div>
            <!-- /.form-block -->
            <div class="space-top">
                <button id="ses-confirm" class="btn btn-confirm">
                    {{ Auth::user()->isAdmin() ? 'Зберегти' : 'Відправити на перевірку адміністратору' }}</button>
                <button class='btn btn-cancel' data-remodal-action="cancel">Скасувати</button>
            </div>
        </form>
    </div>
</div>