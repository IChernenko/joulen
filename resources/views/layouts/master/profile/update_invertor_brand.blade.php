<div  class="remodal" data-remodal-id="update-brand-invertor" id="update-brand-invertor">
    <div class="modal-header">
        <h4>Редагувати бренд (Інвертори)</h4>
    </div>

    <div class="validation_errors_modal backend_validation_errors">
        @component('layouts.validation_errors')
        @endcomponent
    </div>

    <div class="modal-body">
        <form class="validate_form" data-step='brand-update' action="{{ Auth::user()->isAdmin() ? route('admin.master.update.equipment') : route('master.update.equipment') }}" method="POST" id="update-invertor-form" enctype="multipart/form-data" >
            {{ method_field('PUT') }}
            <input type="hidden" value="{{ $master->id }}" name="master_id">
            <div class="brand-invertor publicInfo">
                <div class="modal-form-block">
                    <label>Назва бренду:</label>
                    <span class="equipment_title_text"></span>
                    <input type="hidden" name='equipment_id' class="equipment_title">
                </div>
                <div class="modal-form-block">
                    <label for="update_invertor_warranty_duration">Базова гарантія на інвертор:</label>
                    <input type="text" name="warranty_duration" id="update_invertor_warranty_duration" class='guarantee' value="">років.
                </div>
                <!-- /.modal-form-block -->
                <div class="modal-form-block">
                    <p>Чи є можливість збільшити/продовжити гарантійний строк?</p>
                    <div class="styled-radio">
                        <input type="radio" name="warranty_increase" id="update_invertor_warranty_increase"
                               class="warranty_increase" value="1">
                        <label for="update_invertor_warranty_increase">Так</label>
                    </div>
                    <!-- /.styled-radio -->
                    <div class="styled-radio">
                        <input type="radio" name="warranty_increase" id="update_invertor_no_warranty_increase"
                               class="no_warranty_increase" value="0">
                        <label for="update_invertor_no_warranty_increase">Ні</label>
                    </div>
                    <!-- /.styled-radio -->
                </div>
                <div class="modal-form-block">
                    <p>Хто буде займатися ремонтом/заміною обладнання при настанні гарантійного випадку?</p>
                    @foreach($guarantee_repairs as $id=>$repair)
                        <div class="styled-radio">
                            <input type="radio" name="warranty_case" id="update_invertor_warranty_case{{ $id }}"
                                   class="warranty_case{{ $id }}" value="{{ $id }}">
                            <label for="update_invertor_warranty_case{{ $id }}">{{ $repair }}</label>
                        </div>
                @endforeach

                <!-- /.styled-radio -->
                </div>
                <div class="form-help-wrap">
                    <div class="modal-form-block">
                        <p>Де знаходиться <span class="tooltip">обмінний фонд
                                @component('layouts.profile_help')
                                @endcomponent</span> обладнання/комплектуючих?</p>
                        @foreach($exchange_funds as $id => $fund)
                            <div class="styled-radio">
                                <input type="radio" name="exchange_fund_loc" id="update_invertor_exchange_fund_loc{{ $id }}"
                                       class="exchange_fund_loc{{ $id }}" value="{{ $id }}">
                                <label for="update_invertor_exchange_fund_loc{{ $id }}">{{ $fund }}</label>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="modal-form-block">
                    <label for="update_invertor_built">Скільки сонячних станцій із використанням інверторів цього бренду ви збудували?</label>
                    <input type="text" name="built" id="update_invertor_built" class="built">
                    <span class="form-info">Вказане число станцій повинно збігатися із кількістю об'єктів, доданих у вашому портфоліо</span>
                </div>
                <!-- /.modal-form-block -->
            </div>
            <div class="btn-modal">
                <button class="btn btn-confirm" id="update-invertor-confirm">Зберегти</button>
                <button class="btn btn-cancel" data-remodal-action="cancel">Скасувати</button>
            </div>
        </form>
    </div>
</div>