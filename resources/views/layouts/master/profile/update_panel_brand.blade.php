<div class="remodal" data-remodal-id="update-brand-panel" id="update-brand-panel">
    <div class="modal-header">
        <h4>Редагувати бренд (Сонячні панелі) - </h4>
    </div>

    <div class="validation_errors_modal backend_validation_errors">
        @component('layouts.validation_errors')
        @endcomponent
    </div>

    <div class="modal-body">
        <form class="validate_form" data-step='brand-update' action="{{ Auth::user()->isAdmin() ? route('admin.master.update.equipment') : route('master.update.equipment') }}" method="POST" id="update-panel-form" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            <input type="hidden" value="{{ $master->id }}" name="master_id">
            <div class="brand-invertor publicInfo">
                <div class="modal-form-block">
                    <label for="brand_panel">Назва бренду:</label>
                    <span class="equipment_title_text"></span>
                    <input type="hidden" name='equipment_id' class="equipment_title">
                </div>
                <div class="modal-form-block">
                    <label for="update_panel_warranty_duration">Гарантія на панелі: </label>
                    <input type="text" class="guarantee" name="warranty_duration" id="update_panel_warranty_duration"> років.

                </div>
                <!-- /.modal-form-block -->
                <div class="modal-form-block">
                    <p>Хто буде займатися ремонтом/заміною обладнання при настанні гарантійного випадку?</p>

                    @foreach($guarantee_repairs as $id=>$repair)
                        <div class="styled-radio">
                            <input type="radio" name="warranty_case" id="update_panel_warranty_case{{ $id }}"
                                   class="warranty_case{{ $id }}" value="{{ $id }}">
                            <label for="update_panel_warranty_case{{ $id }}">{{ $repair }}</label>
                        </div>
                @endforeach

                <!-- /.styled-radio -->
                </div>
                <!-- /.modal-form-block -->
                <div class="form-help-wrap">
                    <div class="modal-form-block">
                        <p>Де знаходиться <a class="help">обмінний фонд</a> обладнання/комплектуючих?</p>
                        @foreach($exchange_funds as $id => $fund)
                            <div class="styled-radio">
                                <input type="radio" name="exchange_fund_loc" id="update_panel_exchange_fund_loc{{ $id }}"
                                       class="exchange_fund_loc{{ $id }}" value="{{ $id }}">
                                <label for="update_panel_exchange_fund_loc{{ $id }}">{{ $fund }}</label>
                            </div>
                            <!-- /.styled-radio -->
                        @endforeach
                    </div>
                    @component('layouts.profile_help')
                    @endcomponent
                </div>
                <div class="modal-form-block">
                    <label for="built">Скільки сонячних станцій із використанням панелей цього бренду ви збудували?</label>
                    <input type="text" name="built" id="built" class="built">
                    <span class="form-info">Вказане число станцій повинно збігатися із кількістю об'єктів, доданих у вашому портфоліо</span>
                </div>
                <!-- /.modal-form-block -->

                <div class="btn-modal">
                    <button class="btn btn-confirm" id="update-panel-confirm">Зберегти</button>
                    <button class="btn btn-cancel" data-remodal-action="cancel">Скасувати</button>

                </div>
            </div>

        </form>
    </div>
</div>