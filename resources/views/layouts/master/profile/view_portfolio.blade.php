<div class="remodal set-portfolio" id="show-portfolio" data-remodal-id="show-portfolio">
    <div class="modal-header">
        <h4></h4>
    </div>

    <!--# contentBlock #-->
    <div class="cabinet-portfolio">
        <div id="map-1" class="map"></div>
        <div class="form-block form-portfolio">
            <div class="set-panel-photo">
                <img src="" alt="image">
            </div>
            <div class="set-invertor-photo">
                <img src="" alt="image">
            </div>
        </div>
        <div class="set-date"></div>
        <div class="set-invertor"></div>
        <div class="set-panel"></div>
        <div class="set-executor"></div>
        <ul class="set-settings"></ul>

        <div class='space-top'>
            <button class='btn btn-secondary' data-remodal-action="cancel">Скасувати</button>
        </div>
    </div>
</div>