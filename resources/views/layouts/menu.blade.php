<div class="hamburger">
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" ratio="1">
        <rect y="9" width="20" height="2"></rect>
        <rect y="3" width="20" height="2"></rect>
        <rect y="15" width="20" height="2"></rect>
    </svg>
    <span></span>
</div>

<ul class="header-button">
    <li class="equipment-dropdown">
        <a href="#" class="equipment-btn">
            <img src="{{ asset('images/equipmentIcon.png') }}" alt="image">
            <span>Обладнання</span>
        </a>
        <ul class="equipment-menu">
            <li>
                <a href="{{ route('brand.index', ['equipment_type' => 'solar-panel']) }}">Сонячні панелі</a>
            </li>
            <li>
                <a href="{{ route('brand.index', ['equipment_type' => 'inverter']) }}">Інвертори</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('master.index') }}" class="installer-companies">
            <img src="{{ asset('images/installersIcon.png') }}" alt="image">
            <span>Компанії-інсталятори</span></a>
    </li>
    <li>
        <a href="{{ route('blog') }}" class="installer-companies">
            <img src="{{ asset('images/info_ico.png') }}" alt="image">
            <span>База знань</span></a>
    </li>
    @if(is_null(Auth::user()))
        <li>
            <a href="{{ route('login') }}" class="entrance">
                <img class="icon-desktop" src="{{ asset('images/log-in.svg') }}" alt="image">
                <!--<img class="icon-mobile" src="img/login-Mobile.png" alt="image">-->
                <span class="login">Вхід</span>
                <span class="login-mobile">Увійти</span></a>

        </li>
    @else
        <li>
            @php
                $route = route(Auth::user()->role->getDefaultPageRoute());
            @endphp
            <a href="{{ $route }}" class="entrance">
                <img class="icon-desktop" src="{{ asset('images/log-in.svg') }}" alt="image">
                <!--<img class="icon-mobile" src="img/login-Mobile.png" alt="image">-->
                <span class="login">Мій кабінет</span>
                <span class="login-mobile">Мій кабінет</span></a>

        </li>
    @endif
</ul>
{{--</div>--}}