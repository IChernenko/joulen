<!--# header #-->
<div id="header">
    <div class="container">
        <a href="{{ route('home') }}" id="logo" title="Джоуль - Головна сторінка">
            <img src="{{ asset('images/logo.png') }}" alt="Джоуль - Знайдіть профессіоналів для встановлення сонячної електростанції без посередників" />
        </a>

        @include('layouts.old.menu')
    </div>
</div>
<!--# end header #-->