<div id="mainMenu">
    <ul>
        @if(is_null(Auth::user()))
            <li><a href="{{ route('master.index') }}">Каталог спеціалстів</a></li>
            <li><a href="{{ route('login') }}">Вхід</a></li>
        @else
            @include('layouts.old.menu.' . Auth::user()->role->name)
        @endif
    </ul>
</div>