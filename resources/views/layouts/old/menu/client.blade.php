<li>
    <a href="{{ route('client.order.index') }}">Мої проекти</a>
</li>
<li><a href="{{ route('master.index') }}">Каталог спеціалістів</a></li>
<li>
    <a href="#">Мій профіль</a>
    <span class="showSubMenu"></span>
    <ul>
        <li>
            <a href="{{ route('client.settings') }}">Параметри облікового запису</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>