<li>
    <a class="main_menu_item settings_menu" href="#">Налаштування</a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="secondary_menu">
        <li>
            <a href="{{route('master.profile_pages.subscribe')}}">Мої дилери</a>
        </li>
        <li>
            <a href="{{ route('master.commission')}}">Про компанію</a>
        </li>
        <li>
            <a href="{{route('master.settings.private')}}">Обліковий запис</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>