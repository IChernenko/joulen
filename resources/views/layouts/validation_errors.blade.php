<ul class="contentBlock backend_validation_errors" id="backend_validation_errors" style="color:red; text-align: left;">

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        @endif

</ul>