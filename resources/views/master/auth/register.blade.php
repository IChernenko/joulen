@extends('layouts.app')

@section('content')
    <!--# contentWrapper #-->
    <div class="register-client-wrap select-category">
        <div class="container">
            <div class="service-works">

                <div style="color: red; list-style: none; margin-top: 10px;">
                    {{ session('email') }}
                </div>

                <h2>Джоуль - ваш надійний партнер по залученню нових клієнтів</h2>
                <h3>Як працює сервіс Джоуль?</h3>
                <div class="step-work">
                    <div class="step-1 info-step">
                        <div class="step-work-image">
                            <img src="{{ asset('images/step_1.svg') }}" alt="image">
                            <span>1</span>
                        </div>
                        <h4>Крок 1</h4>
                        <div class="step-work-info">
                            <p>Сервіс Джоуль залучає нових клієнтів. Менеджер сервісу перевіряє кожне замовлення, особисто
                                спілкуючись із кожним клієнтом.</p>
                        </div>
                        <!-- /.step-work-info -->

                        <!-- /.step-work-image -->
                    </div>
                    <div class="step-2 info-step">
                        <div class="step-work-image">
                            <img src="{{ asset('images/step_2.svg') }}" alt="image">
                            <span>2</span>
                        </div>
                        <h4>Крок 2</h4>
                        <div class="step-work-info">
                            <p>Ви отримуєте повідомлення про нове замовлення та даєте клієнту попередню цінову
                                пропозицію.</p>
                        </div><!-- /.step-work-image -->
                    </div>
                    <div class="step-3 info-step">
                        <div class="step-work-image">
                            <img src="{{ asset('images/step_3.svg') }}" alt="image">
                            <span>3</span>
                        </div>
                        <h4>Крок 3</h4>
                        <div class="step-work-info">
                            <p>Ви зустрічаєтеся з клієнтом, узгоджуєте остаточну вартість і починаєте співпрацю.</p>
                        </div><!-- /.step-work-image -->
                    </div>
                    <!-- /.step-1 -->
                </div>
                <div class="service-partners">
                    <div class="conditions-partner">
                        <h3>Хто може стати партнером сервісу?</h3>
                        <p>Щоб стати звичайним партнером сервісу Джоуль (з мінімальним функціоналом):</p>
                        <ul>
                            <li>Ваша компанія повинна мати мінімум три збудовані домашні станції "під ключ" (або 2
                                комерційні)
                            </li>
                            <li>У вашої компанії повинен бути web-сайт або web-сторінка</li>
                            <li>Ваша компанія повинна надавати клієнтам гарантію на обладнання</li>
                        </ul>
                    </div>
                    <!-- /.conditions -->

                    <div class="conditions-status">
                        <p>Щоб отримати статус "Перевіреної" компанії:</p>
                        <ul>
                            <li>Ваша компанія повинна бути офіційно зареєстрованою (як ФОП або юр.особа)</li>
                            <li>У вашої компанії повинен бути постійно-діючий офіс</li>
                            <li>Ваша компанія повинна пройти перевірку та підписати оф.договір про співпрацю</li>
                        </ul>
                    </div>
                </div>

                <div class="auth-items">
                    <div class="register-items">
                        <!-- /.register-item -->
                        <h2>Зареєструватися</h2>
                        <div class="authorized-items">
                            <!-- /.authorized-item -->
                            <div class="auth-facebook">
                                <a id="facebook_master_register_modal"
                                    class="login-facebook" href="{{ route('facebook.master') }}">
                                    <img src="{{ asset('images/facebook.svg') }}" alt="image">
                                    <span>Зареєструватися через Facebook</span>
                                </a>
                            </div>
                            <div class="login-email">
                                <form method="post" class="register-validate"
                                      action="{{ route('master.register') }}">
                                    {{ csrf_field() }}
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li style="color: red; list-style: none; margin-top: 10px;">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                <h3>Зареєструватися через e-mail:</h3>
                                <label for="email">E-mail:</label>
                                <input type="text" id="email" name="email" class="email_required"
                                       value="{{ old('email') }}"/>
                                <div class="repeat-password">
                                        <label for="password1">Придумайте пароль:</label>
                                        <input type="password" id="password1" class="password_required" name="password"/>
                                    <!-- /.enter-password -->
                                </div>
                                <!-- /.repeat-password -->

                                    <div class="g-recaptcha" style="margin-top: 30px" data-sitekey="{{ env('NOCAPTCHA_SITEKEY') }}"></div>
                                <input type="submit" class="btn btn-default" value="Зареєструватися" />
                                </form>
                                <!-- /.btn-default -->

                                <div class="info-register">
                                    <div class="come-in">
                                        <span>Я вже зареєстрований. <a href="{{ route('login') }}">Ввійти</a></span>
                                    </div>
                                </div>

                                <!-- /.come-in -->

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- /.service-partners -->
        </div>
        <!-- /.container -->
    </div>
@endsection

@section('site-title')
    Зареєструватися як спеціаліст
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
@endpush

@push('footer_scripts')
<script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
<script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
<script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
<script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@if ( isset($profile_id) )
    <script>
        $(document).ready(function () {
//            $("#facebook_master_register_modal").trigger('click');
            if($('input[name=profile_id]').val() != '') {
                var modal = $('[data-remodal-id=modal]').remodal();
                modal.open();
            }

        $('#master_facebook_register_form').on('submit', function (e) {
            e.preventDefault(e);
            $('#master_facebook_register_form .error_output').html('');
            $.ajax({
                type: "POST",
                url: '{{ route('master.facebook.register') }}',

                data: $(this).serialize(),
                dataType: 'json',
                success: function (data) {

                    if (data.redirect) {
                        window.location.href = data.redirect;
                    }
                    console.log(data);
                    var emailError = data.errors.email ? '<li>' + data.errors.email + '</li>' : "";
                    $('#master_facebook_register_form .error_output').html(emailError);
                }
            })
        });
        });

    </script>

@endif
@endpush

@push('modals')
<div class="remodal registration-facebook" data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close close-btn"></button>

    <h3>Реєстрація через Facebook</h3>
    <div class="enter-email enter-data">
        <form class="registration-modal" method="POST" id="master_facebook_register_form"
              action="{{ route('master.facebook.register') }}">
            {{ csrf_field() }}
        <label for="name">Ім'я:</label>
        <input type="text" value="{{ $name ?? '' }}" id="name" name="name" style="width: 100%">
        <label for="email">E-mail:</label>
        <input type="text" placeholder="E-mail" id="email"
               name="email" value="{{ $email ?? '' }}"
               class="fb_required" style="width: 100%">


            <input type="hidden" name="profile_id" value="{{ $profile_id ?? '' }}">
            <input type="hidden" name="profile_url" value="{{ $profile_url ?? '' }}">
        <div class="forgot-password-btn">
            <input type="submit" class="accept-btn remodal-confirm" value="Зареєструватися" />
            <a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>
        </div>
        </form>
    </div>
</div>

@endpush