@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <h1>Комісія сервісу</h1>
    </div>

    <!--# contentBlock #-->
    <div class="cabinet-profile">
        <div class="cabinet-item">
            <div class="table-block table-block-filter">
                <div class="table-title">
                    <p>Комісія сервісу для спеціалістів становить:</p>
                    <span>Курс НБУ станом на {{$currency->exchangedate}} <br>1$ = {{$currency->getRate()}} грн.</span>
                </div>
                <table class="table home-ses-table footable footable-1 breakpoint-lg table-top" style="display: table;">
                    <tbody>
                    @foreach ($subcategoriesGroups as $factor => $group)
                        @if ( $group->count() > 0 )
                            <tr class="tr_id">
                                <td data-title="Тип" data-breakpoints="xs sm md" class="footable-first-visible"
                                    style="display: table-cell;">
                                    Для
                                    @for ( $i = 0; $i < $group->count() - 1; $i++ )
                                        «{{$group->get($i)->name}}»,
                                    @endfor

                                    «{{$group->last()->name}}»
                                </td>
                                <td data-title="Відсоток" data-breakpoints="xs sm md" style="display: table-cell;">{{$factor}}% від вартості виконаного замовлення</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <div class="alert alert-primary">
                    <p>Комісія сплачується у гривнях, по курсу НБУ станом на день сплати</p>
                </div>
                <div class="alert alert-orange">
                    <strong>Зверніть увагу!</strong>
                    <p>Чим більше комісії ви сплатили, тим вище ви
                        знаходитеся у <a href="{{ route('master.index') }}">рейтингу спеціалістів</a>.</p>
                </div>
                <div class="table-title">
                    <p>Архів платежів</p>
                </div>
                <table class="table home-ses-table footable footable-1 breakpoint-lg" style="display: table;">
                    <tbody>
                    @foreach($orders as $order)
                        <tr class="tr_id">
                            <td data-title="Тип" data-breakpoints="xs sm md" class="footable-first-visible"
                                style="display: table-cell;">
                                {{$order->title}}
                            </td>
                            <td data-title="Комісія" data-breakpoints="xs sm md" style="display: table-cell;">
                                @if ($order->payment )
                                    {{$order->payment->amount}}
                                @else
                                    {{$order->request->commission}}
                                @endif
                                грн.
                            </td>

                            @if ($order->isPaid() )
                                <td data-title="Дата" data-breakpoints="xs sm md" style="display: table-cell;">{{$order->payment->getForrmatedDate()}}</td>
                            @else
                                <td data-title="Дата" data-breakpoints="xs sm md" style="display: table-cell;">
                                    <a href="{{route('commission.pay.order', ['orderId' => $order->id])}}"
                                       class="site_button-orange button-small">Сплатити</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
    </div>
    <!--# end contentBlock #-->
    <div id="hellopreloader">
        <div id="hellopreloader_preload">
            <p class="preloader">Зачекайте, іде завантаження</p>
        </div>
    </div>
@endsection

@section('site-title')
    Комісія сервісу
@endsection

@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush