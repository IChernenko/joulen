@extends('master.order.show', ['request' => $order->request_by_master])

@section('client-contacts')
    <div class="">
        <h3>Контакти клієнта</h3>
        <br>
        <div class="clientDatas">
            <div>
                <div class="site_icon-person"><strong>Ім’я: </strong>{{ $order->client->user->name }}</div>
            </div>
            <div>
                <div class="site_icon-phone_gray"><strong>Телефон: </strong> {{ $order->client->user->phone }}</div>
            </div>
            <div>
                <div class="site_icon-mail_gray"><strong>E-mail: </strong> <a href="mailto:{{ $order->client->user->email }}"> {{ $order->client->user->email }}</a></div>
            </div>
        </div>
    </div>
    <br>
@endsection