@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Профіль компанії</h1>
        </div>
        @component('layouts.validation_errors')
        @endcomponent
        {{--<ul class="contentBlock" style="color:red; text-align: left;">--}}
        {{--<li>{{ $wait_activation_message ?? '' }}</li>--}}
        {{--</ul>--}}

        {{--<div class="cabinet-profile">--}}
        <div class="cabinet-item">

            <h2><span>Крок 1</span>Загальна інформація про компанію </h2>
            <div class="cabinet-step block {{$master->profile_stage == 'empty' ? 'open' : ''}}" id="step-1">
                <form class="publicInfo step-1" id="step-one-form" data-step='1' method="POST"
                      action="{{ route('master.store.profile') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-block">
                        <label for="company_name">
                            Назва компанії
                        </label>
                        <input data-validation-error-msg="Введіть назву компанії - мінімум 3 символи"
                               data-validation="length" data-validation-length="min3"
                               id="company_name" type="text" value="{{ $master->user->name }}"
                               name="company_name" class="company-name">

                    </div>
                    <div class="form-block">
                        <label for="master_avatar">
                            Ваш логотип файли <span
                                    class="download-size">(jpg та png розміром не менше 100х100px)</span>
                        </label>
                        <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть логотип вашої компанії</span>
                        <div class="logoImgBlock">
                            <div class="logoFile">
                                <input type="file" class="file-upload hidden_validate_file" id="master_avatar"
                                       name="master_avatar" style="display: none">
                                <img src="{{ $master->getAvatarSrc() }}" id="master_avatar_img">
                            </div>
                            <div>
                                <div id="master_avatar_download" class="site_icon-download">Завантажити</div>
                            </div>
                            <div>
                                <a href="" id="master-avatar-delete"
                                   class="site_icon-delete remove-avatar">Видалити</a>
                                <input type="checkbox" checked id="is_deleted_master_avatar" value=""
                                       name="is_deleted_master_avatar"
                                       style="display:none;">
                            </div>
                        </div>
                    </div>

                    <div class="form-block">
                        <label for="about">
                            Про компанію
                        </label>

                        <textarea data-validation="length"
                                  data-validation-error-msg="У цьому полі можна ввести не більше 2500 символів"
                                  data-validation-length="max2500" name="about"
                                  id="about">{{ $master->about }}</textarea>
                    </div>

                    <button id="step-one-submit" class='btn btn-primary' data-step-id="step-2">
                        Зберегти та перейти до кроку 2
                    </button>

                </form>
            </div>
        </div>
        <div class="cabinet-item">

            <h2><span>Крок 2</span>Контакти (для клієнтів)</h2>
            <div class="cabinet-step block {{$master->profile_stage == 'general' ? 'open' : ''}}" id="step-2">
                <form class="publicInfo" id="step-two-form" data-step='2' method="POST"
                      action="{{ route('master.update.profile') }}">
                    <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть всі фото</span>
                    <div class="form-block">
                        <label>Робочий номер телефону:</label>

                        <input class="mask-phone" data-validation-error-msg="Правильно заповніть це поле"
                               data-validation="number" data-validation-ignore="(,),-, ,+"
                               type="text" maxlength="19" name="work_phone"
                               value="{{ $master->work_phone }}"/>
                        <a class="add-link add-second-phone {{ $master->second_work_phone ? 'hide' : ""}}">
                            + додати ще один номер телефону
                        </a>
                    </div>
                    <div class="form-block second_phone" {{ $master->second_work_phone ? 'style=display:block' : ''}}>
                        <label>Робочий номер телефону 2:</label>
                        <input class="mask-phone" type="text" maxlength="19"
                               data-validation="number" data-validation-ignore="(,),-, ,+"
                               name="second_work_phone"
                               value="{{ $master->second_work_phone }}">
                        <a class="remove-second-phone {{ $master->second_work_phone ? '' : "hide"}}">
                            - видалити
                        </a>
                    </div>

                    <div class="form-block">
                        <label>Робочий e-mail</label>
                        <input type="text" data-validation="email" value="{{ $master->work_email }}"
                               name="work_email"/>
                    </div>

                    <div class="form-block">
                        <label>Сайт</label>
                        <input type="text" name="website" data-validation="required length"
                               data-validation-length="min2"
                               data-validation-error-msg="Правильно заповніть це поле"
                               value="{{ $master->website }}"/>
                    </div>
                    <div class="form-block profile-office-block">
                        <div class="profile-office-link">
                            <p><strong>Офіс:</strong></p>
                            <a id="main_office_added" class="add-link">Додати адресу головного офісу</a>
                            <input type="hidden" name="main_office_added">
                        </div>
                        <div class="profile-office-map {{ $master->main_office->id ? "" : "hide" }}"
                             id="office_map-1">
                            <p><strong>Адреса головного офісу</strong> <a class="hider site_icon-delete"
                                                                          data-hide-id="office_map-1"></a>
                            </p>
                            <div class="show-map-block">
                                <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                    Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                </span>
                                <div class="gmap-block">

                                    <input id="pac-input-1"
                                           value="{{ $master->main_office->address }}"
                                           class="search-map-input pac-input"
                                           type="text" placeholder="Введіть адресу офісу тут ...">
                                    <div id="gmap-1" class="map img-border"></div>
                                </div>
                                <div class="office-input">
                                    <input name="main_office_address" type="text" id="output-1"
                                           class="office_output"
                                           value="{{ $master->main_office->address }}" readonly>
                                    <label for="main_office_room">
                                        Офіс №
                                    </label>
                                    <input name="main_office_room" id="main_office_room"
                                           value="{{ $master->main_office->room }}" class="controls office_room"
                                           type="text">
                                </div>

                                <input type="hidden" name="main_office_region" class="office-region" value="{{ $master->main_office->region_id ?
                                        explode(' ', $regions->find($master->main_office->region_id)->name )[0] : ''}}">
                                <input type="hidden" class="geo_coords_lat" id="lat-1" name="main_office_lat"
                                       value="{{ $master->main_office->lat }}">
                                <input type="hidden" class="geo_coords_lng" id="lng-1" name="main_office_lng"
                                       value="{{ $master->main_office->lng }}">
                            </div>


                            <div class="companyLogoBlock">
                                <div>
                                    <strong>
                                        <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                    </strong>
                                </div>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="hidden_validate_file" id="main_office_photo"
                                               name="main_office_photo" style="display:none;"><br>
                                        <img src="{{ $master->main_office->getPictureSrc() }}"
                                             id="main_office_photo_img">
                                    </div>
                                    <div>
                                        <div id="main_office_photo_download" class="site_icon-download">Завантажити
                                        </div>
                                    </div>
                                    <div>
                                        <a href="" id="main_office_photo_delete"
                                           class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" id="is_deleted_main_office_photo" value=""
                                               name="is_deleted_main_office_photo"
                                               style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plus_second_map">
                            <a id="second_office_added" class="add-link">+ Додати ще один офіс</a>
                            <input type="hidden" name="second_office_added">
                        </div>
                        <div class="profile-office-map {{ $master->second_office->id ? "" : "hide" }}"
                             id="office_map-2">
                            <p><strong>Офіс №2:</strong><a class="hider site_icon-delete"
                                                           data-hide-id="office_map-2"></a>
                            </p>
                                <div class="show-map-block">
                                    <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                    <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                        Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                    </span>
                                    <div class="gmap-block">
                                        <!-- /.marker-info -->
                                        <input value="{{ $master->second_office->address }}"
                                               id="pac-input-2"
                                               class="controls pac-input search-map-input" type="text"
                                               placeholder="Введіть адресу офісу тут ...">
                                        <div id="gmap-2" class="map img-border"></div>
                                    </div>
                                    <div class="office-input">
                                        <input name="second_office_address"
                                               type="text" id="output-2" class="office_output " readonly
                                               value="{{ $master->second_office->address }}">
                                        <label for="main_office_room">
                                            Офіс №
                                        </label>
                                        <input name="second_office_room" id="second_office_room"
                                               value="{{ $master->second_office->room }}"
                                               class="controls office_room" type="text">
                                    </div>

                                    <input type="hidden" name="second_office_region" class="office-region" value="{{ $master->second_office->region_id ?
                                            explode(' ', $regions->find($master->second_office->region_id)->name )[0] : ''}}">
                                    <input type="hidden" class="geo_coords_lat" id="lat-2" name="second_office_lat"
                                           value="{{ $master->second_office->lat }}">
                                    <input type="hidden" class="geo_coords_lng" id="lng-2" name="second_office_lng"
                                           value="{{ $master->second_office->lng }}">
                                </div>

                            <div class="companyLogoBlock">
                                <div>
                                    <strong>
                                        <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                    </strong>
                                </div>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="hidden_validate_file" id="second_office_photo"
                                               name="second_office_photo" style="display: none"><br>
                                        <img src="{{ $master->second_office->getPictureSrc() }}"
                                             id="second_office_photo_img">
                                    </div>
                                    <div>
                                        <div id="second_office_photo_download" class="site_icon-download">
                                            Завантажити
                                        </div>
                                    </div>
                                    <div>
                                        <a href="" id="second_office_photo_delete"
                                           class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" id="is_deleted_second_office_photo" value=""
                                               name="is_deleted_second_office_photo"
                                               style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="plus_third_map">
                            <a id="third_office_added" class="add-link">Додати ще один офіс</a>
                            <input type="hidden" name="third_office_added">
                        </div>
                        <div class="profile-office-map {{ $master->third_office->id ? "" : "hide" }}"
                             id="office_map-3">
                            <p><strong>Офіс №3:</strong>
                                <a class="hider site_icon-delete" data-hide-id="office_map-3"></a></p>
                                <div class="show-map-block">
                                    <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                    <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                        Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                    </span>
                                    <div class="gmap-block">
                                        <!-- /.marker-info -->
                                        <input value="{{ $master->third_office->address }}"
                                               id="pac-input-3"
                                               class="controls pac-input search-map-input" type="text"
                                               placeholder="Введіть адресу офісу тут ...">
                                        <div id="gmap-3" class="map img-border"></div>
                                    </div>
                                    <div class="office-input">
                                        <input name="third_office_address"
                                               type="text" id="output-3" class="office_output" readonly
                                               value="{{ $master->third_office->address }}">
                                        <label for="main_office_room">
                                            Офіс №
                                        </label>
                                        <input name="third_office_room" id="third_office_room"
                                               value="{{ $master->third_office->room }}"
                                               class="controls office_room" type="text">
                                    </div>

                                    <input type="hidden" name="third_office_region" class="office-region" value="{{ $master->third_office->region_id ?
                             explode(' ', $regions->find($master->third_office->region_id)->name )[0] : ''}}">
                                    <input type="hidden" class="geo_coords_lat" id="lat-3" name="third_office_lat"
                                           value="{{ $master->third_office->lat }}">
                                    <input type="hidden" class="geo_coords_lng" id="lng-3" name="third_office_lng"
                                           value="{{ $master->third_office->lng }}">
                                </div>

                            <div class="companyLogoBlock">
                                <div>
                                    <strong>
                                        <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                    </strong>
                                </div>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="hidden_validate_file" id='third_office_photo'
                                               name="third_office_photo" style="display: none;"><br>
                                        <img src="{{ $master->third_office->getPictureSrc() }}"
                                             id="third_office_photo_img">
                                    </div>
                                    <div>
                                        <div id="third_office_photo_download" class="site_icon-download">Завантажити
                                        </div>
                                    </div>
                                    <div>
                                        <a href="" id="third_office_photo_delete"
                                           class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" id="is_deleted_third_office_photo" value=""
                                               name="is_deleted_third_office_photo"
                                               style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <a class='btn btn-secondary step_back' data-step-back-id="step-1">Крок назад</a>
                    <button id="step-two-submit" class='btn btn-primary'
                            data-step-id="step-3">Зберегти та перейти до кроку 3
                    </button>
                </form>
            </div>
        </div>
        <div class="cabinet-item">
            <h2><span>Крок 3</span>Дані для співпраці iз сервісом</h2>
            <div class="cabinet-step block {{$master->profile_stage == 'contacts' ? 'open' : ''}}" id="step-3">
                <div class="infoWrap">
                    <form class="publicInfo" id="step-three-form" data-step='3' method="POST"
                          action="{{ route('master.update.profile') }}" enctype="multipart/form-data">
                        {{--<p>Всю інформацію у цьому блоці буде бачити лише адміністратор сервісу Джоуль</p>--}}
                        <div class="form-block form-contact-block">
                            <h3>Контакти менеджера, який обробляє заявки на сервісі</h3>
                            <div class="form-group">
                                <label>
                                    Ім'я:
                                </label>

                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       value="{{  $master->user->manager->name }}"
                                       data-validation="length" data-validation-length="min3"
                                       name="manager_name"/>


                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Телефон:</label>

                                <input id="manager-phone" data-validation-error-msg="Правильно заповніть це поле"
                                       class="mask-phone" type="text"
                                       data-validation="number" data-validation-ignore="(,),-, ,+"
                                       maxlength="19" name="manager_phone"
                                       value="{{ $master->user->manager->phone }}"/>

                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label>Е-mail:</label>

                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="email" name="manager_email"
                                       value="{{ $master->user->manager->email }}"/>

                            </div>
                            <!-- /.form-group -->

                            <div class="styled-checkbox">
                                <input type="checkbox" checked id="manager_mailing" value="1"
                                       {{ $master->user->manager->mailing == 1 ? 'checked' : '' }} name="manager_mailing">
                                <label class="selectItem" for="manager_mailing">
                                    Отримувати
                                    розсилку на цей e-mail</label>
                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                        <div class="form-block form-contact-block">
                            <h3>Контакти керівника компанії, з яким можна обговорювати деталі
                                стратегічного партнерства:</h3>
                            <div class="form-group">


                                <label>
                                    Iм'я:
                                </label>
                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="length" data-validation-length="min3"
                                       value="{{ $master->user->owner->name }}"
                                       name="owner_name"/>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Телефон:</label>
                                <input id="chief-phone" data-validation-error-msg="Правильно заповніть це поле"
                                       class="mask-phone" type="text" maxlength="19"
                                       name="owner_phone"
                                       data-validation="number" data-validation-ignore="(,),-, ,+"
                                       value="{{ $master->user->owner->phone }}"/>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Е-mail:</label>
                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="email"
                                       value="{{ $master->user->owner->email }}" name="owner_email"/>
                            </div>
                            <!-- /.form-group -->
                            <div class="styled-checkbox">
                                <input type="checkbox" checked value="1" id="owner_mailing"
                                       {{ $master->user->owner->mailing == 1 ? 'checked' : '' }} name="owner_mailing">
                                <label for="owner_mailing">
                                    Отримувати
                                    розсилку на цей e-mail
                                </label>
                            </div>

                        </div>
                        <div class="form-block">
                            <p>Ваша компанія зареєстрована як юридична особа?</p>

                            <div class="styled-radio">
                                <input type="radio" value="1" id="legal"
                                       {{ $master->edrpou ? 'checked' : ''}} name="legal"/>
                                <label for="legal" class="selectItem">Так</label>
                            </div>
                            <div class="styled-radio">
                                <input type="radio" value="0" id="legaln"
                                       {{ !$master->edrpou ? 'checked' : ''}} name="legal"/>
                                <label for="legaln" class="selectItem">Ні</label>
                            </div>
                            <div class="codeLegal" @if(!empty($master->edrpou)) style="display: block" @endif>
                                <label class="labelCode">Код ЄДРПОУ юридичної особи:</label>
                                <input type="text" data-validation="number"
                                       data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 8 цифр"
                                       class="edrpuo" value="{{ $master->edrpou }}" name="edrpou"
                                       placeholder="8 цифр">
                            </div>
                        </div>
                        <div class="form-block">
                            <p>Ваша компанія зареєстрована як ФОП?</p>
                            <div class="styled-radio">
                                <input type="radio" value="1" id="physical"
                                       {{ $master->ipn ? 'checked' : ''}} name="physical"/>
                                <label for="physical" class="selectItem">Так</label>
                            </div>
                            <div class="styled-radio">
                                <input type="radio" value="0" id="physicaln"
                                       {{ !$master->ipn ? 'checked' : ''}} name="physical"/>
                                <label for="physicaln" class="selectItem">Ні</label>
                            </div>
                            <div class="codeFOP" @if(!empty($master->ipn)) style="display: block" @endif>
                                <label>Індивідуальний податковий номер:</label>
                                <input type="text"
                                       data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 10 цифр"
                                       data-validation="number" value="{{ $master->ipn }}" class="ipn" name="ipn"
                                       placeholder="10 цифр">
                            </div>
                        </div>
                        <div class='space-top'>
                            <a class='btn btn-secondary step_back'
                               data-step-back-id="step-2">Крок
                                назад</a>
                            <button id="step-three-submit"
                                    class='btn btn-primary'
                                    data-step-id="step-4">Зберегти та перейти до кроку 4
                            </button>
                        </div>
                    </form>

                    <div class="alert alert-primary space-top">
                        <p>Всю інформацію у цьому блоці буде бачити лише адміністратор сервісу Джоуль</p>
                    </div>

                    <!-- /.from-block -->

                </div>

            </div>
        </div>
        <div class="cabinet-item">

            <h2><span>Крок 4</span>Сервіс та обслуговування</h2>
            <div class="cabinet-step block {{$master->profile_stage == 'cooperation' ? 'open' : ''}}" id="step-4">
                <div class="cabinet-service">
                    <form id="step-four-form" data-step='4' method="POST"
                          action="{{ route('master.update.profile') }}"
                          enctype="multipart/form-data">
                        <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть всі фото</span>
                        <div class="form-block ses-type">
                            <p>Станції якого типу будує ваша компанія?</p>
                            <span class="error-block-ses form-error"
                                  style="display: none">Виберіть хоча б один тип</span>
                            <div class="styled-checkbox">

                                <input type="checkbox" id="home_ses"
                                       @if($master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both') checked=""
                                       @endif value="1" name="home_ses">
                                <label for="home_ses" class="selectItem">Домашні СЕС</label>
                                <span class="form-info">У вашому портфоліо повинно бути як мінімум 3 домашні станції</span>
                            </div>
                            <!-- /.styled-checkbox -->
                            <div class="styled-checkbox">
                                <input type="checkbox" id="commercial_ses"
                                       @if($master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both') checked=""
                                       @endif value="1" name="commercial_ses">
                                <label class="selectItem" for="commercial_ses">Комерційні
                                    СЕС</label>
                                <span class="form-info">У вашому портфоліо повинно бути як мінімум 2 комерційні станції</span>

                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                        <div class="form-block exchangeFond">
                            <p>Чи є на складах вашої компанії
                                <span class="tooltip">
                                        @component('layouts.profile_help')
                                    @endcomponent
                                    обмінний фонд

                                    </span> обладнання на випадок
                                настання гарантійного випадку у клієнта?
                            </p>

                            <div class="styled-radio">
                                <input type="radio" id="exchange_fund"
                                       {{ $master->exchange_fund_bool == 1 ? 'checked' : ''}} name="exchange_fund"
                                       value="1" autocomplete="off">
                                <label for="exchange_fund">Так</label>
                            </div>
                            <!-- /.styled-radio -->
                            <div class="styled-radio">
                                <input type="radio" id="exchange_fundn"
                                       {{ $master->exchange_fund_bool === 0 ? 'checked' : ''}} name="exchange_fund"
                                       value="0" autocomplete="off">
                                <label for="exchange_fundn">Ні</label>
                            </div>
                            <!-- /.styled-radio -->

                            <div class="brandName public-info {{ $master->exchange_fund ? '' : "hide" }}">
                                <label>Яке обладнання (вкажіть бренди) та в яких кількостях зарезервовано на складі
                                    вашої компанії?</label>
                                <textarea data-validation="length" data-validation-length="min50"
                                          data-validation-error-msg="Заповніть це поле - не менше 50 символів"
                                          name="exchange_fund_text">{{ $master->exchange_fund }}</textarea>
                            </div>
                        </div>
                        <div class="form-block corporateCar">
                            <span class="show-error-hidden-file form-error car-photo-error" style="display: none">Ви не прикріпили фото вашого корпоративного автомобіля</span>
                            <p>Чи є у вашій компанії корпоративний автомобіль для сервісного
                                обслуговування клієнтів?
                            </p>
                            <div class="styled-radio">
                                <input type="radio" {{ $master->car ? 'checked' : ''}} name="car" id="car"
                                       value="1" autocomplete="off">
                                <label for="car">Так</label>
                            </div>
                            <!-- /.styled-radio -->

                            <div class="styled-radio">
                                <input type="radio" id="nocar"
                                       {{ !$master->car ? 'checked' : ''}} name="car"
                                       value="0" autocomplete="off">
                                <label for="nocar">Ні</label>
                            </div>
                            <!-- /.styled-radio -->

                            <div class="photo-car {{ $master->car ? '' : "hide" }}">
                                <p>Прикріпіть фото автомобіля</p>
                                <span class="form-info">Наявність власного брендованого автомобіля підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="hidden_validate_file" id="car_photo"
                                               name="car_photo" style="display: none"><br>
                                        <img src="{{ $master->getCarPhotoSrc() }}" id="car_photo_img">
                                    </div>
                                    <div>
                                        <div id="car_photo_download" class="site_icon-download">Завантажити
                                        </div>
                                    </div>
                                    <div>
                                        <a href="" id="car_photo_delete"
                                           class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" checked id="is_deleted_car_photo" value="0"
                                               name="is_deleted_car_photo"
                                               style="display:none;">
                                    </div>
                                </div>
                                <!-- /.styled-checkbox -->

                            </div>
                        </div>
                        <div class="form-block projectInfo {{ $master->commercial ? '' : 'hide'}}">
                            <div>
                                <p>Чи є у вашій компанії власний проектний відділ (не на аутсорсінгу)?</p>
                                <div class="styled-radio">
                                    <input type="radio"
                                           {{ $master->project_department ? 'checked' : '' }} name="project_department"
                                           value="1" id="project_department">
                                    <label for="project_department">Так</label>
                                </div>
                                <!-- /.styled-radio -->
                                <div class="styled-radio">
                                    <input type="radio" @if(!$master->project_department) checked
                                           @endif name="project_department" value="0" id="no_department">
                                    <label for="no_department">Ні</label>
                                </div>
                                <!-- /.styled-radio -->

                            </div>
                        </div>
                        <div class="form-block machinery-block {{ $master->commercial ? '' : 'hide'}}">
                            <span class="show-error-hidden-file form-error machinery-photo-error" style="display: none">Ви не прикріпили фото вашої будівельної техніки</span>
                            <p>Чи є у вашій компанії власна будівельна техніка?</p>
                            <div class="styled-radio">
                                <input type="radio" id="machinery"
                                       {{ $master->construction_machinery == 1 ? 'checked' : ''}} name="machinery"
                                       value="1">
                                <label for="machinery">Так</label>
                            </div>
                            <!-- /.styled-radio -->
                            <div class="styled-radio">
                                <input type="radio" id="no_machinery"
                                       {{ $master->construction_machinery == 0 ? 'checked' : ''}} name="machinery"
                                       value="0">
                                <label for="no_machinery">Ні</label>
                            </div>
                            <!-- /.styled-radio -->

                            <div class="photo-machinery {{ $master->construction_machinery ? '' : "hide" }}">
                                <p>Прикріпіть фото власної будівельної техніки</p>
                                <span class="form-info">Наявність власного будівельної техніки підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="hidden_validate_file" id="machinery_photo"
                                               name="machinery_photo" style="display: none"><br>
                                        <img src="{{ $master->getMachineryPhotoSrc() }}"
                                             id="machinery_photo_img">
                                    </div>
                                    <div>
                                        <div id="machinery_photo_download" class="site_icon-download">
                                            Завантажити
                                        </div>
                                    </div>
                                    <div>
                                        <a href="" id="machinery_photo_delete"
                                           class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" checked id="is_deleted_machinery_photo"
                                               value="0"
                                               name="is_deleted_machinery_photo"
                                               style="display:none;">
                                    </div>
                                </div>
                                <!-- /.styled-checkbox -->

                            </div>
                        </div>
                        <div class="form-block companyBrand">
                            <p>Обладнання яких брендів пропонує ваша компанія?</p>
                            <div class="invertor">
                                <p>Інвертори:</p>
                                <div class="brand-list">
                                    <ul>
                                        @foreach($master->equipment as $invertor)
                                            @if($invertor->type_id == 1)
                                                <li>
                                                    <a href="#view-brands" data-id="{{ $invertor->id }}"
                                                       class="brand-item">{{ $invertor->title }}</a>
                                                    <a data-id="{{ $invertor->id }}"
                                                       class="brand-edit edit open-update-invertor open-modal"
                                                       href="#update-brand-invertor"></a>
                                                    <a class="brand-delete delete delete-equipment"
                                                       href="#modal-confirm-delete" data-id="{{ $invertor->id }}"
                                                       data-url="{{ route('master.delete.equipment', ['id' => $invertor->id]) }}"></a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    <a href="#add-brand-invertor"
                                       class="open-modal open-add-invertor add-link open-brand"
                                       data-id="1">
                                        + додати бренд
                                    </a>
                                </div>
                            </div>
                            <div class="sun-panel">
                                <p>Сонячні панелі:</p>
                                <div class="brand-list">
                                    <ul>
                                        @foreach($master->equipment as $panel)
                                            @if($panel->type_id == 2)
                                                <li><a href="#view-brands" data-id="{{$panel->id}}"
                                                       class="brand-item">{{ $panel->title }}</a>
                                                    <a data-id="{{ $panel->id }}"
                                                       class="brand-edit edit open-update-panel open-modal"
                                                       href="#update-brand-panel"></a>
                                                    <a class="brand-delete delete delete-equipment"
                                                       href="#modal-confirm-delete" data-id="{{$panel->id}}"
                                                       data-url="{{ route('master.delete.equipment',['id' => $panel->id]) }}"></a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    <a href="#add-brand-panel" class="open-modal open-add-panel add-link open-brand"
                                       data-id="2">
                                        <span class="orange">+</span> додати бренд
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-block contract
                                 {{ $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                            <p>Прикріпити зразок типового договору, який заключає ваша компанія
                                з клієнтом на будівництво домашньої СЕС?
                            </p>
                            <span class="form-info">Цю інформацію бачитиме лише адміністратор сайту</span>

                            <div class="inputfile-styled">
                                <label class="input-file">
                                    <input type="file" name="contract" value="0"
                                           style="display:none;">Прикріпити
                                    договір</label>
                                <span class="file-name">{{ $master->contract->file_name }}</span>
                                <div>
                                    <a href="" id="company-contract-delete"
                                       class="site_icon-delete">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_contract" value="0"
                                           name="is_deleted_contract"
                                           style="display:none;">
                                </div>
                            </div>

                            {{--<label class="site_icon-download"><input type="file" name="contract" value="0"--}}
                            {{--style="display:none;">Прикріпити--}}
                            {{--договір</label>--}}
                            {{--<div>--}}
                            {{--<a href="" id="company-contract-delete" class="site_icon-delete">Видалити</a>--}}
                            {{--<input type="checkbox" checked id="is_deleted_contract" value="0"--}}
                            {{--name="is_deleted_contract"--}}
                            {{--style="display:none;">--}}
                            {{--</div>--}}
                        </div>
                        <div class="form-block">
                            <p>У яких областях ви надаєте послуги?</p>
                            <div class="service-region checkbox-validation-block">
                                @foreach ($regions as $region)
                                    <div class="styled-checkbox tooltip-checkbox">
                                        @if ($region->slug == 'krym')
                                            <span class="tooltip-text">Тимчасово недоступно</span>
                                        @endif
                                        <input {{ isset($master->regions->keyBy('id')[$region->id]) ? 'checked' : '' }}
                                               type="checkbox" value="{{ $region->id }}"
                                               name="work_location[{{ $region->id }}]"
                                               class="styledChk regionChk" id='{{ $region->slug }}'
                                               @if ($region->slug == 'krym') disabled @endif
                                        />
                                        <label for="{{ $region->slug }}">{{ $region->name }}</label>
                                    </div>
                                @endforeach
                                <input type="checkbox" style="display: none" name="validation-work-location"
                                       class="validation-location">
                            </div>
                            <span class="error-block-region form-error"
                                  style="display:none;">Оберіть хоча б один регіон</span>
                        </div>
                        <div class="form-block orderRegion order-region-wrap">
                            <p>Із яких областей ви б хотіли отримувати замовлення перш <br> за
                                все? <span class="form-info"> (до трьох областей)</span>
                            </p>
                            <ol>
                                <li>
                                    <label for="Region1">1:</label>
                                    <select data-validation="required" name="favorite_work_location[1]" id="Region1"
                                            class="select2">
                                        <option selected value="">Не вказано</option>
                                        @foreach($regions as $region)
                                            @if($region->slug != 'krym')
                                                <option {{ $master->getFavoriteRegions()->count() > 0 &&
                                                        $master->getFavoriteRegions()->toArray()[0] == $region->id ? 'selected' : '' }}
                                                        {{ ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ||
                                                        ($master->getFavoriteRegions()->count() > 2 &&
                                                        $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                        value="{{ $region->id }}">{{ $region->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </li>
                                <li>
                                    <label for="Region2">2:</label>
                                    <select name="favorite_work_location[2]" class="select2"
                                            id="Region2">
                                        <option selected value="">Не вказано</option>
                                        @foreach($regions as $region)
                                            @if($region->slug != 'krym')
                                                <option {{ $master->getFavoriteRegions()->count() > 1 &&
                                                        $master->getFavoriteRegions()->toArray()[1] == $region->id ? 'selected' : '' }}
                                                        {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                        ($master->getFavoriteRegions()->count() > 2 && $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                        value="{{ $region->id }}">{{ $region->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </li>
                                <li>
                                    <label for="Region3">3:</label>
                                    <select name="favorite_work_location[3]"
                                            class="select2"
                                            id="Region3">
                                        <option value="">Не вказано</option>
                                        @foreach($regions as $region)
                                            @if($region->slug != 'krym')
                                                <option {{ $master->getFavoriteRegions()->count() > 2 &&
                                                        $master->getFavoriteRegions()->toArray()[2] == $region->id ? 'selected' : '' }}
                                                        {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                        ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ? 'disabled' : '' }}
                                                        value="{{ $region->id }}">{{ $region->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </li>
                            </ol>

                        </div>
                        <div class="form-block accreditation">
                            <p>Ваша компанія отримала акредитацію від Укргазбанку по програмі
                                кредитування "Еко-енергія"?
                            </p>
                            <div class="styled-radio">
                                <input type="radio" id="eco_energy"
                                       @if($master->eco_energy == '1') checked
                                       @endif name="eco_energy" value="1">
                                <label for="eco_energy" class="selectItem">Так</label>
                            </div>
                            <!-- /.styled-checkbox -->
                            <div class="styled-radio">
                                <input type="radio" id="no_eco_energy"
                                       @if($master->eco_energy == '0') checked
                                       @endif name="eco_energy" value="0">
                                <label for="no_eco_energy">Ні</label>
                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                        <div class="form-block  startInstall">
                            <p>В якому році ваша компанія почала займатися монтажем сонячних
                                електростанцій?
                            </p>
                            <div class="field">
                                @php
                                    $currentYear = date("Y");
                                    $yearArray = range($currentYear, 1991);
                                @endphp
                                <select data-validation="required" name="year_started" class="select2" id="install">
                                    <option disabled selected>Рік</option>

                                    @foreach ($yearArray as $year)
                                        <option @if ($year == $master->year_started) selected
                                                @endif value="{{$year}}">{{$year}}</option>;
                                    @endforeach
                                </select>
                            </div>
                            <span class="form-info">Ви повинні додати у ваше портфоліо станцію введену в експлуатацію відповідного року</span>
                        </div>


                        <div>
                            <a class='step_back btn btn-secondary'
                               data-step-back-id="step-3">Крок
                                назад</a>
                            <button id="step-four-submit"
                                    class='btn btn-primary'
                                    data-step-id="step-5">Зберегти та перейти до кроку 5
                            </button>
                        </div>
                    </form>
                    {{--<div class="consumer_protection">--}}
                    {{--<h4>Згідно закону України Про захист прав споживачів:</h4>--}}
                    {{--<p>На письмову вимогу споживача на--}}
                    {{--час ремонту йому надається (з доставкою) товар аналогічної марки. Для цього продавець,--}}
                    {{--виробник зобов'язані мати обмінний фонд товарів.</p>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        <div class="cabinet-item">

            <h2><span>Крок 5</span>Портфоліо</h2>
            <div class="cabinet-step block {{$master->profile_stage == 'services' ? 'open' : ''}}" id="step-5">
                <form id="step-five-form" data-step='5' method="POST" action="{{ route('master.update.profile') }}"
                      enctype="multipart/form-data">
                    <div class="block-portfolio">
                        <div class="alert alert-primary">
                            <strong>Увага!</strong>
                            <p>
                                Сервіс автоматично рекомендуватиме клієнтам ті компанії, які вже збудували станції у
                                їхньому або найближчому населеному пункті.
                                Тому чим більше станцій ви додасте у портфоліо, тим більше шансів у вашої компанії
                                отримати нове замовлення.
                            </p>
                            <strong>Звертаємо увагу!</strong>
                            <p>Якщо під час перевірки з’ясується, що ви додали не існуючу станцію - ваш аккаунт буде
                                заблоковано без можливості відновлення.
                            </p>

                        </div>
                        <div class="home-ses-block {{ $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                            <h3>Домашні СЕС:</h3>
                            <div class="table-block">
                                <table class="table home-ses-table">
                                    @foreach($master->getHomePortfolios() as $object)
                                        @if($object->subcategory_id != null)
                                            <tr class='tr_id' id="{{ $object->id }}">
                                                <td data-title="Дата" data-breakpoints="xs">
                                                    {{ $object->month }} {{ $object->year }}
                                                </td>

                                                <td data-title="Тип" data-breakpoints="">
                                                    <a href="#show-portfolio" class="get-portfolio"
                                                       data-id="{{ $object->id }}">
                                                        {{ $subcategories[$object->subcategory_id] }}
                                                    </a>
                                                </td>
                                                <td data-title="Потужність" data-breakpoints="">
                                                    {{ $object->power }} кВт
                                                </td>
                                                <td data-title="Адреса" data-breakpoints="xs sm md">
                                                    {{ $object->getCity() }}
                                                </td>

                                                <td data-title="Обладнання" data-breakpoints="xs">
                                                    @if($object->delivery == 1)
                                                        <img src="../images/delivery.png" alt="" title="Доставка">
                                                    @else
                                                        <img class="opacity-img" src="../images/delivery.png" alt=""
                                                             title="Доставка">
                                                    @endif

                                                </td>
                                                <td data-title="Монтаж" data-breakpoints="xs">

                                                    @if($object->installation == 1)
                                                        <img src="../images/installation.png" alt="" title="Монтаж">
                                                    @else
                                                        <img class="opacity-img" src="../images/installation.png"
                                                             title="Монтаж">
                                                    @endif

                                                </td>
                                                <td data-title="Документування" data-breakpoints="xs">

                                                    @if($object->documentation == 1)
                                                        <img src="../images/designing.png" alt=""
                                                             title="Проектування">
                                                    @else
                                                        <img class="opacity-img" src="../images/designing.png"
                                                             alt="" title="Проектування">
                                                    @endif

                                                </td>
                                                <td data-title="Редагувати" data-breakpoints=""
                                                    class="edit_block">
                                                    <a class="open-update-ses update-home site-icon-edit"
                                                       data-id="{{ $object->id }}"
                                                       data-master-id="{{ $object->master_id }}"
                                                       data-remodal-target="add-ses"></a>

                                                </td>
                                                <td data-title="Видалити" data-breakpoints="" class="remove_block">
                                                    <a class="delete_ses site_icon-delete"
                                                       data-id="{{ $object->id }}"
                                                       data-remodal-target="delete-ses"></a>
                                                </td>
                                                <td data-title="Статус" data-breakpoints="xs sm"
                                                    class="application_status">
                                                    @if($object->status->slug == 'verified')
                                                        <p class="active-portfolio">Aктивна</p>
                                                    @elseif($object->status->slug == 'checking')
                                                        <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                                    @else
                                                        <p class="rejected-request">{{ $object->status->name }}</p>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                                <a id='add-home-ses-open' class="add-home open-modal add-link"
                                   data-remodal-target="add-ses">+ додати об'єкт</a>
                                <div class="alert-wrap">
                                    <div class="alert alert-warning validation-home hide">
                                        <p>Потрібно обов'язково додати як мінімум три домашні станції із
                                            фотографіями.
                                            Якщо ваша компанія ще не будувала домашні СЕС, то вам потрібно
                                            <a class="returnToStepFour" href="">повернутися до</a>
                                            кроку №4 цієї анкети і деактивувати пункт "Домашні СЕС".</p>
                                    </div>
                                </div>
                                <!-- /.alert-wrap -->

                            </div>
                            <div id="gmap-4" class="map map_portfolio img-border"></div>
                        </div>
                        <div class="commercial-ses-block {{ $master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                            <h3 class="title-ses">Комерційні СЕС:</h3>
                            <div class="table-block">
                                <table class="table commercial-ses-table">

                                    @foreach($master->getCommercialPortfolios() as $object)
                                        <tr class='tr_id' id="{{ $object->id }}">
                                            <td data-title="Дата" data-breakpoints="xs">
                                                {{ $object->month }} {{ $object->year }}
                                            </td>
                                            <td data-title="Тип" data-breakpoints="xs">
                                                <a href="#show-portfolio" class="get-portfolio"
                                                   data-id="{{ $object->id }}">
                                                    @if($object->ground == 1)
                                                        Наземна
                                                    @else
                                                        Дахова
                                                    @endif
                                                </a>
                                            </td>
                                            <td data-title="Потужність" data-breakpoints="">
                                                {{ $object->power }} кВт
                                            </td>
                                            <td data-title="Адреса" data-breakpoints="xs sm md">
                                                {{ $object->getCity() }}
                                            </td>
                                            <td data-title="Генпіряд" data-breakpoints="xs">
                                                @if($object->general_contractor == 1)
                                                    <img src="../images/gen_contractor.png" alt=""
                                                         title="Генпідряд">
                                                @else
                                                    <img class="opacity-img" src="../images/gen_contractor.png"
                                                         alt="" title="Генпідряд">
                                                    {{--генпідряд--}}
                                                @endif

                                            </td>
                                            <td data-title="Проектування" data-breakpoints="xs">

                                                @if($object->designing == 1)
                                                    <img src="../images/designing.png" alt="" title="Проектування">
                                                @else
                                                    <img class="opacity-img" src="../images/designing.png" alt=""
                                                         title="Проектування">

                                                    {{--проек--}}
                                                @endif

                                            </td>
                                            <td data-title="Обладнання" data-breakpoints="xs">
                                                @if($object->delivery == 1)
                                                    <img src="../images/delivery.png" alt="" title="Доставка">
                                                @else
                                                    <img class="opacity-img" src="../images/delivery.png" alt=""
                                                         title="Доставка">
                                                    {{--обл--}}
                                                @endif

                                            </td>
                                            <td data-title="Монтаж" data-breakpoints="xs">

                                                @if($object->installation == 1)
                                                    <img src="../images/installation.png" alt="" title="Монтаж">
                                                @else
                                                    <img class="opacity-img" src="../images/installation.png" alt=""
                                                         title="Монтаж">
                                                    {{--монт--}}
                                                @endif

                                            </td>
                                            <td data-title="едит" data-breakpoints="" class="edit_block">
                                                <a class="open-update-ses update-commercial site-icon-edit"
                                                   data-id="{{ $object->id }}" data-master-id="{{ $object->master_id }}"
                                                   data-remodal-target="add-ses"></a>

                                            </td>
                                            <td data-title="делейт" data-breakpoints="" class="remove_block">
                                                <a class="delete_ses site_icon-delete"
                                                   data-id="{{ $object->id }}"
                                                   data-remodal-target="delete-ses"></a>
                                            </td>
                                            <td data-title="Статус" data-breakpoints="xs sm"
                                                class="application_status">
                                                @if($object->status->slug == 'verified')
                                                    <p class="active-portfolio">Aктивна</p>
                                                @elseif($object->status->slug == 'checking')
                                                    <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                                @else
                                                    <p class="rejected-request">{{ $object->status->name }}</p>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                </table>
                                <a id='add-commercial-ses-open' class='open-modal add-link add-commercial'
                                   data-remodal-target="add-ses">+ додати об'єкт</a>
                                <div class="alert-wrap">
                                    <div class="alert alert-warning validation-commercial hide">
                                        <p>Потрібно обов'язково додати як мінімум дві комерційі станції із
                                            фотографіями.
                                            Якщо ваша компанія ще не будувала комерційні СЕС, то вам потрібно
                                            <a class="returnToStepFour" href="">повернутися до</a>
                                            кроку №4 цієї анкети і скасувати пункт "Комерційні СЕС".</p>
                                    </div>
                                </div>
                            </div>
                            <div id="gmap-5" class="map map_portfolio img-border"></div>
                        </div>
                        <div class="watch-profile">
                            <a class='add-link'
                               href="{{ route('master.profile.preview', ['id' => $master->id]) }}"
                               target="_blank">Переглянути, як бачитимуть ваш профіль клієнти (у новому вікні)</a>
                        </div>
                        <!-- /.watch-profile -->
                        <div>
                            <a class='step_back btn btn-secondary' data-step-back-id="step-4">Крок
                                назад</a>
                            <button id="step-five-submit"
                                    class='btn btn-primary'>
                                Зберегти та подати інформацю на перевірку адміністрації
                            </button>
                        </div>
                        <div class="alert alert-danger">
                            <div class="alert-danger-item">
                                <p>Якщо при заповнені анкети Ви вказали недостовірну інформацію - Ваш акаунт буде
                                    заблоковано без можливості відновлення.</p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(isset($wait_activation_message))
        <div class="alert alert-green">
            <p>{{ $wait_activation_message }}</p>
        </div>
    @endif

    <div id="hellopreloader" style="display:none">
        <div id="hellopreloader_preload">
            <p class="preloader">Зачекайте, іде завантаження</p>
        </div>
    </div>
    <!-- /.cabinet-page-wrap -->


@endsection

@push('modals')
@component('layouts.master.profile.add_invertor_brand', [
'invertor_type_equipment' => $invertor_type_equipment,
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.update_invertor_brand', [
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.add_panel_brand', [
'panel_type_equipment' => $panel_type_equipment,
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.update_panel_brand', [
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.ses_portfolio', [
'subcategories' => $subcategories,
'invertor_type_equipment' => $invertor_type_equipment,
'panel_type_equipment' => $panel_type_equipment,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.view_brands')
@endcomponent

<div class="remodal" data-remodal-id="delete-ses" id="delete-ses">
    <div class="modal_header delete-ses">
        <p>Ви дійсно бажаєте видалити цю СЕС з портфоліо?</p>
    </div>
    <div class='space-top button-remove'>
        <a class='btn btn-primary delete_ses_confirm' data-remodal-action="confirm">Так</a>
        <button class='btn btn-secondary' data-remodal-action="cancel">Ні</button>
    </div>
</div>

<div class="remodal" id="modal-confirm-delete" data-remodal-id="modal-confirm-delete">
    <div class="modal_header">
        <p>Ви дійсно бажаєте видалити цей бренд зі списку?</p>
    </div>
    <div class="space-top remove-btn">
        <button class='btn btn-secondary' data-remodal-action="cancel">Ні</button>
        <a class="btn btn-primary delete-equipment-confirm" data-remodal-action="confirm">Так</a>
    </div>
</div>

@component('layouts.master.profile.view_portfolio')
@endcomponent

@endpush

@push('footer_scripts')
<script>
    var master = {!! $master !!},
        equipment = {!! $master->equipment->keyBy('id') !!},
        guarantee_repairs = {!! $guarantee_repairs !!},
        exchange_funds = {!! $exchange_funds !!},
        imgDirectoryPath = '{{ asset('images/') }}',
        defaultAvatar = '{{ asset('images/specialist.png') }}',
        markerSrc = '{{ asset('images/map-marker.png') }}',// TODO: use imgDriectoryPath var
        getEquipmentURL = '{{ route('master.get.equipment') }}',
        deleteEquipmentURL = '{{ route('master.delete.equipment') }}',
        getBrandsURL = '{{ route('master.get.brands') }}',
        getPortfolioURL = '{{ route('master.get.portfolio') }}',
        storePortfolioURL = '{{ route('master.store.portfolio') }}',
        updatePortfolioURL = '{{ route('master.update.portfolio') }}',
        deletePortfolioURL = '{{ route('master.delete.portfolio') }}',
        subcategories = {!! $subcategories !!},
        invertor_id = {!! $invertor_type_equipment->keyBy('id') !!},
        panel_id = {!! $panel_type_equipment->keyBy('id') !!},
        masterOrderRoute = '{{ route('master.order.index') }}',
        homePortfolioCoordinates = {!! $master->home_portfolios_coordinates !!},
        commercialPortfolioCoordinates = {!! $master->commercial_portfolios_coordinates !!},
        regions = {!! $regions->keyBy('id') !!},
        count_regions = {!! $regions->count() !!},
        invertorName = {!! $invertor_type_equipment->keyBy('id') !!},
        panelName = {!! $panel_type_equipment->keyBy('id') !!};
</script>
<script src="{{ asset('libs/footable/js/footable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/portfolio_modals.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile-form-handler.js') }}"></script>
<script src="{{ asset('js/jquery.nice-select.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
        defer></script>
<script src="{{ asset('libs/remodal/remodal.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
<script src="{{ asset('js/master/mask.js') }}"></script>
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>

@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/footable/css/footable.standalone.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal-default-theme.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>

@endpush
