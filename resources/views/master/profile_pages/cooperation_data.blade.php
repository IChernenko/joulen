@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Дані для співпраці iз сервісом</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div class="cabinet-item">
            <div class="infoWrap">
                <form method="POST"
                      action="{{ route('master.profile_pages.partnership.update') }}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" value="3" name="profile_stage">
                    {{--<p>Всю інформацію у цьому блоці буде бачити лише адміністратор сервісу Джоуль</p>--}}
                    <div class="form-block form-contact-block">
                        <h3>Контакти менеджера, який обробляє заявки на сервісі</h3>
                        <div class="form-group">
                            <label>
                                Ім'я:
                            </label>

                            <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                   value="{{  $master->user->manager->name }}"
                                   data-validation="length" data-validation-length="min3"
                                   name="manager_name"/>

                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Телефон:</label>

                            <input id="manager-phone" data-validation-error-msg="Правильно заповніть це поле"
                                   class="mask-phone" type="text"
                                   data-validation="number" data-validation-ignore="(,),-, ,+"
                                   maxlength="19" name="manager_phone"
                                   value="{{ $master->user->manager->phone }}"/>

                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label>Е-mail:</label>

                            <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                   data-validation="email" name="manager_email"
                                   value="{{ $master->user->manager->email }}"/>

                        </div>
                        <!-- /.form-group -->

                        <div class="styled-checkbox">
                            <input type="checkbox" id="manager_mailing" value="1"
                                   {{ $master->user->manager->mailing == 1 ? 'checked' : '' }} name="manager_mailing">
                            <label class="selectItem" for="manager_mailing">
                                Отримувати
                                розсилку на цей e-mail</label>
                        </div>
                        <!-- /.styled-checkbox -->

                    </div>
                    <div class="form-block form-contact-block">
                        <h3>Контакти керівника компанії, з яким можна обговорювати деталі
                            стратегічного партнерства:</h3>
                        <div class="form-group">
                            <label>
                                Iм'я:
                            </label>
                            <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                   data-validation="length" data-validation-length="min3"
                                   value="{{ $master->user->owner->name }}"
                                   name="owner_name"/>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Телефон:</label>
                            <input id="chief-phone" data-validation-error-msg="Правильно заповніть це поле"
                                   class="mask-phone" type="text" maxlength="19"
                                   name="owner_phone"
                                   data-validation="number" data-validation-ignore="(,),-, ,+"
                                   value="{{ $master->user->owner->phone }}"/>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Е-mail:</label>
                            <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                   data-validation="email"
                                   value="{{ $master->user->owner->email }}" name="owner_email"/>
                        </div>
                        <!-- /.form-group -->
                        <div class="styled-checkbox">
                            <input type="checkbox" value="1" id="owner_mailing"
                                   {{ $master->user->owner->mailing == 1 ? 'checked' : '' }} name="owner_mailing">
                            <label for="owner_mailing">
                                Отримувати
                                розсилку на цей e-mail
                            </label>
                        </div>

                    </div>
                    <div class="form-block tooltip-checkbox" style="display: block">
                        <p>Ваша компанія зареєстрована як юридична особа?</p>

                        <div class="styled-radio">
                            <input type="radio" value="1" id="legal"
                                   {{ $master->edrpou ? 'checked disabled' : 'disabled'}} name="legal"/>
                            <label for="legal" class="selectItem">Так</label>
                        </div>
                        <div class="styled-radio">
                            <input type="radio" value="0" id="legaln"
                                   {{ !$master->edrpou ? 'checked disabled' : 'disabled'}} name="legal"/>
                            <label for="legaln" class="selectItem">Ні</label>
                        </div>
                        <div class="codeLegal" @if(!empty($master->edrpou)) style="display: block" @endif>
                            <label class="labelCode">Код ЄДРПОУ юридичної особи:</label>
                            <input type="text" data-validation="number"
                                   data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 8 цифр"
                                   class="edrpuo" value="{{ $master->edrpou }}" name="edrpou"
                                   placeholder="8 цифр" readonly>
                        </div>
                        <span class="tooltip-text" style="top: 17%; left: 30%;">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                    </div>
                    <div class="form-block tooltip-checkbox" style="display: block">
                        <p>Ваша компанія зареєстрована як ФОП?</p>
                        <div class="styled-radio">
                            <input type="radio" value="1" id="physical"
                                   {{ $master->ipn ? 'checked disabled' : 'disabled'}} name="physical"/>
                            <label for="physical" class="selectItem">Так</label>
                        </div>
                        <div class="styled-radio">
                            <input type="radio" value="0" id="physicaln"
                                   {{ !$master->ipn ? 'checked disabled' : 'disabled'}} name="physical"/>
                            <label for="physicaln" class="selectItem">Ні</label>
                        </div>
                        <div class="codeFOP" @if(!empty($master->ipn)) style="display: block" @endif>
                            <label>Індивідуальний податковий номер:</label>
                            <input type="text"
                                   data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 10 цифр"
                                   data-validation="number" value="{{ $master->ipn }}" class="ipn" name="ipn"
                                   placeholder="10 цифр" readonly>
                        </div>
                        <span class="tooltip-text" style="top: 17%; left: 30%;">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                    </div>
                    <div class='space-top'>
                        <button class='btn btn-primary' type="submit">Зберегти
                        </button>
                        <a class='btn btn-secondary step_back'>Скасувати</a>
                    </div>
                </form>

                <div class="alert alert-primary space-top">
                    <p>Всю інформацію у цьому блоці буде бачити лише адміністратор сервісу Джоуль</p>
                </div>

                <!-- /.from-block -->

            </div>

        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
<script src="{{ asset('js/master/mask.js') }}"></script>
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile_pages/cooperation_data.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>
@endpush
