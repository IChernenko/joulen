@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Портфоліо</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div class="cabinet-item">
                <div class="block-portfolio">
                    <div class="alert alert-primary">
                        <strong>Увага!</strong>
                        <p>
                            Сервіс автоматично рекомендуватиме клієнтам ті компанії, які вже збудували станції у їхньому або найближчому населеному пункті.
                            Тому чим більше станцій ви додасте у портфоліо, тим більше шансів у вашої компанії отримати нове замовлення.
                        </p>
                        <strong>Звертаємо увагу!</strong>
                        <p>Якщо під час перевірки з’ясується, що ви додали не існуючу станцію - ваш аккаунт буде заблоковано без можливості відновлення.
                        </p>

                    </div>
                    <div class="home-ses-block {{ $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                        <h3>Домашні СЕС:</h3>
                        <div class="table-block">
                        <table class="table home-ses-table">
                            @foreach($master->getHomePortfolios() as $object)
                                @if($object->subcategory_id != null)
                                    <tr class='tr_id' id="{{ $object->id }}">
                                        <td data-title="Дата" data-breakpoints="xs">
                                            {{ $object->month }} {{ $object->year }}
                                        </td>

                                        <td data-title="Тип" data-breakpoints="">
                                            <a href="#show-portfolio" class="get-portfolio" data-id="{{ $object->id }}">
                                            {{ $subcategories[$object->subcategory_id] }}
                                            </a>
                                        </td>
                                        <td data-title="Потужність" data-breakpoints="">
                                            {{ $object->power }} кВт
                                        </td>
                                        <td data-title="Адреса" data-breakpoints="xs sm md">
                                            {{ $object->getCity() }}
                                        </td>

                                        <td data-title="Обладнання" data-breakpoints="xs">
                                            @if($object->delivery == 1)
                                                <img src="../images/delivery.png" alt="">
                                            @else
                                                <img class="opacity-img" src="../images/delivery.png" alt="">
                                            @endif

                                        </td>
                                        <td data-title="Монтаж" data-breakpoints="xs">

                                            @if($object->installation == 1)
                                                <img src="../images/installation.png" alt="">
                                            @else
                                                <img class="opacity-img" src="../images/installation.png"
                                                     alt="">
                                            @endif

                                        </td>
                                        <td data-title="Документування" data-breakpoints="xs">

                                            @if($object->documentation == 1)
                                                <img src="../images/designing.png" alt="">
                                            @else
                                                <img class="opacity-img" src="../images/designing.png" alt="">
                                            @endif

                                        </td>
                                        <td data-title="Редагувати" data-breakpoints=""
                                            class="edit_block">
                                            <a class="open-update-ses update-home site-icon-edit"
                                               data-id="{{ $object->id }}"
                                               data-remodal-target="add-ses"></a>

                                        </td>
                                        <td data-title="Видалити" data-breakpoints="" class="remove_block">
                                            <a class="delete_ses site_icon-delete"
                                               data-id="{{ $object->id }}"
                                               data-remodal-target="delete-ses"></a>
                                        </td>
                                        <td data-title="Статус" data-breakpoints="xs sm"
                                            class="application_status">
                                            @if($object->status->slug == 'verified')
                                                <p class="active-portfolio">Aктивна</p>
                                            @elseif($object->status->slug == 'checking')
                                                <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                            @else
                                                <p class="rejected-request">{{ $object->status->name }}</p>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                        <a id='add-home-ses-open' class="add-home open-modal add-link"
                           data-remodal-target="add-ses">+ додати об'єкт</a>
                        <div class="alert-wrap">
                            <div class="alert alert-warning validation-home hide">
                                <p>Потрібно обов'язково додати як мінімум три домашні станції із фотографіями.
                                    Якщо ваша компанія ще не будувала домашні СЕС, то вам потрібно
                                    <a class="returnToStepFour" href="">повернутися до</a>
                                    до кроку №4 цієї анкети і деактивувати пункт "Домашні СЕС".</p>
                            </div>
                        </div>
                        <!-- /.alert-wrap -->

                    </div>
                        <div id="gmap-4" class="map map_portfolio img-border"></div>
                    </div>
                    <div class="commercial-ses-block {{ $master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                        <h3>Комерційні СЕС:</h3>
                        <div class="table-block">
                        <table class="table commercial-ses-table">

                            @foreach($master->getCommercialPortfolios() as $object)
                                <tr class='tr_id' id="{{ $object->id }}">
                                    <td data-title="Дата" data-breakpoints="xs">
                                        {{ $object->month }} {{ $object->year }}
                                    </td>
                                    <td data-title="Тип" data-breakpoints="">
                                        <a href="#show-portfolio" class="get-portfolio" data-id="{{ $object->id }}">
                                        @if($object->ground == 1)
                                            Наземна
                                        @else
                                            Дахова
                                        @endif
                                        </a>
                                    </td>
                                    <td data-title="Потужність" data-breakpoints="">
                                        {{ $object->power }} кВт
                                    </td>
                                    <td data-title="Адреса" data-breakpoints="xs sm md">
                                        {{ $object->getCity() }}
                                    </td>
                                    <td data-title="Генпіряд" data-breakpoints="xs">
                                        @if($object->general_contractor == 1)
                                            <img src="../images/gen_contractor.png" alt="">
                                        @else
                                            <img class="opacity-img" src="../images/gen_contractor.png" alt="">
                                            {{--генпідряд--}}
                                        @endif

                                    </td>
                                    <td data-title="Проектування" data-breakpoints="xs">

                                        @if($object->designing == 1)
                                            <img src="../images/designing.png" alt="">
                                        @else
                                            <img class="opacity-img" src="../images/designing.png" alt="">

                                            {{--проек--}}
                                        @endif

                                    </td>
                                    <td data-title="Обладнання" data-breakpoints="xs">
                                        @if($object->delivery == 1)
                                            <img src="../images/delivery.png" alt="">
                                        @else
                                            <img class="opacity-img" src="../images/delivery.png" alt="">
                                            {{--обл--}}
                                        @endif

                                    </td>
                                    <td data-title="Монтаж" data-breakpoints="xs">

                                        @if($object->installation == 1)
                                            <img src="../images/installation.png" alt="">
                                        @else
                                            <img class="opacity-img" src="../images/installation.png" alt="">
                                            {{--монт--}}
                                        @endif

                                    </td>
                                    <td data-title="едит" data-breakpoints="" class="edit_block">
                                        <a class="open-update-ses update-commercial site-icon-edit"
                                           data-id="{{ $object->id }}" data-remodal-target="add-ses"></a>

                                    </td>
                                    <td data-title="делейт" data-breakpoints="" class="remove_block">
                                        <a class="delete_ses site_icon-delete"
                                           data-id="{{ $object->id }}"
                                           data-remodal-target="delete-ses"></a>
                                    </td>
                                    <td data-title="Статус" data-breakpoints="xs sm" class="application_status">
                                        @if($object->status->slug == 'verified')
                                            <p class="active-portfolio">Aктивна</p>
                                        @elseif($object->status->slug == 'checking')
                                            <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                        @else
                                            <p class="rejected-request">{{ $object->status->name }}</p>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </table>
                        <a id='add-commercial-ses-open' class='open-modal add-link add-commercial'
                           data-remodal-target="add-ses">+ додати об'єкт</a>
                        <div class="alert-wrap">
                            <div class="alert alert-warning validation-commercial hide">
                                <p>Потрібно обов'язково додати як мінімум дві комерційі станції із фотографіями.
                                    Якщо ваша компанія ще не будувала комерційні СЕС, то вам потрібно
                                    <a class="returnToStepFour" href="">повернутися до</a>
                                    до кроку №4 цієї анкети і скасувати пункт "Комерційні СЕС".</p>
                            </div>
                        </div>
                    </div>
                        <div id="gmap-5" class="map map_portfolio img-border"></div>
                    </div>
                    <!-- /.watch-profile -->
                </div>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('modals')
@component('layouts.master.profile.ses_portfolio', [
'subcategories' => $subcategories,
'invertor_type_equipment' => $invertor_type_equipment,
'panel_type_equipment' => $panel_type_equipment,
'master' => $master
])
@endcomponent

<div class="remodal" data-remodal-id="delete-ses" id="delete-ses">
    <div class="modal_header delete-ses">
        <p>Ви дійсно бажаєте видалити цю СЕС з портфоліо?</p>
    </div>
    <div class='space-top button-remove'>
        <a class='btn btn-primary delete_ses_confirm' data-remodal-action="confirm">Так</a>
        <button class='btn btn-secondary' data-remodal-action="cancel">Ні</button>
    </div>
</div>

@component('layouts.master.profile.view_portfolio')
@endcomponent
@endpush

@push('footer_scripts')
<script>
    var imgDirectoryPath = '{{ asset('images/') }}',
        master = {!! $master !!},
        markerSrc = '{{ asset('images/map-marker.png') }}',// TODO: use imgDriectoryPath var
        getPortfolioURL = '{{ route('master.get.portfolio') }}',
        storePortfolioURL = '{{ route('master.store.portfolio') }}',
        updatePortfolioURL = '{{ route('master.update.portfolio') }}',
        deletePortfolioURL = '{{ route('master.delete.portfolio') }}',
        subcategories = {!! $subcategories !!},
        invertor_id = {!! $invertor_type_equipment->keyBy('id') !!},
        panel_id = {!! $panel_type_equipment->keyBy('id') !!},
        homePortfolioCoordinates = {!! $master->home_portfolios_coordinates !!},
        commercialPortfolioCoordinates = {!! $master->commercial_portfolios_coordinates !!},
        regions = {!! $regions->keyBy('id') !!},
        invertorName = {!! $invertor_type_equipment->keyBy('id') !!},
        panelName = {!! $panel_type_equipment->keyBy('id') !!};
</script>
<script src="{{ asset('libs/footable/js/footable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile-form-handler.js') }}"></script>
<script src="{{ asset('js/jquery.nice-select.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
        defer></script>
<script src="{{ asset('libs/remodal/remodal.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile_pages/portfolio.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/portfolio_modals.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/master/profile.js') }}"></script>--}}
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/footable/css/footable.standalone.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/remodal/remodal-default-theme.css') }}"/>
@endpush
