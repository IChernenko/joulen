@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Сервіс та обслуговування</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div class="cabinet-item">
            <div class="cabinet-service">
                <form id="service" action="{{ route('master.profile_pages.services.update') }}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <input type="hidden" value="4" name="profile_stage">
                    <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть всі фото</span>
                    <div class="form-block ses-type">
                        <p>Станції якого типу будує ваша компанія?</p>
                        <span class="error-block-ses form-error" style="display: none">Виберіть хоча б один тип</span>
                        <div class="styled-checkbox tooltip-checkbox">
                            <span class="tooltip-text">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                            <input type="checkbox" id="home_ses"
                                   {{  $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? 'checked' : ''}} value="1" name="home_ses">
                            <label for="home_ses" class="selectItem">Домашні СЕС</label>
                            <span class="form-info">У вашому портфоліо повинно бути як мінімум 3 домашні станції</span>
                        </div>
                        <!-- /.styled-checkbox -->
                        <div class="styled-checkbox tooltip-checkbox">
                            <span class="tooltip-text">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                            <input type="checkbox" id="commercial_ses"
                                   {{  $master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both' ? 'checked' : ''}}
                                    value="1" name="commercial_ses">
                            <label class="selectItem" for="commercial_ses">Комерційні
                                СЕС</label>
                            <span class="form-info">У вашому портфоліо повинно бути як мінімум 2 комерційні станції</span>

                        </div>
                        <!-- /.styled-checkbox -->

                    </div>
                    <div class="form-block exchangeFond">
                        <p>Чи є на складах вашої компанії
                            <span class="tooltip">
                                        @component('layouts.profile_help')
                                @endcomponent
                                обмінний фонд

                                    </span> обладнання на випадок
                            настання гарантійного випадку у клієнта?
                        </p>

                        <div class="styled-radio">
                            <input type="radio" id="exchange_fund"
                                   {{ $master->exchange_fund ? 'checked' : ''}} name="exchange_fund"
                                   value="1">
                            <label for="exchange_fund">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" id="exchange_fundn"
                                   {{ !$master->exchange_fund ? 'checked' : ''}} name="exchange_fund"
                                   value="0">
                            <label for="exchange_fundn">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                        <div class="brandName public-info {{ $master->exchange_fund ? '' : "hide" }}">
                            <label>Яке обладнання (вкажіть бренди) та в яких кількостях зарезервовано на складі
                                вашої компанії?</label>
                            <textarea data-validation="length" data-validation-length="min50"
                                      data-validation-error-msg="Заповніть це поле - не менше 50 символів"
                                      name="exchange_fund_text">{{ $master->exchange_fund }}</textarea>
                        </div>
                    </div>
                    <div class="form-block corporateCar tooltip-checkbox">
                        <span class="tooltip-text" style="top: 16%; left: 33%;">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                        <p>Чи є у вашій компанії корпоративний автомобіль для сервісного
                            обслуговування клієнтів?
                        </p>
                        <div class="styled-radio ">
                            <input type="radio" {{ $master->car ? 'checked' : ''}} name="car" id="car"
                                   value="1">
                            <label for="car">Так</label>
                        </div>
                        <!-- /.styled-radio -->

                        <div class="styled-radio">
                            <input type="radio" id="nocar"
                                   {{ !$master->car ? 'checked' : ''}} name="car"
                                   value="0">
                            <label for="nocar">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                        <div class="photo-car {{ $master->car ? '' : "hide" }}">
                            <p>Прикріпіть фото автомобіля</p>
                            <span class="form-info">Наявність власного брендованого автомобіля підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="car_photo"
                                           name="car_photo"  style="display: none"><br>
                                    <img src="{{ $master->getCarPhotoSrc() }}" id="car_photo_img">
                                </div>
                                <div>
                                    <div id="car_photo_download" class="site_icon-download">Завантажити
                                    </div>
                                </div>
                                <div>
                                    <a href="" id="car_photo_delete" class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_car_photo" value="0"
                                           name="is_deleted_car_photo"
                                           style="display:none;">
                                </div>
                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                    </div>
                    <div class="form-block projectInfo {{ $master->commercial ? '' : 'hide'}}">
                        <div>
                            <p>Чи є у вашій компанії власний проектний відділ (не на аутсорсінгу)?</p>
                            <div class="styled-radio">
                                <input type="radio"
                                       {{ $master->project_department ? 'checked' : '' }} name="project_department"
                                       value="1" id="project_department">
                                <label for="project_department">Так</label>
                            </div>
                            <!-- /.styled-radio -->
                            <div class="styled-radio">
                                <input type="radio" @if(!$master->project_department) checked
                                       @endif name="project_department" value="0" id="no_department">
                                <label for="no_department">Ні</label>
                            </div>
                            <!-- /.styled-radio -->

                        </div>
                    </div>
                    <div class="form-block machinery-block tooltip-checkbox {{ $master->commercial ? '' : 'hide'}}">
                        <span class="tooltip-text" style="top: 10%; left: 33%;">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                        <p>Чи є у вашій компанії власна будівельна техніка?</p>
                        <div class="styled-radio">
                            <input type="radio" id="machinery"
                                   {{ $master->construction_machinery ? 'checked' : ''}} name="machinery"
                                   value="1">
                            <label for="machinery">Так</label>
                        </div>
                        <!-- /.styled-radio -->
                        <div class="styled-radio">
                            <input type="radio" id="no_machinery"
                                   {{ !$master->construction_machinery ? 'checked' : ''}} name="machinery"
                                   value="0">
                            <label for="no_machinery">Ні</label>
                        </div>
                        <!-- /.styled-radio -->

                        <div class="photo-machinery {{ $master->construction_machinery ? '' : "hide" }}">
                            <p>Прикріпіть фото власної будівельної техніки</p>
                            <span class="form-info">Наявність власного будівельної техніки підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="machinery_photo"
                                           name="machinery_photo" style="display: none"><br>
                                    <img src="{{ $master->getMachineryPhotoSrc() }}"
                                         id="machinery_photo_img">
                                </div>
                                <div>
                                    <div id="machinery_photo_download" class="site_icon-download">
                                        Завантажити
                                    </div>
                                </div>
                                <div>
                                    <a href="" id="machinery_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_machinery_photo"
                                           value="0"
                                           name="is_deleted_machinery_photo"
                                           style="display:none;">
                                </div>
                            </div>
                            <!-- /.styled-checkbox -->

                        </div>
                    </div>
                    <div class="form-block companyBrand">
                        <p>Обладнання яких брендів пропонує ваша компанія?</p>
                        <div class="invertor">
                            <p>Інвертори:</p>
                            <div class="brand-list">
                                <ul>
                                    @foreach($master->equipment as $invertor)
                                        @if($invertor->type_id == 1)
                                            <li>
                                                <a href="#view-brands" data-id="{{ $invertor->id }}"
                                                   class="brand-item">{{ $invertor->title }}</a>
                                                <a data-id="{{ $invertor->id }}"
                                                   class="brand-edit edit open-update-invertor open-modal"
                                                   href="#update-brand-invertor"></a>
                                                <a class="brand-delete delete delete-equipment"
                                                   href="#modal-confirm-delete" data-id="{{ $invertor->id }}"
                                                   data-url="{{ route('master.delete.equipment', ['id' => $invertor->id]) }}"></a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <a href="#add-brand-invertor" class="open-modal open-add-invertor add-link open-brand"
                                   data-id="1">
                                    + додати бренд
                                </a>
                            </div>
                        </div>
                        <div class="sun-panel">
                            <p>Сонячні панелі:</p>
                            <div class="brand-list">
                                <ul>
                                    @foreach($master->equipment as $panel)
                                        @if($panel->type_id == 2)
                                            <li><a href="#view-brands" data-id="{{$panel->id}}"
                                                   class="brand-item">{{ $panel->title }}</a>
                                                <a data-id="{{ $panel->id }}"
                                                   class="brand-edit edit open-update-panel open-modal"
                                                   href="#update-brand-panel"></a>
                                                <a class="brand-delete delete delete-equipment"
                                                   href="#modal-confirm-delete" data-id="{{$panel->id}}"
                                                   data-url="{{ route('master.delete.equipment',['id' => $panel->id]) }}"></a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <a href="#add-brand-panel" class="open-modal open-add-panel add-link open-brand"
                                   data-id="2">
                                    <span class="orange">+</span> додати бренд
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-block contract
                                 {{ $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                        <p>Прикріпити зразок типового договору, який заключає ваша компанія
                            з клієнтом на будівництво домашньої СЕС?
                        </p>
                        <span class="form-info">Цю інформацію бачитиме лише адміністратор сайту</span>

                        <div class="inputfile-styled">
                            <label class="input-file">
                                <input type="file" name="contract" value="0"
                                       style="display:none;">Прикріпити
                                договір</label>
                            <span class="file-name">{{ $master->contract->file_name }}</span>
                            <div>
                                <a href="" id="company-contract-delete"
                                   class="site_icon-delete">Видалити</a>
                                <input type="checkbox" checked id="is_deleted_contract" value="0"
                                       name="is_deleted_contract"
                                       style="display:none;">
                            </div>
                        </div>

                        {{--<label class="site_icon-download"><input type="file" name="contract" value="0"--}}
                        {{--style="display:none;">Прикріпити--}}
                        {{--договір</label>--}}
                        {{--<div>--}}
                        {{--<a href="" id="company-contract-delete" class="site_icon-delete">Видалити</a>--}}
                        {{--<input type="checkbox" checked id="is_deleted_contract" value="0"--}}
                        {{--name="is_deleted_contract"--}}
                        {{--style="display:none;">--}}
                        {{--</div>--}}
                    </div>
                    <div class="form-block">
                        <p>У яких областях ви надаєте послуги?</p>
                        <div class="service-region checkbox-validation-block">
                            @foreach ($regions as $region)
                                <div class="styled-checkbox">
                                    <input @if ($region->slug == 'krym') disabled @endif
                                    {{ isset($master->regions->keyBy('id')[$region->id]) ? 'checked' : '' }}
                                           type="checkbox" value="{{ $region->id }}"
                                           name="work_location[{{ $region->id }}]"
                                           class="styledChk regionChk" id='{{ $region->slug }}'
                                    />
                                    <label for="{{ $region->slug }}">{{ $region->name }}</label>
                                </div>
                            @endforeach
                            <input type="checkbox" style="display: none" name="validation-work-location"
                                   class="validation-location">
                        </div>
                        <span class="error-block-region form-error" style="display:none;">Оберіть хоча б один регіон</span>
                    </div>
                    <div class="form-block orderRegion order-region-wrap">
                        <p>Із яких областей ви б хотіли отримувати замовлення перш <br> за
                            все? <span class="form-info"> (до трьох областей)</span>
                        </p>
                        <ol>
                            <li>
                                <label for="Region1">1:</label>
                                <select data-validation="required" name="favorite_work_location[1]" id="Region1"
                                        class="select2">
                                    <option selected value="">Не вказано</option>
                                    {{--TODO: fix selectors--}}
                                    @foreach($regions as $region)
                                        @if($region->slug != 'krym')
                                            <option {{ $master->getFavoriteRegions()->count() > 0 &&
                                                        $master->getFavoriteRegions()->toArray()[0] == $region->id ? 'selected' : '' }}
                                                    {{ ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ||
                                                    ($master->getFavoriteRegions()->count() > 2 &&
                                                    $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                    value="{{ $region->id }}">{{ $region->name }}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </li>
                            <li>
                                <label for="Region2">2:</label>
                                <select name="favorite_work_location[2]" class="select2"
                                        id="Region2">
                                    <option selected value="">Не вказано</option>
                                    @foreach($regions as $region)
                                        @if($region->slug != 'krym')
                                            <option {{ $master->getFavoriteRegions()->count() > 1 &&
                                                        $master->getFavoriteRegions()->toArray()[1] == $region->id ? 'selected' : '' }}
                                                    {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                    ($master->getFavoriteRegions()->count() > 2 && $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                    value="{{ $region->id }}">{{ $region->name }}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </li>
                            <li>
                                <label for="Region3">3:</label>
                                <select name="favorite_work_location[3]"
                                        class="select2"
                                        id="Region3">
                                    <option selected value="">Не вказано</option>
                                    @foreach($regions as $region)
                                        @if($region->slug != 'krym')
                                            <option {{ $master->getFavoriteRegions()->count() > 2 &&
                                                        $master->getFavoriteRegions()->toArray()[2] == $region->id ? 'selected' : '' }}
                                                    {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                    ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ? 'disabled' : '' }}
                                                    value="{{ $region->id }}">{{ $region->name }}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </li>
                        </ol>

                    </div>
                    <div class="form-block accreditation tooltip-checkbox">
                        <span class="tooltip-text">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                        <p>Ваша компанія отримала акредитацію від Укргазбанку по програмі
                            кредитування "Еко-енергія"?
                        </p>
                        <div class="styled-radio">
                            <input type="radio" id="eco_energy"
                                   @if($master->eco_energy == '1') checked
                                   @endif name="eco_energy" value="1">
                            <label for="eco_energy" class="selectItem">Так</label>
                        </div>
                        <!-- /.styled-checkbox -->
                        <div class="styled-radio">
                            <input type="radio" id="no_eco_energy"
                                   @if($master->eco_energy == '0') checked
                                   @endif name="eco_energy" value="0">
                            <label for="no_eco_energy">Ні</label>
                        </div>
                        <!-- /.styled-checkbox -->

                    </div>
                    <div class="form-block startInstall tooltip-checkbox">
                        <span class="tooltip-text" style="top: 76%">Щоб внести зміни - напишіть лист адміністратору сайту.</span>
                        <p>В якому році ваша компанія почала займатися монтажем сонячних
                            електростанцій?
                        </p>
                        <div class="field">
                            @php
                                $currentYear = date("Y");
                                $yearArray = range($currentYear, 1991);
                            @endphp
                            <select data-validation="required" name="year_started"
                                    class="select2 disabled-year" id="install">
                                <option disabled selected>Рік</option>

                                @foreach ($yearArray as $year)
                                    <option @if ($year == $master->year_started) selected
                                            @endif value="{{$year}}">{{$year}}</option>;
                                @endforeach
                            </select>
                        </div>
                        <span class="form-info">Ви повинні додати у ваше портфоліо станцію введену в експлуатацію відповідного року</span>
                    </div>


                    <div>
                        <button id="step-four-submit"
                                class='btn btn-primary' type="submit">Зберегти</button>
                        <a class='step_back btn btn-secondary'>Скасувати</a>
                    </div>
                </form>
            </div>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, йде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('modals')
@component('layouts.master.profile.add_invertor_brand', [
'invertor_type_equipment' => $invertor_type_equipment,
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.update_invertor_brand', [
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.add_panel_brand', [
'panel_type_equipment' => $panel_type_equipment,
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.update_panel_brand', [
'guarantee_repairs' => $guarantee_repairs,
'exchange_funds' => $exchange_funds,
'master' => $master
])
@endcomponent

@component('layouts.master.profile.view_brands')
@endcomponent

<div class="remodal" id="modal-confirm-delete" data-remodal-id="modal-confirm-delete">
    <div class="modal_header">
        <p>Ви дійсно бажаєте видалити цей бренд зі списку?</p>
    </div>
    <div class="space-top remove-btn">
        <a class="btn btn-primary delete-equipment-confirm" data-remodal-action="confirm">Так</a>
        <button class='btn btn-secondary' data-remodal-action="cancel">Ні</button>
    </div>
</div>

@endpush

@push('footer_scripts')
<script>
    var master = {!! $master !!},
        equipment = {!! $master->equipment->keyBy('id') !!},
        guarantee_repairs = {!! $guarantee_repairs !!},
        exchange_funds = {!! $exchange_funds !!},
        getEquipmentURL = '{{ route('master.get.equipment') }}',
        deleteEquipmentURL = '{{ route('master.delete.equipment') }}',
        getBrandsURL = '{{ route('master.get.brands') }}',
        regions = {!! $regions->keyBy('id') !!},
        count_regions = {!! $regions->count() !!};
</script>
<script src="{{ asset('js/jquery.nice-select.js') }}"></script>
<script src="{{ asset('libs/remodal/remodal.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile_pages/service.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile-form-handler.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>
@endpush
