@extends('layouts.master.app')

@section('content')
    {{--TODO: FRONT remove html breaks--}}
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Розсилка</h1>
        </div>

        <div class="cabinet-profile cabinet-item">
            <form id="formparams" class="validate_form" method="POST"
                  action="{{ route('master.profile_pages.subscription.update') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="dispatch-block">
                        <div class="styled-checkbox-blue">
                            <input {{ $master->subcategoriesWithMailingTrue->count() > 0 ? 'checked' : '' }}
                                   type="checkbox" id="orders_mailing"
                                   value="1" name="orders_mailing">
                            <label class="selectItem" for="orders_mailing"><strong>Нові замовлення</strong></label>
                        </div>
                        <div class="spacing-bottom">
                            <strong>Тип замовлень:</strong>
                            @foreach($master->subcategoriesWithMailingTrue as $subcategory)
                                {{ $subcategory->name . ($subcategory != $master->subcategoriesWithMailingTrue->last() ? ',' : '')}}
                            @endforeach
                            <a class="site_icon-edit-blue" href="{{ route('master.profile_pages.services') }}#stationType">Змінити</a>
                        </div>
                        <div class="spacing-bottom">
                            <strong>З наступних регіонів:</strong>
                            <a class="site_icon-edit-blue" href="{{ route('master.profile_pages.services') }}#serviceRegion">Змінити</a>
                            <ul>
                            @foreach($master->regions as $region)
                                <li>{{ $region->name }}</li>
                            @endforeach
                            </ul>
                        </div>
                        <div class="spacing-bottom">
                            <strong>На такі емейли:</strong>
                            <a class="site_icon-edit-blue" href="{{ route('master.profile_pages.partnership') }}">Змінити</a>
                            <ul>
                                @if ($master->user->owner->mailing)
                                <li>{{ $master->user->owner->email }}</li>
                                @endif
                                @if ($master->user->manager->mailing)
                                <li>{{ $master->user->manager->email }}</li>
                                @endif
                            </ul>
                        </div>
                    <div class="styled-checkbox-blue">
                        <input value="1" name="news_mailing" type="checkbox"
                               id="news_mailing" {{ $master->news_mailing ? 'checked' : '' }}>
                        <label for="news_mailing"><strong>Новини сервісу</strong></label>
                    </div>
                </div>

                <div>
                    <button class='btn btn-primary' type="submit">Зберегти</button>
                </div>
            </form>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
    @push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/master/settings/private.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
    @endpush
    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>
    @endpush
@endsection

@section('site-title')
    Параметри облікового запису
@endsection