@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Отримати статус "Перевірена компанія"</h1>
        </div>
        @component('layouts.validation_errors')
        @endcomponent
        <div class="request-block">
            <div class="cabinet-item">
                <p>Перевірені компанії мають ряд переваг:</p>
                <ol>
                    <li>Перевірені компанії мають можливість спілкуватися із клієнтом напряму.</li>
                    <li>Перевірені компанії першими отримують замовлення від клієнтів.</li>
                    {{--TODO: SET RANDOM VERIFIED MASTER ID TO LINK Переглянути зразок ??--}}
                    <li>У каталозі спеціалістів інформація про перевірені компанії відображається<br>
                        у розгорнутому форматі. <a href="{{ route('master.show', 2) }}">Переглянути зразок</a></li>
                </ol>

                <p>Щоб отримати статус "Перевірена компанія", потрібно відповідати наступним вимогам: </p>
                <ol>
                    <li>Компанія повинна бути зареєстрована як ФОП або юридична особа.</li>
                    <li>У компанії повинен бути постійнодіючий офіс.</li>
                    <li>Компанія повинна займатися будівництвом станцій "під ключ": постачати обладнання,<br>
                        займатися монтажем, проектуванням, документальним оформленням.
                    </li>
                    <li>Компанія повинна продавати лише сертифіковане та офіційно-ввезене обладнання.</li>
                </ol>

                <p>Процес верифікації проходить наступним чином: </p>
                <ul>
                    <li><span>Крок 1.</span> Подача заявки на отримання статусу "Перевірена компанія".</li>
                    <li><span>Крок 2.</span> Анонімний візит представника сервісу "Джоуль" у ваш офіс для попереднього
                        ознайомлення із
                        діяльністю.
                    </li>
                    <li><span>Крок 3.</span> Планова зустріч представника сервісу "Джоуль" із керівництвом вашої
                        компанії.
                    </li>
                    <li><span>Крок 4.</span> Перевірка зазначеної у анкеті інформації:</li>
                    <ul>
                        <li>перевірка документів</li>
                        <li>спільний візит мінімум до 3-х клієнтів, зазначених у вашому портфоліо</li>
                    </ul>
                    <li><span>Крок 5.</span> Схвалення вашої участі іншими компаніями, які вже отримали статус
                        перевірених.
                    </li>
                    <li><span>Крок 6.</span> Підписання договору про співпрацю.</li>
                </ul>
                <div class="alert alert-danger">
                    <div class="alert-danger-item">
                        <p>Увага! Якщо під час перевірки виявиться, що у анкеті вказана недостовірна інформація -
                            аккаунт
                            вашої компанії буде заблоковано.</p><br>
                        <p>Не подавайте заявку на перевірку, поки не перевірите всю інформацію, зазначену у анкеті.</p>
                    </div>
                </div>

                @if(!$master->verification_request)
                    <a class="btn btn-primary" data-remodal-target="modal-confirm">Стати перевіреним спеціалістом</a>
                @endif
            </div>

            @if(isset($status))
                <div class="alert alert-green">
                    <p>{{ $status ?? '' }}</p>
                </div>
            @endif
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
    @push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/master/settings/private.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
    @endpush
    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>
    @endpush
@endsection

@section('site-title')
    Параметри облікового запису
@endsection

@push('modals')
<div class="remodal modal-verification" id="modal-master-verification" data-remodal-id="modal-confirm">
    <div class="triangle"></div>
    <div class="text-modal">
        <p>Ви бажаєте подати заявку на отримання статусу <strong>"Перевірена компанія"</strong></p>
        <br>
        <p>Чи підтверджуєте ви, що всі дані у вашій анкеті відповідають дійсності?</p>
    </div>
    <form id="master-verification-form" class="validate_form" method="POST"
          action="{{ route('master.profile_pages.verification_request.store') }}">
        {{ csrf_field() }}
        <div class="button-modal">
            <button id="master-verification-confirmed" class="btn btn-green" data-remodal-action="confirm">Так, всі дані
                відповідають дійсності
            </button>
            <button id="master-verification-canceled" class='btn btn-secondary' data-remodal-action="cancel">Скасувати
            </button>
        </div>
    </form>
</div>
@endpush