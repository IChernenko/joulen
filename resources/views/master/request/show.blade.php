@extends('order.show')
@section('user-section')
    <div class="contentBlock listOfApp">
        <div class="container">

            @if(Auth::user()->master->status->slug == 'verified' && $order->show_phone == true && $order->master_id != Auth::user()->master->id)
            <div class="">
                <h3>Контакти клієнта</h3>
                <br>
                <div class="clientDatas">
                    <div>
                        <div class="site_icon-person"><strong>Ім’я: </strong>{{ $order->client->user->name }}</div>
                    </div>
                    <div>
                        <div class="site_icon-phone_gray"><strong>Телефон: </strong> {{ $order->client->user->phone }}</div>
                    </div>
                    <div>
                        <div class="site_icon-mail_gray"><strong>E-mail: </strong> <a href="mailto:{{ $order->client->user->email }}"> {{ $order->client->user->email }}</a></div>
                    </div>
                </div>
            </div>
            <br>
            @endif

            <h3>Ваша заявка</h3>
            <div>
                <div class="app-item">
                    <div class="app-item-author clr">
                        <div class="app-item-author_info clr">
                            <h3><a href="{{ route('master.show', ['id' => $request->master->id]) }}">{{ $request->master->user->name }}</a></h3>
                            <div class="app-item-reviews site_icon-reviews">Відгуків: <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="">{{ $request->master->reviews_count }}</a>
                                @if($request->master->negative_reviews_count > 0)
                                    <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="site_color-red" title="Негативні відгуки">(-{{ $request->master->negative_reviews_count }})</a>
                                @endif
                            </div>
                            <a href="{{ route('master.show', ['id' => $request->master->id]) }}" class="app-item-author_img">
                                <img src="{{ $request->master->getAvatarSrc() }}" alt="{{ $request->master->user->name }}">
                            </a>
                            {{--TODO: make url filter maker--}}
                            <div class="app-item-loc site_icon-location"><a href="{{ route('master.index') . '/' . $request->master->region->slug }}" title="Переглянути інших спеціалістів із цього регіону">
                                    {{ $request->master->main_office->address ? explode(',', $request->master->main_office->address)[0] : $request->master->region->name }}
                                </a></div>
                        </div>
                        <div class="app-item-price">{{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }} <span>${{ $request->price }}</span></div>
                    </div>
                    <div class="app-item-content">
                        @foreach($request->prices as $price)
                            <h4>{{ $price->type->name }}: ${{ $price->price }}</h4>
                            <p style="margin-left:40px;white-space:pre-wrap;">{!! nl2br(e($price->comment)) !!}</p>
                        @endforeach
                        <a href="{{ route('master.request.edit', ['id' => $request->id]) }}" class="site_button-orange button-small leaveFeedback">Редагувати заявку</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection