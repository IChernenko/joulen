@extends('layouts.old.app')
@section('content')
    <div class="edit-content">
        <div class="container pageTitle">
            <h1>Публічна інформація про вас</h1>
        </div>

        <div>
        <span id="clearcontainer">
            <input type="file" style="display: none;" name="file" id="selfile" accept="image/jpeg,image/png">
        </span>
        </div>
        <div>

            <div class="contentBlock">
                <div class="container">
                    <form class="publicInfo" id="master-public-settings-form" method="post"
                          action="{{ route('master.update.public') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li style="color: red; list-style: none; margin-top: 10px;">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div>
                            <div class="site_icon-person">
                                <div class="label">
                                    <span class="companyLabel">Назва компанії</span>
                                    <span class="privateLabel">Ім'я, Прізвище</span>
                                </div>
                                <div class="field">
                                    <input type="text" value="{{ $user->getOriginal('name') !== null ? $user->getOriginal('name') : old('name') }}" name="name"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="site_icon-phone_gray">
                                <div class="label">Телефон</div>
                                <div class="field">
                                    <input id="phone-number" class="mask-phone" type="text" maxlength="19" name="phone"
                                           value="{{ $user->phone }}"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="site_icon-mail_gray">
                                <div class="label">Робочий e-mail</div>
                                <div class="field">
                                    <input type="text" value="{{ $user->master->work_email }}" name="work_email"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="site_icon-link">
                                <div class="label">Сайт</div>
                                <div class="field">
                                    <input type="text" value="{{  $user->master->website  }}" name="site"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="site_icon-loc">
                                <div class="label">
                                    <span class="companyLabel">Де знаходиться ваш центральний офіс</span>
                                    <span class="privateLabel">Звідки ви?</span>
                                </div>
                                <div class="field">
                                    <select name="location" class="custom_select hidden_validate">
                                        @foreach ($regions as $region)
                                            <option @if ($region->id == $user->master->region->id) selected
                                                    @endif value="{{ $region->id }}">{{ $region->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="services_block">
                            <input type="checkbox"
                                   name="services[ses]" value="1"
                                   class="styledChk servicesChk" id="ses" checked>
                        </div>
                        <div>
                            <div class="site_icon-info">
                                <div class="label">
                                    <span class="companyLabel">Про компанію</span>
                                    <span class="privateLabel">Про вас</span>
                                </div>
                                <span class="field_info">Не більше 2000 символів</span>
                                <div class="field">
                                    <textarea name="about">{{ $user->master->about }}</textarea>
                                    <div class="companyLogoBlock">
                                        <div>
                                            <strong>
                                                <span class="companyLabel">Ваш логотип</span>
                                                <span class="privateLabel">Ваше фото</span>
                                            </strong> файли jpg та png розміром не менше 100х100px
                                        </div>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id="your_photo" name="master_avatar"
                                                       style="display:none;"><br>
                                                <img src="{{ $user->master->getAvatarSrc() }}" alt="" id="avatar">
                                            </div>
                                            <div>
                                                <div class="site_icon-download">Завантажити</div>
                                            </div>
                                            <div>
                                                <a href="" id="master-avatar-delete"
                                                   class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" checked id="is_deleted_avatar" value="0"
                                                       name="is_deleted_avatar"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="site_icon-map">
                                <div class="label">В яких областях ви готові надавати свої послуги</div>
                                <div class="field clr checkbox-validation-block">
                                    @foreach ($regions as $region)
                                        <div class="col3">
                                            <input {{ isset($user->master->regions->keyBy('id')[$region->id]) ? 'checked' : '' }}
                                                   type="checkbox" value="{{ $region->id }}"
                                                   name="work_location[{{ $region->id }}]"
                                                   class="styledChk regionChk" id='{{ $region->slug }}'
                                            />
                                            <label for="{{ $region->slug }}">{{ $region->name }}</label>
                                        </div>
                                    @endforeach

                                    <input type="checkbox" style="display: none" name="validation-work-location"
                                           class="validation-location">
                                </div>
                            </div>
                        </div>
                        <div class="contentBlock block-portfolio">
                            <div class="container">
                                <h3>Ваше портфоліо:</h3>
                                <div class="clr portfolioContainer">


                                    @foreach( $portfolios as $portfolio)

                                        <div class="portfolio-item portfolio-item_new">
                                            <p class="portfolio-description">{{$portfolio->description}}</p>

                                            <div class="upload-photo-block">
                                                @foreach( $portfolio->attachments as $attachment)
                                                    <div class="uploaded-photo qq-file-id-0 qq-upload-success"
                                                         qq-file-id="0" attachment-id="{{$attachment->id}}">
                                                        <img class="qq-thumbnail-selector" qq-max-size="400"
                                                             qq-server-scale="" src="{{$attachment->getPath()}}">
                                                        <button type="button"
                                                                class="qq-btn qq-upload-cancel-selector qq-upload-cancel remove-uploaded-photo site_icon-delete qq-hide">
                                                            Cancel
                                                        </button>

                                                        <div class="status-info">
                                                            <span role="status"
                                                                  class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                                            <span class="qq-upload-spinner-selector qq-upload-spinner qq-hide"></span>
                                                            <button type="button"
                                                                    class="qq-btn qq-upload-retry-selector qq-upload-retry qq-hide">
                                                                Повторити
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div>
                                                <input type="hidden" class="portfolio-id" value="{{$portfolio->id}}">
                                                <a href="javascript:" class="site_icon-edit edit-portfolio"
                                                   data-modal="#addPortfolio">Редагувати об’єкт</a>
                                                <a href="#" class="site_icon-delete remove-portfolio-item">Видалити
                                                    обєкт</a></div>


                                        </div>
                                    @endforeach

                                    <a href="#" id="addObject"
                                       class="addObjectPortfolio portfolio-item showModal"
                                       data-modal="#addPortfolio">
                                    <span class="object-portfolio-text">
                                        Додати об'єкт
                                    </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container controlBtn textCenter">
                            <input type="submit" class="site_button-orange validation-btn" value="Зберегти">
                            <a href="{{ route('master.order.index') }}"
                               class="site_button-orange onlyBorder">Скасувати</a>

                            {{--<button class="site_button-orange validation-btn">Зберегти</button>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="modalsWrapper" class="modalsWrapper" style="display: none;">
        <div id='addPortfolio' class='modal modal-portfoilo'>
            <form action='' class='portfolioForm'>
                <input type="hidden" value="" name="portfolio_id">
                <div class='label'>
                    <span>Опис об'єкту</span> (максимум 75 символів)
                </div>
                <div class='field'>
                    <textarea maxlength='106' class='portfolio-descrioption validate_required' name='description'
                              id='descr'></textarea>

                    <span style="display: none" class="error error-descr">Це поле необхідно заповнити</span>
                </div>
                <div class='afterField textNormal'>
                    Наприклад: СЕС потужністю 25 кВт збудована у 2015 році у с.Лашківка Чернівецької обл
                </div>
                <div>
                    <div class='label'>
                        <span>Фото об’єкта</span>
                    </div>
                    <div class='field'>
                        <div class="fine-uploader-manual-trigger" id="fine-uploader-manual-trigger"></div>
                    </div>
                    <span style="display: none" class="error error-photo">Ви не завантажли жодного фото</span>
                </div>
                <div class='space-top'>
                    <button class='site_button-orange button-small save-btn save-portfolio'>Зберегти</button>
                    <a href='#' class='site_button-orange onlyBorder button-small closewind'>Скасувати</a>
                </div>
                <span class='closeModal'></span>
            </form>
        </div>
    </div>

    @push('footer_scripts')
    <script>
        var delete_image = '{{ roleRoute("portfolio.delete.image") }}';
        var portfolio_attach_image = '{{ roleRoute("portfolio.attach.image") }}';
        var portfolio_attach_edit = '{{ roleRoute("portfolio.attachment.edit") }}';
        var portfolio_delete = '{{ roleRoute("portfolio.delete") }}';
        var portfolio_edit = '{{ roleRoute("portfolio.edit") }}';
        var portfolio_save = '{{ roleRoute("settings.portfolio.save") }}';
    </script>
    <script src="{{ asset('js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/master/edit-validation-rules.js') }}"></script>
    <script src="{{ asset('js/master/master.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fine-uploader/jquery.fine-uploader.js')}}"></script>
    <script type="text/template" id="preloader-template">
        <div class="lds-css ng-scope">
            <div class="lds-spinner" style="width: 100%;height:100%">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </script>
    <script type="text/template" id="qq-template-manual-trigger">

        <div class="upload-photo-block qq-uploader-selector qq-uploader" qq-drop-area-text="Перетягніть файли сюди">

            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>

            <span class="qq-drop-processing-selector qq-drop-processing">
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <div class="uploaded_photos qq-upload-list-selector qq-upload-list" aria-live="polite"
                 aria-relevant="additions removals">
                <div class="uploaded-photo">
                    <img class="qq-thumbnail-selector" qq-max-size="400" qq-server-scale>
                    <button type="button"
                            class="qq-btn qq-upload-cancel-selector qq-upload-cancel remove-uploaded-photo site_icon-delete">
                        Cancel
                    </button>
                    {{--<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>--}}
                    <div class="status-info">
                        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                        <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Повторити</button>
                    </div>
                </div>
            </div>
            <div class="buttons upload_photo_buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div class="upload_photo">
                        <div class="pick-image-btn">
                            Додати фото
                        </div>
                        <!-- /.pick-image-btn -->

                    </div>
                </div>
                <button style="display: none" type="button" id="trigger-upload" class="btn btn-primary trigger-upload">
                    Upload
                </button>
            </div>
            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector site_button-orange">ОК</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
    <script>
        var defaultAvatar = '{{ asset('images/specialist.png') }}';
    </script>
    @endpush

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    @endpush

@endsection

@section('site-title')
    Публічна інформація про вас
@endsection