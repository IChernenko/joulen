@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container content-container">
            <!--<div class="master-public-page-wrap">-->
            <div class="page-content--white">
                <div class="master-block-wrap">
                    <div class="master-block">
                        <div class="master-photo">
                            <img src="{{ $master->getAvatarSrc() }}" alt="{{ $master->name }}">
                        </div>
                        <!-- /.master-photo -->
                        <h1 class="master-name">
                            {{ $master->user->name }}
                            @can('edit', $master)
                                <a class="site-icon-edit" href="{{ route('admin.master.edit', $master->id)}}"></a>
                            @endcan
                        </h1>
                        <!-- /.master-name -->
                        <div class="master-review-link">
                            <a href="#reviews">{{ $master->reviews_count }}</a>відгуків
                            @if($master->negative_reviews_count > 0)
                                <span><a href="{{ route('master.show.reviews', ['id' => $master->id]) }}"
                                         class="site_color-red"
                                         title="Негативні відгуки">(-{{ $master->negative_reviews_count }})</a>
                    </span>
                            @endif
                        </div>
                        <!-- /.master-reviews -->
                        <div class="master-works">
                            В процесі виконання: <a
                                    href="{{ route('master.show.orders_in_progress', ['id' => $master->id]) }}">{{ $master->orders_in_progress_count }}</a>
                        </div>
                        <!-- /.master-works -->
                        <div class="master-message">
                            <a href="#client_master_mail" id="client_master_mail" class="btn btn-primary"
                               data-name="{{$master->user->name}}"
                               data-img="{{ $master->getAvatarSrc() }}"
                               data-id="{{ $master->id }}">Написати повідомлення</a>
                        </div>
                        <!-- /.master-message -->
                    </div>
                    <!-- /.master-description -->
                    <div class="alerts {{  $master->status->slug == 'verified' ? 'verified-alerts' : 'unchecked-alerts' }}">
                        <div class="alert-master-wrap {{  $master->status->slug == 'verified' ? 'verified-alert-master-wrap' : 'unchecked-alert-master-wrap' }}">
                            @if($master->status->slug == 'verified' )
                                <div class="alert master-check {{  $master->status->slug == 'verified' ? 'verified-master-check' : 'unchecked-master-check' }}">
                                    <div class="svg-icon-wrap">
                                        <svg class="green_mark"
                                             width="73px" height="73px">
                                            <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                                  d="M50.835,48.835 C59.036,40.634 59.036,27.342 50.835,19.140 C42.634,10.939 29.342,10.939 21.141,19.140 C12.939,27.342 12.939,40.634 21.141,48.835 C29.342,57.036 42.642,57.036 50.835,48.835 ZM28.459,30.779 L33.181,35.501 L43.525,25.165 L47.184,28.824 L36.849,39.160 L33.181,42.819 L29.522,39.160 L24.800,34.438 L28.459,30.779 Z"/>
                                        </svg>
                                    </div>
                                    <strong>Компанія перевірена!</strong>
                                    <p>Представник сервісу Джоуль особисто перевірив діяльність даної компанії та
                                        рекомендує
                                        її
                                        до
                                        співпраці.</p>
                                </div>
                        @endif
                        <!-- /.alert master-check -->
                            <div class="alert master-contacts {{  $master->status->slug == 'verified' ? 'verified-master-contacts' : 'unchecked-master-contacts' }}">
                                <svg
                                        width="56px" height="56px">
                                    <path fill-rule="evenodd" fill="rgb(240, 165, 79)"
                                          d="M27.499,37.999 C20.596,37.999 15.000,32.403 15.000,25.500 C15.000,18.596 20.596,13.000 27.499,13.000 C34.403,13.000 40.000,18.596 40.000,25.500 C40.000,32.403 34.403,37.999 27.499,37.999 ZM32.579,31.094 L32.579,20.215 L32.579,19.351 C32.579,18.873 32.192,18.486 31.715,18.486 L30.850,18.486 L26.079,18.486 L23.069,18.486 C22.114,18.486 21.340,19.260 21.340,20.215 L21.340,31.094 C21.340,32.049 22.114,32.823 23.069,32.823 L26.079,32.823 L30.850,32.823 L31.715,32.823 C32.192,32.823 32.579,32.436 32.579,31.959 L32.579,31.094 ZM34.308,31.094 L34.308,20.215 L34.308,19.351 C34.308,18.873 33.921,18.486 33.444,18.486 L33.210,18.486 C33.358,18.741 33.444,19.036 33.444,19.351 L33.444,20.215 L33.444,31.094 L33.444,31.959 C33.444,32.274 33.358,32.568 33.210,32.823 L33.444,32.823 C33.921,32.823 34.308,32.436 34.308,31.959 L34.308,31.094 ZM29.227,30.543 L26.959,30.543 L24.692,30.543 C23.608,30.543 23.976,29.551 23.976,28.326 C23.976,27.101 25.600,26.108 26.684,26.108 L26.959,26.108 L27.235,26.108 C28.318,26.108 29.943,27.101 29.943,28.326 C29.943,29.551 30.311,30.543 29.227,30.543 ZM26.959,25.833 C25.507,25.833 24.983,24.532 24.983,23.441 C24.983,22.349 25.868,21.464 26.959,21.464 C28.051,21.464 28.936,22.349 28.936,23.441 C28.936,24.532 28.555,25.833 26.959,25.833 Z"/>
                                </svg>
                                <strong>Контакти</strong>
                                @if(Auth::check())
                                    <ul class="contact-wrap">
                                        <li>Телефон {{ ($master->second_work_phone) ? '1' : ''}}: <span
                                                    class="contact-number mask-phone">{{$master->work_phone}}</span>
                                        </li>
                                        @if ($master->second_work_phone)
                                            <li>Телефон 2: <span
                                                        class="contact-number mask-phone">{{$master->second_work_phone}}</span>
                                            </li>
                                        @endif
                                        <li>E-mail: <a style="color: #5b6374"
                                                       href="mailto:{{$master->work_email}}">{{$master->work_email}}</a>
                                        </li>
                                        @if($master->website)
                                            <li>Сайт: <a style="color: #5b6374" class="cut-website" target="_blank" rel="nofollow"
                                                         href="{{$master->website}}">{{$master->website}}</a>
                                            </li>
                                        @endif
                                    </ul>
                                @else
                                    <div>Контакти доступні тільки для <a
                                                style="color: #5b6374; text-decoration: underline"
                                                href="{{ route('login') }}">авторизованих</a>
                                        користувачів
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="master-news {{  $master->status->slug == 'verified' ? 'verified-master-news' : 'unchecked-master-news' }}">
                            <h3>Новини про компанію (з Інтернету):</h3>
                            <ul>
                                @if($master->news->count() == 0)
                                    <p class="no-news">Новин про компанію поки що немає.</p>
                                @endif
                                @foreach($master->news as $news)
                                    <li><a target="_blank" href="{{ $news->url }}">{{ $news->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.master-block -->
                    <div class="master-box {{  $master->status->slug == 'verified' ? 'verified-master-box' : 'unchecked-master-box' }}">
                        <div class="master-description">
                            <p>{{ $master->about }}</p>

                            <div class="master-news master-news-visible {{  $master->status->slug == 'verified' ? 'verified-master-news' : 'unchecked-master-news' }}">
                                <h3>Новини про компанію (з Інтернету):</h3>
                                <ul>
                                    @if($master->news->count() == 0)
                                        <p class="no-news">Новин про компанію поки що немає.</p>
                                    @endif
                                    @foreach($master->news as $news)
                                        <li><a href="{{ $news->url }}">{{ $news->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        {{--<h3>Новини про компанію (з Інтернету):</h3>--}}

                    </div>
                </div>
                <!-- /.master-block-wrap -->
                @if($master->status->slug == 'verified')
                    @if($master->offices->count() > 0)
                        <div class="master-office">
                            <h2 class="master-office-location active">
                                <span>Центральний офіс: </span>{{$master->main_office->address}}
                                @if($master->main_office->room != null)
                                    , оф.{{$master->main_office->room}}
                                @endif</h2>
                            <div class="master-office-item">
                                <div class="map img-border" id="gmap-1"></div>
                                <input type="hidden" class="geo_coords_lat" id="main_office_lat"
                                       value="{{$master->main_office->lat}}">
                                <input type="hidden" class="geo_coords_lng" id="main_office_lng"
                                       value="{{$master->main_office->lng}}">
                                <!-- /.map -->
                                <div class="master-office-photo  img-border">
                                    <img src="{{ $master->main_office->getPictureSrc() }}" alt="image"/>
                                </div>
                                <!-- /.master-office-photo -->
                            </div>
                            @if($master->offices->count() > 1)
                                <h2 class="master-office-location">
                                    <span>Офіс №2: </span>{{ $master->offices->get(1)->address }}
                                    @if($master->offices->get(1)->room)
                                        , оф.{{$master->offices->get(1)->room}}
                                    @endif</h2>
                                <div class="master-office-item">
                                    <div class="map img-border" id="gmap-2"></div>
                                    <input type="hidden" class="geo_coords_lat" id="second_office_lat"
                                           value="{{ $master->offices->get(1)->lat }}">
                                    <input type="hidden" class="geo_coords_lng" id="second_office_lng"
                                           value="{{ $master->offices->get(1)->lng }}">
                                    <!-- /.map -->
                                    <div class="master-office-photo img-border">
                                        <img src="{{ $master->offices->get(1)->getPictureSrc() }}" alt="image"/>
                                    </div>
                                    <!-- /.master-office-photo -->
                                </div>
                            @endif
                            @if($master->offices->count() > 2)
                                <h2 class="master-office-location">
                                    <span>Офіс №3: </span>{{ $master->offices->get(2)->address }}
                                    @if($master->offices->get(2)->room)
                                        , оф.{{ $master->offices->get(2)->address }}
                                    @endif</h2>
                                <div class="master-office-item">
                                    <div class="map img-border" id="gmap-3"></div>
                                    <input type="hidden" class="geo_coords_lat" id="third_office_lat"
                                           value="{{ $master->offices->get(2)->lat }}">
                                    <input type="hidden" class="geo_coords_lng" id="third_office_lng"
                                           value="{{ $master->offices->get(2)->lng }}">
                                    <!-- /.map -->
                                    <div class="master-office-photo  img-border">
                                        <img src="{{ $master->offices->get(2)->getPictureSrc() }}" alt="image"/>
                                    </div>
                                    <!-- /.master-office-photo -->
                                </div>
                            @endif
                        </div>
                    @endif
                @endif
            <!-- /.master-office -->
                <div class="master-info">
                    <div class="master-info-list">
                        <ul>
                            <li>
                                @if($master->status->slug == 'verified')
                                    <svg class="green_mark"
                                         width="51px" height="51px">
                                        <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                              d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                    </svg>
                                @endif
                                <p class="info-company-build {{  $master->status->slug == 'verified' ? '' : 'custom-company-build' }}">
                                    Компанія будує: <b>
                                        @if($master->getSesTypes() == 'home_ses') домашні СЕС
                                        @elseif($master->getSesTypes() == 'commercial_ses') комерційні СЕС
                                        @elseif($master->getSesTypes() == 'both') домашні СЕС, комерційні СЕС
                                        @endif
                                    </b></p></li>
                            @if($master->status->slug == 'verified')
                                <li>
                                    <svg class="green_mark"
                                         width="51px" height="51px">
                                        <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                              d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                    </svg>
                                    <div>Реєстраційні документи:
                                        @if($master->ipn != null && $master->edrpou != null)
                                            <div class="on_hover document-fop">
                                                ФОП,
                                            </div>
                                            <div class="on_hover document-edrpou">
                                                <a class="show-registry-document">
                                                    юр. особа
                                                </a>
                                                <div class="visible-registry-document document-edrpou">
                                                    <p>Код ЄДРПОУ юридичної особи:
                                                        <a target="_blank" class="link-edrpou"
                                                           href="https://usrinfo.minjust.gov.ua/edr.html">
                                                            {{ $master->edrpou }}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        @elseif($master->ipn != null)
                                            <div class="on_hover">
                                                ФОП
                                            </div>
                                        @elseif($master->edrpou != null)
                                            <div class="on_hover">
                                                <a href="https://usrinfo.minjust.gov.ua/edr.html" target="_blank"
                                                   class="show-registry-document document-edrpou">
                                                    юр. особа
                                                </a>
                                                <div class="visible-registry-document document-edrpou">
                                                    <p>Код ЄДРПОУ юридичної особи:
                                                        <a target="_blank" class="link-edrpou"
                                                           href="https://usrinfo.minjust.gov.ua/edr.html">
                                                            {{ $master->edrpou }}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                                <li>
                                    <svg class="green_mark"
                                         width="51px" height="51px">
                                        <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                              d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                    </svg>
                                    <p>Встановлює сонячні системи починаючи з <b>{{$master->year_started}} р.</b></p>
                                </li>
                                @if($master->car == true)
                                    <li>
                                        <svg class="green_mark"
                                             width="51px" height="51px">
                                            <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                                  d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                        </svg>
                                        <p>Наявний <a href="#show-photo-car" class="show-photo show-photo-car"> власний
                                                транспорт для сервісного
                                                обслуговування <img class="img-border visible-photo photo-car"
                                                                    src="{{$master->getCarPhotoSrc()}}"
                                                                    alt="image"/></a>
                                        </p>
                                    </li>
                                @endif
                                @if($master->project_department == true)
                                    <li>
                                        <svg class="green_mark"
                                             width="51px" height="51px">
                                            <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                                  d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                        </svg>
                                        <p>Наявний власний проектний відділ</p>
                                    </li>
                                @endif
                                @if($master->construction_machinery == true)
                                    <li>
                                        <svg class="green_mark"
                                             width="51px" height="51px">
                                            <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                                  d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                        </svg>
                                        <p>Наявна <a href="#show-photo-machinery"
                                                     class=" show-photo show-photo-machinery">власна
                                                будівельна техніка <img class="img-border visible-photo photo-machinery"
                                                                        src="{{$master->getMachineryPhotoSrc()}}"
                                                                        alt="image"/></a>
                                        </p>
                                    </li>
                                @endif
                                @if($master->eco_energy == true)
                                    <li>
                                        <svg class="green_mark"
                                             width="51px" height="51px">
                                            <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                                  d="M32.038,30.038 C35.937,26.138 35.937,19.819 32.038,15.920 C28.139,12.020 21.819,12.020 17.920,15.920 C14.020,19.819 14.020,26.138 17.920,30.038 C21.819,33.937 28.143,33.937 32.038,30.038 ZM21.399,21.453 L23.644,23.698 L28.562,18.784 L30.302,20.524 L25.388,25.438 L23.644,27.177 L21.905,25.438 L19.660,23.193 L21.399,21.453 Z"/>
                                        </svg>
                                        <p>Акредитований учасник крединтої програми "Еко-енергія" від Укргазбанку</p>
                                    </li>
                                @endif
                            @endif
                        </ul>
                        <div class="alert alert-info">
                            <h3>Компанія працює з обладнанням таких брендів:</h3>
                            <strong>Інвертори:</strong>
                            @foreach($master->invertors as $invertor)
                                <a style="color: #4b5160;" href="{{ route('brand.show', ['equipment_type' =>
                        array_search($invertor->type->slug, $equipment_slug_mapping), 'brand_slug' => $invertor->slug]) }}">
                                    {{ $invertor->title }}@if(!$loop->last),@endif
                                </a>
                            @endforeach
                            <strong>Сонячні панелі:</strong>
                            @foreach($master->panels as $panel)
                                <a style="color: #4b5160;" href="{{ route('brand.show', ['equipment_type' =>
                        array_search($panel->type->slug, $equipment_slug_mapping), 'brand_slug' => $panel->slug]) }}">
                                    {{ $panel->title }}@if(!$loop->last),@endif
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.alert -->

                    <!-- /.master-info-list -->
                    <div class="master-info-map">
                        <h3>Компанія надає послуги у наступних областях:</h3>
                        {{--<div class="view-location-map">--}}
                        {{--<div class="gmap-block" style="display: block; height: 300px;">--}}
                        {{--<div id="gmap-4" class="map"></div>--}}
                        {{--</div>--}}
                        @component('layouts.master.profile.regions_map')
                        @endcomponent
                        {{--</div>--}}

                    </div>
                    <!-- /.master-info-map -->
                </div>
                <!-- /.master-info -->
                <div class="master-build">
                    <h3>Компанія збудувала станції у наступних регіонах:</h3>
                    <div class="master-build-item">
                        <div class="build-commercial">
                            <svg width="19px" height="26px">
                                <path fill-rule="evenodd" fill="rgb(166, 192, 20)"
                                      d="M9.012,0.822 C4.043,0.822 -0.000,4.925 -0.000,9.969 C-0.000,11.284 0.265,12.540 0.788,13.702 C3.041,18.706 7.362,23.989 8.632,25.489 C8.728,25.601 8.866,25.665 9.012,25.665 C9.159,25.665 9.297,25.601 9.393,25.489 C10.663,23.989 14.983,18.707 17.237,13.702 C17.760,12.540 18.025,11.284 18.025,9.969 C18.025,4.925 13.982,0.822 9.012,0.822 ZM9.012,14.719 C6.431,14.719 4.331,12.588 4.331,9.968 C4.331,7.349 6.431,5.217 9.012,5.217 C11.594,5.217 13.694,7.349 13.694,9.968 C13.694,12.588 11.594,14.719 9.012,14.719 Z"/>
                            </svg>
                            Комерційні СЕС
                        </div>
                        <!-- /.commercial -->
                        <div class="build-home">
                            <svg width="19px" height="27px">
                                <path fill-rule="evenodd" fill="rgb(255, 144, 0)"
                                      d="M9.256,0.911 C4.152,0.911 -0.000,5.110 -0.000,10.270 C-0.000,11.616 0.272,12.901 0.809,14.090 C3.124,19.211 7.561,24.616 8.866,26.151 C8.964,26.266 9.106,26.332 9.256,26.332 C9.406,26.332 9.549,26.266 9.646,26.151 C10.951,24.617 15.388,19.211 17.703,14.090 C18.240,12.901 18.512,11.616 18.512,10.270 C18.512,5.110 14.360,0.911 9.256,0.911 ZM9.256,15.131 C6.605,15.131 4.448,12.950 4.448,10.270 C4.448,7.590 6.605,5.408 9.256,5.408 C11.907,5.408 14.064,7.590 14.064,10.270 C14.064,12.950 11.907,15.131 9.256,15.131 Z"/>
                            </svg>
                            Домашні СЕС
                        </div>
                    </div>

                    <!-- /.commercial -->
                    <div class="build-map img-border" style="max-width: 100%">
                        <div id="gmap-5" class="map"></div>
                    </div>
                    <!-- /.build-map -->
                </div>
                <!-- /.master-build -->
                @if($master->portfolios->count() > 0 )
                    <div class="master-portfolio">
                        <h3>Портфоліо:</h3>
                        <div class="slider-portfolio">
                            <div class="swiper-wrapper">
                                @foreach( $master->portfolios as $portfolio )
                                    @foreach( $portfolio->attachments as $photo )
                                        @if($photo->public == 1)
                                            <div class="slide-portfolio swiper-slide">
                                                <a class="test-popup-link" href="{{ $photo->getPath() }}">
                                                    <img src="{{ $photo->getPath(true) }}" alt="image">
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                            <!-- /.slide-portfolio swiper-slide -->
                        </div>
                        <div class="slider-arrow slider-arrow-left">
                            <svg
                                    width="8px" height="17px">
                                <image x="0px" y="0px" width="8px" height="17px"
                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAARCAQAAABK3yVTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfhChQONA+lDHmCAAAAfUlEQVQY03WQwQnCUBBEnxBSQYrIwWsqSFfeAtaQiwXYgl7Sg5AegpBbQBQCz0P8/K8xu6eBYebtIsnWPlJZ+rSJsnCylSBzBy8iOwGgB/bAx9E5mC9exLOTRcjCoy/L2IWzh5Tlj2OVgXiNLRscCen3LSfJCDNScePO7z/eoCKMeV+V/foAAAAASUVORK5CYII="/>
                            </svg>
                        </div>
                        <!-- /.slider-arrow slider-arrow-left -->
                        <div class="slider-arrow slider-arrow-right">
                            <svg
                                    width="8px" height="17px">
                                <image x="0px" y="0px" width="8px" height="17px"
                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAARCAQAAABK3yVTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfhChQONguQV98ZAAAAc0lEQVQY032QsQ2DQBAEh+QroAgCd0FXzixRAwkF0IJJXIV7+AJesoyENAQIcRCgzUa7p93Dn60cwpd/mwiwt1hHgG+zaQOVAPAFHgB7Npn9HBHE2uIYATbOdhHg0+XGcbmRzE43PUJTxOG8pXM+r738YwVfuZM3Kj0DkgAAAABJRU5ErkJggg=="/>
                            </svg>
                        </div>
                        <!-- /.slider-arrow slider-arrow-left -->
                        <!-- /.swiper-wrapper -->
                    </div>
                @endif
            <!-- /.slider-portfolio -->

                <!-- /.master-portfolio -->
                @if(count($master->orders_in_progress) > 0)
                    <div class="master-order" id="orders_in_progress">
                        <h3>Замовлення у процесі виконання:</h3>
                        @foreach($master->orders_in_progress as $order)
                            <div class="master-order-item">
                                <div class="order-item item-object">
                                    {{ $order->title }}
                                </div>
                                <!-- /.order-item-object -->
                                <div class="order-item item-place">
                                    {{ $order->region ? $order->region->name : '' }}
                                </div>
                                <!-- /.order-item-place -->
                                <div class="order-item item-cost">
                                    Вартість виконання: <span class="price-project">
                        {{ $order->price }}$</span>
                                </div>
                                <!-- /.oreder-item-cost -->
                            </div>
                    @endforeach
                    <!-- /.master-order-item -->
                    </div>
                @endif
            <!-- /.master-order -->
                <a name="reviews"></a>
                <div class="master-reviews">
                    <h2 style="text-align: left">Відгуки:</h2>
                    @if(count($master->reviews) == 0)
                        Відгуків поки що нема.
                    @endif
                    @foreach($master->reviews as $review)
                        @if($review->positive)
                            <div class="master-review-item master-review-positive">
                                <div class="review-item item-name">
                                    <p>
                                        <span class="assessment positive-rewiew">+</span>{{ $review->client->user->name }}
                                    </p>
                                    <span class="date-review">{{ $review->created_at }}</span>
                                </div>
                                <!-- /.review-item item-name -->
                                <div class="review-item item-text">
                                    <p style="white-space: pre-wrap">{{ $review->text }}</p>
                                </div>
                                <!-- /.review-item item-name -->
                                <div class="review-item item-project">
                                    <p>Проект:</p>
                                    <a href="{{ roleRoute('order.show', ['id' => $review->order->id]) }}">{{ $review->order->title }}</a>
                                    <span class="price-project">
                    {{ round($review->order->price) }}$
                </span>
                                    <!-- /.price -->
                                </div>
                                <!-- /.review-item item-name -->
                            </div>
                    @else
                        <!-- /.master-review-item -->
                            <div class="master-review-item master-review-negative">
                                <div class="review-item item-name">
                                    <p>
                                        <span class="assessment negative-rewiew">-</span>{{ $review->client->user->name }}
                                    </p>
                                    <span class="date-review">{{ $review->created_at }}</span>
                                </div>
                                <!-- /.review-item item-name -->
                                <div class="review-item item-text">
                                    <p>{{ $review->text }}</p>
                                </div>
                                <!-- /.review-item item-name -->
                                <div class="review-item item-project">
                                    <p>Проект:</p>
                                    <a href="{{ roleRoute('order.show', ['id' => $review->order->id]) }}">{{ $review->order->title }}</a>
                                    <span class="price-project">
                    {{ round($review->order->price) }}$
                </span>
                                    <!-- /.price -->
                                </div>
                                <!-- /.review-item item-name -->
                            </div>
                    @endif
                @endforeach

                <!-- /.master-review-item -->
                </div>
                <!-- /.master-reviews -->
                {{--<div id="hellopreloader">--}}
                {{--<div id="hellopreloader_preload">--}}
                {{--<p class="preloader">Зачекайте, іде завантаження</p>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

@section('site-title')
    {{ $master->user->name }}: відгуки, фото виконаних об'єктів, контакти
@endsection

@push('modals')
<div class="remodal modal-client-mail modal-client-master" data-remodal-id="client_master_mail">
    <div class="modal_header">
        <p class="write-letter">Написати повідомлення</p>
    </div>
    <div class="modal_body">
        <div class="error_block" style="color: red; font-size: 14px">
            <ul></ul>
        </div>
        <input type="hidden" name="master_id" value="{{ $master->id }}" class="company-id">
        <div class="form_block">
            <label for="">Кому:</label>
            <div class="user_block">
                <div class="company-img user_img">
                    <img src="{{ $master->getAvatarSrc() }}" alt="{{ $master->name }}">
                </div>
                <div class="company-name-location">
                    <div class="company-name-wrap">
                        <p class="company-name name">{{$master->user->name}}</p>
                    </div>
                    <div class="company-location-wrap">
                        <p class="company-location location site_icon-location">{{ explode(',', $master->main_office->address)[0] }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <label for="">Повідомлення:</label>
            <textarea name="message" id="message-to-master-text"></textarea>
        </div>
        @if(!Auth::check())
            <h3 class="text-center">
                Ваші дані
            </h3>
            <div class="form_block">
                <label for="">Ім'я:</label>
                <input type="text" name="user_name">
            </div>
            <div class="form_block">
                <label for="">Телефон:</label>
                <input type="tel" data-validation-error-msg="Правильно заповніть це поле"
                       data-validation="number" data-validation-ignore="(,),-, ,+"
                       maxlength="19" name="user_phone" class="mask-phone">
            </div>
            <div class="form_block">
                <label for="">E-mail:</label>
                <input type="email" name="user_email">
            </div>
            {!! app('captcha')->display($attributes = [], $lang = null) !!}
            <input type="hidden" name="g-recaptcha-response" id="">

            {{--            <div class="g-recaptcha" data-sitekey="{{ env('NOCAPTCHA_SITEKEY') }}"></div>--}}

            <div class="btn_block ">
                <button class='btn btn-secondary step_back' data-remodal-action="cancel">Скасувати</button>
                <button id="guestSendMaster" class='btn btn-primary'>Надіслати</button>
            </div>
        @else
            <div class="btn_block">
                <button class='btn btn-secondary step_back' data-remodal-action="cancel">Скасувати</button>
                <button id="sendMaster" class='btn btn-primary'>Надіслати</button>
            </div>
        @endif
    </div>
</div>

<div class="remodal modal-client-mail" data-remodal-id="confirm-mail-to-master">
    <div class="modal_header">
        <p>Ваш лист надіслано</p>
    </div>
</div>

<div class="remodal modal-client-mail" data-remodal-id="fail-mail-to-master">
    <div class="modal_header">
        <p>Виникла помилка. Спробуйте надіслати лист пізніше.</p>
    </div>
</div>

<div class="remodal remodal-photo-car" data-remodal-id="show-photo-car">
    <img class="photo-car" src="{{$master->getCarPhotoSrc()}}" alt="image"/>
</div>

<div class="remodal remodal-photo-machinery" data-remodal-id="show-photo-machinery">
    <img src="{{$master->getMachineryPhotoSrc()}}" alt="image"/>
</div>
@endpush

@push('footer_scripts')

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
        defer></script>
<script src="{{ asset('js/swiper/swiper.jquery.min.js') }}"></script>
<script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
<script src="{{ asset('js/master/show.js') }}"></script>
<script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js') }}"></script>
<script src="{{ asset('js/jquery.collapsible.js') }}"></script>
<script src="{{ asset('js/swiper-slider.js') }}"></script>
@if(Auth::check())
    <script type="text/javascript">
        var url = '{{ route('client.master.email.send') }}';
    </script>
@else
    <script type="text/javascript">
        var url = '{{ route('guest.master.email.send') }}';
    </script>
@endif
<script>
    var master = {!! $master !!},
        masterStatus = {!! $master->status !!},
        masterOffices = {!! $master->offices !!},
        markerSrc = '{{ asset('images/map-marker.png') }}',// TODO: use imgDriectoryPath var
        commercialMarkerSrc = '{{ asset('images/map-marker-green.png') }}',
        homeMarkerSrc = '{{ asset('images/map-marker-orange.png') }}',
        imgDirectoryPath = '{{ asset('images/') }}',
        portfolios = {!! $master->portfolios !!};
</script>
<script src="{{ asset('js/magnific-popup-slider/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/magnific-popup-slider/magnific-popup-slider.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>--}}
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/client-register/phoneMask.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('js/swiper/swiper.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/lightbox/css/lightbox.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/magnific-popup-slider/magnific-popup.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/master-show.css') }}"/>
{{--<link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>--}}
@endpush