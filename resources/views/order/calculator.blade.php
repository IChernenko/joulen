@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="calc-form validate_form">
            <h1>Для чого вам потрібна автономна СЕС?</h1>
            <div class="styled-radio">
                <input type="radio" name="radio" id="radio1" checked>
                <label for="radio1">Для дачі. Буде використовуватись переважно влітку</label>
            </div>
            <!-- /.styled-radio -->
            <div class="styled-radio">
                <input type="radio" name="radio" id="radio2">
                <label for="radio2">Для щоденного використання. Навіть взимку</label>
            </div>
            <span id="msred" style="color:red; display:none;">Обов'язково потрібно вказати для чого вам потрібна автономна СЕС.</span>
            <!-- /.styled-radio -->
            <div class="autonomy-input">
                <label for="autonomy">Автономність, днів
                    <a class="tooltip-btn">
                        ?
                        <span class="tooltip">
                      Вкажіть скільки днів повинні працювати акумулятори в разі повної відсутності сонячної енергії
                    </span>
                        <!-- /.tooltip -->
                    </a>
                </label>
                <input type="text" id="dd" class="validate_number_3" value="1" >
            </div>
            <!-- /.autonomus-input -->
            <h2>
                Якими приладами, та як часто ви користуєтесь?
            </h2>
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc1" class="check-calc">
                    <label for="calc1">Лампа розжарювання</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal1" value="100">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal2" value="5">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal3" value="5">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc2" class="check-calc">
                    <label for="calc2">Лампа люмінісцентна (економна)</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal4" value="25" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal5" value="5" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal6" value="5" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc3" class="check-calc">
                    <label for="calc3">Лампа LED</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal7" value="10" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal8" value="5" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal9" value="5" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc4" class="check-calc">
                    <label for="calc4">Ноутбук</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal10" value="50" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal11" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal12" value="4" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc5" class="check-calc">
                    <label for="calc5">Настільний ПК</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal13" value="400" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal14" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal15" value="4" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc6" class="check-calc">
                    <label for="calc6">Холодильник</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal16" value="200" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal17" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal18" value="6" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc7" class="check-calc">
                    <label for="calc7">Телевізор</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal19" value="150" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal20" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal21" value="6" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc8" class="check-calc">
                    <label for="calc8">Електрочайник</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal22" value="2000" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal23" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal24" value="0.5" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc9" class="check-calc">
                    <label for="calc9">Мікрохвильовка</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal25" value="1000" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal26" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal27" value="0.5" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc10" class="check-calc">
                    <label for="calc10">Електробойлер</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal28" value="2000" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal29" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal30" value="2" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc11" class="check-calc">
                    <label for="calc11">Водяний насос</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal31" value="1000" >
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal32" value="1" >
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal33" value="0.5" >
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc12" class="check-calc">
                    <label for="calc12">Кондиціонер</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal34" value="1000">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal35" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal36" value="4">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc13" class="check-calc">
                    <label for="calc13">Електрообігрівач</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal37" value="2000">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal38" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal39" value="6">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc14" class="check-calc">
                    <label for="calc14">Електроплита</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal40" value="2000">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal41" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal42" value="1">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc15" class="check-calc">
                    <label for="calc15">Порохотяг</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal43" value="1800">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal44" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal45" value="2">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc16" class="check-calc">
                    <label for="calc16">Пральна машина</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal46" value="1200">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal47" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal48" value="5">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc17" class="check-calc">
                    <label for="calc17">Праска</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal49" value="1500">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal50" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal51" value="2">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>

            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc18" class="check-calc">
                    <label for="calc18">Фен</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal52" value="1500">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal53" value="1">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal54" value="1">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <!-- /.calc-block -->
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc19" class="check-calc">
                    <label for="calc19">Інше</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power  only-number" type="text" name="calc" id="cal55">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal56">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal57">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc20" class="check-calc">
                    <label for="calc20">Інше</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal58">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal59">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal60">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <div class="calc-block calcRow">
                <div class="styled-checkbox">
                    <input type="checkbox" id="calc21" class="check-calc">
                    <label for="calc21">Інше</label>
                </div>
                <!-- /.styled-checkbox -->
                <div class="calc-field-wrap disabled calcData">
                    <label class="calc-field multiply">
                        <input class="calc-elem calc_wt validate_power only-number" type="text" name="calc" id="cal61">
                        <span>Вт</span>
                    </label>

                    <label class="calc-field multiply">
                        <input class="calc-elem calc_days validate_number_3 only-number" type="text" name="calc" id="cal62">
                        <span>Шт</span>
                    </label>

                    <label class="calc-field">
                        <input class="calc-elem calc_hours validate_hours only-number" type="text" name="calc" id="cal63">
                        <span>годин за</span>
                    </label>
                    <label class="calc-field style-select">
                        <select class="calc-elem">
                            <option>Добу</option>
                            <option>Тиждень</option>
                        </select>
                    </label>
                    <!-- /.style-select -->

                    <!-- /# -->
                    <div class="calc-result">
                        = <span class="calc_result">0</span> кВт·год/добу
                    </div>
                    <!-- /.calc-result -->
                </div>
                <!-- /.calc-field-wrap -->

            </div>
            <div class="total-result">
                Середнє споживання: <span id="powerCalcResult">0</span> кВт·год/добу
            </div>
            <!-- /.total-result -->
            <button type="submit" id="calculate" class="btn btn-default">Розрахувати</button>
            <button onClick="history.go(-1);" class="btn btn-primary">Скасувати</button>
        </form>
        <!-- /.calc-form -->
    </div>

    @push('footer_scripts')
        <script type="text/javascript" src="{{ asset('js/jquery-validation/jquery.validate.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-validation/localization/messages_uk.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/order/calculator.js') }}"></script>
    @endpush
@endsection

@section('site-title')
    Розрахунок потужності для автономної СЕС
@endsection