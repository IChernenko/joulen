@extends('order.create.ses')

@section('ses-title', 'Оцінка вартості будівництва Комерційної СЕС')

@section('ses-img-class', 'cost_header-commercial')

@section('ses-header-title', 'Комерційна СЕС')

@section('ses-header-description', 'Потужна СЕС (> 30 кВт) для продажу електроенергії у мережу по “зеленому” тарифу.')

@section('ses-legal-checkbox')
    <div>
        <input name="price_types[3]" type="checkbox" id="c3" data-switchBlock=".data-doc" class="checkbox-special" value="3" />
        <label for="c3">Документальний супровід (Підключення та оформлення “зеленого” тарифу)</label>
    </div>
@endsection

@section('ses-power-block')
    <div class="clr">
        <div class="label">Яка потужність фотомодулів вас цікавить?
            <div class="field error-block">
                <input type="number" min="1" value="" id="powerf" class="power-value commercial-photosell only-number validate_ses_network validate_number" name="photocell_power"/> <strong><b>кВт</b></strong></div>
        </div>
            <div class="afterField">
                Для цього необхідна площа ≈
                <strong><b><span><span class="squad-info">0</span> Га</span></b></strong>
                <input type="hidden" class="input-squad" name="required_area">
            </div>
    </div>
@endsection

@section('ses-delivery-appointment')
    <div id="ses-delivery-appointment"  class="montageBlock"></div>
@endsection

@section('ses-where-title', 'Де знаходиться ділянка')

@section('ses-house-info-title', 'Інформація про ділянку')

@section('ses-area-title', 'Площа ділянки')

@section('ses-area-unit')
    <select name="area_unit">
        <option value="m" selected>м.кв</option>
        <option value="ha">Гектари</option>
    </select>
@endsection

@section('ses-photo-title', 'Прикріпіть фото/план ділянки')

@section('ses-other-info', 'Інша інформація про ділянку')

@section('ses-legal-block')
    <div class="contentBlock bordered data-doc" id="block-jur">
        <div class="container">
            <h2>Документальний супровід</h2>
            <div>
                <div>
                    <input type="checkbox" class="styledChk" id="c150" name="green_tariff" value="1"/>
                    <label for="c3">Потрібна допомога у підключенні до енергомережі та оформленні “зеленого” тарифу.</label>
                </div>
            </div>
            <div id="legal_dynamic_blocks" class="montageBlock"></div>
            <div id="location_legal" style="display: none" class="location-block">
                <h3 class="site_icon-location">Де знаходиться об'єкт?</h3>
                <div>
                    <div class="label">Область</div>
                    <div class="field">
                        <select id="selectregion" name="region_id" class="validate_required">
                            <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}">{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="label">Населений пункт</div>
                    <div class="field">
                        <input id="city" type="text" class="validate_required only-letter" value="" name="city"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ses-subcategory-hidden')
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['commercial']->id }}">
@endsection

@section('site-title')
    Комерційні сонячні електростанції в Україні - дізнатися вартість
@endsection