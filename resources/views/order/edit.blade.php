@extends('layouts.old.app')

@section('content')
    <script type="text/javascript">
        var waitingPath = '{{ asset("js/fine-uploader/placeholders/waiting-generic.png") }}';
        var notAvailablePath = '{{ asset("js/fine-uploader/placeholders/not_available-generic.png") }}';
        var userAttachmentHandler = '{{ roleRoute("attachment.upload") }}';
        var userUploadComplete = '{{ roleRoute("attachment.upload.complete") }}';
        var delete_image = '{{ roleRoute("order.photo.delete") }}';
    </script>
    <div id="content" class="costEstimationWrapper search_auditor">

        <form action="{{ roleRoute('order.update', ['id' => $order->id]) }}" class="validate_form" id="update-order-form" method="POST">

            {{ csrf_field() }}
            {{ method_field('PUT') }}

            @yield('category-content')
            <div style="display:none;" class="contentBlock" id="comm">
                <div class="container">
                    <div class="showHideBlock">
                        <div class="showHideLabel">Додати коментар до замовлення</div>
                        <div class="showHideContent">
                            <textarea id="coments" name="client_comment">{{ $order->client_comment }}</textarea>
                        </div>
                    </div>

                    <div>
                        <div>
                            <input {{ $order->show_phone ? 'checked' : '' }} type="checkbox" class="styledChk" id="show_phone" name="show_phone" value="1"/>
                            <label for="show_phone">показувати номер телефону перевіреним спеціалістам</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container controlBtn textCenter">
                <a href="{{ roleRoute('order.show', ['id' => $order->id]) }}" class="site_button-orange">Назад</a>
                <input type="submit" class="site_button-orange confirm-btn disabled" value="Зберегти">
                @yield('after-submit')
            </div>

        </form>

        @yield('after-order-form')

    </div>
    {{--end create body--}}

    @include('banners.order')

    @push('footer_scripts')
        <script type="text/javascript" src="{{ asset('js/jquery.nice-select.js')}}"></script>
    {{--TODO: move it to file--}}
        <script type="text/javascript">$(document).ready(function(){$('select').niceSelect();});</script>
        <script type="text/javascript" src="{{ asset('js/jquery-validation/jquery.validate.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-validation/localization/messages_uk.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/order/validationFormRules.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/order/edit/edit.js')}}"></script>
    @endpush

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    @endpush

@endsection

@section('site-title')
    Редагувати заявку - {{ $order->title }}
@endsection