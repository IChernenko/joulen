@extends('order.edit')

@section('category-content')
    <div class="container pageTitle withBlock">
        <h1>Знайти енергоаудитора</h1>
    </div>

    <div class="contentBlock">
        <div class="container">
            {{--TODO: yield subcaterory class--}}
            <div class="cost_header @yield('energy-audit-img-class')">
                <h4>@yield('energy-audit-header-title')</h4>
                <p>Знайти спеціаліста, який допоможе вам зменшити витрати на комунальні послуги</p>
            </div>
        </div>
    </div>
    <!--# end contentBlock #-->

    @component('layouts.validation_errors')
    @endcomponent

    <!--# contentBlock #-->
    <div class="contentBlock bordered overflow_fix">
        <div class="container">
            <h2>@yield('energy-audit-about')</h2>
            <div>
                <div class="input_block">
                    <div class="label">Загальна площа, кв.м.</div>
                    <div class="field small_field">
                        <input type="number" min="1" value="{{ $order->orderable->area }}" class="validate_required" name="area">
                    </div>
                </div>
                <div class="input_block">
                    <div class="label">Кількість поверхів</div>
                    <div class="field small_field">
                        <input type="number" min="1" value="{{ $order->orderable->floors }}" class="validate_required" name="floors">
                    </div>
                </div>
                <div class="input_block">
                    <div class="label">@yield('energy-audit-explotation')</div>
                    <select class="custom_select exploitation " name="exploitation">
                        <option data-display="Виберіть зі списку" selected disabled>Виберіть зі списку</option>
                        <option value="1" {{ $order->orderable->exploitation == 1 ? 'selected' : '' }}>@yield('energy-audit-explotation-true')</option>
                        <option value="0" {{ $order->orderable->exploitation == 0 ? 'selected' : '' }}>@yield('energy-audit-explotation-false')</option>
                    </select>
                </div>
                <div class="house_content">



                    <div>
                        <div class="label">@yield('energy-audit-photo') <span class="textNormal">(Максимум 5 фото)</span></div>

                        <div id="fine-uploader-manual-trigger"></div>
                        <div style="display: none" class="hidden-order-photos">
                            @foreach( $order->attachments as $attachment)
                                <div class="hidden-order-photo" data-src="{{$attachment->getPath()}}" data-attachment_id="{{ $attachment->id }}"></div>
                            @endforeach
                        </div>
                    </div>

                    <div class="house_repeat">
                        @yield('energy-audit-residents')
                        <div class="input_block">
                            <div class="label">Споживання електроенергії за рік, квт-год.</div>
                            <div class="field">
                                <input type="number" min="0"  value="{{ $order->orderable->electricity_per_year }}" class="validate_required" name="electricity_per_year">
                            </div>
                        </div>
                        <div class="input_block">
                            <div class="label">споживання води за рік, куб.м.</div>
                            <div class="field">
                                <input type="number" min="0"  value="{{ $order->orderable->water_per_year }}" class="validate_required" name="water_per_year">
                            </div>
                        </div>
                        <div class="input_block">
                            <div class="label">споживання газу за рік, куб.м.</div>
                            <div class="field">
                                <input type="number" min="0"  value="{{ $order->orderable->gas_per_year }}" class="validate_required" name="gas_per_year">
                            </div>
                        </div>
                        <div class="input_block">
                            <div class="label">Тип опалення</div>
                            <select class="custom_select hidden_validate" id="heating_type" name="heating_type">
                                {{--TODO: move this to viewComposer--}}
                                @foreach(\App\Models\EnergyAuditHeatingType::all() as $heating_type)
                                    <option value="{{ $heating_type->id }}" {{ $order->orderable->energy_audit_heating_type->id == $heating_type->id ? 'selected' : '' }}>{{ $heating_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <h3 class="site_icon-location">@yield('energy-audit-where-title')</h3>
                    <div class="input_block">
                        <div class="label">Область</div>
                        <select class="custom_select hidden_validate" id="region" name="region_id">
                            <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ $order->region_id == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input_block">
                        <div class="label">Населений пункт</div>
                        <div class="field">
                            <input type="text" value="{{ $order->city }}" class="validate_required" name="city">
                        </div>
                    </div>
                    <div class="input_block">
                        <div class="label">@yield('energy-audit-other-info')</div>
                        <div class="field">
                            <textarea class="" name="house_comment">{{ $order->orderable->house_comment }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--# end contentBlock #-->

    <!--# contentBlock #-->
    <div class="contentBlock bordered block-montage house_repeat">
        <div class="container">
            <h2>Які проблеми вас турбують?</h2>
            <div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="high_costs" id="c5" {{ $order->orderable->high_costs ? 'checked' : '' }}>
                    <label for="c5">високі витрати на енергоносії</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="cold_winter" id="c6" {{ $order->orderable->cold_winter ? 'checked' : '' }}>
                    <label for="c6">холодно взимку</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="hot_summer" id="c7" {{ $order->orderable->hot_summer ? 'checked' : '' }}>
                    <label for="c7">спекотно влітку</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="mold" id="c8" {{ $order->orderable->mold ? 'checked' : '' }}>
                    <label for="c8">у приміщенні пліснява/грибки</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="draft" id="c9" {{ $order->orderable->draft ? 'checked' : '' }}>
                    <label for="c9">протяги</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="windows_fogging" id="c10" {{ $order->orderable->windows_fogging ? 'checked' : '' }}>
                    <label for="c10">«потіють» вікна</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="disbalance" id="c11" {{ $order->orderable->disbalance ? 'checked' : '' }}>
                    <label for="c11">невідбалансована система опалення (перегрів/недогрів деяких приміщень)</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="blackout" id="c12"{{ $order->orderable->blackout ? 'checked' : '' }} >
                    <label for="c12">часто «вибиває» пробки</label>
                </div>
                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="provider_problems" id="c13" {{ $order->orderable->provider_problems ? 'checked' : '' }}>
                    <label for="c13">проблеми з енергопостачальниками</label>
                </div>
                <div class="input_block">
                    <div class="label">Iнша проблема:</div>
                    <div class="field">
                        <textarea class="" name="other_problems">{{ $order->orderable->other_problems }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--# end contentBlock #-->

    <!--# contentBlock #-->
    <div class="contentBlock bordered house_content" >
        <div class="container">
            <h2>Необхідно провести такі заходи:</h2>
            <div>
                <div class="checkbox_block house_project">
                    <input type="checkbox" value="1" class="styledChk" name="thermal_examination" id="c20" {{ $order->orderable->thermal_examination ? 'checked' : '' }}>
                    <label for="c20">тепловізійне обстеження (рекомендується) <a href="#" class="open_tooltip">
                                            <span class="tooltip_info">
                                     <p>Тепловізійне обстеження надає можливість виявити:</p>
                                        <ol>
                                            <li>приховані дефекти теплоізоляції або недоліки конструкції (неякісний монтаж віконних блоків, дефекти теплоізоляції стиків між панелями, містки холоду, якість перекриття та покрівель);</li>
                                            <li>місця можливого утворення конденсату на стінах та ділянки з підвищеним вмістом вологи;</li>
                                            <li>місця поривів тепломагістралей, або порушення їх ізоляції;</li>
                                            <li>недоліки опалювальних систем, засміченість батарей;</li>
                                            <li>місця протікання в покрівлі;</li>
                                            <li>місця прокладки труб або електричних нагрівачів в теплих підлогах;</li>
                                            <li>ділянки з теплопровідними включеннями;</li>
                                            <li>порушення теплоізоляційних властивостей огороджувальних конструкцій та тепломереж</li>
                                            <li>місця перегріву контактних з'єднань електрообладнання та діагностувати стан електричних мереж, розеток, подовжувачів, вимикачів тощо.</li>
                                        </ol>
                                    </span></a></label>
                </div>
                <div class="checkbox_block house_project">
                    <input type="checkbox" value="1" class="styledChk" name="blower_door_test" id="c21" {{ $order->orderable->blower_door_test ? 'checked' : '' }}>
                    <label for="c21">проведення тесту на герметичність (Blower Door Test) <a rel="nofollow" target="_blank" href="http://ecotown.com.ua/slovnyk/test-na-hermetychnist-aerodveri-blower-door-test/" class="without_question_mark">що це таке?</a></label>
                </div>

                <div class="checkbox_block">
                    <input type="checkbox" value="1" class="styledChk" name="detailed_calculation" id="c23" {{ $order->orderable->detailed_calculation ? 'checked' : '' }}>
                    <label for="c23">провести детальний розрахунок: які енергоефективні заходи потрібно впровадити та через скільки вони окупляться</label>
                </div>

                <div class="checkbox_block house_project">
                    <input type="checkbox" value="1" class="styledChk" name="technical_supervision" id="c25" {{ $order->orderable->technical_supervision ? 'checked' : '' }}>
                    <label for="c25">Технічний нагляд під час утеплення/реконструкції<a href="#" class="open_tooltip">
                                        <span class="tooltip_info">
                                            <p>Будівельники часто не дотримуються будівельних норм або використовують неякісні матеріали. Через це впровадження енергоефективних заходів може не дати очікуваних результатів.
Незалежний енергоаудитор може проконтролювати щоб всі будівельні роботи були виконані на належному рівні.</p></span></a></label>
                </div>
            </div>
        </div>
    </div>
    <!--# end contentBlock #-->

    <input type="hidden" name="category" value="{{ 'energy_audit' }}">

    @yield('energy-audit-subcategory-hidden')

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('js/fine-uploader/fine-uploader-new.css') }}"/>
    @endpush
    @push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/fine-uploader/jquery.fine-uploader.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/order/create/energy_audit.js')}}"></script>
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="upload-photo-block qq-uploader-selector qq-uploader">

            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>

            <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Процес завантаження файлів...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
            <div class="uploaded_photos qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <div class="uploaded-photo">
                    <img class="qq-thumbnail-selector" qq-max-size="400" qq-server-scale>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel remove-uploaded-photo site_icon-delete">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <div class="status-info">
                        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                        <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Повторити</button>
                    </div>
                </div>
            </div>
            <div class="buttons upload_photo_buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div class="upload_photo"><span>+</span> ДОДАТИ ЗОБРАЖЕННЯ</div>
                </div>
                <button type="button" id="trigger-upload" class="btn btn-primary">
                    Upload
                </button>
            </div>
            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector site_button-orange">ОК</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
    <script type="text/template" id="preloader-order-template">
        <div class="preloader-order-wrap">
            <div class="preloader-order">
                <h4>Дочекайтесь заватаження фото</h4>
                <div class="lds-css ng-scope">
                    <div class="lds-spinner" style="width: 100%;height:100%">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <!-- /.preloader-order -->

        </div>
        <!-- /.d -->
    </script>

    <script>
        var remove_photo_url = '{{ roleRoute('order.photo.delete') }}';
    </script>
    @endpush
@endsection
@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/order/edit/energy_audit.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/order/edit/uploader-photo.js')}}"></script>
@endpush