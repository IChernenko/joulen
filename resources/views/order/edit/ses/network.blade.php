@extends('order.edit.ses')

@section('ses-title', 'Оцінка вартості будівництва Мережевої СЕС')


@section('ses-img-class', 'cost_header-network')

@section('ses-header-title', 'Мережева СЕС')

@section('ses-header-description', 'Для продажу електрики по «зеленому тарифу», та для власного споживання. Без акумуляторів.')

@section('ses-legal-checkbox')
    <div>
        <input name="price_types[3]" value="3" type="checkbox" id="c3" data-switchBlock=".data-doc" class="checkbox-special" {{ $order->price_types->contains('slug', 'ses_legal_support') ? 'checked' : '' }}/>
        <label for="c3">Документальний супровід (Підключення та оформлення “зеленого” тарифу)</label>
    </div>
@endsection

@section('ses-power-block')
    <div class="label">
        Яка потужність фотомодулів вас цікавить?
        <strong>
            <span class="photocell_power">{{ $order->orderable->photocell_power }}</span>
            <b> кВт</b>
            <input type="hidden" class="photocell_power_input hidden_validate" value="{{ $order->orderable->photocell_power }}" name="photocell_power" >
        </strong>
    </div>

    <div class="field">
        <div id="sliderWrapper">
            <div id="slider">
                <div id="custom-handle" class="ui-slider-handle"></div>
            </div>
        </div>
        <div id="slider_resp">
            <input type="text" value="" id="slider_value"/>
        </div>
    </div>
    <div class="afterField">
        Для цього необхідна площа ≈
        <strong>
            <b>
                <span>
                    <span class="required_area">{{ $order->orderable->required_area }}</span> м<sup><small>2</small></sup>
                </span>
            </b>
            <input type="hidden" class="required_area_input" value="{{ $order->orderable->required_area }}" name="required_area">
        </strong>
    </div>
@endsection

@section('ses-delivery-appointment')
    <div id="ses-delivery-appointment"  class="montageBlock"></div>
@endsection

@section('ses-where-title', 'Де потрібно встановити сонячну станцію?')

@section('ses-where-text')
    <div style="margin-top: 0"><p>Поставте маркер <img src="{{ asset('/images/map-marker.png') }}"> на ваш будинок або подвір'я -
            в залежності від того, де буде монтуватися станція.</p>
        <div>
            <p><strong>Для чого це потрібно?</strong><br>
                Вартість монтажу залежить від складності монтажу (конструкція даху, планування ділянки, тощо).<br>
                Вкажіть точне розміщення, і монтажник зможе оцінити вартість монтажу,
                а також зможе відразу надати рекомендації у вашому конкретному випадку.
            </p>
        </div>
    </div>
    <div style="display: none; margin-top: 12px; color: red" class="error-input-map">
        Потрібно обов'язково вказати точне місце встановлення станції.<br>
        Цю інформацію буде бачити лише адміністратор сервісу та авторизовані монтажники.
    </div>
@endsection

@section('ses-house-info-title', 'Інформація про ваш будинок')

@section('ses-area-title')
    Приблизна площа даху, <span class="toLowerCase">м.кв.</span>
@endsection

@section('ses-photo-title', 'Прикріпити фото будинку')

@section('ses-other-info', 'Інша інформація про ваш будинок')

@section('ses-legal-block')
    <div class="contentBlock bordered data-doc" id="block-jur">
        <div class="container">
            <h2>Документальний супровід</h2>
            <div>
                <div>
                    <input type="checkbox" class="styledChk" id="c150" name="green_tariff" value="1" {{ $order->orderable->legal_block ? 'checked' : '' }}/>
                    <label for="c3">Потрібна допомога у підключенні до енергомережі та оформленні “зеленого” тарифу.</label>
                </div>
            </div>
            <div id="legal_dynamic_blocks" class="montageBlock"></div>
            <div id="location_legal" style="display: none" class="location-block">
                <h3 class="site_icon-location">Де знаходиться об'єкт?</h3>
                <div>
                    <div class="label">Область</div>
                    <div class="field">
                        <select id="selectregion" name="region_id" class="validate_required">
                            <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ $order->region_id == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="label">Населений пункт</div>
                    <div class="field">
                        <input id="city" type="text" class="validate_required only-letter" value="{{ $order->city }}" name="city"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ses-subcategory-hidden')
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['network']->id }}">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-ui/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/order/edit/ses_network.js')}}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-ui/jquery-ui.css')}}"/>
@endpush