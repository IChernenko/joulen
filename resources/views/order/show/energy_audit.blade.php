@if($order->orderable->{'about_block'})
    <div class="contentBlock bordered">
        <div class="container">
            @yield('energy-audit-about-title')
            <div>
                <ul>
                    <li>Загальна площа: <span>{{$energy_audit->area}} кв.м.</span></li>
                    <li>Кількість поверхів: <span>{{$energy_audit->floors}}</span></li>
                    @yield('energy-audit-exploitation')
                </ul>

                @if(count($order->attachments) > 0)
                    @yield('energy-audit-photo-title')
                    <div class="project_photo">

                        @foreach($order->attachments as $photo)

                            @if(!empty($photo))
                                <a href="{{ $photo->getPath()}}"
                                   data-lightbox="project-pictures" data-title="{{ $order->city }}">
                                    <img width="118" src="{{ $photo->getPath()}}" alt="">
                                </a>
                            @endif
                        @endforeach
                    </div>
                @endif
                @if($energy_audit->exploitation)
                    <ul>
                        @yield('energy-audit-residents')
                        <li>споживання електроенергії за рік: <span>{{$energy_audit->electricity_per_year}} кВт-год</span></li>
                        <li>споживання води за рік: <span>{{$energy_audit->water_per_year}} м<sup>3</sup></span></li>
                        <li>споживання газу за рік: <span>{{$energy_audit->gas_per_year}} м<sup>3</sup></span></li>
                        <li>тип опалення:
                            <span>
                                @if(count($energy_audit->energy_audit_heating_type) > 0)
                                    {{ $energy_audit->energy_audit_heating_type->name }}
                                @endif
                            </span>
                        </li>
                    </ul>
                @endif
                <p>Область: <span class="energoaudit_region">{{ count($order->region) > 0 ? $order->region->name : '' }}</span></p>
                <p>Населений пункт: <span class="energoaudit_city">{{ $order->city }}</span></p>
                @if($order->house_comment != '')
                    @yield('energy-audit-other-info-title')
                    <h5>{!! nl2br(e($order->house_comment)) !!}</h5>
                @endif
            </div>
        </div>
    </div>
@endif

@if($order->orderable->{'problems_block'})
<div class="contentBlock bordered">
    <div class="container">
        <h2>Проблеми що турбують</h2>
        <div>
            <ul>
                {{--TODO: move problems labels somewhere (probably to view composer)--}}
                {!! $energy_audit->high_costs ? '<li>високі витрати на енергоносії</li>' : '' !!}
                {!! $energy_audit->cold_winter ? '<li>холодно взимку</li>' : '' !!}
                {!! $energy_audit->hot_summer ? '<li>спекотно влітку </li>' : '' !!}
                {!! $energy_audit->mold ? '<li>у приміщенні пліснява/грибки</li>' : '' !!}
                {!! $energy_audit->draft ? '<li>протяги</li>' : '' !!}
                {!! $energy_audit->windows_fogging ? '<li>«потіють» вікна</li>' : '' !!}
                {!! $energy_audit->disbalance ? '<li>невідбалансована система опалення (перегрів/недогрів деяких приміщень)</li>' : '' !!}
                {!! $energy_audit->blackout ? '<li>часто вибивають пробки</li>' : '' !!}
                {!! $energy_audit->provider_problems ? '<li>проблеми з енергопостачальниками</li>' : '' !!}
            </ul>
            @if($energy_audit->other_problems != '')
                <p>Інші проблеми:</p>
                <h5>{!! nl2br(e($energy_audit->other_problems)) !!}</h5>
            @endif
        </div>
    </div>
</div>
@endif

@if($order->orderable->{'measures_block'})
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Заходи</h2>
            <div>
                <ul>
                    {{--TODO: move measures labels somewhere (probably to view composer)--}}
                    {!! $energy_audit->thermal_examination ? '<li>тепловізійне обстеження</li>' : '' !!}
                    {!! $energy_audit->blower_door_test ? '<li>проведення тесту на герметичність (Blower Door Test)</li>' : '' !!}

                    {!! $energy_audit->detailed_calculation ? '<li>провести детальний розрахунок: які енергоефективні заходи потрібно впровадити та через скільки вони окупляться</li>' : '' !!}
                    {!! $energy_audit->technical_supervision ? '<li>технічний нагляд під час утеплення/реконструкції</li>' : '' !!}
                </ul>
            </div>
        </div>
    </div>
@endif

@push('footer_scripts')
    <script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/lightbox/css/lightbox.css') }}"/>
@endpush