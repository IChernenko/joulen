@extends('order.show.energy_audit')

@section('energy-audit-about-title')
    <h2>Про заклад</h2>
@endsection

@section('energy-audit-about-title')
    <li>Експлуатація закладу:
        <span>{{ $energy_audit->exploitation ? 'Заклад функціонує' : 'Заклад ще не добудовано (не функціонує)' }}</span>
    </li>
@endsection

@section('energy-audit-photo-title')
    <h5>Фото закладу:</h5>
@endsection

@section('energy-audit-other-info-title')
    <p>Інша інформація про заклад: </p>
@endsection