@extends('order.show.energy_audit')

@section('energy-audit-about-title')
    <h2>Про будинок</h2>
@endsection

@section('energy-audit-about-title')
    <li>Експлуатація будинку:
        <span>{{ $energy_audit->exploitation ? 'У будинку зараз проживають' : 'Будинок на стадії ремонту (ще не експлуатується)' }}</span>
    </li>
@endsection

@section('energy-audit-photo-title')
    <h5>Фото будинку:</h5>
@endsection

@section('energy-audit-residents')
    <li>Скільки людей проживає у будинку: <span>{{$energy_audit->residents}}</span></li>
@endsection

@section('energy-audit-other-info-title')
    <p>Інша інформація про будинок: </p>
@endsection
