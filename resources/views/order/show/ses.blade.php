@if($order->price_types->contains('slug', 'ses_parts'))
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Поставка обладнання та комплектуючих</h2>
            <div>
                <ul>
                    <li>Необхідна потужність СЕС: <b>{{ $ses->photocell_power }}</b> кВт</li>
                    @yield('ses-battery-capacity')
                    <li>
                        {{ $ses->ses_price_category->name }}
                        @if($ses->ses_price_category->slug == 'custom')
                            <ul class="specialBlock">
                                <li style="white-space:pre-wrap">{{$ses->parts_comment}}</li>
                            </ul>
                        @endif
                    </li>
                        {{--TODO: split region and city output--}}
                        @if((!empty($order->city) || count($order->region)) && !$order->price_types->contains('slug', 'ses_installation'))
                            <li>Куди доставити
                                обладнання: {{ $order->city }}{{ count($order->region) > 0 ? ', ' . $order->region->name : '' }}</li>
                        @endif
                </ul>
            </div>
        </div>
    </div>
@endif
@if($order->price_types->contains('slug', 'ses_installation'))
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Монтаж та налагодження</h2>
            <div>
                @can('view', $order)
                    @if(!empty($ses->lat) && !empty($ses->lng))
                        <div class="projectMap">
                            <div id="projectMap"></div>
                            <script>
                                var map;
                                function initMap() {
                                    map = new google.maps.Map(document.getElementById('projectMap'), {
                                        center: {lat: {{ $ses->lat }}, lng: {{ $ses->lng }}},
                                        zoom: 18,
                                        scrollwheel: false,
                                        mapTypeId: 'hybrid'
                                    });

                                    var marker = new google.maps.Marker({
                                        position: {lat: {{ $ses->lat }}, lng: {{ $ses->lng }}},
                                        map: map,
                                        title: '{{ $order->city}}'
                                    });
                                }
                            </script>
                            <script src="https://maps.googleapis.com/maps/api/js?callback=initMap&key={{ env('GOOGLE_MAP_API_KEY') }}"
                                    async defer></script>
                        </div>
                    @endif
                @endcan

                <ul>
                    @if(!$order->price_types->contains('slug', 'ses_parts'))
                    <li>Потужність станції: <b>{{ $ses->photocell_power }}</b> кВт</li>
                    @endif
                    {{--TODO: split region and city output--}}
                    @if(!empty($order->city) || count($order->region))
                        <li>Місце розташування
                            об’єкту: {{ $order->city }}{{ count($order->region) > 0 ? ', ' . $order->region->name : '' }}</li>
                    @endif
                    @yield('ses-area')
                </ul>

                @if($ses->house_comment != '')
                    @yield('ses-house-comment-title')
                    <p style="white-space:pre-wrap">{{$ses->house_comment}}</p>
                @endif

                @if(count($order->attachments) > 0)
                    @yield('ses-photo-title')

                    <div class="project_photo">

                        @foreach($order->attachments as $photo)

                            @if(!empty($photo))
                                <a href="{{ $photo->getPath()}}"
                                   data-lightbox="project-pictures" data-title="{{ $order->city }}">
                                    <img width="118" src="{{ $photo->getPath()}}" alt="">
                                </a>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif

@if($order->price_types->contains('slug', 'ses_legal_support'))
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Документальний супровід</h2>
            <div>
                <ul>
                    @if(!empty($ses->green_tariff))
                        <li>Потрібна допомога у підключенні до енергомережі та оформленні “зеленого” тарифу</li>
                    @endif
                        @if(!$order->price_types->contains('slug', 'ses_parts') && !$order->price_types->contains('slug', 'ses_installation'))
                                <li>Потужність станції: <b>{{ $ses->photocell_power }}</b> кВт</li>
                            {{--TODO: split region and city output--}}
                            @if(!empty($order->city) || count($order->region))
                                <li>Місце розташування
                                    об’єкту: {{ $order->city }}{{ count($order->region) > 0 ? ', ' . $order->region->name : '' }}</li>
                            @endif
                        @endif
                </ul>
            </div>
        </div>
    </div>
@endif


@push('footer_scripts')
    <script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/lightbox/css/lightbox.css') }}"/>
@endpush