@extends('order.show.ses')

@section('ses-area')
    {{--TODO: use area_unit here--}}
    <li>Площа {{ $order->ground == '0' ? 'даху' : 'ділянки'}}: {{$ses->area}} {{ ($ses->area_unit == 'ha') ? 'Га' : 'м.кв.' }}</li>
@endsection

@section('ses-house-comment-title')
    <h5>Інша інформація {{ $order->ground == '0' ? 'про дах приміщення' : 'по ділянці'}}:</h5>
@endsection

@section('ses-photo-title')
    <h5>Фото {{ $order->ground == '0' ? 'даху приміщення' : 'ділянки'}}:</h5>
@endsection