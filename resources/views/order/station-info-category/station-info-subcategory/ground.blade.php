@extends('order.station-info-category.station-commercial')

@section('station-title', 'наземна комерційна')

@section('station-cost-depends')
    Вартість наземної станції залежить від <b>потужності.</b>
@endsection

@section('slider-power')
    <div class="slider-block power-station">
        <div id="power-ground-station" data-min="150" data-max="10000"></div>
        <!-- /#power-station -->
    </div>
@endsection

@section('station-type-photo')
    <img src="{{ asset('images/area.png') }}" alt="image">
@endsection

@section('station-area-type', 'ділянки')

@section('tooltip-text', 'Для будівництва СЕС потрібно попередньо змінити цільове призначення земельної ділянки')

@section('station-max-income')
    <div class="param-item">
        <img src="{{ asset('images/income.png') }}" alt="image">
        <p>
            Максимальний дохід за рік:
        </p>
        <strong>$2 000</strong>
        <span>(Якщо власне споживання = 0)</span>
    </div>
@endsection

@section('station-faq-info')
    <div class="station-info-block left-image right-arrow">
        <div class="station-info-img">
            <img src="{{ asset('images/sun-station.png') }}" alt="image">
        </div>
        <!-- /.station-info-img -->
        <h2>Чому наземні комерційні станції такі дорогі?</h2>
        <p>
            Основна причина, чому комерційні станції дорожчі від домашніх - це необхідність проходження багатьох бюрократичних процедур для отримання "зеленого" тарифу.
            <br><br>
            Саме тому назумні комерційні станції варто будувати потужністю від 200 кВт.<br><br>
            При цьому варто пам'ятати, що для будівництва комерційних станцій можна отримати дуже хороші
            умови кредитування.
        </p>
    </div>
@endsection

@section('ses-link')
    <li><a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'commercial', 'ground' => 'ground']) }}" class="choise-check">
            <img src="{{ asset('images/check.png') }}">
            Хочу дізнатися точну ціну на наземну комерційну станцію</a></li>
@endsection

@section('consult-link')
    <li><a href="{{ route('services.consulting') }}"><img src="{{asset('images/chat-message.png')}}" alt=""> Замовити послуги персонального консультанта</a></li>
@endsection

@section('site-title', 'Скільки коштує наземна комерційна сонячна електростанція під ключ для продажу енергії по “зеленому” тарифу - калькулятор, ціни, виробіток')