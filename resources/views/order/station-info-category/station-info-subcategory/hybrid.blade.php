@extends('order.station-info-category.station-home')

@section('station-title', 'гібридна')

@section('station-cost-depends')
    Вартість гібридної станції залежить від <b>потужності та ємності акумуляторів.</b>
@endsection

@section('slider-power')
    <div class="slider-block power-station" >
        <div id="power-hybrid-station" data-min="3" data-max="30"></div>
        <!-- /#power-station -->
    </div>
    <p class="title">Ємність акумуляторів:</p>
    <!-- /.title -->
    <div class="slider-block capacity-slider">
        <div id="capacity-slider" data-min="0" data-max="100"></div>
        <!-- /#capacity-slider -->
    </div>
    <!-- /.slider-block -->
    <p class="info">
        Не знаєте, яка потужність та ємінсть акумуляторів вам потрібно? <a href="{{ route('order.calculator') }}">Вам допоможе цей
            калькулятор.</a>
    </p>
@endsection

@section('station-max-income')
    <div class="param-item">
        <img src="{{ asset('images/income.png') }}" alt="image">
        <p>
            Максимальний дохід за рік:
        </p>
        <strong class="max-income">&euro;2 000</strong>
        <span>(Якщо власне споживання = 0)</span>
    </div>
@endsection

@section('alert')
    <div class="alert alert-danger">
        <b>Увага!</b> Тут дуже приблизні дані щодо доходів. Якщо у вас споживання та виробіток станції будуть
        однакові, то чистих доходів така
        станція не принесе, а лише дозволить економити.
    </div>
@endsection

@section('station-link')
    <p>Якщо ви вважаєте, що вам не потрібні акумулятори, то можете перейти на сторінку з інформацією про
        <a href="{{ route('assistant.network') }}">мережеві сонячні електростанції</a></p>
@endsection

@section('ses-link')
    <li><a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'hybrid']) }}" class="choise-check">
            <img src="{{ asset('images/check.png') }}">
            Хочу дізнатися точну ціну на гібридну станцію для мого будинку</a></li>
@endsection

@section('consult-link')
    <li><a href="{{ route('services.consulting') }}"><img src="{{asset('images/chat-message.png')}}" alt=""> Замовити послуги персонального консультанта</a></li>
@endsection

@section('site-title', 'Скільки коштує гібридна сонячна електростанція під ключ для продажу енергії по “зеленому” тарифу - калькулятор, ціни, виробіток')