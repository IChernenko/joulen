@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="station-block @yield('station-class')">
            <h1>Ваш вибір: <b>@yield('station-title') сонячна електростанція</b></h1>

            <h2>Скільки коштує @yield('station-title') станція?</h2>

            <p class="info">
                @yield('station-cost-depends')
            </p>
            @yield('station-methods-calc')
            <!-- /.info -->

            @yield('station-power')
            <!-- /.station-power-block -->
            @yield('station-faq-info')
            <!-- /.station-info-block -->
            @yield('landing-benefits')

            <div class="station-info-block left-image left-arrow">
                <div class="station-info-img">
                    <img src="{{ asset('images/power-station-img.png') }}" alt="image">
                </div>
                <!-- /.station-info-img -->
                <h2>Яке обладнання краще вибрати?</h2>
                <p>
                    Дві основні складові кожної сонячної електростанції - це інвертор та сонячні панелі.
                </p>
                <ul class="styled-list">
                    <li>
                        <span class="list-number">1</span>
                        <!-- /.list-number -->
                        <b>Сонячні панелі</b> - становлять близько 60-70% вартості сонячної станції.
                        Не потрібно купувати найдорожчі сонячні панелі.
                        Якісні китайські сонячні панелі без проблем працюватимуть більше 25 років.
                        Проте варто уникати найдешевших панелей (так звані “no-name”) - у них заявлений ККД
                        може не відповідати реальному, а також вони можуть вийти з ладу раніше заявленого строку.
                        Сонячні панелі не бояться граду. Вони досить міцні і добре переносять погодні умови в Україні.
                        Сонячні панелі українського виробництва існують. По ціні вони коштують так само як і якісні китайські.
                    </li>
                    <li>
                        <span class="list-number">2</span>
                        <b>Інвертор</b> - становить близько 20-30% вартості сонячних станцій.
                        Інвертор це та річ, на якій не варто економити. Він виходить з ладу значно швидше ніж сонячні панелі.
                        Враховуючи нестабільність роботи українських електромереж, інвертори краще брати лише якісні.
                        Тому якщо у вас обмежений бюджет, то краще взяти дорожчий інвертор, а не панелі.
                        При замовленні сонячної станції обов’язково дізнайтеся у монтажної організації, яку гарантію вони
                        надають на інвертор і як буде відбуватися процес заміни/ремонту обладнання в разі поломки.
                    </li>
                    @yield('station-equipment')
                </ul>
                <!-- /.styled-list -->
            </div>
            <!-- /.station-info-block -->

            <div class="choise-block">
                <h2>Що ви вирішили?</h2>
                <ul>
                    @yield('ses-link')
                    <li><a class="choise-subscribe" data-remodal-target="modal">Це цікаво, але я ще не готовий. Хочу підписатися на цікаву розсилку від Джоуля</a></li>
                    @yield('consult-link')
                </ul>
            </div>
            <!-- /.choise-block -->
        </div>
        <!-- /.station-block -->
    </div>
@endsection

@push('modals')
<div class="remodal subscription-popup" data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close close-btn"></button>
    <h3>Підписка на розсилку</h3>

    <div class="sp-form-outer sp-force-hide">
        <div id="sp-form-108067" sp-id="108067" sp-hash="fa6667bcd4efb6eff3c1611b4d5085ccfbd588153381313dc148545ec2b42603"
             sp-lang="ua" class="sp-form sp-form-regular sp-form-embed"
             sp-show-options="%7B%22satellite%22%3Afalse%2C%22maDomain%22%3A%22login.sendpulse.com%22%2C%22formsDomain%22%3A%22forms.sendpulse.com%22%2C%22condition%22%3A%22onEnter%22%2C%22scrollTo%22%3A25%2C%22delay%22%3A10%2C%22repeat%22%3A3%2C%22background%22%3A%22rgba(0%2C%200%2C%200%2C%200.5)%22%2C%22position%22%3A%22bottom-right%22%2C%22animation%22%3A%22%22%2C%22hideOnMobile%22%3Afalse%2C%22urlFilter%22%3Afalse%2C%22urlFilterConditions%22%3A%5B%7B%22force%22%3A%22hide%22%2C%22clause%22%3A%22contains%22%2C%22token%22%3A%22%22%7D%5D%7D">
            <div class="sp-form-fields-wrapper">
                <div class="sp-message">
                    <div></div>
                </div>
                <form novalidate="" class="sp-element-container ui-sortable ui-droppable ">
                    <div class="sp-field sp-field-full-width" sp-id="sp-b111c905-e8da-4f1a-a63e-d6c71402b46a">
                        <div style="font-family: inherit; line-height: 1.2;"><p>Ви зможете відписатися в будь-який
                                момент</p></div>
                    </div>
                    <div class="sp-field " sp-id="sp-270c993a-e0a3-4054-9566-73a6c70f83d0"><label
                                class="sp-control-label"><span>Email</span><strong>*</strong></label>
                        <input type="email" sp-type="email"
                               name="sform[email]"
                               class="sp-form-control "
                               placeholder="username@gmail.com"
                               sp-tips="%7B%22required%22%3A%22%D0%9E%D0%B1%D1%8F%D0%B7%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D0%BF%D0%BE%D0%BB%D0%B5%22%2C%22wrong%22%3A%22%D0%9D%D0%B5%D0%B2%D0%B5%D1%80%D0%BD%D1%8B%D0%B9%20email-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%22%7D"
                               required="required">
                    </div>
                    <div class="sp-field sp-button-container " sp-id="sp-b153339d-a37a-4ff6-ab0f-727744a17951">
                        <button id="sp-b153339d-a37a-4ff6-ab0f-727744a17951" class="sp-button">Підписатися</button>
                    </div>
                </form>
                <div class="sp-link-wrapper sp-brandname__left">
                    <a class="sp-link " target="_blank" href="https://sendpulse.com/ru/forms-powered-by-sendpulse?sn=Sm91bGUucHJv"><span
                                class="sp-link-img">&nbsp;</span>
                        <span translate="FORM.PROVIDED_BY">Предоставлено SendPulse</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--End mc_embed_signup-->
</div>

<!-- Subscription Form -->
<!-- /Subscription Form -->
@endpush

@push('styles')
    <style>
        .sp-force-hide {
            display: none;
        }

        .sp-form[sp-id="108067"] {
            display: block;
            background: #ffffff;
            padding: 15px;
            width: 450px;
            max-width: 100%;
            border-radius: 8px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border-color: #dddddd;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, "Helvetica Neue", sans-serif;
            background-repeat: no-repeat;
            background-position: center;
            background-size: auto;
        }

        .sp-form[sp-id="108067"] input[type="checkbox"] {
            display: inline-block;
            opacity: 1;
            visibility: visible;
        }

        .sp-form[sp-id="108067"] .sp-form-fields-wrapper {
            margin: 0 auto;
            width: 420px;
        }

        .sp-form[sp-id="108067"] .sp-form-control {
            background: #ffffff;
            border-color: #cccccc;
            border-style: solid;
            border-width: 1px;
            font-size: 15px;
            padding-left: 8.75px;
            padding-right: 8.75px;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            height: 35px;
            width: 100%;
        }

        .sp-form[sp-id="108067"] .sp-field label {
            color: #444444;
            font-size: 13px;
            font-style: normal;
            font-weight: bold;
        }

        .sp-form[sp-id="108067"] .sp-button {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            background-color: #f8c960;
            color: #ffffff;
            width: auto;
            font-weight: 700;
            font-style: normal;
            font-family: Arial, sans-serif;
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
        }

        .sp-form[sp-id="108067"] .sp-button-container {
            text-align: left;
        }
    </style>
@endpush

@push('footer_scripts')
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type="text/javascript" src="//static-login.sendpulse.com/apps/fc3/build/default-handler.js?1538406444467"></script>
@endpush
