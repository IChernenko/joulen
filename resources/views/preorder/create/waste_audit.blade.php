@extends('preorder.create')

@section('category-content')
    <div class="container pageTitle withBlock">
        <h1>Аудит ринку сміття</h1>
    </div>

    <div class="contentBlock">
        <div class="container">
            <div class="cost_header cost_header_garbage">
                <h4>Попередній аудит ринку сміття</h4>
                <p>це унікальна послуга, яку надає компанія Джоуль
                    та яка передбачає детальне вивчення кон’юктури
                    на конкретному ринку побутових відходів.</p>
            </div>
        </div>
    </div>
    <div class="contentBlock" id="block-montage">
        <div class="container">
            <h2>Ви можете обрати один із трьох пакетів:</h2>
            {{--TODO: FRONT fix table styles--}}
            <div class="table_garbage">
                <div class="table_garbage_header" style="position: relative; width: 100%;">
                    <div class="table_flex_big table_garbage_name table_garbage_name_none">Можливості</div>
                    <div class="table_flex_small table_garbage_package">Експрес</div>
                    <div class="table_flex_small table_garbage_package">Стандарт</div>
                    <div class="table_flex_small table_garbage_package">Розгорнутий</div>
                </div>
                <div class="table_garbage_body">
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Аналіз реальних об’ємів відходів</div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Інформація про місцевих перевізників сміття</div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Надання інформації про найближчих (територіально)
                            покупців вторсировини та їх готовності купувати
                        </div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Підготовка рекомендацій оптимального вибору
                            сміттєсортувального обладнання з урахуванням місцевої специфіки.
                        </div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Аналіз складу сміття</div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Аналіз охоплення населення послугами
                            сміттєвивезення
                        </div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Проведення переговорів з місцевою владою по
                            підготовці необхідних документів для сміттєсортувального підприємства
                        </div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Аналіз місцевого бюджету та прийнятих рішень
                            місцевою владою у&nbsp;сфері побутових відходів
                        </div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Аналіз можливості співфінансування та проведення
                            відповідних переговорів
                        </div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                    <div class="table_garbage_body_block">
                        <div class="table_flex_big table_garbage_name">Детальний розрахунок термінів окупності</div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote_non"></div>
                        <div class="table_flex_small table_garbage_dote"></div>
                    </div>
                </div>
                <div class="table_garbage_footer">
                    <div class="table_flex_big table_garbage_name">Ціна:</div>
                    <div class="table_flex_small table_garbage_package">
                        <p>950 грн.</p>
                        <a href="#modal" class="site_button-orange place_order"
                           data-subcategory_name="«{{ $preorder_subcategories['express']->name }}»"
                           data-subcategory_id="{{ $preorder_subcategories['express']->id }}">Замовити</a></div>
                    <div class="table_flex_small table_garbage_package">
                        <p>3700 грн.</p>
                        <a href="#modal" class="site_button-orange place_order"
                           data-subcategory_name="«{{ $preorder_subcategories['standard']->name }}»"
                           data-subcategory_id="{{ $preorder_subcategories['standard']->id }}">Замовити</a></div>
                    <div class="table_flex_small table_garbage_package">
                        <p>6500 грн.</p>
                        <a href="#modal" class="site_button-orange place_order"
                           data-subcategory_name="«{{ $preorder_subcategories['advanced']->name }}»"
                           data-subcategory_id="{{ $preorder_subcategories['advanced']->id }}">Замовити</a></div>
                </div>
            </div>
            <!--# end contentWrapper #-->

        </div>

        @push('modals')
        <div class="remodal packages_modal" data-remodal-id="modal">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Замовити аудит ринку сміття</h1>
            <p>Пакет <span>«Експрес»</span>
            </p>
            @component('layouts.validation_errors')
            @endcomponent
            <form method="POST" action="{{ roleRoute('preorder.store') }}" class="modal-validate" id="wasteaudit_form">
                {{ csrf_field() }}
                <div class="form-line">
                    <p>Ваше ім'я</p>
                    <input type="text" name="name" class="name name_required">
                </div>
                <!-- /.form-line -->
                <div class="form-line">
                    <p>Телефон</p>
                    <input type="text" name="phone" class="phone mask-phone">
                </div>
                <div class="form-line">
                    <p>e-mail</p>
                    <input type="email" name="email" class="email email_required">
                </div>
                <input type="hidden" name="subcategory_id">
                <div class="remodal_footer">
                    <button class="site_button-orange submit">Замовити</button>
                    <button data-remodal-action="cancel" class="site_button-transperent">Скасувати</button>
                </div>
            </form>

        </div>
        @endpush
    <!--# scripts #-->

        @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-ui/jquery-ui.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
        @endpush

        @push('footer_scripts')
        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
        <script src="{{ asset('js/jquery-ui/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
        <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
        <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>

        <script>
            var open_modal = {{ count($errors->all()) > 0 ? 'true' : 'false' }};
            var waste_audit_type = {{ Session::get('waste_audit_type', '1') }};
            var preorderStoreRoute = '{{ roleRoute('preorder.store') }}';
        </script>
        <script src="{{ asset('js/preorder/create.js') }}"></script>
        @endpush

    </div>
@endsection