@extends('layouts.old.app')

@section('content')
    <div id="content">
        <div id="leaveTestimonials" class="textCenter">
            <h2>Залишити відгук</h2>
            <div>
                <form class="validate_form" action="{{ roleRoute('review.update', ['id' => $review->id]) }}" method="POST">
                    <div id="leaveTestimonials-type" class="checkboxChoices clr">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="master_id" value="{{ $master_id }}">
                        <input type="hidden" name="order_id" value="{{ $order_id }}">
                        <input name="negative" type="checkbox" id="isNegative" value="0" {{ $review->positive ? '' : 'checked' }}>
                        <label for="isNegative">
                            <div class="clr">
                                <span>Позитивний</span>
                                <span>Негативний</span>
                            </div>
                        </label>
                    </div>
                    <div style="color:red; font-size:small">
                        @if (!empty($errors))
                            @foreach ($errors as $error)
                                {{ $error }}
                            @endforeach
                        @endif
                    </div>
                    <div id="leaveTestimonials-text">
                        <div class="label">ТЕКСТ ВІДГУКУ</div>
                        <textarea class="validate_required" name="text" placeholder="Залишайте відгук лише після того, як спеціаліст повністю виконає свою роботу">{{ $review->text }}</textarea>
                    </div>
                    <div class="textCenter">
                        <a href="{{ roleRoute('order.show', ['id' => $order_id]) }}" class="site_button-orange onlyBorder">Скасувати</a>
                        <input type="submit" value="Зберегти" class="site_button-orange">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @push('footer_scripts')
        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
        <script src="{{ asset('js/client/review.js') }}"></script>
    @endpush
@endsection