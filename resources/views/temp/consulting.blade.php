@extends('layouts.app')

@section('site-title', 'Консалтинг щодо будівництва сонячних електростанцій')

@section('site-meta-description', 'Консультування щодо будівництва домашніх та комерційних сонячних станцій, зміни цільового призначення ділянок на землі енергетики та щодо зеленого тарифу.')

@section('content')
<div class="page-wrapper consultant">
    <div class="container">
        <div class="page-content--white consultant__item">
            <h1 class="consultant__title">Послуги персонального консультанта</h1>
            <p class="consultant__text text-light">Консультанти сервісу Джоуль мають багаторічний досвід в галузі
                сонячної
                енергетики і готові застосувати свої знання та контакти для того, щоб зекономити ваш час та гроші при
                будівництві сонячної станції.</p>

            <div class="consultant__head">
                <img class="consultant__icon" src="{{ asset('images/consultant-page-icon.png') }}" alt="">
                <h2 class="consultant__subtitle">
                    Консультування щодо будівництва домашніх сонячних станцій</h2>
            </div>
            <p class="consultant__text text-light">На жаль, не всі інсталятори зацікавлені у тому, щоб ви отримали
                сонячну станцію
                по оптимальним параметрам ціна/якість. Деякі компанії зацікавлені продати вам сонячну станцію у такій
                комплектації, де вони отримають максимальний прибуток.
            </p>
            <p class="consultant__text text-semibold">6 основних проблем при замовленні домашніх сонячних станцій:</p>

            <ol class="numbered-list consultant__list">
                <li class="numbered-list__item text-light">Неправильний вибір типу сонячної станції
                    (мережева/гібридна/автономна);
                </li>
                <li class="numbered-list__item text-light">Нераціональне розміщення панелей на даху;</li>
                <li class="numbered-list__item text-light">Вибір неякісних панелей/інвертора;</li>
                <li class="numbered-list__item text-light">Будівництво системи із недостатньою кількістю
                    MPPT-трекерів;
                </li>
                <li class="numbered-list__item text-light">Нераціональний порядок будівництва станції (збільшення
                    потужності / монтаж обладнання / підписання
                    договору);
                </li>
                <li class="numbered-list__item text-light">Вибір монтажника, який не зможе в подальшому якісно
                    обслуговувати станцію.
                </li>
            </ol>

            <p class="consultant__text text-semibold">Як вам допоможе персональний консультант?</p>
            <ul class="system-items--step">
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">1</span> Супроводжуватиме вас протягом всього процесу будівництва і
                    налаштування станції: від моменту вибору обладнання до моменту успішного запуску і отримання перших
                    виплат по Зеленому тарифу.
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">2</span> Допоможе визначитися із оптимальною потужністю сонячної
                    станції та місцем розміщення на території домогосподарства.
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">3</span> Допоможе обрати оптимальне обладнання саме для вашого
                    домогосподарства
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">4</span> Допоможе провести тендер між компаніями інсталяторами і
                    отримати найбільш вигідну цінову пропозицію
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">5</span> Консультуватиме щодо незрозумілих питань як незацікавлена
                    сторона.
                </li>
            </ul>

            <span class="consultant-total consultant__consultant-total">
               <span class="consultant-total__cost consultant-total__item">Вартість послуги:</span>
               <span class="consultant-total__price consultant-total__item">200 грн</span>
           </span>

            <a class="main-btn consultant__btn" data-remodal-target="modal1">Замовити послугу</a>

            <p class="consultant__text text-light">Ми <span class="text-semibold">гарантуємо,</span> що наші
                консультації щодо домашніх станцій допоможуть вам
                зекономити час та кошти.
                Якщо ви будете незадоволені наданими послугами, то ми повернемо вам оплату за консультацію.</p>
        </div>

        <div class="page-content--white consultant__item">
            <div class="consultant__head">
                <img class="consultant__icon" src="{{ asset('images/consultant-page-icon2.png') }}" alt="">
                <h2 class="consultant__subtitle">
                    Консультування щодо будівництва наземних сонячних станцій </h2>
            </div>
            <p class="consultant__text text-light">Персональний консультант допоможе вам уникнути невдалих інвестицій та
                типових помилок при підготовці проекту сонячної станції та її будівництві.
            </p>

            <p class="consultant__text text-semibold">Як вам допоможе персональний консультант?</p>
            <p class="consultant__text">Консультант Джоуля розкаже про актуальну ситуацію на ринку сонячної енергетики,
                а також проаналізує ваш проект сонячної станції відносно:</p>
            <ul class="system-items--step">
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">1</span> Району розташування ділянки
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">2</span> Річної інсоляції та кількості річного виробництва енергії
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">3</span> Максимальної потужності панелей, які можна встановити на
                    ділянці
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">4</span> Впливу рельєфу та рослинності на роботу станції
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">5</span> Можливості приєднання до зовнішніх мереж
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">6</span> Орієнтовної вартості та окупності проекту
                </li>
                <li class="system-item--step consultant__text text-light">
                    <span class="number-item--blue">7</span> Аспектів, які можуть потягнути за собою додаткові витрати в
                    майбутньому
                </li>
            </ul>

            <span class="consultant-total consultant__consultant-total">
               <span class="consultant-total__cost consultant-total__item">Вартість послуги:</span>
               <span class="consultant-total__price consultant-total__item">2 700 грн</span>
           </span>

            <a class="main-btn consultant__btn" data-remodal-target="modal2">Замовити послугу</a>

            <p class="consultant__text text-light">За додатковою домовленістю, консультант:</p>

            <ol class="numbered-list">
                <li class="numbered-list__item text-italic">Допоможе підготувати презентацію вашого проекту;</li>
                <li class="mumbered-list__item text-italic">Супроводжуватиме вас під час переговорів з інвесторами
                    (вартість послуг розраховується в залежності від об’єму робіт).
                </li>
            </ol>
        </div>

    </div>

</div>
@endsection

@push('modals')
    <div class="remodal modal-consultant registration-facebook" data-remodal-id="modal1">
        <button data-remodal-action="close" class="remodal-close close-btn"></button>
        <h3>Послуги персонального консультанта по домашнім станціям</h3>
        <div class="enter-email">
            <form action="{{ route('consulting.mail') }}" method="POST" class="formLogin register-validate mail-form">
                {{ csrf_field() }}
                <input type="hidden" name="ses_type" value="home_ses">

                <p class="modal-consultant__price">Вартість: <span>200 грн</span></p>
                <div class="form-group">
                    <label for="input-name1">Ваше ім’я</label>
                    <input type="text" placeholder="Ім’я" class=""
                           id="input-name1" name="name" value="" style="width: 100%">
                </div>
                <div class="form-group">
                    <label for="input-phone1">Ваш телефон</label>
                    <input type="text" placeholder="Телефон" class="check-phone mask-phone tel_required"
                           id="input-phone1" name="phone" value="" style="width: 100%">
                </div>
                <div class="form-group">
                    <label for="input-email1">E-mail:</label>
                    <input type="email" placeholder="E-mail" class="email_required"
                           id="input-email1" name="email" value="" style="width: 100%">
                </div>
                <div class="forgot-password-btn">
                    <input type="submit" class="accept-btn remodal-confirm send-letter" value="Надіслати">
                    <a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>
                </div>
            </form>
        </div>
    </div>

    <style>
        .modal-consultant .form-group:first-child {
            margin-top: 0;
        }
        .modal-consultant .form-group:last-child {
            margin-bottom: 0;
        }
        .modal-consultant .form-group {
            margin: 15px 0;
        }

        .modal-consultant .modal-consultant__price {
            color: #0c5f0c;
            font-size: 16px;
        }
    </style>

    <div class="remodal modal-consultant registration-facebook" data-remodal-id="modal2">
        <button data-remodal-action="close" class="remodal-close close-btn"></button>

        <h3>Послуги персонального консультанта по наземним станціям</h3>
        <div class="enter-email">
            <form action="{{ route('consulting.mail') }}" method="POST" id="" class="formLogin register-validate mail-form">
                {{ csrf_field() }}
                <input type="hidden" name="ses_type" value="commercial_ses">

                <p class="modal-consultant__price">Вартість: <span>2700 грн</span></p>
                <div class="form-group">
                    <label for="input-name2">Ваше ім’я</label>
                    <input type="text" placeholder="Ім’я" class=""
                           id="input-name2" name="name" value="" style="width: 100%">
                </div>
                <div class="form-group">
                    <label for="input-phone2">Ваш телефон</label>
                    <input type="text" placeholder="Телефон" class="check-phone mask-phone tel_required"
                           id="input-phone2" name="phone" value="" style="width: 100%">
                </div>
                <div class="form-group">
                    <label for="input-email2">E-mail:</label>
                    <input type="email" placeholder="E-mail" class="email_required"
                           id="input-email2" name="email" value="" style="width: 100%">
                </div>
                <div class="forgot-password-btn">
                    <input type="submit" class="accept-btn remodal-confirm send-letter" value="Надіслати">
                    <a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>
                </div>
            </form>
        </div>
    </div>

    <div class="remodal modal-consultant registration-facebook" data-remodal-id="modal3">
        <button data-remodal-action="close" class="remodal-close close-btn"></button>

        <h4>Ваша заявка прийнята. Незабаром із вами зв'яжеться менеджер</h4>
    </div>
@endpush

@push('footer_scripts')
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
    <script src="{{ asset('libs/remodal/remodal.min.js') }}"></script>
    <script>
        $(document).ready(function () {
             $('.mail-form').on('submit', function (e) {
                 e.preventDefault();

                 $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serializeArray(),
                     type: 'POST',
                    success: function (response) {
                        var modal = $('[data-remodal-id=modal3]').remodal();
                        modal.open();
                    }
                 });
             })
        });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('libs/remodal/remodal.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/remodal/remodal-default-theme.css') }}">
@endpush
