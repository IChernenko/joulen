@extends('layouts.app')

@section('content')

    <div class="page-wrapper">
        <div class="container">
            @include('includes.knowledge')
            <div class="question-list">
                @include('includes.question-list')
            </div>
        </div>
    </div>

    @push('footer_scripts')
    <script src="{{ asset('js/jquery.collapsible.js') }}"></script>
    <script>
        $('.collapsible').collapsible({
            defaultOpen: ''
        });
    </script>
    @endpush
@endsection

@section('site-title', 'Сонячні електростанції в Україні: що потрібно знати?')

@section('site-meta-description', 'Перш ніж купувати сонячні панелі, вам потрібно ознайомитися із базовою інформацією, щоб зрозуміти чи доцільно у вашому випадку використовувати енергію сонця.')

{{--@push('modals')--}}
{{--<div class="remodal advantages-sun-system" data-remodal-id="modal-3">--}}
    {{--<button data-remodal-action="close" class="remodal-close close-btn"></button>--}}
    {{--<h3>Економія на опаленні завдяки сонячній електростанції</h3>--}}
    {{--<p>Звертаємо увагу, що використовувати сонячну електростанцію для електроопалення або підігріву води електробойлером--}}
        {{--- не доцільно. Це може мати зміст лише тоді, якщо для опалення ви використовуєте тепловий насос. І то, не--}}
        {{--завжди.</p>--}}
    {{--<p>Проте ви можете економити на опаленні наступним чином:</p>--}}
    {{--<p>ви можете встановити <span>мережеву сонячну електростанцію</span> для продажу електроенергії по "зеленому"--}}
        {{--тарифу. Таким чином--}}
        {{--протягом літа на ваш рахунок буде нараховано достатню суму грошей, щоб у зимній період можна було повністю--}}
        {{--покрити рахунки за опалення.</p>--}}

    {{--<div class="modal-btn">--}}
        {{--<a href="#" class="accept-btn remodal-confirm" data-remodal-action="confirm">Дізнатися вартість мережевої--}}
            {{--станції</a>--}}
        {{--<a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endpush--}}
