@extends('layouts.old.app')

@section('content')
<div id="content">

    <!--# contentBlock #-->
    <div class="contentBlock searchProfHeader">
        <div class="container">

            <h1>Що вас цікавить?</h1>

        </div>
    </div>
    <!--# end contentBlock #-->

    <div class="contentBlock">
        <div class="container">
            <div class="searchProf_select">
                <div class="searchProf_title">
                    <h3>Оберіть потрібну категорію</h3>
                </div>
                <div class="searchProf_select_wrap">
                    <a href="{{url('/faq')}}" class="searchProf_select_item solar">
                        <img src="{{ asset('images/pic-solar-station.png') }}" alt="img">
                        <h3>Сонячні електростанції</h3>
                        <p>Знайти спеціаліста по монтажу сонячних електростанцій «під ключ»</p>
                        {{--<a  class="site_button-orange onlyBorder">Обрати</a>--}}
                    </a>
                    <a href="{{ url('/user/search/energyauditors') }}" class="searchProf_select_item line">
                        <img src="{{ asset('images/pic-energoaudit.png') }}" alt="img">
                        <h3>Енергоаудит</h3>
                        <p>Знайти спеціаліста, який допоможе вам зменшити витрати на комунальні послуги</p>
                        {{--<a  class="site_button-orange onlyBorder">Обрати</a>--}}
                    </a>
                    <a href="{{ url('/faq_waste') }}" class="searchProf_select_item">
                        <img src="{{ asset('images/pic-garbage-sorting.png') }}" alt="img">
                        <h3>Cміттєсортувальні лінії</h3>
                        <p>Знайти спеціалістів по будівництву сміттєсортувальних комплексів «під ключ»</p>
                        {{--<a href="{{ url('/faq_waste') }}" class="site_button-orange onlyBorder">Обрати</a>--}}
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('site-title')
    Пошук спеціаліста у галузі енергетики та енергозбереження
@endsection
