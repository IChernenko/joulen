@extends('layouts.app')

@section('content')
    <div class="general-information-wrap select-category">
        <div class="container">
            <div class="general-information select-station">
                <h2 class="gray">Уточніть, що саме вас цікавить?</h2>
                <div class="general-information-items" style="margin-bottom: 35px;">
                    <a href="{{ route('temp.faq') }}" class="information-item for-client">
                        <h3>Загальна інформація</h3>
                        <h4>Хочу дізнатися більше про сонячні електростанції:</h4>
                        <ul>
                            <li>Скільки вони коштують?</li>
                            <li>Чи вигідно їх встановлювати?</li>
                            <li>Чи потрібні мені акумулятори?</li>
                            <li>Яке обладнання краще вибрати?</li>
                        </ul>
                    </a>
                    <a href="{{ route('temp.step1') }}" class="suggestions-item for-client">
                        <h3>Конкретні пропозиції</h3>
                        <h4>Я добре розбираюся в цьому питанні:</h4>
                        <ul>
                            <li>Я знаю який тип станції мені потрібен</li>
                            <li>Я знаю яка потужність мені потрібна</li>
                            <li>Я знаю яке обладнання мені потрібне</li>
                            <li>Пришліть мені конкретні пропозиції</li>
                        </ul>
                    </a>
                </div>
                <!-- /.general-information-items -->

                <!-- /.suggestions-item -->
                <!-- /.information-items -->
            </div>
            <!-- /.general-information -->
            @include('includes.consult-banner')
        </div>
        <!-- /.container -->
    </div>
@endsection

@section('site-title', 'Отримати цінові пропозиції - крок 1')

@section('site-meta-description', 'Ви можете отримати як загальну інформацію про сонячні електростанції в Україні, так і конкретні пропозиції від монтажників.')