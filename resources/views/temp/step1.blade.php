@extends('layouts.app')

@section('content')
    <div class="type-station-wrap select-category">
        <div class="container">
            <div class="type-station select-station">
                <h2 class="gray">Який тип станції вас цікавить?</h2>
                <div class="station-items">
                    <!-- /.station-items -->
                    <a class="station-item home-station" href="{{ route('temp.step2') }}" >
                        <div class="station-info">
                            <img src="{{ asset('images/home-ses.svg') }}" alt="image">
                            <h3>Домашня</h3>
                            <h4>Для домогосподарств</h4>
                            <p>монтується на даху будинку або подвір'ї,
                                максимальна потужність - 30 кВт </p>
                        </div>
                    </a>
                    <a class="station-item" href="{{ route('temp.step22') }}" >
                        <div class="station-info">
                            <img src="{{ asset('images/commercial_ses.svg') }}" alt="image">
                            <h3>Комерційна</h3>
                            <h4>Для юридичних осіб</h4>
                            <p>монтується на дахах приміщень<br>
                                або на земельних ділянках </p>
                        </div>
                    </a>
                </div>
                <!-- /.commeercial-ses -->
                <!-- /.home-station -->
            </div>
            <!-- /.type-station -->
        </div>
        <!-- /.container -->
    </div>
@endsection

@section('site-title')
    Сонячні електростанції в Україні: ціни, каталог монтажників. Зелений тариф
@endsection