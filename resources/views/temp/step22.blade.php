@extends('layouts.app')

@section('content')
    <div class="commercial-station-wrap select-category">
        <div class="container">
            <div class="type-station select-station">
                <h2 class="gray">Який тип комерційної станції вас цікавить?</h2>
                <div class="station-items">
                    <!-- /.station-items -->
                    <div class="station-item commercial-station">
                        <a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'commercial', 'ground' => 'roof']) }}" class="station-info">
                            <img src="{{ asset('images/roof_station.svg') }}" alt="image">
                            <h3>Дахова</h3>
                            <p>Розміщується на дахах комерційних приміщень. Для дахових станцій "зелений" тариф вищий ніж
                                для наземних.</p>
                        </a>
                    </div>
                    <div class="station-item commercial-station">
                        <a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'commercial', 'ground' => 'ground']) }}" class="station-info">
                            <img src="{{ asset('images/commercial_ses.svg') }}" alt="image">
                            <h3>Наземна</h3>
                            <p>Розміщується на земельній ділянці. Потребує зміни цільового призначення ділянки.</p>
                        </a>
                    </div>
                </div>
                <!-- /.commeercial-ses -->
                <!-- /.home-station -->
            </div>
            <!-- /.type-station -->
        </div>
        <!-- /.container -->
    </div>
@endsection

@section('site-title')
    Сонячні електростанції для дому: ціни, каталог монтажників. Отримання зеленого тарифу
@endsection