@extends('layouts.app')

@section('content')
    <div class="page-wrapper">
        <div class="container">
            <div class="page-content--white">
                <div class="sun-system">
                    <div class="sun-sustem--head">
                        <img src="{{ asset('images/main-info-5.png') }}" alt="image">
                        <h1 class="sun-system--title">Сонячна станція для шкіл, лікарень, ОТГ:
                            <span>що потрібно знати?</span></h1>
                    </div>
                    <p style="margin-bottom: 30px">Для прогресивних територіальних громад, директорів шкіл чи лікарень є
                        можливість долучитися до розвитку сонячної енергетики і при цьому отримати додатковий дохід.
                    </p>
                    <div class="system-meaning border-gray">
                        <h3>Зміст:</h3>
                        <ul>
                            <li class="system-meaning--list"><a href="#link_1">Чи доцільно будувати сонячну станцію на
                                    даху школи чи лікарні?
                                </a></li>
                            <li class="system-meaning--list"><a href="#link_2">Як муніципалітети можуть залучити
                                    інвесторів для будівництва сонячних станцій?

                                </a></li>
                        </ul>
                    </div>

                    <div class="system-item--info">
                        <h3 id="link_1">Чи доцільно будувати сонячну станцію на даху школи чи лікарні?
                        </h3>

                        <p>
                            Оскільки школи та лікарні - це неприбуткові організації, то заробляти на продажу сонячної
                            енергії по “зеленому” тарифі вони не можуть. Проте є дві альтернативи, якими можна
                            скористатися:
                        </p>
                        <ul class="system-items--step">
                            <li class="system-item--step"><span class="number-item--blue">1</span>Здати дах в оренду
                                інвесторами для будівництва сонячної станції

                            </li>
                            <li class="system-item--step"><span class="number-item--blue">2</span>Збудувати невелику
                                сонячну станцію для власних потреб залучивши грант чи держфінансування
                            </li>
                        </ul>

                        <p>
                            У першому випадку, дах вашого приміщення повинен відповідати декільком вимогам: дах повинен
                            бути такої площі, щоб на ньому можна було розмістити сонячну станцію від 100 кВт (від 700
                            м²), а також електричну лінію, яка зможе прийняти цю потужність.
                        </p>
                        <p>
                            У другому випадку, вам потрібно збудувати сонячну станцію такої потужності, щоб навіть у
                            вихідні дні заклад міг повністю споживати 100% виробленої ел.енергії. А оскільки термін
                            окупності такої інвестиції буде більше 10 років (при нинішніх цінах), то будівництво такої
                            станції доцільно починати лише при грантовій чи державній підтримці.
                        </p>
                        <p>
                            Для пошуку інвестора, якому можна здати дах в оренду, можете написати запит на <a
                                    href="mailto:{{ env('SUPPORT_MAIL') }}">{{ env('SUPPORT_MAIL') }}</a>.
                        </p>
                    </div>
                    <div class="system-item--info">
                        <h3 id="link_2">Як ОТГ можуть залучити інвесторів для будівництва сонячних станцій?
                        </h3>

                        <p>Інвесторам дуже важливо, щоб місцева влада їх підтримувала.
                        </p>
                        <p>
                            Тому якщо ви бажаєте залучити потужного інвестора і готові реально допомогти йому в
                            проходженні всіх бюрократичних процедур, напишіть лист на <a href="mailto:{{ env('SUPPORT_MAIL') }}">{{ env('SUPPORT_MAIL') }}</a>, і ми знайдемо
                            вам надійного інвестора.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-wrapper">
        <div class="container">
            <div class="page-content--white no-spacing">
                <div class="question-list question-list--assistant">
                    @include('includes.question-list')
                </div>
            </div>
        </div>
    </div>
@endsection



@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/assistant/index.js')}}"></script>
@endpush

@section('site-title', 'Сонячна електростанція для шкіл, лікарень, ОТГ')

@section('site-meta-description', 'Найважливіша інформація про будівництво сонячних електростанцій на дахах шкіл, лікарень, ОТГ')
