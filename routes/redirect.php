<?php

/*
|--------------------------------------------------------------------------
| Redirect Routes
|--------------------------------------------------------------------------
*/

Route::get('/assistant/start', function () {
    return redirect()->route('temp.step0', [], 301);
});

Route::get('/assistant/step1', function () {
    return redirect()->route('temp.faq', [], 301);
});

Route::get('/assistant/step2_1', function () {
    return redirect()->route('assistant.step2_1', [], 301);
});

Route::get('/assistant/step2_2', function () {
    return redirect()->route('assistant.step2_2', [], 301);
});

Route::get('/assistant/step2_3', function () {
    return redirect()->route('assistant.step2_3', [], 301);
});

Route::get('/assistant/step2_4', function () {
    return redirect()->route('assistant.step2_4', [], 301);
});

Route::get('/assistant/step2_5', function () {
    return redirect()->route('assistant.step2_5', [], 301);
});