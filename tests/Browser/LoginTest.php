<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testMasterLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/');
            $browser->visit('/login')
                ->type('email', 'mrozakevych@gmail.com')
                ->type('password', '123456')
                ->press('Увійти');
            $browser->pause(1000);
        });
    }
}
